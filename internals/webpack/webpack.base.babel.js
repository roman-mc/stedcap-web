/**
 * COMMON WEBPACK CONFIGURATION
 */

const path = require('path');
const webpack = require('webpack');

// const postcss = require('../../postcss.config.css');

const rules = require('./webpack.rules');

module.exports = (options) => ({
  entry: options.entry,
  mode: options.mode || 'none',
  output: Object.assign({ // Compile into js/build.js
    path: path.resolve(process.cwd(), 'build'),
    publicPath: '/',
  }, options.output), // Merge with env dependent settings
  module: {
    rules: rules(options),
  },
  plugins: options.plugins.concat([
    new webpack.ContextReplacementPlugin(/\.\/locale$/, 'empty-module', false, /js$/),
    new webpack.ProvidePlugin({
      // make fetch available
      fetch: 'exports-loader?self.fetch!whatwg-fetch',
    }),

    // Always expose NODE_ENV to webpack, in order to use `process.env.NODE_ENV`
    // inside your code for any environment checks; UglifyJS will automatically
    // drop any unreachable code.
    new webpack.DefinePlugin({
      'process.env': {
        NODE_ENV: JSON.stringify(process.env.NODE_ENV),
        APP_ENV: JSON.stringify(process.env.APP_ENV),
        SERVER_ENV: JSON.stringify(process.env.SERVER_ENV),
      },
      API_HOST: JSON.stringify(options.hostName || ''),
      REAL_API_HOST: JSON.stringify(options.realApiHostName || options.hostName || ''), // TODO: Replace with only 1 endpoint
    }),
    new webpack.NamedModulesPlugin(),
  ]),
  resolve: {
    modules: ['app', 'node_modules'],
    extensions: [
      '.js',
      '.jsx',
      '.react.js',
    ],
    mainFields: [
      'browser',
      'jsnext:main',
      'main',
    ],
    alias: {
      Model: path.resolve(process.cwd(), 'app/model'),
      Api: path.resolve(process.cwd(), 'app/api'),
      Svg: path.resolve(process.cwd(), 'app/assets/svg'),
      Img: path.resolve(process.cwd(), 'app/img'),
      Utils: path.resolve(process.cwd(), 'app/utils'),
      Components: path.resolve(process.cwd(), 'app/components'),
      Containers: path.resolve(process.cwd(), 'app/containers'),
      immutable: path.resolve(process.cwd(), 'node_modules/immutable'),
      lodash: path.resolve(process.cwd(), 'node_modules/lodash'),
      'save-svg-as-png': path.resolve(process.cwd(), 'node_modules/save-svg-as-png'),
    },
  },
  devtool: options.devtool,
  target: 'web', // Make web variables accessible to webpack, e.g. window
  performance: options.performance || {},
});
