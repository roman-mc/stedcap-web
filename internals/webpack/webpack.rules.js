const cssLoaders = [
  {
    loader: 'css-loader',
    options: {
      modules: true,
      localIdentName: '[path]___[name]__[local]___[hash:base64:5]',
    },
  },
  'postcss-loader',
];

module.exports = (options) => [
  {
    test: /\.(js|jsx)$/, // Transform all .js files required somewhere with Babel
    use: [{
      loader: 'babel-loader',
      query: options.babelQuery,
    }],
  }, {
    test: /\.css$/,
    include: (p) => p.indexOf('.global.css') >= 0 || p.indexOf('node_modules') >= 0,
    use: [
      {
        loader: 'style-loader',
        options: {},
      },
      'css-loader',
    ],
  }, {
    test: /\.css$/,
    include: (p) => p.indexOf('.global.css') === -1 && p.indexOf('node_modules') === -1,
    use: options.cssLoader ? options.cssLoader(cssLoaders) : ['style-loader'].concat(cssLoaders),
  }, {
    test: /\.svg$/,
    use: [{
      loader: 'svg-react-loader',
    }],
  }, {
    test: /\.(eot|ttf|woff|woff2)$/,
    use: ['file-loader'],
  }, {
    test: /\.(jpg|png|gif)$/,
    use: [
      'file-loader',
    ],
  }, {
    test: /\.html$/,
    use: ['html-loader'],
  },
];
