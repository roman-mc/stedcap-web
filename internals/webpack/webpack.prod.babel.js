// Important modules this config uses
const path = require('path');
const webpack = require('webpack');
const HtmlWebpackPlugin = require('html-webpack-plugin');
// const OfflinePlugin = require('offline-plugin');
const config = require(path.resolve(process.cwd(), 'config/prod.js'));
const MiniCssExtractPlugin = require('mini-css-extract-plugin');

const CopyWebpackPlugin = require('copy-webpack-plugin');

const appEnv = process.env.APP_ENV || 'unity';


module.exports = require('./webpack.base.babel')(Object.assign({
  // In production, we skip all hot-reloading stuff
  mode: 'development',
  entry: [
    path.join(process.cwd(), 'app/app.js'),
  ],

  // Utilize long-term caching by adding content hashes (not compilation hashes) to compiled assets
  output: {
    filename: '[name].[chunkhash].js',
    chunkFilename: '[name].[chunkhash].chunk.js',
  },

  cssLoader: (loaders) => [
    MiniCssExtractPlugin.loader,
  ].concat(loaders),

  plugins: [
    new MiniCssExtractPlugin({
      filename: '[name].[hash]-[chunkhash]-[contenthash].css',
      chunkFilename: '[id].[chunkhash].css',
    }),
    new webpack.optimize.SplitChunksPlugin(),

    // Minify and optimize the index.html
    new HtmlWebpackPlugin({
      template: 'app/index.html',
      minify: {
        removeComments: true,
        collapseWhitespace: true,
        removeRedundantAttributes: true,
        useShortDoctype: true,
        removeEmptyAttributes: true,
        removeStyleLinkTypeAttributes: true,
        keepClosingSlash: true,
        minifyJS: true,
        minifyCSS: true,
        minifyURLs: true,
      },
      inject: true,
    }),
    new CopyWebpackPlugin([
      { from: `app/assets/favicons/${appEnv}`, to: 'icons' },
      { from: 'app/assets/documents', to: 'documents' },
    ], {}),
  ],

  performance: {
    assetFilter: (assetFilename) => !(/(\.map$)|(^(main\.|favicon\.))/.test(assetFilename)),
  },

}, config));
