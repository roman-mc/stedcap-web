#!/usr/bin/env bash

BINTRAY_USER=xzwiex
BINTRAY_KEY=e4d8d326fd7bbe5e106f1b6138dc37f960859bb3
REPOURL=unity-systems/deb/$PROJECT_NAME
VERSION='0.1.3'
NOBRANCH='nobranch'
TRAVIS_BRANCH=${TRAVIS_BRANCH:-$NOBRANCH}
DISTRO_NAME=${TRAVIS_BRANCH//\//_}
DISTRIBUTION_PARAMS="deb_distribution=$DISTRO_NAME;deb_component=main;deb_architecture=all"

for i in "$@"
do
case $i in
    --version=*)
    VERSION="${i#*=}"
    shift # past argument=value
    ;;
    --snapshot)
    SNAPSHOT=TRUE
    shift # past argument with no value
    ;;
esac
done

if [ $SNAPSHOT == TRUE ]; then
  VERSION=$VERSION~`date +%s`
fi;

./build-frontend.sh --version=$VERSION --distro=$DISTRO_NAME --user=$BINTRAY_USER --key=$BINTRAY_KEY
