#!/usr/bin/env bash

PROJECT_NAME='unity-frontend'
ORGANISATION='unity-systems/deb'

BUILD_DIR=/tmp/$PROJECT_NAME
DST_DIR=var/www/$PROJECT_NAME
REPOURL=$ORGANISATION/$PROJECT_NAME


for i in "$@"
do
case $i in
    --version=*)
    VERSION="${i#*=}"
    shift # past argument=value
    ;;
    --distro=*)
    DISTRO_NAME="${i#*=}"
    shift # past argument=value
    ;;
    --user=*)
    BINTRAY_USER="${i#*=}"
    shift # past argument=value
    ;;
    --key=*)
    BINTRAY_KEY="${i#*=}"
    shift # past argument=value
    ;;

esac
done

DISTRIBUTION_PARAMS="deb_distribution=$DISTRO_NAME;deb_component=main;deb_architecture=all"


echo "---Start build $PROJECT_NAME deb for version $VERSION in branch $DISTRO_NAME---"


cp -rv build docker/
cd docker

docker login -u "bulay" -p "303d6063b389d3dbcbf8769aa72afd23fef408be" unity-systems-docker-docker.bintray.io
docker build -t unity-systems-docker-docker.bintray.io/$PROJECT_NAME:$DISTRO_NAME .
docker push unity-systems-docker-docker.bintray.io/$PROJECT_NAME:$DISTRO_NAME
