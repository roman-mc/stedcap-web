#!/bin/ash
#envsubst < /etc/nginx/nginx.tmpl > /etc/nginx/nginx.conf
sed -i -e "s/UNITY_HOST_SUB/${UNITY_HOST}/" /etc/nginx/nginx.conf
sed -i -e "s/UNITY_SITE_SUB/${UNITY_SITE}/" /etc/nginx/nginx.conf

cat /etc/nginx/nginx.conf
nginx -g 'daemon off;'
