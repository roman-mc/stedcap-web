module.exports = {
  hostName: '/',
  realApiHostName: '/rest/',
  websocketHostName: '/ws/',
};
