// These are the pages you can go to.
// They are all wrapped in the App component, which should contain the navbar etc
// See http://blog.mxstbr.com/2016/01/react-apps-with-pages for more information
// about the code splitting business
import Loadable from './components/Loadable';

export default function createRoutes() {
  return [
    {
      path: '/login',
      name: 'loginPage',
      component: Loadable(() => import('pages/LoginPage')),
    },
    {
      path: '/register',
      name: 'registrationPage',
      component: Loadable(() => import('pages/RegistrationPage')),
    },
    {
      path: '/resetpassword',
      name: 'passwordResetPage',
      component: Loadable(() => import('pages/PasswordResetPage')),
    },
    {
      path: '/logout',
      name: 'logoutPage',
      component: Loadable(() => import('pages/LogoutPage')),
    },
    {
      path: '/',
      name: 'authorizedPages',
      component: Loadable(() => import('pages/AuthWrapper')),
    },
  ];
}
