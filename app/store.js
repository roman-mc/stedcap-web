/**
 * Create the store with asynchronously loaded reducers
 */

import { createStore, applyMiddleware, compose } from 'redux';
import { fromJS } from 'immutable';
import { routerMiddleware, LOCATION_CHANGE } from 'react-router-redux';
import createSagaMiddleware from 'redux-saga';
import Raven from 'raven-js';
import deleteKey from 'key-del';
import { reducer as formReducer } from 'redux-form/immutable';

import createReducer from 'unity-frontend-core/utils/reducers';

import createHistory from 'history/createBrowserHistory';

import createRavenMiddleware from 'raven-for-redux';

import globalReducer from './containers/App/reducer';
import languageProviderReducer from './containers/LanguageProvider/reducer';
import notificationsReducer from './containers/Notifications/reducer';
export const h = createHistory();


const sagaMiddleware = createSagaMiddleware({
  onError: (e) => console.warn('onError', e) || Raven.captureException(e),
});

const RAVEN_DSN = 'https://8aab937f87fb409a94fe10a4cf4265f8@sentry.io/1180335';
const SENSITIVE_KEYS = ['token', 'temporaryToken', 'verifyToken', 'phone'];


if (process.env.NODE_ENV === 'production') {
  Raven.config(RAVEN_DSN, {
    ignoreErrors: [/Non-Error exception captured.*/],
  }).install();
}

function configureStore(initialState = {}, history = h) {
  // Create the store with two middlewares
  // 1. sagaMiddleware: Makes redux-sagas work
  // 2. routerMiddleware: Syncs the location/URL path to the state
  const middlewares = [
    sagaMiddleware,
    routerMiddleware(history),
    createRavenMiddleware(Raven, {
      actionTransformer: (action) => deleteKey(action, SENSITIVE_KEYS),
      stateTransformer: (state) => (state && state
        .removeIn(['global', 'token'])
        .removeIn(['global', 'temporaryToken'])
        .removeIn(['global', 'verifyToken'])
        .toJS()),
    }),
  ];

  const enhancers = [
    applyMiddleware(...middlewares),
  ];


  // Initial routing state
  const routeInitialState = fromJS({
    locationBeforeTransitions: null,
  });


  /**
 * Merge route into the global application state
 */
  function routeReducer(state = routeInitialState, action) {
    switch (action.type) {
      /* istanbul ignore next */
      case LOCATION_CHANGE:
        return state.merge({
          locationBeforeTransitions: action.payload,
        });
      default:
        return state;
    }
  }

  // If Redux DevTools Extension is installed use it, otherwise use Redux compose
  /* eslint-disable no-underscore-dangle */
  const composeEnhancers = process.env.NODE_ENV !== 'production'
      && typeof window === 'object'
      && window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__
    ? window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ : compose;
  /* eslint-enable */

  const asyncReducers = {
    route: routeReducer,
    global: globalReducer,
    language: languageProviderReducer,
    form: formReducer,
    notifications: notificationsReducer,
  };

  const store = createStore(
    createReducer(asyncReducers),
    fromJS(initialState),
    composeEnhancers(...enhancers)
  );


  // Extensions
  store.runSaga = sagaMiddleware.run;
  store.asyncReducers = { ...asyncReducers }; // Async reducer registry

  // Make reducers hot reloadable, see http://mxs.is/googmo
  /* istanbul ignore next */
  if (module.hot) {
    module.hot.accept('unity-frontend-core/utils/reducers', () => {
      import('unity-frontend-core/utils/reducers').then((reducerModule) => {
        const createReducers = reducerModule.default;
        const nextReducers = createReducers(store.asyncReducers);

        store.replaceReducer(nextReducers);
      });
    });
  }

  return store;
}


const initialState = {};
export default configureStore(initialState);

export { h as history };
