import { isString } from 'lodash';

const CONFIG_NAME = 'tradingTree';

export function lsSaveConfig(config) {
  if (!config) {
    return null;
  }
  const conf = isString(config) ? config : JSON.stringify(config);

  localStorage.setItem(CONFIG_NAME, conf);

  return conf;
}

export function lsRemoveConfig() {
  localStorage.removeItem(CONFIG_NAME);
  return true;
}

export function lsGetConfig() {
  const strConfig = localStorage.getItem(CONFIG_NAME);
  return JSON.parse(strConfig);
}
