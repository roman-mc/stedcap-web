
import SETTINGS from 'unity-frontend-core/Config/SharedSettings';
const STORAGE_USER_KEY = 'unity_user';
const STORAGE_TOKEN_KEY = 'unity_token';

export function setToken(token) {
  localStorage.setItem(STORAGE_TOKEN_KEY, token);
  SETTINGS.token = token;
  return token;
}

export function getToken() {
  return localStorage.getItem(STORAGE_TOKEN_KEY);
}

export function removeToken() {
  localStorage.removeItem(STORAGE_TOKEN_KEY);
  SETTINGS.token = null;
  return true;
}

export function setUserData(data) {
  localStorage.setItem(STORAGE_USER_KEY, JSON.stringify(data));
  return data;
}

export function getUserData() {
  return JSON.parse(localStorage.getItem(STORAGE_USER_KEY) || '{}');
}


export function removeUserData() {
  localStorage.removeItem(STORAGE_USER_KEY);
  return true;
}
