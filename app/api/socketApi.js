import SocketApi from 'unity-frontend-core/api/socketApi';
import SETTINGS from 'unity-frontend-core/Config/SharedSettings';

import { getToken } from './authorizationApi';
import store from '../store';

const { protocol, host } = window.location;

const socketApi = new SocketApi(`${protocol}//${host}`, getToken, () => store);

SETTINGS.socketApi = socketApi;
export default socketApi;
