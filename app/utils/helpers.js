/* eslint-disable bh */
import { Map } from 'immutable';

export function formatFileSize(fileSizeInBytes) {
  let result = fileSizeInBytes;
  let i = -1;
  const byteUnits = [' kB', ' MB', ' GB', ' TB', 'PB', 'EB', 'ZB', 'YB'];
  do {
    result = result / 1024; // eslint-disable-line operator-assignment
    i += 1;
  } while (result > 1024);

  return Math.max(result, 0.1).toFixed(1) + byteUnits[i];
}

export function allValuesExists(values = new Map(), required = []) {
  return !required.find((r) => !values.has(r));
}
