import Ajv from 'ajv/dist/ajv.min';
import set from 'lodash/set';
import get from 'lodash/get';

import ajvKeywords from 'ajv-keywords';
import AjvErrors from 'ajv-errors';

const enumToSelectValues = (enm, replacements) =>
  enm.map((v) => ({
    value: v,
    label: Object
      .keys(replacements)
      .reduce((acc, key) => acc.replace(`%${key}%`, replacements[key]), v),
  }));


export const selectValues = (schema, propName, replacements = {}, schemaPart = 'properties') => {
  const property = get(schema, `${schemaPart}.${propName}`);
  if (property.enum) {
    return enumToSelectValues(property.enum, replacements);
  } if (property.$ref) {
    return selectValues(schema, property.$ref.replace('#/definitions/', ''), replacements, 'definitions');
  }
  return [];
};

export const formValidator = (values, props) => {
  const { schema } = props;

  const ajvErrorsOptions = { keepErrors: false };

  const ajv = new Ajv({
    allErrors: true,
    jsonPointers: true,
    useDefaults: true,
    verbose: true,
    schemaId: 'auto',
    $data: true,
  });

  ajvKeywords(ajv);

  const ajvWithErrors = new AjvErrors(ajv, ajvErrorsOptions);
  const errorMessage = (error) => error.message;
  const localize = (errors) => errors;

  if (schema) {
    const options = { ajv: ajvWithErrors, errorMessage, localize };

    const validateSchema = options.ajv.compile(schema);
    const errors = {};
    if (values.toJS) {
      const vals = values.toJS();
      validateSchema(vals);

      if (validateSchema.errors) {
        validateSchema.errors.forEach((_error) => {
          const error = _error.params.errors ? _error.params.errors[0] : _error;

          const rootPath = error.dataPath;


          const property = error.params.missingProperty || '';
          const pathArray = [...rootPath.split('/'), property];

          let fullPath = pathArray
            .filter((p) => p)
            .map((p, idx) => {
              if (idx === 0) {
                return p;
              }
              return parseInt(p, 10) >= 0 ? `[${p}]` : `.${p}`;
            }).join('');
          // console.log('path', path);
          // let fullPath = `${rootPath}${property}`.replace(/\//g, '.').substring(1);

          // if (error.parentSchema && error.parentSchema.type === 'array') {
          //     fullPath += '._error';
          // }
          // console.log(fullPath)
          fullPath += '._error';

          const message = options.errorMessage(_error);
          set(errors, fullPath, message);
        });
      }
    }

    return errors;
  }
  return { _error: 'error' };
};
