/**
 * @param  {...string|false|null} arr
 * @return {string}
 */
export const styleNames = (...arr) => arr.filter(Boolean).join(' ');
