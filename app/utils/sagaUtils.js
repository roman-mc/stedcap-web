import { take, call, fork, takeEvery, put } from 'redux-saga/effects';
import { eventChannel } from 'redux-saga';
import Raven from 'raven-js';

export function* takeFirst(pattern, saga, ...args) {
  const task = yield fork(function* takeFirstFork() {
    while (true) { // eslint-disable-line no-constant-condition
      const action = yield take(pattern);
      yield call(saga, ...args.concat(action));
    }
  });
  return task;
}


export function createChannel(ws, ...eventTypes) {
  return eventChannel((emit) => {
    const cancelCallbacks = eventTypes.map((eventType) => {
      const cb = (data) => {
        emit({ data, eventType });
      };
      ws.startMessaging(eventType, cb);
      // unsubscribe function
      return () => {
        ws.stopMessaging(eventType, cb);
      };
    });

    // Unsubscribe from all callbacks
    return () => {
      cancelCallbacks.forEach((cb) => cb);
    };
  });
}


export function simpleApiSaga(action, apiMethod, paramsParser = () => []) {
  function* handler({ payload }) {
    const params = paramsParser(payload);

    yield call(callApi, action, apiMethod, ...params);
  }

  return generateListener(action, handler);
}

export function* callApi(action, apiMethod, ...params) {
  try {
    const result = yield call(apiMethod, ...params);
    yield put(action.success(result));
    yield put(action.fulfill({ result, params }));
  } catch (e) {
    console.warn('Error on loading', action.toString(), e);
    yield put(action.error(e));
    Raven.captureException(e);
  }
}

export function generateListener(action, handler) {
  return function* saga() {
    yield takeEvery(action.toString(), handler);
  };
}

export const runAllSagas = (sagasToRun) => {
  function* runner() {
    yield [
      ...sagasToRun,
    ].map(fork);
  }

  return runner;
};
