import PhoneNumber from 'awesome-phonenumber';

export function parsePhone(phoneNumber) {
  const number = PhoneNumber(`${phoneNumber}`);
  const countryCode = PhoneNumber.getCountryCodeForRegionCode(number.getRegionCode()).toString();
  return { countryCode, phone: number.getNumber('significant') };
}

export function getRegion(phone) {
  let region = 'ZZ';
  let countryCodeLength = 1;

  while (region === 'ZZ' && countryCodeLength <= phone.length) {
    region = PhoneNumber.getRegionCodeForCountryCode(phone.substr(0, countryCodeLength));
    countryCodeLength += 1;
  }
  return region;
}
