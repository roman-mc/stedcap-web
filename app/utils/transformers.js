/* eslint no-param-reassign: ["error", { "props": false }] */
export function transformPaymentMethodData(data) {
  const commonBankFields = [
    'correspondentBankName',
    'correspondentBankSwift',
    'correspondentBankAccount',
    'beneficiaryBankName',
    'beneficiaryBankSwift',
    'beneficiaryBankBeneficiary',
    'beneficiaryBankAccountNumber',
    'beneficiaryBankIbanCode',
    'paymentDetails',
    'paymentType',
    'documents',
    'isRussianBank',
  ];

  const rusBankFields = [
    'rusBankBik',
    'rusBankBeneficiaryBank',
    'rusBankCorrBankAccount',
    'rusBankBankAddress',
    'rusBankBeneficiaryName',
    'rusBankBeneficiaryAddress',
    'rusBankBeneficiaryAccountNo',
    'rusBankBeneficiaryTaxCode',
    'rusBankKppCode',
    'rusBankVoCode',
    'paymentDetails',
    'paymentType',
    'documents',
    'isRussianBank',
  ];

  const allowedFileds = (data.paymentType === 'RUB' && data.isRussianBank) ? rusBankFields : commonBankFields;

  const filteredData = Object.keys(data)
    .filter((key) => allowedFileds.includes(key))
    .reduce((obj, key) => ({ ...obj, [key]: data[key] }), {});

  return filteredData;
}
