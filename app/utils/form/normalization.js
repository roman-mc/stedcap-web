import PhoneNumber from 'awesome-phonenumber';
import numeral from 'numeral';
import { getRegion } from '../phoneUtils';

// https://github.com/erikras/redux-form/issues/1218#issuecomment-285032695
export function numberOnly(value) {
  const filtered = value.replace(/[^0-9.]/g, ''); // Remove all chars except numbers and .

  // Create an array with sections split by .
  const sections = filtered.split('.');

  // Remove any leading 0s apart from single 0
  if (sections[0] !== '0' && sections[0] !== '00') {
    sections[0] = sections[0].replace(/^0+/, '');
  } else {
    sections[0] = '0';
  }

  // If numbers exist after first .
  if (sections[1]) {
    // Join first two sections and truncate end section to length 2
    return `${sections[0]}.${sections[1]}`;
    // If original value had a decimal place at the end, add it back
  } if (filtered.indexOf('.') !== -1) {
    return `${sections[0]}.`;
    // Otherwise, return only section
  }
  return sections[0];
}

export function formatNumeral(value) {
  if (value) {
    const val = value.toString();
    const filtered = val.replace(/[^0-9.]/g, '');
    const sections = filtered.split('.');

    if (sections[1]) {
      return `${numeral(sections[0]).format('0,0')}.${sections[1]}`;
    } if (filtered.indexOf('.') !== -1) {
      return `${numeral(sections[0]).format('0,0')}.`;
    }
    return numeral(val).format('0,0');
  }
  return '';
}

export function intOnly(value) {
  if (value) {
    const val = value.toString();
    const filtered = val.replace(/[^0-9.]/g, '');
    const sections = filtered.split('.');

    return numeral(sections[0]).format('0,0');
  }
  return '';
}

export function parseNumeral(value) {
  if (value) {
    const val = value.toString();
    const sections = val.split('.');
    if (!sections[1] && val.indexOf('.') !== -1) {
      return `${numeral(val).value().toString()}.`;
    }
    return numeral(val).value().toString();
  }
  return '';
}

export function normalizePhone(value) {
  const cleanValue = value.replace(/[\s+-]/g, '');
  const regionCode = getRegion(cleanValue);
  if (regionCode === 'ZZ') {
    return value;
  }
  const yat = PhoneNumber.getAsYouType(regionCode);
  const countryCode = PhoneNumber.getCountryCodeForRegionCode(regionCode);
  const phoneWithoutCode = cleanValue.replace(countryCode, '');

  const prefix = `+${countryCode}`;

  const result = Array.from(phoneWithoutCode).reduce((acc, ch) => yat.addChar(ch), '');

  if (!result) {
    return prefix;
  }

  return `${prefix} ${result}`;
}


export function normalizeBoolean(value) {
  if (value === 'true') {
    return true;
  }
  if (value === 'false') {
    return false;
  }
  return value;
}
