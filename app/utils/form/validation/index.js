import PhoneNumber from 'awesome-phonenumber';

import messages from './messages';

export function isEmailValid(email) {
  const re = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
  return re.test(email);
}

export function passwordValidate(password) {
  if (!password) {
    return messages.requiredError;
  } if (password.length < 6) {
    return messages.shortPasswordError;
  }
  return undefined;
}

export function nonEmptyArrayValidate(arr) {
  if (!arr || !arr.size) {
    return messages.atLeastOneError;
  }
  return undefined;
}

export function emailValidate(email) {
  if (!isEmailValid(email)) {
    return messages.emailError;
  }
  return undefined;
}

export function requireValidate(value) {
  if (value === null || typeof value === 'undefined' || value === '') {
    return messages.requiredError;
  }
  return undefined;
}

export function phoneValidate(phone) {
  if (!phone) {
    return messages.requiredError;
  } if (!PhoneNumber(phone).isMobile()) {
    return messages.invalidPhoneError;
  }
  return undefined;
}

export const minLength = (min) => (val) => val && val.length < min ? true : undefined;
