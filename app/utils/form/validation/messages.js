/*
 * Validation Messages
 *
 * This contains all the text for the Validation utils.
 */
import { defineMessages } from 'react-intl';

export default defineMessages({
  passwordError: {
    id: 'app.validation.errors.password',
    defaultMessage: 'Password too short',
  },
  emailError: {
    id: 'app.validation.errors.email',
    defaultMessage: 'Must be email',
  },
  requiredError: {
    id: 'app.validation.errors.required',
    defaultMessage: 'Required',
  },
  shortPasswordError: {
    id: 'app.validation.errors.shortPassword',
    defaultMessage: 'Password is too short',
  },
  invalidPasswordError: {
    id: 'app.validation.errors.invalidPassword',
    defaultMessage: 'Passsword is not valid',
  },
  invalidPhoneError: {
    id: 'app.validation.errors.invalidPhone',
    defaultMessage: 'Please enter valid phone number',
  },
  atLeastOneError: {
    id: 'app.validation.errors.atLeastOneError',
    defaultMessage: 'Select at least one element',
  },
});
