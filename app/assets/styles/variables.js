const secVariables = require('./variables-sec');
const unityVariables = require('./variables-unity');

const vars = {
  unity: unityVariables,
  sec: secVariables,
};

module.exports = vars[process.env.APP_ENV] || vars.unity;
