const variables = require('./variables-unity');

module.exports = Object.assign({}, variables, {
  $logoUrl: 'Img/sec/sec-logo-dark-theme@1x.png',
  $logoLightUrl: 'Img/sec/sec-logo-light-theme@1x.png',
  $logoWidth: '130px',

  $logoLargeUrl: 'Img/sec/sec-logo-large-dark-theme@1x.png',

  $backBg: 'Img/sec/back.jpg',

  $inputBorderColor: 'transparent',
  $inputBackground: 'rgba(255, 255, 255, 0.1)',

  $submitButtonBgDisabled: 'linear-gradient(to bottom, rgb(97, 15, 18), rgb(85, 13, 15))',
  $submitButtonBg: 'linear-gradient(to bottom, rgb(235, 29, 37), rgb(119, 14, 18))',

  $authFooterLinkColor: '#ffffff',
  $authFooterLinkDecoration: 'underline',
  $authForgotLinkDecoration: '1px dashed white',

  $authInputLabelColor: '#ffffff',

  $authSendCodeBg: '#232c4d',
  $authSendCodeBgDisabled: '#232c4d',
  $authSendCodeBorderColor: '#3a4361',
  $authSendCodeColor: '#ffffff',
  $authSendCodeColorDisabled: '#5c6377',

  $selectOptionsBackground: '#222632',
  $selectOptionHoverBackground: '#232c4d',
});
