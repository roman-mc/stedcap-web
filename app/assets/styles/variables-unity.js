const colors = require('./colors.json');

module.exports = {
  ...colors,

  $redGradient: 'linear-gradient(to bottom, #f81919, #ff4000)',
  $demoAccountBorderColor: 'rgba(255, 255, 255, 0.3)',
  $demoAccountBackgroundColorColor: 'rgba(255, 255, 255, 0.1)',

  $header_height: '50px',

  $logoUrl: 'Img/unity/unity-logo-2-dark-theme.png',
  $logoLightUrl: 'Img/unity/unity-logo-2-light-theme.png',
  $logoWidth: '185px',

  $logoLargeUrl: 'Img/unity/unity-logo-3-dark-theme@2x.png',
  $logoLargeLightUrl: 'Img/unity/unity-logo-2-light-theme@2x.png',

  $backBg: 'Img/unity/back.png',

  $inputBorderColor: colors.$tundora,
  $inputBackground: 'transparent',

  $submitButtonBgDisabled: 'linear-gradient(to bottom, rgb(69, 25, 25), rgb(70, 32, 19))',
  $submitButtonBg: 'linear-gradient(to bottom, rgb(248, 25, 25), rgb(255, 64, 0))',

  $authFooterLinkColor: '#ff4000',
  $authFooterLinkDecoration: 'none',

  $authForgotLinkDecoration: 'none',

  $authInputLabelColor: colors.$gray,

  $authSendCodeBg: '#272727',
  $authSendCodeBgDisabled: '#272727',
  $authSendCodeBorderColor: '#272727',
  $authSendCodeColor: colors.$white,
  $authSendCodeColorDisabled: colors.$darkGray,

  $selectOptionsBackground: colors.$baseCodeGray,
  $selectOptionHoverBackground: colors.$mineShaft,

  $tabsBorderColor: colors.$darkGray,

  colors,
};
