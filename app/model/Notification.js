import { Record } from 'immutable';

class NotificationData extends Record({
  amount: undefined,
  currency: undefined,
  id: undefined,
  executionDate: undefined,
  tradeDate: undefined,
  orderId: undefined,
  symbol: undefined,
  price: undefined,
  valueDate: undefined,
  direction: undefined,
  reason: undefined,
  instrumentId: undefined,
  text: undefined,
}, 'NotificationData') {
  constructor(data, instrument) {
    super(data);

    return instrument ? this.set('symbol', instrument.displayName) : this;
  }
}

export default class Notification extends Record({
  created: undefined,
  message: undefined,
  notificationType: undefined,
  userId: undefined,
  data: undefined,
}, 'Notification') {
  constructor(data, instrument) {
    super(data);
    return this.set('data', new NotificationData(data.data, instrument));
  }

  isAfter(timestamp) {
    return this.created > timestamp;
  }
}
