import { Record } from 'immutable';

class ChartPointKey extends Record({
  instrumentId: undefined,
  tg: undefined,
}, 'ChartPointKey') { }

export default class ChartPoint extends Record({
  close: undefined,
  high: undefined,
  key: undefined,
  low: undefined,
  open: undefined,
  time: undefined,
  ts: undefined,
  volume: undefined,
}, 'ChartPointData') {
  constructor(data) {
    super(data);

    return this
      .set('ts', data.time || data.ts)
      .set('key', new ChartPointKey(data.key));
  }
}
