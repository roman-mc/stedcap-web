import messages from './messages';

export const USER_STATUS = {
  new: 'New',
  active: 'Active',
};


export const QUESTIONNARIE_STATUS = {
  NEW: 1,
  VERIFICATION_IN_PROGRESS: 2,
  ACCEPTED: 3,
  REJECTED: 4,
  UPDATE_IS_NEEDED: 5,
};


export const REST_ERRORS = {
  0: messages.commonError,
  50: messages.authyFailed,
  51: messages.authyValidationFailed,

  100: messages.phoneAlreadyExtists,
  101: messages.unsupportedWorkflowTransition,
  102: messages.emailAlreadyExtists,
  103: messages.failedToCreateCFH,

  110: messages.phoneNotFound,
  111: messages.wrongPassword,

  150: messages.profileNotFound,

  130: messages.orderRejected,
  131: messages.positionNotFound,

  180: messages.wrongProfileStatus,

  200: messages.currencyAlreadyExists,

  210: messages.wrongOrderRequestStatus,

  250: messages.externalAccountForCurrencyExists,
  251: messages.currenciesMustBeSame,
  252: messages.wrongAccountType,
  253: messages.bindingAlreadyExists,
  254: messages.bindingForCurrencyAlreadyExists,
  255: messages.localAccountExists,

  600: messages.imgUploadError,
  601: messages.resetPasswordWrongPass,
  602: messages.resetPasswordWrongNewPass,
};

export const PROFILE_TYPE = {
  person: 1,
  corporate: 2,
};

export const BOOL_STRING = {
  true: 'yes',
  false: 'no',
};


export const GOOD_TILL_TYPE = {
  FILL_OR_KILL: {
    id: 0,
    name: 'Fill or kill',
    orderTypes: [0, 1],
  },
  G_T_C: {
    id: 1,
    name: 'G.T.C.',
    orderTypes: [1],
  },
  // GOOD_FOR_A_DAY: {
  //     id: 2,
  //     name: 'Good For A Day',
  //     orderTypes: [1],
  // },
  I_O_C: {
    id: 3,
    name: 'I.O.C',
    orderTypes: [0, 1],
  },
};

export const ORDER_TYPE = {
  MARKET: {
    id: 0,
    name: 'Market',
  },
  LIMIT: {
    id: 1,
    name: 'Limit',
  },
};

export const PAYMENT_METHOD_STATUS = {
  new: 1,
  verificationInProgress: 2,
  accepted: 3,
  rejected: 4,
  updateIsNeeded: 5,
};

export const TRADE_DIRECTION = {
  BUY: 0,
  SELL: 1,
};


const colors = {
  CFD: '#f22613',
  CFDc: '#ef4836',
  CFDi: '#d64541',
  FX: '#199f2b',
  FXf: '#00b16a',
  FXo: '#2abb9b',
  EQ: '#f89406',
  OP: '#e56f0a',
  EQo: '#f9690e',
  FU: '#22a7f0',
  FUo: '#4183d7',
  FUs: '#446cb3',
  BO: '#8e44ad',
};


export const INSTRUMENT_COLORS = Object.keys(colors).reduce((acc, key) => ({ ...acc, [key.toUpperCase()]: colors[key] }), {});

export const WIDGET_TYPES = {
  TICKET: 'ticket',
  MARKET_DEPTH: 'marketDepth',
  OVERVIEW: 'overview',
  CHAT: 'chat',
  POSITIONS: 'positions',
  WATCHLIST: 'watchlist',
  NEWS: 'news',
};
