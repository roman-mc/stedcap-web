import { defineMessages } from 'react-intl';

export default defineMessages({
  commonError: {
    id: 'app.enums.commonError',
    defaultMessage: 'An error occured ({message})',
  },
  authyFailed: {
    id: 'app.enums.authyFailed',
    defaultMessage: 'SMS sending error',
  },
  authyValidationFailed: {
    id: 'app.enums.authyValidationFailed',
    defaultMessage: 'Incorrect confirmation code',
  },
  phoneAlreadyExtists: {
    id: 'app.enums.phoneAlreadyExtists',
    defaultMessage: 'Phone already exists',
  },
  unsupportedWorkflowTransition: {
    id: 'app.enums.unsupportedWorkflowTransition',
    defaultMessage: 'Unsupported workflow transition',
  },
  emailAlreadyExtists: {
    id: 'app.enums.emailAlreadyExtists',
    defaultMessage: 'Email already exists',
  },
  failedToCreateCFH: {
    id: 'app.enums.failedToCreateCFH',
    defaultMessage: 'Failed to create external account',
  },
  phoneNotFound: {
    id: 'app.enums.phoneNotFound',
    defaultMessage: 'Phone not found',
  },
  profileNotFound: {
    id: 'app.enums.profileNotFound',
    defaultMessage: 'Profile not found',
  },
  wrongPassword: {
    id: 'app.enums.wrongPassword',
    defaultMessage: 'Wrong password',
  },
  orderRejected: {
    id: 'app.enums.orderRejected',
    defaultMessage: 'Order rejected for unknown reason',
  },
  positionNotFound: {
    id: 'app.enums.positionNotFound',
    defaultMessage: 'Position not found',
  },
  wrongProfileStatus: {
    id: 'app.enums.wrongProfileStatus',
    defaultMessage: 'Wrong profile status',
  },
  currencyAlreadyExists: {
    id: 'app.enums.currencyAlreadyExists',
    defaultMessage: 'Currency already exists',
  },
  wrongOrderRequestStatus: {
    id: 'app.enums.wrongOrderRequestStatus',
    defaultMessage: 'Wrong order request status',
  },
  externalAccountForCurrencyExists: {
    id: 'app.enums.externalAccountForCurrencyExists',
    defaultMessage: 'External account for currency already exists',
  },
  currenciesMustBeSame: {
    id: 'app.enums.currenciesMustBeSame',
    defaultMessage: 'Currencies for local and external account must be the same',
  },
  wrongAccountType: {
    id: 'app.enums.wrongAccountType',
    defaultMessage: 'Wrong account type',
  },
  bindingAlreadyExists: {
    id: 'app.enums.bindingAlreadyExists',
    defaultMessage: 'Binding already exists',
  },
  bindingForCurrencyAlreadyExists: {
    id: 'app.enums.bindingForCurrencyAlreadyExists',
    defaultMessage: 'Binding for currency and platform already exists',
  },
  localAccountExists: {
    id: 'app.enums.localAccountExists',
    defaultMessage: 'Local account for currency already exists',
  },
  resetPasswordWrongPass: {
    id: 'app.enums.resetPasswordWrongPass',
    defaultMessage: 'Wrong password',
  },
  resetPasswordWrongNewPass: {
    id: 'app.enums.resetPasswordWrongNewPass',
    defaultMessage: 'Passwords do not match',
  },
  imgUploadError: {
    id: 'app.enums.imgUploadError',
    defaultMessage: 'Upload error',
  },
});
