import { Record } from 'immutable';

export default class FileRecord extends Record({
  id: undefined,
  fileSize: undefined,
  name: undefined,
}, 'FileRecord') {
  constructor(data) {
    super(data);
    return this.set('fileSize', data.size);
  }
}
