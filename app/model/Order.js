import { Record } from 'immutable';

export default class Order extends Record({
  amount: undefined,
  created: undefined,
  direction: undefined,
  goodTillType: undefined,
  id: undefined,
  instrumentId: undefined,
  multiAccountId: undefined,
  orderStatus: undefined,
  orderType: undefined,
  platformId: undefined,
  price: undefined,
}, 'Order') {
}
