import { Record } from 'immutable';

class OrderDataValues extends Record({
  afterTradeMarginAvailable: undefined,
  cost: undefined,
  currentMarginAvailable: undefined,
  marginImpact: undefined,
}, 'OrderDataValues') {}

export default class OrderData extends Record({
  buy: undefined,
  sell: undefined,
  instrumentId: undefined,
  amount: undefined,
}, 'OrderDataValues') {
  constructor(data) {
    super(data);

    return this
      .set('buy', new OrderDataValues(data.buy))
      .set('sell', new OrderDataValues(data.sell));
  }
}
