import { Map, Record, fromJS } from 'immutable';
import generateUuid from 'uuid/v4';

import Widget from './Widget';

export default class Panel extends Record({
  config: undefined,
  widgets: new Map(),
}, 'Panel') {
  constructor(data) {
    super(data);

    const uuidsByZindex = Object.keys(data.widgets)
      .sort((uuidA, uuidB) =>
        data.widgets[uuidA].position.zIndex - data.widgets[uuidB].position.zIndex
      ); // recalculate zIndexes to prevent infinite increment

    return this
      .set('config', fromJS(data.config || {}))
      .set(
        'widgets',
        Object.keys(data.widgets)
          .reduce(
            (acc, uuid) => acc.set(uuid, new Widget(data.widgets[uuid]).setIn(['position', 'zIndex'], uuidsByZindex.indexOf(uuid))),
            new Map()
          )
      );
  }

  addWidget(widget) {
    const maxZindex = this.widgets
      .valueSeq()
      .map((w) => w.position.zIndex)
      .sortBy((zIndex) => zIndex)
      .last() || 0;

    return this.setIn(
      ['widgets', generateUuid()],
      new Widget(widget).setIn(['position', 'zIndex'], maxZindex + 1)
    );
  }
}
