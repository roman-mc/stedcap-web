// The structure of Dashboard is here:
// platform-frontend/web/app/config/dashboardConfig.js
import { List, Record, fromJS } from 'immutable';

import Panel from './Panel';

export default class Dashboard extends Record({
  version: undefined,
  config: undefined,
  panels: new List(),
}, 'Dashboard') {
  constructor(data = {}) {
    super(data);
    const result = this
      .set('config', fromJS(data.config || {}))
      .set('panels', new List((data.panels || []).map((p) => new Panel(p))));
    return result;
  }

  setConfig(blockId, config) {
    return this.updateIn([...this.getPath(blockId), 'config'], (p) => p.merge(config));
  }

  setPosition(blockId, position) {
    return this.updateIn([...this.getPath(blockId), 'position'], (p) => p.merge(position));
  }

  getConfig(blockId, notSetValue = null) {
    return this.getIn([...this.getPath(blockId), 'config'], notSetValue);
  }

  removeWidget([panelId, widgetId]) {
    return this.removeIn(this.getPath([panelId, widgetId]));
  }

  addWidget(panelId, widget) {
    const path = this.getPath([panelId]);
    const isSameWidgetExists = !!this.getIn(path).widgets.find((w) => w.component === widget.component);
    if (isSameWidgetExists) {
      return this;
    }
    return this.updateIn(path, (panel) => panel.addWidget(widget));
  }

  getPath(blockId) {
    const prefixes = ['panels', 'widgets'];
    return blockId.reduce((acc, id, idx) => [...acc, prefixes[idx], id], []);
  }
}
