import { Record, Map } from 'immutable';

class WidgetPosition extends Record({
  x: 0,
  y: 0,
  width: 50,
  height: 50,
  zIndex: 0,
}) {

}

export default class Widget extends Record({
  component: undefined,
  config: new Map(),
  position: undefined,
}, 'Widget') {
  constructor(data) {
    super(data);

    return this
      .set('config', new Map(data.config))
      .set('position', new WidgetPosition(data.position));
  }
}
