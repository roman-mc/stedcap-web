import React from 'react';
import PropTypes from 'prop-types';

import WithInjection from 'unity-frontend-core/containers/WithInjection';
import ScrollBar from 'Components/Scrollbar';

import { WindowHeader, Window, WindowHeaderTitle } from 'Components/Window';
import LoadingIndicator from 'Components/LoadingIndicator';
import {} from './styles.css';
import TransactionsList from './TransactionsList';
import Filters from './Filters';

import sagas from './sagas';

class TransactionsPage extends React.PureComponent {
  state={}

  componentDidMount() {
    const { loadTransactions } = this.props;
    loadTransactions({ clear: true });
  }

  loadMore = () => {
    const { totalCount, transactions, loading, loadTransactions } = this.props;
    if (totalCount > transactions.size && !loading) {
      loadTransactions({ loadMore: transactions.size });
    }
  }

  render() {
    const { transactions, filters, loading, setFilters, setFilter, showTransactionDetails, loadTransactionNetting } = this.props;

    return (
      <div styleName="panels">
        <div styleName="panel">
          <Window fullWidth fullHeight>
            <WindowHeader renderTitle={() => <WindowHeaderTitle><i className="icon-icon-transactions" styleName="transactions-title"></i><b>Transactions</b></WindowHeaderTitle>} />
            <Filters
              filters={filters}
              setFilters={setFilters}
              setFilter={setFilter}
            />
            {loading && <div styleName="loader"><LoadingIndicator loading /></div>}
            <ScrollBar
              style={{ position: 'static', minWidth: '100%' }}
              containerRef={(ref) => {
                if (ref) {
                  this.container = ref;
                  this.container.style.width = '100%';
                }
              }}
              onYReachEnd={this.loadMore}
            >
              <TransactionsList
                loadTransactionNetting={loadTransactionNetting}
                showTransactionDetails={showTransactionDetails}
                transactions={transactions}
              />
            </ScrollBar>
          </Window>
        </div>
      </div>
    );
  }
}
export default WithInjection({ sagas })(TransactionsPage);

TransactionsPage.propTypes = {
  transactions: PropTypes.object,
  filters: PropTypes.object,
  totalCount: PropTypes.number,
  loading: PropTypes.bool,
  loadTransactionNetting: PropTypes.func,
  showTransactionDetails: PropTypes.func,
  setFilters: PropTypes.func,
  setFilter: PropTypes.func,
  loadTransactions: PropTypes.func,
};
