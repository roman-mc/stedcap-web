import React from 'react';
import PropTypes from 'prop-types';
import { FormattedNumber } from 'react-intl';

import { INSTRUMENT_CATEGORIES, CURRENCIES } from 'unity-frontend-core/Models/Enums';
import InstrumentResolver from 'unity-frontend-core/containers/InstrumentResolver';

import ImmutableListView from 'Components/ImmutableListView';

import {} from './styles.css';

export default class TransactionsNettings extends React.PureComponent {
  state={
    opened: {},
  }

  toggle = (key) => {
    this.setState((s) => ({
      opened: {
        ...s.opened,
        [key]: !s.opened[key],
      },
    }));
  }

  renderNetting = (instrument, id, { netting }) => {
    const { currency } = this.props;
    return <div styleName="transaction-row">
      <div styleName="small-cell account-currency">{currency} account</div>
      <div styleName="big-cell">
        {instrument && instrument.displayName}
      </div>
      <div styleName="small-cell text-right">
        {`${netting.accountAmount < 0 ? '-' : ''} ${CURRENCIES[currency]}`}<FormattedNumber value={Math.abs(netting.accountAmount)} />
      </div>
    </div>;
  }

  renderRow = (netting, index, key) => {
    const { opened } = this.state;
    return opened[key] ? <div key={index}>
      <InstrumentResolver netting={netting} id={netting.instrumentId} render={this.renderNetting} />
    </div> : null;
  }

  render() {
    const { nettings, currency } = this.props;
    const { opened } = this.state;
    return (
      <ImmutableListView
        immutableData={nettings.groupBy((n) => n.instrumentCategory)}
        renderSection={(key, items) => {
          const totalAmount = items.reduce((acc, t) => acc + t.accountAmount, 0);
          return (
            <div key={key} styleName="transaction-instrument-row">
              <div styleName="instrument-header">
                <span styleName="hide-show" onClick={() => this.toggle(key)}>{opened[key] ? '-' : '+'}</span>
                <span styleName="instrument" style={{ backgroundColor: INSTRUMENT_CATEGORIES[key].color }}>{key}</span>
                <span>{INSTRUMENT_CATEGORIES[key].name}</span>
              </div>
              <div>
                {`${totalAmount < 0 ? '-' : ''} ${CURRENCIES[currency]}`}<FormattedNumber value={Math.abs(totalAmount)} />
              </div>
            </div>
          );
        }}
        renderRow={this.renderRow}
      />
    );
  }
}

TransactionsNettings.propTypes = {
  nettings: PropTypes.object,
  currency: PropTypes.string,
};
