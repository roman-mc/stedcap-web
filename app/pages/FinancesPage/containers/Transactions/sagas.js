import { put } from 'redux-saga/effects';
import { generateListener } from 'unity-frontend-core/utils/sagaUtils';

import { loadTransactions } from 'unity-frontend-core/containers/Transactions/actions';
import { selectAccount } from 'unity-frontend-core/containers/SelectAccount/actions';

export function* updateAccountHandler() {
  yield put(loadTransactions({ clear: true }));
}


export default [
  generateListener(selectAccount.success, updateAccountHandler),
];
