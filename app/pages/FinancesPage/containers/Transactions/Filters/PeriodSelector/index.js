import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';

import moment from 'moment';

import {} from './styles.css';

export default class PeriodSelector extends PureComponent {
  state = {
    periods: [
      {
        text: 'Today',
        dateFrom: moment(),
        dateTo: moment(),
      },
      {
        text: 'Yesterday',
        dateFrom: moment().subtract(1, 'days'),
        dateTo: moment().subtract(1, 'days'),
      },
      {
        text: 'Last week',
        dateFrom: moment().subtract(1, 'weeks'),
        dateTo: moment(),
      },
      {
        text: 'Last month',
        dateFrom: moment().subtract(1, 'months'),
        dateTo: moment(),
      },
    ],
  };

  isPeriodSelected = (periodFrom, periodTo) => {
    const { dateFrom, dateTo } = this.props.period;
    return dateFrom && dateTo && periodFrom.isSame(dateFrom, 'day') && periodTo.isSame(dateTo, 'day');
  }

  selectPeriod = ({ dateFrom, dateTo }) => () => {
    if (this.isPeriodSelected(dateFrom, dateTo)) {
      this.props.selectPeriod();
    } else {
      this.props.selectPeriod(dateFrom, dateTo);
    }
  }

  render() {
    const { periods } = this.state;

    return <div styleName="btn-group">
      {periods.map((period, i) => (
        <div
          onClick={this.selectPeriod(period)}
          styleName={`input-label ${this.isPeriodSelected(period.dateFrom, period.dateTo) ? 'active' : ''}`}
          key={`radio__${i}`} // eslint-disable-line react/no-array-index-key
        >
          <span>{period.text}</span>
        </div>
      ))}
    </div>;
  }
}

PeriodSelector.propTypes = {
  period: PropTypes.object,
  selectPeriod: PropTypes.func,
};
