import React from 'react';
import PropTypes from 'prop-types';
import moment from 'moment/moment';
import Select from 'react-select';

import { STANDARD_DATE } from 'unity-frontend-core/utils/dateUtils';
import { TRANSACTION_TYPES } from 'unity-frontend-core/Models/Enums';
import { DateInput } from 'Components/FormInputs/DateInput';

import PeriodSelector from './PeriodSelector';

import styles from './styles.css';

export default class Filters extends React.PureComponent {
  state={}

  changeDateFilter = (key) => (value) => {
    const { setFilter } = this.props;
    setFilter(key, value.startOf('day').format(STANDARD_DATE));
  }

  changeTypeFilter = (value) => {
    const { setFilter } = this.props;
    setFilter('transactionType', value ? value.value : '');
  }

  selectPeriod = (from, to) => {
    const { setFilters } = this.props;
    setFilters({
      toValueDate: to ? to.startOf('day').format(STANDARD_DATE) : undefined,
      fromValueDate: from ? from.startOf('day').format(STANDARD_DATE) : undefined,
    });
  }

  render() {
    const { filters: { fromValueDate = '', toValueDate = '', transactionType } } = this.props;
    const fromDate = fromValueDate !== '' ? moment(fromValueDate, STANDARD_DATE) : undefined;
    const toDate = toValueDate !== '' ? moment(toValueDate, STANDARD_DATE) : undefined;

    const transactionTypesOptions = [{ value: '', label: 'All types' }, ...TRANSACTION_TYPES.selectOptions];

    return (
      <div styleName="filters">
        <div styleName="filter filter-select">
          <div styleName="filter-label">Show</div>
          <div styleName="filter-input">
            <Select
              value={transactionType}
              options={transactionTypesOptions}
              onChange={this.changeTypeFilter}
              styleName="filter-select"
              clearable={false}
              optionstyleName="filter-select-option"
            />
          </div>
        </div>
        <div styleName="filter date-range">
          <div styleName="filter-label">Select date range</div>
          <div styleName="filter-input">
            <PeriodSelector
              period={{
                dateFrom: fromDate,
                dateTo: toDate,
              }}
              selectPeriod={this.selectPeriod}
            />
          </div>
        </div>
        <div styleName="filter">
          <div styleName="filter-input date-input">
            <DateInput
              value={fromDate}
              onChange={this.changeDateFilter('fromValueDate')}
              label="From"
              maxDate={toDate || moment()}
              inputClassName={styles['date-input-border']}
              labelClassName={styles['date-label']}
            />
          </div>
        </div>
        <div styleName="filter date-input">
          <div styleName="filter-input">
            <div>
              <DateInput
                value={toDate}
                onChange={this.changeDateFilter('toValueDate')}
                label="To"
                minDate={fromDate}
                maxDate={moment()}
                inputClassName={styles['date-input-border']}
                labelClassName={styles['date-label']}
              />
            </div>
          </div>
        </div>
      </div>
    );
  }
}

Filters.propTypes = {
  setFilter: PropTypes.func,
  setFilters: PropTypes.func,
  filters: PropTypes.object,
};
