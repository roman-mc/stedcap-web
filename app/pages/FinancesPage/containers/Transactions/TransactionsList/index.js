import React from 'react';
import { FormattedNumber } from 'react-intl';

import { CURRENCIES, TRANSACTION_TYPES } from 'unity-frontend-core/Models/Enums';
import ImmutableListView from 'Components/ImmutableListView';
import PropTypes from 'prop-types';

import {} from './styles.css';
import TransactionsNettings from '../TransactionNetting';

export default class TransactionsList extends React.PureComponent {
  state={}

  loadTransactionNetting = ({ id, nettingsEmpty, nettings }) => () => {
    const { loadTransactionNetting, showTransactionDetails } = this.props;
    if (!nettingsEmpty && (!nettings || !nettings.size)) {
      loadTransactionNetting(id);
    } else {
      showTransactionDetails(id);
    }
  }

  renderTransactionType = ({ transactionType, amount }) => {
    if (transactionType === 0) {
      return amount > 0 ? 'Cash in' : 'Cash out';
    }
    return TRANSACTION_TYPES.getById(transactionType).label;
  }

  renderToggle = (transaction) => {
    if (transaction.transactionType === 0) {
      return <span styleName="hide-show" />;
    }
    return (<span styleName="hide-show" onClick={this.loadTransactionNetting(transaction)}>{transaction.opened ? '-' : '+'}</span>);
  }

  renderNoData() {
    return (
      <div styleName="transactions">
        <div styleName="transactions-dummy">
          <div
            className="icon-icon-transactions"
            styleName="transactions-dummy-icon"
          />
          <div styleName="transactions-dummy-text">
            No transactions for selected period
          </div>
        </div>
      </div>
    );
  }

  renderTransactions(transactions) {
    return (
      <ImmutableListView
        immutableData={transactions.groupBy((t) => t.valueDate)}
        renderSection={(key, items) => {
          const totalAmount = {};
          items.forEach((t) => totalAmount[t.currency] ? totalAmount[t.currency] += t.amount : totalAmount[t.currency] = t.amount);
          return (<div styleName="transactions-date-row">
            <div>{key}</div>
            <div styleName="daily-total-amounts">
              {Object.keys(totalAmount).map((currency) => (
                <span key={currency} styleName="daily-total-amount">
                  {`${totalAmount[currency] < 0 ? '-' : ''} ${CURRENCIES[currency]}`}<FormattedNumber value={Math.abs(totalAmount[currency])} />
                </span>
              ))}
            </div>
          </div>);
        }}
        renderRow={(item) => (
          <div key={item.id}>
            <div styleName="transaction-row">
              <div styleName="small-cell">
                {!item.nettingsEmpty && this.renderToggle(item)}
                {this.renderTransactionType(item)}
              </div>
              <div styleName="big-cell">
                {item.comment}
              </div>
              <div styleName="small-cell text-right">
                {item.amount && <React.Fragment>
                  {`${item.amount < 0 ? '-' : ''} ${CURRENCIES[item.currency]}`}<FormattedNumber value={Math.abs(item.amount)} />
                </React.Fragment>}
              </div>
            </div>
            {item.nettings.size > 0 && item.opened
            && <TransactionsNettings key={item.id} nettings={item.nettings} currency={item.currency} />}
          </div>
        )}
      />
    );
  }

  render() {
    const { transactions } = this.props;
    return transactions.size > 0 ? this.renderTransactions(transactions)
      : this.renderNoData();
  }
}

TransactionsList.propTypes = {
  transactions: PropTypes.object,
  loadTransactionNetting: PropTypes.func,
  showTransactionDetails: PropTypes.func,
};
