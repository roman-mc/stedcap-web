import React from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import { createStructuredSelector } from 'reselect';

import WithInjection from 'unity-frontend-core/containers/WithInjection';
import { WindowHeader, Window, WindowHeaderTitle } from 'Components/Window';
import { makeSelectPaymentMethodSchema } from 'Containers/DictionariesResolver/selectors';

import AddPaymentMethodForm from './Payments/AddPaymentMethodForm';
import PaymentMethods from './Payments/PaymentMethods';
import FundsWithdrawal from './Withdrawal/FundsWithdrawal';

import {} from './styles.css';
import { makeSelectIsFormVisible } from './selectors';
import reducer from './reducer';
import sagas from './sagas';

class PaymentsPage extends React.PureComponent {
  render() {
    const { paymentMethodSchema, isFormVisible } = this.props;

    if (!paymentMethodSchema) {
      return null;
    }
    return (
      <div styleName="panels">
        <div styleName="panel">
          <Window fullWidth fullHeight>
            <WindowHeader renderTitle={() => <WindowHeaderTitle><i className="icon-icon-payment-methods" styleName="withdrawal-title"></i><b>Payment methods</b></WindowHeaderTitle>} />
            {!isFormVisible ? <PaymentMethods /> : <AddPaymentMethodForm />}
          </Window>
        </div>
        <div styleName="panel">
          <Window fullWidth fullHeight>
            <WindowHeader renderTitle={() => <WindowHeaderTitle><i className="icon-icon-withdraw" styleName="withdrawal-title"></i><b>Funds withdrawal</b></WindowHeaderTitle>} />
            <FundsWithdrawal />
          </Window>
        </div>
      </div>
    );
  }
}

PaymentsPage.propTypes = {
  paymentMethodSchema: PropTypes.any,
  isFormVisible: PropTypes.bool,
};

const mapStateToProps = createStructuredSelector({
  paymentMethodSchema: makeSelectPaymentMethodSchema(),
  isFormVisible: makeSelectIsFormVisible(),
});

export default WithInjection({ reducer, key: 'financesPage', sagas })(connect(mapStateToProps)(PaymentsPage));
