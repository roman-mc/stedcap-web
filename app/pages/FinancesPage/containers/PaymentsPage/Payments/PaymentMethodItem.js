/* eslint-disable jsx-a11y/no-static-element-interactions */
import { connect } from 'react-redux';
import React from 'react';
import PropTypes from 'prop-types';
import { createStructuredSelector } from 'reselect';

import { } from './style.css';
import { PAYMENT_METHOD_STATUS } from '../../../../../model/enums';

const rowsEnum = {
  correspondentBankName: 'Correspondent bank',
  correspondentBankSwift: 'SWIFT',
  correspondentBankAccount: 'Account',
  beneficiaryBankName: 'Beneficiary',
  beneficiaryBankSwift: 'SWIFT',
  beneficiaryBankBeneficiary: 'Beneficiary bank',
  beneficiaryBankAccountNumber: 'Account number',
  beneficiaryBankIbanCode: 'IBAN code',
  rusBankBik: 'Bik',
  rusBankBeneficiaryBank: 'Beneficiary bank',
  rusBankCorrBankAccount: 'Corr. bank account',
  rusBankBankAddress: 'Bank address',
  rusBankBeneficiaryName: 'Beneficiary name',
  rusBankBeneficiaryAddress: 'Beneficiary address',
  rusBankBeneficiaryAccountNo: 'Beneficiary account no',
  rusBankBeneficiaryTaxCode: 'Beneficiary tax code (inn)',
  rusBankKppCode: 'Kpp code',
  rusBankVoCode: 'Vo code',
  paymentDetails: 'Payment details',
};

export class PaymentMethodItem extends React.PureComponent {
  state = {
    expanded: false,
  }

  renderRow = (rowName, title) => {
    const { item } = this.props;
    if (item.getIn(['content', rowName])) {
      return (
        <div key={rowName} styleName="method-content-row">
          <div styleName="method-content-row-title">
            {title}
          </div>
          <div styleName="method-content-row-text">
            {item.getIn(['content', rowName])}
          </div>
        </div>
      );
    }
    return null;
  }

  render() {
    const { expanded } = this.state;
    const { item } = this.props;
    return (
      <div styleName="method-item">
        <div styleName="method-title" onClick={() => this.setState({ expanded: !expanded })}>
          <div styleName="method-title-cur">
            {item.getIn(['content', 'paymentType'])}
          </div>
          <div styleName="method-title-text">
            {item.getIn(['content', 'beneficiaryBankName']) || item.getIn(['content', 'rusBankBeneficiaryName'])}
          </div>
          <div styleName="method-title-status">
            {(item.get('status') === PAYMENT_METHOD_STATUS.new || item.get('status') === PAYMENT_METHOD_STATUS.verificationInProgress)
              && <div
                className="icon-icon-in-verification"
                styleName="method-title-status-icon pending"
              />
            }
            {item.get('status') === PAYMENT_METHOD_STATUS.accepted
              && <div
                className="icon-icon-verified"
                styleName="method-title-status-icon accepted"
              />
            }
            {item.get('status') === PAYMENT_METHOD_STATUS.rejected
              && <div
                className="icon-icon-not-verified"
                styleName="method-title-status-icon rejected"
              />
            }
            <div
              className="icon-icon-dropdown-arrow"
              styleName={`method-title-status-arrow ${expanded ? 'method-title-status-arrow-up' : ''}`}
            />
          </div>
        </div>
        {expanded
          && <div styleName="method-main">
            {/*
                        <div styleName="method-message">
                            We couldn't verify these details because they
                            dont belong to an existing banking account
                        </div>
                        */}
            <div styleName="method-c-wrapper">
              <div styleName="method-content">
                {Object.keys(rowsEnum).map((rowName) => this.renderRow(rowName, rowsEnum[rowName]))}
              </div>
              {/*
                            <div styleName="method-delete">
                                <div
                                    className="icon-icon-remove"
                                    styleName="method-delete-icon"
                                />
                                Delete
                            </div>
                            */}
            </div>
          </div>
        }
      </div>
    );
  }
}

PaymentMethodItem.propTypes = {
  item: PropTypes.object.isRequired,
};

const mapStateToProps = createStructuredSelector({
});

function mapDispatchToProps() {
  return {
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(PaymentMethodItem);
