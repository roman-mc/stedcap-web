import { connect } from 'react-redux';
import React from 'react';
import PropTypes from 'prop-types';
import { createStructuredSelector } from 'reselect';
import { reduxForm, Form } from 'redux-form/immutable';
import { FormattedMessage, injectIntl, intlShape } from 'react-intl';

import Scrollbar from 'Components/Scrollbar';
import { BtnGroup, InlineInput, Button, Checkbox, InlineFileInput } from 'Components/ReduxFields';
import { makeSelectPaymentMethodSchema } from 'Containers/DictionariesResolver/selectors';

import { formValidator } from 'Utils/jsonSchemaUtils';

import { putPaymentMethod, hideForm } from '../actions';


import { makeSelectPaymentMethodFormData } from '../selectors';

import messages from '../../../messages';
import styles from './style.css';


const availableCurrencies = ['USD', 'EUR', 'RUB'];

export class AddPaymentMethodForm extends React.PureComponent {
  submit = (values) => {
    this.props.putPaymentMethod(values);
  }

  renderCommonBank = () => {
    const { intl } = this.props;
    return (
      <div>
        <div styleName="form-row">
          <InlineInput
            name="correspondentBankName"
            placeholder={intl.formatMessage(messages.correspondentBank)}
            text={<FormattedMessage {...messages.correspondentBank} />}
          />
        </div>
        <div styleName="form-row">
          <InlineInput
            name="correspondentBankSwift"
            placeholder={intl.formatMessage(messages.swift)}
            text={<FormattedMessage {...messages.swift} />}
          />
        </div>
        <div styleName="form-row">
          <InlineInput
            name="correspondentBankAccount"
            placeholder={intl.formatMessage(messages.account)}
            text={<FormattedMessage {...messages.account} />}
          />
        </div>
        <div styleName="form-row">
          <InlineInput
            name="beneficiaryBankBeneficiary"
            placeholder={intl.formatMessage(messages.beneficiaryBank)}
            text={<FormattedMessage {...messages.beneficiaryBank} />}
            required
          />
        </div>
        <div styleName="form-row">
          <InlineInput
            name="beneficiaryBankSwift"
            placeholder={intl.formatMessage(messages.swift)}
            text={<FormattedMessage {...messages.swift} />}
            required
          />
        </div>
        <div styleName="form-row">
          <InlineInput
            name="beneficiaryBankName"
            placeholder={intl.formatMessage(messages.beneficiary)}
            text={<FormattedMessage {...messages.beneficiary} />}
            required
          />
        </div>
        <div styleName="form-row">
          <InlineInput
            name="beneficiaryBankAccountNumber"
            placeholder={intl.formatMessage(messages.accountNumber)}
            text={<FormattedMessage {...messages.accountNumber} />}
            required
          />
        </div>
        <div styleName="form-row">
          <InlineInput
            name="beneficiaryBankIbanCode"
            placeholder={intl.formatMessage(messages.iban)}
            text={<FormattedMessage {...messages.iban} />}
            required
          />
        </div>
      </div>
    );
  }

  renderRusBank = () => (
    <div>
      <div styleName="form-row">
        <InlineInput
          name="rusBankBik"
          placeholder="BIK"
          text="BIK"
          required
        />
      </div>
      <div styleName="form-row">
        <InlineInput
          name="rusBankBeneficiaryBank"
          placeholder="Beneficiary bank"
          text="Beneficiary bank"
          required
        />
      </div>
      <div styleName="form-row">
        <InlineInput
          name="rusBankCorrBankAccount"
          placeholder="Corr. bank account"
          text="Corr. bank account"
        />
      </div>
      <div styleName="form-row">
        <InlineInput
          name="rusBankBankAddress"
          placeholder="Bank address"
          text="Bank address"
        />
      </div>
      <div styleName="form-row">
        <InlineInput
          name="rusBankBeneficiaryName"
          placeholder="Beneficiary name"
          text="Beneficiary name"
          required
        />
      </div>
      <div styleName="form-row">
        <InlineInput
          name="rusBankBeneficiaryAddress"
          placeholder="Beneficiary address"
          text="Beneficiary address"
          required
        />
      </div>
      <div styleName="form-row">
        <InlineInput
          name="rusBankBeneficiaryAccountNo"
          placeholder="Beneficiary account no"
          text="Beneficiary account no"
          required
        />
      </div>
      <div styleName="form-row">
        <InlineInput
          name="rusBankBeneficiaryTaxCode"
          placeholder="Beneficiary tax code (INN)"
          text="Beneficiary tax code (INN)"
          required
        />
      </div>
      <div styleName="form-row">
        <InlineInput
          name="rusBankKppCode"
          placeholder="KPP code"
          text="KPP code"
        />
      </div>
      <div styleName="form-row">
        <InlineInput
          name="rusBankVoCode"
          placeholder="VO code"
          text="VO code"
        />
      </div>
    </div>
  );


  render() {
    const { intl, handleSubmit, formValues } = this.props;

    return (
      <Form styleName="panel-content" onSubmit={handleSubmit(this.submit)}>
        <div styleName="content-title">Adding payment method</div>
        <Scrollbar style={{ position: 'static' }}>
          <div styleName="form">
            <div styleName="form-row">
              <BtnGroup
                name="paymentType"
                options={availableCurrencies.map((cur) => ({ label: cur, value: cur }))}
                text={<FormattedMessage {...messages.currency} />}
                required
              />
            </div>
            {(formValues && formValues.get('paymentType') === 'RUB')
              && <div styleName="form-row">
                <div styleName="form-checkbox-pad" />
                <div styleName="form-checkbox-wrapper">
                  <Checkbox
                    name="isRussianBank"
                    text="Bank is based in Russian Federation"
                  />
                </div>
              </div>
            }
            {(formValues && formValues.get('paymentType') === 'RUB' && formValues.get('isRussianBank'))
              ? this.renderRusBank()
              : this.renderCommonBank()
            }
            <div styleName="form-row">
              <InlineInput
                name="paymentDetails"
                placeholder={intl.formatMessage(messages.paymentDetails)}
                text={<FormattedMessage {...messages.paymentDetails} />}
                required
              />
            </div>
            <div styleName="form-row">
              <InlineFileInput
                name="documents"
                placeholder="Select or drop files"
                text="Document"
                subline="Upload document to prove your affiliation with the provided banking account"
                maxFiles={10}
                required
              />
            </div>
          </div>
        </Scrollbar>
        <div styleName="form-footer">
          <Button big onClick={this.props.hideForm} subClass={styles['btn-cancel']} styleType="secondary">Cancel</Button>
          <Button big type="submit" subClass={styles['btn-ok']}>Add</Button>
        </div>
      </Form>
    );
  }
}

AddPaymentMethodForm.propTypes = {
  putPaymentMethod: PropTypes.func.isRequired,
  handleSubmit: PropTypes.func.isRequired,
  intl: intlShape,
  formValues: PropTypes.object,
  hideForm: PropTypes.func.isRequired,
};

const mapStateToProps = createStructuredSelector({
  schema: makeSelectPaymentMethodSchema(),
  formValues: makeSelectPaymentMethodFormData(),
});

function mapDispatchToProps(dispatch) {
  return {
    putPaymentMethod: (values) => dispatch(putPaymentMethod(values)),
    hideForm: () => dispatch(hideForm()),
  };
}

const form = reduxForm({
  form: 'addPaymentMethodForm',
  validate: formValidator,
  initialValues: {
    paymentType: 'USD',
    documents: [],
  },
})(injectIntl(AddPaymentMethodForm));

export default connect(mapStateToProps, mapDispatchToProps)(form);
