/* eslint-disable jsx-a11y/no-static-element-interactions */
import { connect } from 'react-redux';
import React from 'react';
import PropTypes from 'prop-types';
import { createStructuredSelector } from 'reselect';

import Scrollbar from 'Components/Scrollbar';
import { Button } from 'Components/ReduxFields';

import styles from './style.css';
import { getPaymentMethods, showForm } from '../actions';
import { makeSelectPaymentMethods } from '../selectors';
import PaymentMethodItem from './PaymentMethodItem';

export class PaymentMethods extends React.PureComponent {
  componentDidMount() {
    this.props.getPaymentMethods();
  }

  renderData() {
    const { paymentMethods } = this.props;
    return (
      <div styleName="panel-content">
        <div styleName="content-title">Banking accounts</div>
        <Scrollbar style={{ position: 'static' }}>
          <div styleName="methods-list">
            {paymentMethods.map((method, i) => <PaymentMethodItem key={`pmethod_${i + 1}`} item={method} />)}
          </div>
        </Scrollbar>
        <div styleName="methods-footer">
          <Button big subClass={styles['btn-ok']} onClick={this.props.showForm}>
            Add payment method
          </Button>
        </div>
      </div>
    );
  }

  renderNoData() {
    return (
      <div styleName="panel-content">
        <div styleName="transactions">
          <div styleName="transactions-dummy">
            <div
              className="icon-icon-payment-methods"
              styleName="transactions-dummy-icon"
            />
            <div styleName="transactions-dummy-text">
                                Your banking accounts will be listed here
            </div>
          </div>
        </div>
        <div styleName="methods-footer">
          <Button big subClass={styles['btn-ok']} onClick={this.props.showForm}>
                            Add payment method
          </Button>
        </div>
      </div>
    );
  }

  render() {
    const { paymentMethods } = this.props;
    return paymentMethods.size ? this.renderData() : this.renderNoData();
  }
}

PaymentMethods.propTypes = {
  showForm: PropTypes.func.isRequired,
  getPaymentMethods: PropTypes.func.isRequired,
  paymentMethods: PropTypes.object.isRequired,
};

const mapStateToProps = createStructuredSelector({
  paymentMethods: makeSelectPaymentMethods(),
});

function mapDispatchToProps(dispatch) {
  return {
    getPaymentMethods: () => dispatch(getPaymentMethods()),
    showForm: () => dispatch(showForm()),
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(PaymentMethods);
