/*
 *
 * Finances reducer
 *
 */

import { fromJS, List } from 'immutable';
import { createReducer } from 'redux-act';
import {
  getPaymentMethods,
  showForm,
  hideForm,
} from './actions';

const initialState = fromJS({
  loading: false,
  isFormVisible: false,
  paymentMethods: new List(),
});

export default createReducer({
  [showForm]: (state) => state.set('isFormVisible', true),
  [hideForm]: (state) => state.set('isFormVisible', false),
  [getPaymentMethods]: (state) => state.set('loading', true),
  [getPaymentMethods.error]: (state) => state.set('loading', false),
  [getPaymentMethods.success]: (state, payload) =>
    state
      .set('paymentMethods', fromJS(payload)),
}, initialState);
