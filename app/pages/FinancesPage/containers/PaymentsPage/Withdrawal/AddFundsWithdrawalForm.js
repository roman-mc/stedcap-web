import { connect } from 'react-redux';
import React from 'react';
import PropTypes from 'prop-types';
import { createStructuredSelector } from 'reselect';
import { reduxForm, Form } from 'redux-form/immutable';
import { FormattedMessage, injectIntl, intlShape, FormattedNumber } from 'react-intl';

import { makeSelectActiveAccountInfo } from 'unity-frontend-core/containers/SelectAccount/selectors';

import { formatNumeral, parseNumeral } from 'Utils/form/normalization';
import { AVAILABLE_CURRENCIES, PAYMENT_METHOD_STATUS } from 'unity-frontend-core/Models/Enums';
import { BtnGroup, InlineInput, Button, InlineSelect, InlineTextarea } from 'Components/ReduxFields';

import { makeSelectPaymentMethods, makeSelectFundsWithdrawalFormData } from '../selectors';


import messages from '../../../messages';
import styles from './styles.css';


export class AddFundsWithdrawalForm extends React.PureComponent {
  submit = (values) => {
    const data = values.toJS();
    this.props.createFundsWithdrawal({ withdrawal: { ...data, amount: parseFloat(parseNumeral(data.amount)) } });
  }

  validateForm = () => {
    const { formValues } = this.props;
    if (formValues && parseInt(formValues.get('amount'), 0) > 0 && formValues.get('paymentMethodId') > 0) {
      return true;
    }
    return false;
  }

  checkReset = () => {
    const { formValues } = this.props;
    if (formValues && (parseInt(formValues.get('amount'), 0) > 0 || formValues.get('paymentMethodId') > 0 || formValues.get('description') !== '')) {
      return true;
    }
    return false;
  }

  render() {
    const { intl, handleSubmit, paymentMethods, activeAccount } = this.props;
    return (
      <Form onSubmit={handleSubmit(this.submit)}>
        <div styleName="form">
          <p styleName="text-center">
            Your funds will be withdrawn from {activeAccount.accountAccessName || <FormattedMessage {...messages.accountFull} values={{ id: activeAccount.multiAccountId }} />}.
            Available amount {activeAccount.currency} <FormattedNumber value={activeAccount.assetValue} />
          </p>
          <div styleName="form-row">
            <div styleName="half-width">
              <div styleName="withdrawal-amount-input">
                <InlineInput
                  name="amount"
                  placeholder={intl.formatMessage(messages.fundsWithdrawalAnmount)}
                  text={<FormattedMessage {...messages.fundsWithdrawalAnmountLabel} />}
                  labelClassName={styles['withdrawal-form-label']}
                  format={formatNumeral}
                />
              </div>
              <div styleName="btn-group">
                <BtnGroup
                  name="currency"
                  options={AVAILABLE_CURRENCIES.map((cur) => ({ label: cur, value: cur }))}
                  hideLabel
                />
              </div>
            </div>
            <div styleName="half-width">
              <InlineSelect
                disableClearable
                name="paymentMethodId"
                placeholder={<span styleName="placeholder">
                  <FormattedMessage {...messages.fundsWithdrawalPaymentMethodPlaceholder} />
                </span>}
                text={<FormattedMessage {...messages.fundsWithdrawalPaymentMethod} />}
                options={paymentMethods.filter((p) => (p.get('status') === PAYMENT_METHOD_STATUS.ACCEPTED)).map((item) => ({ value: item.get('id'), currency: item.getIn(['content', 'paymentType']), label: `${item.getIn(['content', 'beneficiaryBankName']) || item.getIn(['content', 'rusBankBeneficiaryName'])}` })).toArray()}
                optionRenderer={(option) => (<span>
                  {option.label} <span styleName="currency">
                    {option.currency}
                  </span>
                </span>)}
                valueRenderer={(option) => (<span>
                  {option.label} <span styleName="currency">
                    {option.currency}
                  </span>
                </span>)}
                labelClassName={styles['withdrawal-form-label']}
              />
            </div>
          </div>
          <div styleName="form-row">
            <InlineTextarea
              name="description"
              placeholder={intl.formatMessage(messages.fundsWithdrawalCommentPlaceholder)}
              text={<FormattedMessage {...messages.fundsWithdrawalComment} />}
              labelClassName={styles['withdrawal-form-label']}
              controlClassName={styles['withdrawal-textarea']}
            />
          </div>
        </div>
        <div styleName="form-footer">
          <Button onClick={this.props.reset} styleType="secondary" subClass={styles['btn-cancel']} disabled={!this.checkReset()}>
            Reset
          </Button>
          <Button type="submit" subClass={styles['btn-ok']} disabled={!this.validateForm()}>
            Request withdrawal
          </Button>
        </div>
      </Form>
    );
  }
}

AddFundsWithdrawalForm.propTypes = {
  createFundsWithdrawal: PropTypes.func.isRequired,
  handleSubmit: PropTypes.func.isRequired,
  reset: PropTypes.func.isRequired,
  paymentMethods: PropTypes.object.isRequired,
  activeAccount: PropTypes.object.isRequired,
  formValues: PropTypes.object,
  intl: intlShape,
};

const mapStateToProps = createStructuredSelector({
  paymentMethods: makeSelectPaymentMethods(),
  formValues: makeSelectFundsWithdrawalFormData(),
  activeAccount: makeSelectActiveAccountInfo(),
});

const form = reduxForm({
  form: 'addFundsWithdrawalForm',
  initialValues: {
    currency: 'USD',
    description: '',
  },
})(injectIntl(AddFundsWithdrawalForm));

export default connect(mapStateToProps)(form);
