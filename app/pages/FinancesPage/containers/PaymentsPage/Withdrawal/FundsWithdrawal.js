/* eslint-disable jsx-a11y/no-static-element-interactions */
import { connect } from 'react-redux';
import React from 'react';
import PropTypes from 'prop-types';
import moment from 'moment/moment';
import { FormattedNumber } from 'react-intl';
import { createStructuredSelector } from 'reselect';

import { FUNDS_WITHDRAWAL_STATUS } from 'unity-frontend-core/Models/Enums';
import FundsWithdrawal from 'unity-frontend-core/containers/FundsWithdrawal/index';
import { STANDARD_DATE_TIME } from 'unity-frontend-core/utils/dateUtils';
import { makeSelectActiveAccountInfo } from 'unity-frontend-core/containers/SelectAccount/selectors';

import Scrollbar from 'Components/Scrollbar';

import AddFundsWithdrawalForm from './AddFundsWithdrawalForm';
import styles from './styles.css';
import { makeSelectPaymentMethods } from '../selectors';

export class FundsWithdrawalContainer extends React.PureComponent {
  componentDidMount() {
    this.props.loadFundsWithdrawal();
  }

  cancel = (id) => () => {
    this.props.cancelFundsWithdrawal(id);
  }

  renderWithdrawals(fundsWithdrawalList, paymentMethods, canCreateUpdate = false) {
    return (
      <Scrollbar style={{ position: 'static' }}>
        <table styleName="withdrawal">
          <tbody>
            {fundsWithdrawalList.map((withdrawal) => {
              const status = FUNDS_WITHDRAWAL_STATUS.getById(withdrawal.status);
              const pMethod = paymentMethods.find((p) => (p.get('id') === withdrawal.paymentMethodId));
              return (
                <tr key={withdrawal.get('id')} styleName="withdrawal-table-row">
                  <td styleName="withdrawal-table-cell fix-width-sm">
                    {moment(withdrawal.creationTime).format(STANDARD_DATE_TIME)}
                  </td>
                  <td styleName="withdrawal-table-cell fix-width-lg">
                    <FormattedNumber value={withdrawal.amount} /> {withdrawal.currency}
                    <br />
                    {pMethod && (pMethod.getIn(['content', 'beneficiaryBankName']) || pMethod.getIn(['content', 'rusBankBeneficiaryName']))}
                  </td>
                  <td styleName="withdrawal-table-cell fix-width-lg">
                    {status.value} {status.id !== FUNDS_WITHDRAWAL_STATUS.NEW && moment(withdrawal.canceledTime || withdrawal.confirmationTime).format(STANDARD_DATE_TIME)}
                    {canCreateUpdate && (status.id === FUNDS_WITHDRAWAL_STATUS.NEW) && <button onClick={this.cancel(withdrawal.id)} styleName="cancel-request">
                      <i className="icon-icon-remove"></i>
                      Cancel request
                    </button>}
                    {withdrawal.comment && <p>
                      {withdrawal.comment}
                    </p>}
                  </td>
                  <td styleName="withdrawal-table-cell icon-cell">
                    {(status.id === FUNDS_WITHDRAWAL_STATUS.NEW)
                    && <div
                      className="icon-icon-in-verification"
                      styleName="method-title-status-icon pending icon"
                    />
                    }
                    {status.id === FUNDS_WITHDRAWAL_STATUS.DONE
                    && <div
                      className="icon-icon-verified"
                      styleName="method-title-status-icon accepted icon"
                    />
                    }
                    {(status.id === FUNDS_WITHDRAWAL_STATUS.CANCELLED || status.id === FUNDS_WITHDRAWAL_STATUS.DECLINED)
                    && <div
                      className="icon-icon-not-verified"
                      styleName="method-title-status-icon rejected icon"
                    />
                    }
                  </td>
                </tr>
              );
            })}
          </tbody>
        </table>
      </Scrollbar>
    );
  }

  render() {
    const { fundsWithdrawalList, paymentMethods, currentAccountInfo } = this.props;
    const canCreateUpdate = currentAccountInfo && currentAccountInfo.permissions.includes('WITHDRAWAL_REQUEST');
    return <div className={`${styles['funds-withdrawal__root']}`} styleName="panel-content">
      {canCreateUpdate && <div styleName="withdrawal-add">
        <AddFundsWithdrawalForm createFundsWithdrawal={this.props.createFundsWithdrawal} />
      </div>}
      {this.renderWithdrawals(fundsWithdrawalList, paymentMethods, canCreateUpdate)}
    </div>;
  }
}

FundsWithdrawalContainer.propTypes = {
  loadFundsWithdrawal: PropTypes.func.isRequired,
  createFundsWithdrawal: PropTypes.func.isRequired,
  cancelFundsWithdrawal: PropTypes.func.isRequired,
  fundsWithdrawalList: PropTypes.any.isRequired,
  paymentMethods: PropTypes.any,
  currentAccountInfo: PropTypes.object,
};
const mapStateToProps = createStructuredSelector({
  paymentMethods: makeSelectPaymentMethods(),
  currentAccountInfo: makeSelectActiveAccountInfo(),
});

export default connect(mapStateToProps)(FundsWithdrawal(FundsWithdrawalContainer));
