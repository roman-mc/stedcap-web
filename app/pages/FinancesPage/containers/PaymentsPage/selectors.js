import { createSelector } from 'reselect';
import { makeSelectForms } from 'Containers/App/selectors';

/**
 * Direct selector to the accounts state domain
 */
const selectFinancesDomain = () => (state) => state.get('financesPage');

/**
 * Other specific selectors
 */

/**
 * Default selector used by Accounts
 */


export const makeSelectPaymentMethodFormData = () => createSelector(
  makeSelectForms,
  (substate) => substate.getIn(['addPaymentMethodForm', 'values'])
);


export const makeSelectFundsWithdrawalFormData = () => createSelector(
  makeSelectForms,
  (substate) => substate.getIn(['addFundsWithdrawalForm', 'values'])
);

export const makeSelectPaymentMethods = () => createSelector(
  selectFinancesDomain(),
  (substate) => substate.get('paymentMethods')
);

export const makeSelectIsFormVisible = () => createSelector(
  selectFinancesDomain(),
  (substate) => substate.get('isFormVisible')
);

export { selectFinancesDomain };
