/*
 *
 * Finances actions
 *
 */
import { createAction } from 'redux-act';
import createApiAction from 'unity-frontend-core/utils/createAction';

export const getPaymentMethods = createApiAction('app/containers/FinancesPage/GET_PAYMENT_METHODS');
export const putPaymentMethod = createApiAction('app/containers/FinancesPage/PUT_PAYMENT_METHOD');
export const showForm = createAction('app/containers/FinancesPage/SHOW_FORM');
export const hideForm = createAction('app/containers/FinancesPage/HIDE_FORM');
