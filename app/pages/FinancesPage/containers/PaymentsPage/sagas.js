// import { LOCATION_CHANGE } from 'react-router-redux';
import { call, put, fork, takeLatest } from 'redux-saga/effects';
import { reset } from 'redux-form';
import Raven from 'raven-js';
import * as paymentMethodsApi from 'unity-frontend-core/api/paymentMethodsApi';
import { generateListener } from 'unity-frontend-core/utils/sagaUtils';
import { simpleApiSaga } from 'Utils/sagaUtils';
import { transformPaymentMethodData } from 'Utils/transformers';
import { loadFundsWithdrawal, createFundsWithdrawal } from 'unity-frontend-core/containers/FundsWithdrawal/actions';
import { selectAccount } from 'unity-frontend-core/containers/SelectAccount/actions';

import {
  getPaymentMethods,
  putPaymentMethod,
  hideForm,
} from './actions';

export function* putPaymentMethodHandler({ payload }) {
  try {
    yield call(paymentMethodsApi.putPaymentMethod, transformPaymentMethodData(payload.toJS()));
    yield put(putPaymentMethod.success());
    yield put(hideForm());
  } catch (e) {
    yield put(putPaymentMethod.error(e));
    Raven.captureException(e);
  }
}

export function* putPaymentMethodSaga() {
  return yield takeLatest(putPaymentMethod.toString(), putPaymentMethodHandler);
}

export function* updateAccountHandler() {
  try {
    yield put(loadFundsWithdrawal());
  } catch (e) {
    console.warn(e);
  }
}

export function* createWithdrawalRequestHandler() {
  try {
    yield put(reset('addFundsWithdrawalForm'));
  } catch (e) {
    console.warn(e);
  }
}

/**
 * Root saga manages watcher lifecycle
 */
export function* runAllSagas() {
  yield [
    simpleApiSaga(getPaymentMethods, paymentMethodsApi.getPaymentMethods),
    putPaymentMethodSaga,
    generateListener(selectAccount.success, updateAccountHandler),
    generateListener(createFundsWithdrawal.success, createWithdrawalRequestHandler),
  ].map(fork);

  // yield take(({ type, payload }) => type === LOCATION_CHANGE && payload.action !== 'REPLACE');
  // yield watchers.map((w) => w.cancel());
}


export default [
  runAllSagas,
];
