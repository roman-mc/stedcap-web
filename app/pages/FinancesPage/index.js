import React from 'react';
import PropTypes from 'prop-types';
import { Route, Switch } from 'react-router';
import { Link } from 'react-router-dom';
import Transactions from 'unity-frontend-core/containers/Transactions';
import Helmet from 'Components/Helmet';

import messages from './messages';
import styles from './style.css';


import TransactionsPage from './containers/Transactions';
import PaymentsPage from './containers/PaymentsPage';

const FinancePage = ({ match: { url } }) => (
  <div className="page-container">
    <Helmet pageTitle={messages.pageTitle} pageDescription={messages.pageDescription} />
    <div styleName="wrapper">
      <div styleName="top-bar">
        <Link to="/finances" styleName="top-button" className={location.pathname === '/finances' ? styles.current : ''}>
          Funds
        </Link>
        <Link to="/finances/transactions" styleName="top-button" className={location.pathname === '/finances/transactions' ? styles.current : ''}>
          Transactions
        </Link>
      </div>
      <Switch>
        <Route exact path={url} component={PaymentsPage} />
        <Route exact path={`${url}/transactions`} component={Transactions(TransactionsPage)} />
      </Switch>
    </div>
  </div>
);

FinancePage.propTypes = {
  match: PropTypes.object,
};

export default FinancePage;
