/*
 * Accounts Messages
 *
 * This contains all the text for the Accounts component.
 */
import { defineMessages } from 'react-intl';

export default defineMessages({
  pageTitle: {
    id: 'app.containers.FinancesPage.pageTitle',
    defaultMessage: 'Finances',
  },
  pageDescription: {
    id: 'app.containers.FinancesPage.pageDescription',
    defaultMessage: 'Finances overview',
  },
  currency: {
    id: 'app.containers.FinancesPage.currency',
    defaultMessage: 'Currency',
  },
  correspondentBank: {
    id: 'app.containers.FinancesPage.correspondentBank',
    defaultMessage: 'Correspondent bank',
  },
  swift: {
    id: 'app.containers.FinancesPage.swift',
    defaultMessage: 'SWIFT',
  },
  beneficiary: {
    id: 'app.containers.FinancesPage.beneficiary',
    defaultMessage: 'Beneficiary',
  },
  beneficiaryBank: {
    id: 'app.containers.FinancesPage.beneficiaryBank',
    defaultMessage: 'Beneficiary bank',
  },
  accountNumber: {
    id: 'app.containers.FinancesPage.accountNumber',
    defaultMessage: 'Account number',
  },
  iban: {
    id: 'app.containers.FinancesPage.iban',
    defaultMessage: 'IBAN code',
  },
  paymentDetails: {
    id: 'app.containers.FinancesPage.paymentDetails',
    defaultMessage: 'Payment details',
  },
  account: {
    id: 'app.containers.FinancesPage.account',
    defaultMessage: 'Account',
  },
  fundsWithdrawalAnmount: {
    id: 'app.containers.FinancesPage.amount',
    defaultMessage: '0',
  },
  fundsWithdrawalAnmountLabel: {
    id: 'app.containers.FinancesPage.amountLabel',
    defaultMessage: 'Amount',
  },
  fundsWithdrawalCommentPlaceholder: {
    id: 'app.containers.FinancesPage.commentPlaceholder',
    defaultMessage: 'Write your comment here',
  },
  fundsWithdrawalComment: {
    id: 'app.containers.FinancesPage.comment',
    defaultMessage: 'Comment',
  },
  fundsWithdrawalPaymentMethod: {
    id: 'app.containers.FinancesPage.paymentMethod',
    defaultMessage: 'Withdraw to',
  },
  fundsWithdrawalPaymentMethodPlaceholder: {
    id: 'app.containers.FinancesPage.paymentMethodPlaceholder',
    defaultMessage: 'Choose from your payment methods',
  },
  accountFull: {
    id: 'app.containers.FinancesPage.modalForm.account',
    defaultMessage: 'Account #{id}',
  },
});
