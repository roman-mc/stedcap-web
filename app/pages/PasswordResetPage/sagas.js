import { takeEvery } from 'redux-saga';
import { call, put, fork, select } from 'redux-saga/effects';
import { push } from 'react-router-redux';
import Raven from 'raven-js';

import { requestPasswordReset, resetPassword } from 'unity-frontend-core/api/authApi';
import { temporaryTokenReceived } from '../../containers/App/actions';
import { makeSelectTemporaryToken } from '../../containers/App/selectors';

import {
  sendChangePasswordRequest,
  changePassword,
} from './actions';

export function* requestPasswordChangeHandler({ payload }) {
  try {
    const result = yield call(requestPasswordReset, payload);
    yield put(temporaryTokenReceived({ token: result.token }));
    yield put(sendChangePasswordRequest.success(result));
  } catch (e) {
    yield put(sendChangePasswordRequest.error(e));
    Raven.captureException(e);
  }
}

export function* requestPasswordChangeSaga() {
  return yield takeEvery(sendChangePasswordRequest.toString(), requestPasswordChangeHandler);
}


export function* resetPasswordHandler({ payload }) {
  const tempToken = yield select(makeSelectTemporaryToken());

  try {
    const result = yield call(resetPassword, payload, tempToken);
    yield put(changePassword.success(result));
    yield put(push('/login'));
  } catch (e) {
    yield put(changePassword.error(e));
    Raven.captureException(e);
  }
}

export function* resetPasswordSaga() {
  return yield takeEvery(changePassword.toString(), resetPasswordHandler);
}

/**
 * Root saga manages watcher lifecycle
 */
export function* runAllSagas() {
  yield [
    requestPasswordChangeSaga,
    resetPasswordSaga,
  ].map(fork);

  // yield take(LOCATION_CHANGE);
  // yield watchers.map((w) => w.cancel());
}

export default [
  runAllSagas,
];
