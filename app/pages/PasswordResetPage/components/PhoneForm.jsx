import React from 'react';
import PropTypes from 'prop-types';

import { injectIntl, intlShape, FormattedMessage } from 'react-intl';
import { reduxForm } from 'redux-form/immutable';

import { Button, FormGroup, PhoneInput, Input, Password, Text, FooterLink } from 'Components/AuthForm';
import { phoneValidate, passwordValidate, requireValidate } from 'Utils/form/validation';

import messages from '../messages';

function PhoneForm({ invalid, onCodeSend, requestSent, handleSubmit, loading, intl: { formatMessage } }) {
  return (
    <form onSubmit={handleSubmit} autoComplete="off">
      <FormGroup>
        <PhoneInput
          id="phone"
          name="phone"
          type="text"
          placeholder={formatMessage(messages.phoneNumber)}
          validate={[phoneValidate]}
        />
      </FormGroup>
      <Text text={formatMessage(messages.enterCodeText)} />
      <FormGroup>
        <Button
          disabled={invalid || requestSent || loading}
          basis={50}
          type="button"
          className="send-code"
          text={formatMessage(messages.sendCode)}
          onClick={onCodeSend}
        />
        <Input
          id="reset_form_code"
          name="code"
          type="text"
          placeholder={formatMessage(messages.verificationCode)}
          validate={requestSent ? [requireValidate] : []}
          autoComplete="off"
        />
      </FormGroup>
      {requestSent && <FormGroup>
        <Password
          id="reset_form_password"
          name="password"
          placeholder={formatMessage(messages.password)}
          validate={requestSent ? [passwordValidate] : []}
          autoComplete="off"
        />
      </FormGroup>}
      <FormGroup>
        <Button
          type="submit"
          className="open-account"
          disabled={!requestSent || invalid || loading}
          text={formatMessage(messages.resetPassword)}
        />
      </FormGroup>
      <FormGroup>
        <FooterLink to="/login">
          <FormattedMessage {...messages.cancel} />
        </FooterLink>
      </FormGroup>
    </form>
  );
}


PhoneForm.propTypes = {
  loading: PropTypes.bool.isRequired,
  invalid: PropTypes.bool.isRequired,
  handleSubmit: PropTypes.func.isRequired,
  intl: intlShape,
  requestSent: PropTypes.bool.isRequired,
  onCodeSend: PropTypes.func.isRequired,
};


export default reduxForm({
  form: 'resetPasswordPhoneForm',
})(injectIntl(PhoneForm));
