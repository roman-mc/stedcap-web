import { createSelector } from 'reselect';
import { makeSelectForms } from 'Containers/App/selectors';

/**
 * Direct selector to the passwordResetPage state domain
 */
const selectPasswordResetPageDomain = () => (state) => state.get('passwordResetPage');

/**
 * Other specific selectors
 */


/**
 * Default selector used by PasswordResetPage
 */

export const makeSelectPasswordResetPage = () => createSelector(
  selectPasswordResetPageDomain(),
  (substate) => substate.toJS()
);


export const makeSelectResetPasswordForm = () => createSelector(
  makeSelectForms,
  (substate) => substate.getIn(['resetPasswordPhoneForm', 'values'])
);

export const makeSelectLoading = () => createSelector(
  selectPasswordResetPageDomain(),
  (substate) => substate.get('loading'),
);

export const maskeSelectRequestSent = () => createSelector(
  selectPasswordResetPageDomain(),
  (substate) => substate.get('requestSent'),
);

export default makeSelectPasswordResetPage;
