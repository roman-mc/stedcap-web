/*
 *
 * PasswordResetPage
 *
 */

import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import Helmet from 'react-helmet';
import ImmutableProptypes from 'react-immutable-proptypes';

import { createStructuredSelector } from 'reselect';

import { AuthForm } from 'Components/AuthForm';
import { parsePhone } from 'Utils/phoneUtils';

import WithInjection from 'unity-frontend-core/containers/WithInjection';


import {
  makeSelectResetPasswordForm,
  makeSelectLoading,
  maskeSelectRequestSent,
} from './selectors';
import messages from './messages';
import { changePassword, sendChangePasswordRequest, destroyPage } from './actions';

import PhoneForm from './components/PhoneForm';

import reducer from './reducer';
import sagas from './sagas';

export class PasswordResetPage extends React.PureComponent {
  componentWillUnmount() {
    this.props.destroyPage();
  }

  changePassword = (values) => {
    const { code, password } = values.toJS();
    this.props.changePassword({ code, password });
  }

  sendCode = () => {
    const phone = parsePhone(this.props.form.get('phone'));
    this.props.sendChangePasswordRequest({ phone });
  }

  render() {
    const { loading, requestSent } = this.props;
    return (
      <AuthForm subtitle={messages.resetPasswordSubtitle}>
        <Helmet
          title="Reset password"
          meta={[
            { name: 'description', content: 'Description of PasswordResetPage' },
          ]}
        />
        <PhoneForm
          loading={loading}
          requestSent={requestSent}
          onSubmit={this.changePassword}
          onCodeSend={this.sendCode}
        />
      </AuthForm>
    );
  }
}

PasswordResetPage.propTypes = {
  changePassword: PropTypes.func.isRequired,
  destroyPage: PropTypes.func.isRequired,
  sendChangePasswordRequest: PropTypes.func.isRequired,
  form: ImmutableProptypes.map,
  loading: PropTypes.bool.isRequired,
  requestSent: PropTypes.bool.isRequired,
};

const mapStateToProps = createStructuredSelector({
  form: makeSelectResetPasswordForm(),
  loading: makeSelectLoading(),
  requestSent: maskeSelectRequestSent(),
});

function mapDispatchToProps(dispatch) {
  return {
    changePassword: (payload) => dispatch(changePassword(payload)),
    destroyPage: () => dispatch(destroyPage()),
    sendChangePasswordRequest: (payload) => dispatch(sendChangePasswordRequest(payload)),
  };
}

const connected = connect(mapStateToProps, mapDispatchToProps)(PasswordResetPage);

export default WithInjection({ reducer, sagas, key: 'passwordResetPage' })(connected);
