/*
 *
 * PasswordResetPage reducer
 *
 */

import { Map } from 'immutable';
import { createReducer } from 'redux-act';
import {
  sendChangePasswordRequest,
  changePassword,
  destroyPage,
} from './actions';

const initialState = new Map({
  loading: false,
  requestSent: false,
});

export default createReducer({
  [sendChangePasswordRequest]: (state) => state.set('loading', true),
  [sendChangePasswordRequest.success]: (state) =>
    state
      .set('loading', false)
      .set('requestSent', true),
  [sendChangePasswordRequest.error]: (state) =>
    state
      .set('loading', false)
      .set('requestSent', false),
  [changePassword]: (state) =>
    state
      .set('loading', true),
  [changePassword.error]: (state) =>
    state
      .set('loading', false),
  [destroyPage]: () => initialState,
}, initialState);
