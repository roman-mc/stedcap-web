/*
 * PasswordResetPage Messages
 *
 * This contains all the text for the PasswordResetPage component.
 */
import { defineMessages } from 'react-intl';

export default defineMessages({
  phoneNumber: {
    id: 'app.containers.PasswordResetPage.phoneNumber',
    defaultMessage: 'Phone number',
  },
  password: {
    id: 'app.containers.PasswordResetPage.password',
    defaultMessage: 'New password',
  },
  resetButton: {
    id: 'app.containers.PasswordResetPage.resetButton',
    defaultMessage: 'Reset',
  },
  resetPasswordSubtitle: {
    id: 'app.containers.PasswordResetPage.resetPasswordSubtitle',
    defaultMessage: 'Reset password',
  },
  resetPassword: {
    id: 'app.containers.PasswordResetPage.resetPasswordSubtitle',
    defaultMessage: 'Reset password',
  },
  enterCodeText: {
    id: 'app.containers.PasswordResetPage.enterCodeText',
    defaultMessage: 'Enter code from SMS to confirm phone number',
  },
  sendCode: {
    id: 'app.containers.PasswordResetPage.sendCode',
    defaultMessage: 'Send code',
  },
  verificationCode: {
    id: 'app.containers.PasswordResetPage.verificationCode',
    defaultMessage: 'Verification code',
  },
  cancel: {
    id: 'app.containers.PasswordResetPage.cancel',
    defaultMessage: 'Cancel',
  },
});
