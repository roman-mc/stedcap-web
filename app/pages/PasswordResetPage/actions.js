/*
 *
 * PasswordResetPage actions
 *
 */
import { createAction } from 'redux-act';
import createApiAction from 'unity-frontend-core/utils/createAction';


export const sendChangePasswordRequest = createApiAction('app/containers/PasswordResetPage/SEND_CHANGE_PASSWORD_REQUEST');
export const changePassword = createApiAction('app/containers/PasswordResetPage/CHANGE_PASSWORD');
export const destroyPage = createAction('app/containers/PasswordResetPage/DESTROY_PAGE');
