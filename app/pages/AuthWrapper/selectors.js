
import { createSelector } from 'reselect';
import { API_LATENCY_THRESHOLD } from 'unity-frontend-core';

export const makeSelectAccountState = () => (state) => state.get('account');

export const selectAuthState = () => createSelector(
  makeSelectAccountState(),
  (substate) => substate.get('auth'),
);

export const makeSelectIsConnectionBad = () => createSelector(
  selectAuthState(),
  (account) => account.get('apiLatency') > API_LATENCY_THRESHOLD
);

export const makeSelectNetworkDisconnected = () => createSelector(
  selectAuthState(),
  (globalState) => globalState.get('networkDisconnected'),
);

export const makeSelectUserInfoFetched = () => createSelector(
  selectAuthState(),
  (globalState) => globalState.get('userInfoFetched'),
);
