/*
 *
 * AuthWrapper
 *
 */

import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { replace } from 'react-router-redux';
import IdleTimer from 'react-idle-timer';
import { Route, Switch } from 'react-router';
import WithInjection from 'unity-frontend-core/containers/WithInjection';
// import ImmutablePropTypes from 'react-immutable-proptypes';
import { createStructuredSelector } from 'reselect';
// import makeSelectAuthWrapper from './selectors';

import moment from 'moment';

import 'react-datepicker/dist/react-datepicker.css';


import DictionariesResolver from 'Containers/DictionariesResolver';
import WithSelectedAccount from 'Containers/WithSelectedAccount';
import LoadingIndicator from 'Components/LoadingIndicator';


import {
  makeSelectToken,
  makeSelectTheme,
  makeSelectLogoutTime,
  makeSelectCurrentUserStatus,
} from '../../containers/App/selectors';
import { startTokenRefresh } from './actions';
import { makeSelectUserInfoFetched } from './selectors';

import ProfileSettingsPopup from '../../containers/ProfileSettings';
import ConfirmationDialog from '../../containers/ConfirmationDialog';
import AuthMethodAccept from '../../containers/AuthMethodChange/AuthMethodAccept';

import sagas from './sagas';
import reducer from './reducer';

import Loadable from '../../components/Loadable';

import '../../utils/moment-en-gb';

moment.locale('en-gb');

/**
 * Authorization layer
 * Check access permissions, roles etc
 * For login/logout actions, token revalidation see `App`
 */

function AuthorizedPages({ location }) {
  return (
    <WithSelectedAccount location={location}>
      <DictionariesResolver location={location}>
        <Switch>
          <Route path="/trading" component={Loadable(() => import('../TradingPage'))} />
          <Route path="/finances" component={Loadable(() => import('../FinancesPage'))} />
          <Route path="/news" component={Loadable(() => import('../NewsPage'))} />
        </Switch>
      </DictionariesResolver>
    </WithSelectedAccount>
  );
}


AuthorizedPages.propTypes = {
  location: PropTypes.object.isRequired,
};

export class AuthWrapper extends React.Component { // eslint-disable-line react/prefer-stateless-function
  componentWillMount() {
    this.checkAccess(this.props.token, this.props.location.pathname, this.props.userStatus);
  }


  componentWillReceiveProps(nextProps) {
    if (
      nextProps.token !== this.props.token
      || nextProps.location.pathname !== this.props.location.pathname
      || nextProps.userStatus !== this.props.userStatus
    ) {
      this.checkAccess(nextProps.token, nextProps.location.pathname, nextProps.userStatus);
    }
  }

  onIdle = () => {
    const { goTo } = this.props;
    goTo('/logout');
  }

  checkAccess(token, pathname, userStatus) {
    const { goTo, userInfoFetched } = this.props;

    if (!token) {
      goTo('/login');
    } else {
      if (!userInfoFetched) {
        this.props.startTokenRefresh();
      }

      if (userStatus === 'New' && pathname !== '/verification') {
        goTo('/verification');
      } else if (pathname === '/') {
        goTo('/trading'); // Go to default page
      }
    }
  }

  render() {
    const {
      userInfoFetched, token, theme, logoutTime } = this.props;

    if (!userInfoFetched || !token) {
      return (
        <LoadingIndicator loading />
      );
    }


    return (
      <IdleTimer
        element={document}
        idleAction={this.onIdle}
        timeout={logoutTime}
        format="MM-DD-YYYY HH:MM:ss.SSS"
      >
        <div className={`height_100 _${theme}`}>
          <Switch>
            <Route path="/verification" component={Loadable(() => import('../VerificationPage'))} />
            <Route component={AuthorizedPages} />
          </Switch>
          <ProfileSettingsPopup />
          <AuthMethodAccept />
          <ConfirmationDialog />
          <div id="modal-windows-container"></div>
        </div>
      </IdleTimer>
    );
  }
}


AuthWrapper.propTypes = {
  goTo: PropTypes.func.isRequired,
  token: PropTypes.string,
  location: PropTypes.shape({
    pathname: PropTypes.string,
  }),
  startTokenRefresh: PropTypes.func.isRequired,
  userInfoFetched: PropTypes.bool.isRequired,
  userStatus: PropTypes.string,
  theme: PropTypes.string,
  logoutTime: PropTypes.number,
};


const mapStateToProps = createStructuredSelector({
  // AuthWrapper: makeSelectAuthWrapper(),
  token: makeSelectToken(),
  theme: makeSelectTheme(),
  logoutTime: makeSelectLogoutTime(),
  userStatus: makeSelectCurrentUserStatus(),
  userInfoFetched: makeSelectUserInfoFetched(),
});


function mapDispatchToProps(dispatch) {
  return {
    // dispatch,
    startTokenRefresh: () => dispatch(startTokenRefresh()),
    goTo: (url) => dispatch(replace(url)),
  };
}


const Connected = connect(mapStateToProps, mapDispatchToProps)(AuthWrapper);

export default WithInjection({ reducer, sagas, key: 'account' })(Connected);
