import { delay } from 'redux-saga';
import { call, put } from 'redux-saga/effects';
import { push } from 'react-router-redux';
import Raven from 'raven-js';

import { authError } from 'unity-frontend-core/actions';

import { generateListener } from 'Utils/sagaUtils';

import { refreshToken, getUserInfo } from 'unity-frontend-core/api/authApi';


import {
  userLoggedOut,
  userDataReceived,
  userLoggedIn,
} from '../../containers/App/actions';

import { removeToken } from '../../api/authorizationApi';
import { startTokenRefresh } from './actions';


export function* deauthorizationHandler() {
  yield call(removeToken);
}

export function* restrictedAccessHandler() {
  yield put(push('/logout'));
}

export function* refreshTokenHandler() {
  try {
    const user = yield call(getUserInfo);
    yield put(userDataReceived({ user }));
    while (true) { // eslint-disable-line no-constant-condition
      try {
        yield call(refreshToken);
      } catch (e) {
        yield put(push('/logout'));
      }
      yield call(delay, 60 * 60 * 1000);
    }
  } catch (e) {
    yield put(push('/logout'));
  }
}

function* userLoggedInHandler({ payload }) {
  Raven.setUserContext({
    email: payload.email,
  });
}


// All sagas to be loaded
export default [
  generateListener(authError, restrictedAccessHandler),
  generateListener(userLoggedOut, deauthorizationHandler),
  generateListener(startTokenRefresh, refreshTokenHandler),
  generateListener(userLoggedIn, userLoggedInHandler),
];
