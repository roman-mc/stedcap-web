/*
 *
 * AuthWrapper reducer
 *
 */
import { Map } from 'immutable';
import { combineReducers } from 'redux-immutable';
import { createReducer } from 'redux-act';

import { setApiLatency, noNetworkConnection, networkConnected } from 'unity-frontend-core/actions';


import { userLoggedOut, userDataReceived } from '../../containers/App/actions';


const initialState = new Map({
  apiLatency: 0,
  networkDisconnected: false,
  userInfoFetched: false,
});

const authReducer = createReducer({
  [noNetworkConnection]: (state) => state.set('networkDisconnected', true),
  [networkConnected]: (state) => state.set('networkDisconnected', false),
  [setApiLatency]: (state, { latency }) => state.set('apiLatency', latency),
  [userDataReceived]: (state) => state.set('userInfoFetched', true),

  [userLoggedOut]: () => initialState,
}, initialState);

export default combineReducers({
  auth: authReducer,
});
