
import { fromJS } from 'immutable';
import authWrapperReducer from '../reducer';

const initialState = {
  notificationsCenter: [],
  auth: { userInfoFetched: false },
  verification: {
    popupOpened: false,
    initialValues: null,
    statusInProgressOpened: false,
    statusNewOpened: false,
    documents: null,
  },
  profileSettings: {
    opened: false,
  },
  confirmation: {
    messages: {},
    opened: false,
  },
};

describe('authWrapperReducer', () => {
  it('returns the initial state', () => {
    expect(authWrapperReducer(undefined, {})).toEqual(fromJS(initialState));
  });
});
