import { createAction } from 'redux-act';

export const startTokenRefresh = createAction('app/container/AuthWrapper/START_TOKEN_REFRESH');
