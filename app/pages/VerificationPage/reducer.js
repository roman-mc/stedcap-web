import { createReducer } from 'redux-act';
import { Map, fromJS } from 'immutable';

import FileRecord from 'Model/FileRecord';

import { doLogout } from '../LogoutPage/actions';

import {
  savePersonalQuestionnaireStep,
  loadPersonSchema,
  loadDraftStep,
  loadQuestionaryStatus,
  goToStep,
  setFormValue,
  loadFiles,
  setPaymentMethodExist,
} from './actions';

const initialState = new Map({
  step: undefined,
  status: undefined,
  initialValues: new Map(),
  files: new Map(),
  stepsFilled: 0,
  isPaymentMethodExist: false,
});

export default createReducer({
  [setFormValue]: (state, { step, data }) => state.setIn(['initialValues', step], fromJS(data)),
  [setPaymentMethodExist]: (state, { isPaymentMethodExist }) => state.set('isPaymentMethodExist', isPaymentMethodExist),
  [goToStep]: (state, { step }) => state.set('step', step),
  [loadQuestionaryStatus.success]: (state, { status }) => state.set('status', status),
  [loadFiles.success]: (state, { items }) =>
    state
      .set('files', items.reduce((acc, file) => acc.set(file.id.toString(), new FileRecord(file)), state.get('files'))),
  [loadDraftStep.success]: (state, { result }) =>
    state
      .set('step', result === 0 ? result : result + 1)
      .set('stepsFilled', result),
  [savePersonalQuestionnaireStep.success]: (state) =>
    state
      .set('stepsFilled', state.get('stepsFilled') >= state.get('step') ? state.get('stepsFilled') : state.get('step') + 1)
      .set('step', state.get('step') + 1),
  [loadPersonSchema.success]: (state, { schema, step }) => state.setIn(['personSchema', step], schema.schema),
  [doLogout.success]: () => initialState,
}, initialState);
