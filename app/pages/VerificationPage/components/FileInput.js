import React from 'react';
import ImmutableProptypes from 'react-immutable-proptypes';
import PropTypes from 'prop-types';
import { FieldArray } from 'redux-form/immutable';
import { FormattedMessage } from 'react-intl';

import FileDrop from '../../../containers/FileDrop';
import { formatFileSize } from '../../../utils/helpers';

import { } from '../style.css';
import messages from '../messages';

class InputFileField extends React.PureComponent { // eslint-disable-line react/prefer-stateless-function
  render() {
    const {
      children, onFileLoaded, savedIds = [], savedFiles, label, description, required, meta: { error, warning, submitFailed },
    } = this.props;
    const isError = Boolean(submitFailed && (error || warning));

    return (
      <div styleName="file-upload-group">
        {label && <h3 styleName="file-upload-title"><FormattedMessage {...label} />{required && <span styleName="required-mark">*</span>}</h3>}
        {description && <p styleName="file-upload-description"><FormattedMessage {...description} /></p>}
        {children}
        <div styleName="uploaded-files">
          {savedIds.filter((id) => savedFiles.get(id)).map((id) =>
            (<div key={`f_${id}`} styleName="file-upload-uploaded">
              <span styleName="file-upload-uploaded-name">{savedFiles.get(id).name}</span>
              <span styleName="file-upload-uploaded-size">({formatFileSize(savedFiles.get(id).fileSize)})</span>
            </div>))}
        </div>

        <div styleName="file-upload-dropzone">
          <FileDrop placeholder={<FormattedMessage {...messages.selectFiles} />} {...this.props} setFiles={onFileLoaded} />
        </div>
        {isError
        && <div styleName="input-error-text">
          {error}
        </div>
        }
      </div>
    );
  }
}

InputFileField.propTypes = {
  onFileLoaded: PropTypes.func.isRequired,
  savedIds: PropTypes.object,
  label: PropTypes.object,
  description: PropTypes.object,
  meta: PropTypes.any,
  required: PropTypes.any,
  children: PropTypes.node,
  savedFiles: ImmutableProptypes.map,
};


export default function FileInput(props) {
  return (
    <FieldArray
      component={InputFileField}
      {...props}
    />
  );
}
