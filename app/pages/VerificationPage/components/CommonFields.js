import React from 'react';

import { RadioGroupField, BtnGroupField } from './FormComponents';

const communicationMethods = [
  { value: 'fax', label: 'Fax' },
  { value: 'email', label: 'Email' },
  { value: 'phone', label: 'Phone' },
];


export function CommunicationMethodSelector(props) {
  return (<RadioGroupField options={communicationMethods} {...props} />);
}

const yesNo = [
  { value: false, label: 'No' },
  { value: true, label: 'Yes' },
];

export function YesNoSelector(props) {
  return (<RadioGroupField options={yesNo} {...props} />);
}


const availableCurrencies = [
  { value: 0, label: 'USD' },
  { value: 1, label: 'EUR' },
];

export function CurrencySelector(props) {
  return (
    <BtnGroupField options={availableCurrencies} {...props} />
  );
}
