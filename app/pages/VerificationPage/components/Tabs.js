import React from 'react';
import PropTypes from 'prop-types';
import { Map } from 'immutable';
import { FormattedMessage } from 'react-intl';
import { FieldArray } from 'redux-form/immutable';

import '../style.css';
import messages from '../messages';

class Tabs extends React.PureComponent {
  state = {
    selectedTab: 0,
  }

  removeTab = (tabId, e) => {
    e.stopPropagation();
    this.props.fields.remove(tabId);
    if (tabId === this.state.selectedTab) {
      this.setState({ selectedTab: tabId - 1 });
    }
  }

  render() {
    /* eslint-disable */
    const { fields, tabPrefix, renderTab, errors = [] } = this.props;

    return (
      <div styleName="tabs-container">
        <div styleName="tabs">
          {fields.map((field, idx) =>
            <div
              key={`tab_${field}_${idx}`}
              onClick={() => this.setState({ selectedTab: idx })}
              styleName={`tab ${this.state.selectedTab === idx ? 'tab-active' : ''} ${errors[idx] ? 'has-error' : ''}`}
            >
              {`${tabPrefix} #${idx + 1}`}
              {idx > 0 && <i className="icon-icon-cancel" onClick={(e) => this.removeTab(idx, e)}></i>}
            </div>
          )}
          <a onClick={() => { fields.push(new Map({})); this.setState({ selectedTab: fields.length }); }} styleName="add-tab"><FormattedMessage {...messages.addTabPerson} /></a>
        </div>
        <div styleName="tabs-content">
          {fields.map((field, idx) =>
            <div styleName={`tab-content ${idx === this.state.selectedTab ? 'active' : ''}`} key={`tab_content_${field}_${idx}`}>{renderTab(field, idx)}</div>
          )}
        </div>
      </div>
    );
    /* eslint-enable */
  }
}

Tabs.propTypes = {
  fields: PropTypes.object,
  tabPrefix: PropTypes.string.isRequired,
  renderTab: PropTypes.func.isRequired,
  errors: PropTypes.array,
};

export default function TabsField(props) {
  return <FieldArray component={Tabs} {...props} />;
}
