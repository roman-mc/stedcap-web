import React from 'react';
import { injectIntl, intlShape, FormattedMessage } from 'react-intl';
import { Link } from 'react-router-dom';

import { Button } from 'Components/AuthForm';

import messages from '../../messages';

import Steps from '../Steps';


// import Step1 from './Step1';
// import Step2 from './Step2';
// import Step3 from './Step3';

import '../../style.css';

// const steps = [
//     Step1,
//     Step2,
//     Step3,
// ];

const stepTitles = [
  messages.openingAccount,
  messages.corporateClientInfo,
  messages.beneficialOwners,
  messages.authorizedPersons,
  messages.investmentProfile,
  messages.paymentDetails,
];


class CorporatelVerificationForm extends React.PureComponent { // eslint-disable-line react/prefer-stateless-function
  // state = {
  //  currentStep: 0,
  // }

  // nextStep = () => {
  //     const { currentStep } = this.state;
  //     if (steps.length >= currentStep + 2) {
  //         this.setState({ currentStep: currentStep + 1 });
  //     }
  // }
  render() {
    // const { currentStep } = this.state;
    const { intl: { formatMessage } } = this.props;
    return (
      <div styleName="form">
        <div styleName="header-wrapper">
          <header styleName="header">
            <Link to="/" styleName="logo">
              <div styleName="logo-img"></div>
              <div styleName="logo-text">
                unity
              </div>
            </Link>
            <span styleName="title">
              <FormattedMessage {...messages.corporateVerification} />
            </span>
            <span styleName="stub">
              <span styleName="logo">
                <div styleName="logo-img"></div>
                <div styleName="logo-text">
                  unity
                </div>
              </span>
            </span>
          </header>
          <Steps titles={stepTitles} />
        </div>
        <div styleName="body">
          <div styleName="vertical-center">
            <div styleName="text-center">
              <i className="icon-icon-not-verified"></i>
              <h2 styleName="text-center">
                Verification is temporarily unavailable.
                {' '}
                <br />
                {' '}
                Please contact support.
              </h2>
            </div>
          </div>

        </div>
        <div styleName="footer">
          <Link to="/logout">
            <Button
              type="submit"
              className="open-account"
              text={formatMessage(messages.logOut)}
            // onClick={this.nextStep}
            />
          </Link>
        </div>
      </div>
    );
  }
}

CorporatelVerificationForm.propTypes = {
  intl: intlShape,
};

export default injectIntl(CorporatelVerificationForm);
