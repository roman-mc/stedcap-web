import React from 'react';
import ImmutableProptypes from 'react-immutable-proptypes';
import PropTypes from 'prop-types';
import moment from 'moment';
import { FormattedHTMLMessage } from 'react-intl';
import { reduxForm } from 'redux-form/immutable';
import { createStructuredSelector } from 'reselect';
import { connect } from 'react-redux';

import Scrollbar from 'Components/Scrollbar';
import { InputField, DateInputField, TextAreaField, SelectInputField, CheckboxField } from '../FormComponents';
import FileInputField from '../FileInput';
import { CommunicationMethodSelector } from '../CommonFields';


import '../../style.css';
import messages from '../../messages';

import { makeSelectPersonSchema, makeSelectInitialValues, makeSelectFiles } from '../../selectors';
import { loadFiles } from '../../actions';
import { selectValues, formValidator } from '../../../../utils/jsonSchemaUtils';

const adultDate = moment().subtract(18, 'year');
const today = moment();
const halfYear = moment().add(7, 'month');

function Step2({
  handleSubmit, schema, formValues, files, loadFiles: doLoadFiles,
}) {
  return (
    <form styleName="form-step-1" onSubmit={handleSubmit}>
      <p className="text-center italic"><FormattedHTMLMessage {...messages.fieldsRequiredNote} /></p>
      <div styleName="form-step-container">
        <div styleName="form-step">
          <Scrollbar style={{ position: 'static' }}>
            <div styleName="form-fields-container">
              <InputField required label="Full name" name="fullName" />
              <DateInputField maxDate={adultDate} required label="Date of birth" name="dateOfBirth" />
              <InputField required label="Place of birth" name="placeOfBirth" />
              <InputField required label="Nationality" name="nationality" />
              <InputField required label="Passport number" name="passportNumber" />
              <SelectInputField options={selectValues(schema, 'issuanceCountry')} disableClearable required label="Country of issuance" name="issuanceCountry" />
              <DateInputField maxDate={today} required label="Passport issue date" name="passportIssueDate" />
              <DateInputField disabled={formValues.get('passportNoExpiry')} minDate={halfYear} required label="Passport expiry date" name="passportExpiryDate" />
              <div styleName="no-label-checkbox">
                <CheckboxField name="passportNoExpiry" label="My passport validity is indefinite" />
              </div>
              <InputField required label="Home address" name="homeAddress" />
              <InputField label="Mailing address" name="mailingAddress" placeholder="Fill in mailing address if different from home address" />
              <InputField required label="Phone number" name="phoneNumber" />
              <InputField label="Additional phone number" name="additionalPhoneNumber" />
              <InputField label="Fax number" name="faxNumber" required={formValues.get('preferredCommunication') === 'fax'} />
              <InputField label="Email" name="email" required={formValues.get('preferredCommunication') === 'email'} />
              <CommunicationMethodSelector label="Preferred communication" required name="preferredCommunication" />
              <TextAreaField
                required
                name="purposeForRequesting"
                label="Purpose and reason for requesting the establishment of a business relationship with the Company"
                placeholder="Fill in the explanation for your request"
              />
            </div>
          </Scrollbar>
        </div>
        <div styleName="file-upload">
          <Scrollbar style={{ position: 'static' }}>
            <FileInputField
              savedIds={formValues.get('proofOfIdentity')}
              savedFiles={files}
              name="proofOfIdentity"
              label={messages.proofOfIdentity}
              description={messages.proofOfIdentityDescription}
              required
              onFileLoaded={doLoadFiles}
            >
              <DateInputField micro disabled={formValues.get('passportNoExpiry')} required label="Expiry date" name="expiryDate" minDate={today} />
            </FileInputField>
            <FileInputField
              savedIds={formValues.get('proofOfResidency')}
              savedFiles={files}
              name="proofOfResidency"
              label={messages.proofOfResidency}
              description={messages.proofOfResidencyDescription}
              onFileLoaded={doLoadFiles}
              required
            />
            <div styleName="sub-text">
                            Please note that we will not accept documents<br />
                            that do not satisfy the following criteria:<br /><br />
                            · certified true copy of the original;<br />
                            · with translation (if needed);<br />
                            · unexpired;<br />
                            · valid more than 6 months.<br />
            </div>
          </Scrollbar>
        </div>
      </div>
    </form>
  );
}

Step2.propTypes = {
  handleSubmit: PropTypes.func.isRequired,
  schema: PropTypes.object,
  formValues: ImmutableProptypes.map,
  files: ImmutableProptypes.map,
  loadFiles: PropTypes.func.isRequired,
};

const mapStateToProps = createStructuredSelector({
  schema: makeSelectPersonSchema(1),
  initialValues: makeSelectInitialValues(1, {
    passportNoExpiry: false,
  }),
  files: makeSelectFiles(),
});

const mapDipsatchToProps = (dispatch) => ({
  loadFiles: (items) => dispatch(loadFiles.success({ items })),
});

const form = reduxForm({
  form: 'verificationFormStep1',
  validate: formValidator,
  destroyOnUnmount: false,
  forceUnregisterOnUnmount: true,
})(Step2);

export default connect(mapStateToProps, mapDipsatchToProps)(form);
