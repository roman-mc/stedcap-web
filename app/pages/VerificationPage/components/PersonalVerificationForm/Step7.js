import React from 'react';
import PropTypes from 'prop-types';
import ImmutableProptypes from 'react-immutable-proptypes';
import { FormattedMessage, FormattedHTMLMessage } from 'react-intl';
import { connect } from 'react-redux';
import { createStructuredSelector } from 'reselect';
import { reduxForm } from 'redux-form/immutable';

import Scrollbar from 'Components/Scrollbar';
import { BtnGroupField, RadioGroupField, InputField } from '../FormComponents';

import '../../style.css';
import messages from '../../messages';

import { makeSelectPersonSchema, makeSelectInitialValues } from '../../selectors';

import { selectValues, formValidator } from '../../../../utils/jsonSchemaUtils';

const valueSymbols = {
  USD: '$',
  EUR: '€',
};

function Step7({ schema, formValues }) {
  const currency = { currency: valueSymbols[formValues.get('financialStatusCurrency')] };
  return (
    <div styleName="form-step-7">
      <p className="text-center italic"><FormattedHTMLMessage {...messages.fieldsRequiredNote} /></p>
      <div styleName="form-step-container">
        <div styleName="form-step">
          <Scrollbar style={{ position: 'static' }}>
            <div styleName="form-fields-container">
              <BtnGroupField name="financialStatusCurrency" options={selectValues(schema, 'financialStatusCurrency')} label="Specify all amounts in answers in" required />
              <InputField multiRow name="earnings" label="Earnings" postfix={formValues.get('financialStatusCurrency')} required />
              <InputField multiRow name="monthlyIncome" label="Monthly net income" postfix={formValues.get('financialStatusCurrency')} required />
              <InputField multiRow name="totalLiabilities" label="Total liabilities including sureties" postfix={formValues.get('financialStatusCurrency')} required />
              <InputField multiRow name="fundsAvailable" label="Funds available for investment" postfix={formValues.get('financialStatusCurrency')} required />

              <h3 styleName="subtitle"><FormattedMessage {...messages.currentKnowledge} /></h3>
              <p styleName="subtitle-description"><FormattedMessage {...messages.currentKnowledgeDescription} /><span styleName="required-mark">*</span></p>
              <RadioGroupField options={selectValues(schema, 'cryptoKnowledge')} label="Cryptocurrencies" name="cryptoKnowledge" />
              <RadioGroupField options={selectValues(schema, 'optionsKnowledge')} label="Options, futures, swaps, forwards, other derivatives" name="optionsKnowledge" />
              <RadioGroupField options={selectValues(schema, 'securitiesKnowledge')} label="Securities" name="securitiesKnowledge" />
              <RadioGroupField options={selectValues(schema, 'currenciesKnowledge')} label="Currencies, interest rates" name="currenciesKnowledge" />
              <RadioGroupField options={selectValues(schema, 'derivativesKnowledge')} label="Derivatives on commodities" name="derivativesKnowledge" />
              <RadioGroupField options={selectValues(schema, 'depositsKnowledge')} label="Deposits" name="depositsKnowledge" />
              <RadioGroupField options={selectValues(schema, 'moneyMarketsKnowledge')} label="Money market instruments" name="moneyMarketsKnowledge" />
              <RadioGroupField options={selectValues(schema, 'depositaryKnowledge')} label="Depositary receipts" name="depositaryKnowledge" />
              <RadioGroupField options={selectValues(schema, 'bondsKnowledge')} label="Bonds or other securitised debt" name="bondsKnowledge" />
              <RadioGroupField options={selectValues(schema, 'unitsKnowledge')} label="Units" name="unitsKnowledge" />
              <RadioGroupField options={selectValues(schema, 'otherInstrumentsKnowledge')} label="Other non-complex financial instruments" name="otherInstrumentsKnowledge" />

              <p styleName="subtitle-description"><FormattedMessage {...messages.knowledgeSourceDescription} /><span styleName="required-mark">*</span></p>
              <RadioGroupField multiRow options={selectValues(schema, 'cryptoSource')} label="Cryptocurrencies" name="cryptoSource" />
              <RadioGroupField multiRow options={selectValues(schema, 'optionsSource')} label="Options, futures, swaps, forwards, other derivatives" name="optionsSource" />
              <RadioGroupField multiRow options={selectValues(schema, 'securitiesSource')} label="Securities" name="securitiesSource" />
              <RadioGroupField multiRow options={selectValues(schema, 'currenciesSource')} label="Currencies, interest rates" name="currenciesSource" />
              <RadioGroupField multiRow options={selectValues(schema, 'derivativesSource')} label="Derivatives on commodities" name="derivativesSource" />
              <RadioGroupField multiRow options={selectValues(schema, 'depositsSource')} label="Deposits" name="depositsSource" />
              <RadioGroupField multiRow options={selectValues(schema, 'moneyMarketsSource')} label="Money market instruments" name="moneyMarketsSource" />
              <RadioGroupField multiRow options={selectValues(schema, 'depositarySource')} label="Depositary receipts" name="depositarySource" />
              <RadioGroupField multiRow options={selectValues(schema, 'bondsSource')} label="Bonds or other securitised debt" name="bondsSource" />
              <RadioGroupField multiRow options={selectValues(schema, 'unitsSource')} label="Units" name="unitsSource" />
              <RadioGroupField multiRow options={selectValues(schema, 'otherInstrumentsSource')} label="Other non-complex financial instruments" name="otherInstrumentsSource" />

              <p styleName="subtitle-description"><FormattedMessage {...messages.amountDescription} /><span styleName="required-mark">*</span></p>

              <RadioGroupField large options={selectValues(schema, 'cryptoAmount', currency)} label="Cryptocurrencies" name="cryptoAmount" />
              <RadioGroupField large options={selectValues(schema, 'optionsAmount', currency)} label="Options, futures, swaps, forwards, other derivatives" name="optionsAmount" />
              <RadioGroupField large options={selectValues(schema, 'securitiesAmount', currency)} label="Securities" name="securitiesAmount" />
              <RadioGroupField large options={selectValues(schema, 'currenciesAmount', currency)} label="Currencies, interest rates" name="currenciesAmount" />
              <RadioGroupField large options={selectValues(schema, 'derivativesAmount', currency)} label="Derivatives on commodities" name="derivativesAmount" />
              <RadioGroupField large options={selectValues(schema, 'depositsAmount', currency)} label="Deposits" name="depositsAmount" />
              <RadioGroupField large options={selectValues(schema, 'moneyMarketsAmount', currency)} label="Money market instruments" name="moneyMarketsAmount" />
              <RadioGroupField large options={selectValues(schema, 'depositaryAmount', currency)} label="Depositary receipts" name="depositaryAmount" />
              <RadioGroupField large options={selectValues(schema, 'bondsAmount', currency)} label="Bonds or other securitised debt" name="bondsAmount" />
              <RadioGroupField large options={selectValues(schema, 'unitsAmount', currency)} label="Units" name="unitsAmount" />
              <RadioGroupField large options={selectValues(schema, 'otherInstrumentsAmount', currency)} label="Other non-complex financial instruments" name="otherInstrumentsAmount" />

              <p styleName="subtitle-description"><FormattedMessage {...messages.knowledgeDurationDescription} /><span styleName="required-mark">*</span></p>
              <RadioGroupField large options={selectValues(schema, 'cryptoDuration')} label="Cryptocurrencies" name="cryptoDuration" />
              <RadioGroupField large options={selectValues(schema, 'optionsDuration')} label="Options, futures, swaps, forwards, other derivatives" name="optionsDuration" />
              <RadioGroupField large options={selectValues(schema, 'securitiesDuration')} label="Securities" name="securitiesDuration" />
              <RadioGroupField large options={selectValues(schema, 'currenciesDuration')} label="Currencies, interest rates" name="currenciesDuration" />
              <RadioGroupField large options={selectValues(schema, 'derivativesDuration')} label="Derivatives on commodities" name="derivativesDuration" />
              <RadioGroupField large options={selectValues(schema, 'depositsDuration')} label="Deposits" name="depositsDuration" />
              <RadioGroupField large options={selectValues(schema, 'moneyMarketsDuration')} label="Money market instruments" name="moneyMarketsDuration" />
              <RadioGroupField large options={selectValues(schema, 'depositaryDuration')} label="Depositary receipts" name="depositaryDuration" />
              <RadioGroupField large options={selectValues(schema, 'bondsDuration')} label="Bonds or other securitised debt" name="bondsDuration" />
              <RadioGroupField large options={selectValues(schema, 'unitsDuration')} label="Units" name="unitsDuration" />
              <RadioGroupField large options={selectValues(schema, 'otherInstrumentsDuration')} label="Other non-complex financial instruments" name="otherInstrumentsDuration" />

              <h3 styleName="subtitle"><FormattedMessage {...messages.currentExperience} /></h3>

              <p styleName="subtitle-description"><FormattedMessage {...messages.crypto} /><span styleName="required-mark">*</span></p>
              <InputField multiRow name="cryptoCurrentValue" label="Current value" postfix={formValues.get('financialStatusCurrency')} />
              <InputField multiRow name="cryptoPastYearSize" label="Size of transactions over the past year" placeholder="Fill in size of transactions" postfix={formValues.get('financialStatusCurrency')} />
              <InputField multiRow name="cryptoLastYearAverage" label="Average frequency of trading per quarter over the last year" placeholder="Fill in average frequrency of trading" postfix={formValues.get('financialStatusCurrency')} />

              <p styleName="subtitle-description"><FormattedMessage {...messages.options} /><span styleName="required-mark">*</span></p>
              <InputField multiRow name="optionsCurrentValue" label="Current value" postfix={formValues.get('financialStatusCurrency')} />
              <InputField multiRow name="optionsPastYearSize" label="Size of transactions over the past year" placeholder="Fill in size of transactions" postfix={formValues.get('financialStatusCurrency')} />
              <InputField multiRow name="optionsLastYearAverage" label="Average frequency of trading per quarter over the last year" placeholder="Fill in average frequrency of trading" postfix={formValues.get('financialStatusCurrency')} />

              <p styleName="subtitle-description"><FormattedMessage {...messages.securities} /><span styleName="required-mark">*</span></p>
              <InputField multiRow name="securitiesCurrentValue" label="Current value" postfix={formValues.get('financialStatusCurrency')} />
              <InputField multiRow name="securitiesPastYearSize" label="Size of transactions over the past year" placeholder="Fill in size of transactions" postfix={formValues.get('financialStatusCurrency')} />
              <InputField multiRow name="securitiesLastYearAverage" label="Average frequency of trading per quarter over the last year" placeholder="Fill in average frequrency of trading" postfix={formValues.get('financialStatusCurrency')} />

              <p styleName="subtitle-description"><FormattedMessage {...messages.interestRates} /><span styleName="required-mark">*</span></p>
              <InputField multiRow name="interestRatesCurrentValue" label="Current value" postfix={formValues.get('financialStatusCurrency')} />
              <InputField multiRow name="interestRatesPastYearSize" label="Size of transactions over the past year" placeholder="Fill in size of transactions" postfix={formValues.get('financialStatusCurrency')} />
              <InputField multiRow name="interestRatesLastYearAverage" label="Average frequency of trading per quarter over the last year" placeholder="Fill in average frequrency of trading" postfix={formValues.get('financialStatusCurrency')} />

              <p styleName="subtitle-description"><FormattedMessage {...messages.derivatives} /><span styleName="required-mark">*</span></p>
              <InputField multiRow name="derivativesCurrentValue" label="Current value" postfix={formValues.get('financialStatusCurrency')} />
              <InputField multiRow name="derivativesPastYearSize" label="Size of transactions over the past year" placeholder="Fill in size of transactions" postfix={formValues.get('financialStatusCurrency')} />
              <InputField multiRow name="derivativesLastYearAverage" label="Average frequency of trading per quarter over the last year" placeholder="Fill in average frequrency of trading" postfix={formValues.get('financialStatusCurrency')} />

              <p styleName="subtitle-description"><FormattedMessage {...messages.deposits} /><span styleName="required-mark">*</span></p>
              <InputField multiRow name="depositsCurrentValue" label="Current value" postfix={formValues.get('financialStatusCurrency')} />
              <InputField multiRow name="depositsPastYearSize" label="Size of transactions over the past year" placeholder="Fill in size of transactions" postfix={formValues.get('financialStatusCurrency')} />
              <InputField multiRow name="depositsLastYearAverage" label="Average frequency of trading per quarter over the last year" placeholder="Fill in average frequrency of trading" postfix={formValues.get('financialStatusCurrency')} />

              <p styleName="subtitle-description"><FormattedMessage {...messages.moneyMarkets} /><span styleName="required-mark">*</span></p>
              <InputField multiRow name="moneyMarketsCurrentValue" label="Current value" postfix={formValues.get('financialStatusCurrency')} />
              <InputField multiRow name="moneyMarketsPastYearSize" label="Size of transactions over the past year" placeholder="Fill in size of transactions" postfix={formValues.get('financialStatusCurrency')} />
              <InputField multiRow name="moneyMarketsLastYearAverage" label="Average frequency of trading per quarter over the last year" placeholder="Fill in average frequrency of trading" postfix={formValues.get('financialStatusCurrency')} />

              <p styleName="subtitle-description"><FormattedMessage {...messages.depositaryReceipts} /><span styleName="required-mark">*</span></p>
              <InputField multiRow name="depositaryCurrentValue" label="Current value" postfix={formValues.get('financialStatusCurrency')} />
              <InputField multiRow name="depositaryPastYearSize" label="Size of transactions over the past year" placeholder="Fill in size of transactions" postfix={formValues.get('financialStatusCurrency')} />
              <InputField multiRow name="depositaryLastYearAverage" label="Average frequency of trading per quarter over the last year" placeholder="Fill in average frequrency of trading" postfix={formValues.get('financialStatusCurrency')} />

              <p styleName="subtitle-description"><FormattedMessage {...messages.bonds} /><span styleName="required-mark">*</span></p>
              <InputField multiRow name="bondsCurrentValue" label="Current value" postfix={formValues.get('financialStatusCurrency')} />
              <InputField multiRow name="bondsPastYearSize" label="Size of transactions over the past year" placeholder="Fill in size of transactions" postfix={formValues.get('financialStatusCurrency')} />
              <InputField multiRow name="bondsLastYearAverage" label="Average frequency of trading per quarter over the last year" placeholder="Fill in average frequrency of trading" postfix={formValues.get('financialStatusCurrency')} />

              <p styleName="subtitle-description"><FormattedMessage {...messages.units} /><span styleName="required-mark">*</span></p>
              <InputField multiRow name="unitsCurrentValue" label="Current value" postfix={formValues.get('financialStatusCurrency')} />
              <InputField multiRow name="unitsPastYearSize" label="Size of transactions over the past year" placeholder="Fill in size of transactions" postfix={formValues.get('financialStatusCurrency')} />
              <InputField multiRow name="unitsLastYearAverage" label="Average frequency of trading per quarter over the last year" placeholder="Fill in average frequrency of trading" postfix={formValues.get('financialStatusCurrency')} />

              <p styleName="subtitle-description"><FormattedMessage {...messages.otherInstruments} /><span styleName="required-mark">*</span></p>
              <InputField multiRow name="otherInstrumentsCurrentValue" label="Current value" postfix={formValues.get('financialStatusCurrency')} />
              <InputField multiRow name="otherInstrumentsPastYearSize" placeholder="Fill in size of transactions" label="Size of transactions over the past year" postfix={formValues.get('financialStatusCurrency')} />
              <InputField multiRow name="otherInstrumentsLastYearAverage" placeholder="Fill in average frequency of trading" label="Average frequency of trading per quarter over the last year" postfix={formValues.get('financialStatusCurrency')} />
            </div>
          </Scrollbar>
        </div>
      </div>
    </div>
  );
}


Step7.propTypes = {
  schema: PropTypes.object.isRequired,
  formValues: ImmutableProptypes.map,
};


const mapStateToProps = createStructuredSelector({
  schema: makeSelectPersonSchema(7),
  initialValues: makeSelectInitialValues(7, {
    financialStatusCurrency: 'USD',
  }),
});


const form = reduxForm({
  form: 'verificationFormStep7',
  validate: formValidator,
  destroyOnUnmount: false,
  forceUnregisterOnUnmount: true,
})(Step7);

export default connect(mapStateToProps, null)(form);
