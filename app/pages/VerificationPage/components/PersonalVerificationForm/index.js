import React from 'react';
import PropTypes from 'prop-types';
import ImmutableProptypes from 'react-immutable-proptypes';
import { submit } from 'redux-form/immutable';
import { connect } from 'react-redux';
import { createStructuredSelector } from 'reselect';

import { QUESTIONNARIE_STATUS } from 'Model/enums';


import FormWrapper from '../FormWrapper';
import Steps from '../Steps';

import Step0 from './Step0';
import Step1 from './Step1';
import Step2 from './Step2';
import Step3 from './Step3';
import Step4 from './Step4';
import Step5 from './Step5';
import Step6 from './Step6';
import Step7 from './Step7';
import Step8 from './Step8';
import Step9 from './Step9';

import WaitinfForApprove from './WaitingForApprove';

import '../../style.css';
import messages from '../../messages';

import { savePersonalQuestionnaireStep, loadDraftStep, goToStep } from '../../actions';
import { makeSelectStep, makeSelectFormValues, makeSelectQuestionaryStatus, makeSelectStepsFilled, makeSelectAdditionalFilesFormValues } from '../../selectors';

const steps = [
  Step0,
  Step1,
  Step2,
  Step3,
  Step4,
  Step5,
  Step6,
  Step7,
  Step8,
  Step9,
];

const stepTitles = [
  messages.openingAccount,
  messages.customerInformation,
  messages.crsInformation,
  messages.fatcaInformation,
  messages.education,
  messages.politically,
  messages.investment,
  messages.financialStatus,
  messages.paymentDetails,
  messages.explicitDeclrations,
];

class PersonalVerificationForm extends React.PureComponent { // eslint-disable-line react/prefer-stateless-function
  componentDidMount() {
    this.props.loadDraftStep();
  }

  onSubmit = (values) => {
    this.props.savePersonalQuestionnaireStep(values.toJS());
  }

  nextStep = () => {
    this.props.submit(this.props.step);
  }

  render() {
    const {
      status, step, formValues, stepsFilled, additionalFilesForm,
    } = this.props;
    const Screen = steps[step];

    if (status === QUESTIONNARIE_STATUS.VERIFICATION_IN_PROGRESS || status === QUESTIONNARIE_STATUS.REJECTED) {
      return (
        <WaitinfForApprove status={status} additionalFilesForm={additionalFilesForm} />
      );
    }
    if (step !== undefined && (status === QUESTIONNARIE_STATUS.NEW || status === QUESTIONNARIE_STATUS.UPDATE_IS_NEEDED)) {
      return (
        <FormWrapper
          titleMessage={messages.personVerification}
          renderSteps={() => <Steps stepsFilled={stepsFilled} onGoToStep={this.props.goToStep} titles={stepTitles} currentStep={step} />}
          onNextStep={this.nextStep}
          verifyButton={step === steps.length - 1}
        >
          <Screen formValues={formValues} onSubmit={this.onSubmit} />
        </FormWrapper>
      );
    }
    return (
      <div className="loader">
        <div className="loader_animation" />
      </div>
    );
  }
}

PersonalVerificationForm.propTypes = {
  savePersonalQuestionnaireStep: PropTypes.func.isRequired,
  submit: PropTypes.func.isRequired,
  loadDraftStep: PropTypes.func.isRequired,
  goToStep: PropTypes.func.isRequired,
  step: PropTypes.number,
  formValues: ImmutableProptypes.map,
  additionalFilesForm: ImmutableProptypes.map,
  status: PropTypes.number,
  stepsFilled: PropTypes.number.isRequired,
};

const mapDispatchToProps = (dispatch) => ({
  loadDraftStep: () => dispatch(loadDraftStep()),
  goToStep: (step) => dispatch(goToStep({ step })),
  savePersonalQuestionnaireStep: (data) => dispatch(savePersonalQuestionnaireStep({ data })),
  submit: (step) => dispatch(submit(`verificationFormStep${step}`)),
});

const mapStateToProps = createStructuredSelector({
  step: makeSelectStep(),
  formValues: makeSelectFormValues(),
  additionalFilesForm: makeSelectAdditionalFilesFormValues(),
  status: makeSelectQuestionaryStatus(),
  stepsFilled: makeSelectStepsFilled(),
});

export default connect(mapStateToProps, mapDispatchToProps)(PersonalVerificationForm);
