import React from 'react';
import PropTypes from 'prop-types';
import { reduxForm } from 'redux-form/immutable';
import { FormattedMessage } from 'react-intl';

import Scrollbar from 'Components/Scrollbar';
import '../../style.css';
import messages from '../../messages';

function Step1({ handleSubmit }) {
  return (
    <form styleName="form-step-0" onSubmit={handleSubmit}>
      <div styleName="form-step-container">
        <div styleName="form-step">
          <Scrollbar>
            <h2 styleName="step1-title"><FormattedMessage {...messages.step1DescriptionTitle1} /></h2>
            <p styleName="step1-text"><FormattedMessage {...messages.step1DescriptionText1} /></p>
            <h2 styleName="step1-title"><FormattedMessage {...messages.step1DescriptionTitle2} /></h2>
            <p styleName="step1-text"><FormattedMessage {...messages.step1DescriptionText2} /></p>
            <h2 styleName="step1-title"><FormattedMessage {...messages.step1DescriptionTitle3} /></h2>
            <p styleName="step1-text"><FormattedMessage {...messages.step1DescriptionText3} /></p>
            <p styleName="step1-text text-bottom"><FormattedMessage {...messages.step1DescriptionText3Part2} /></p>
          </Scrollbar>
        </div>
      </div>
    </form>
  );
}


Step1.propTypes = {
  handleSubmit: PropTypes.func.isRequired,
};

export default reduxForm({ form: 'verificationFormStep0' })(Step1);
