
import React from 'react';
import PropTypes from 'prop-types';
import { FormattedMessage, FormattedHTMLMessage } from 'react-intl';
import { connect } from 'react-redux';
import { createStructuredSelector } from 'reselect';
import { reduxForm } from 'redux-form/immutable';

import Scrollbar from 'Components/Scrollbar';
import { CheckboxField, RadioGroupField } from '../FormComponents';

import {} from '../../style.css';
import messages from '../../messages';

import { makeSelectPersonSchema, makeSelectInitialValues } from '../../selectors';

import { formValidator, selectValues } from '../../../../utils/jsonSchemaUtils';

function Step9({ schema }) {
  return (
    <div styleName="form-step-7">
      <p className="text-center italic" styleName="fields-required-title"><FormattedHTMLMessage {...messages.fieldsRequiredNote} /></p>
      <div styleName="form-step-container">
        <div styleName="form-step">
          <Scrollbar style={{ position: 'static' }}>
            <div styleName="form-fields-container">
              <h3 styleName="subtitle"><FormattedMessage {...messages.explicitDeclrations} /></h3>

              <div styleName="subtitle-description">
                                Please carefully review all documents in this section and check all boxes<br />
                                to indicate that you have read them and agree to our terms.
              </div>

              <div style={{ marginTop: '23px' }}>
                <CheckboxField
                  required
                  name="agreeTermsAndConditions"
                  label={
                    <span>
                                            I agree to <a href="/documents/terms_and_conditions.pdf" target="_blank">General Terms and Conditions Agreement</a>
                    </span>
                  }
                />
                <CheckboxField
                  required
                  name="agreePolicies"
                  label={
                    <span>
                                            I agree to <a href="https://stedcap.com/documents.html" target="_blank">All the Policies published on website</a>
                    </span>
                  }
                />
                <CheckboxField required name="confirmInformation" label="I confirm that all information and documentation provided in this questionnaire is neither false or misleading as this constitutes a criminal offence" />
                <CheckboxField
                  required
                  name="agreeDpr"
                  label={
                    <span>
                                        I consent to the processing of personal data relating to me, in line with the provisions of&nbsp;
                      <a href="https://eur-lex.europa.eu/legal-content/EN/TXT/PDF/?uri=CELEX:32016R0679&from=EN" target="_blank">EU General Data Protection Regulation</a>
                    </span>
                  }
                />
              </div>

              <h3 styleName="subtitle" style={{ marginTop: '40px' }}><FormattedMessage {...messages.investmentAdvise} /></h3>

              <div styleName="subtitle-description">
                                Please agree or disagree with the statements below.
              </div>

              <div style={{ marginTop: '23px' }}>
                <RadioGroupField
                  newLineForAnswers
                  options={selectValues(schema, 'notPrepared')}
                  required
                  label="I am not prepared to give you the requested specific information / specific information and for this reason I do not want to receive any relevant advice"
                  name="notPrepared"
                />
                <RadioGroupField
                  newLineForAnswers
                  options={selectValues(schema, 'notWantAdvice')}
                  required
                  label="I do not want to receive advice beyond the legally prescribed minimum"
                  name="notWantAdvice"
                />
                <RadioGroupField
                  newLineForAnswers
                  options={selectValues(schema, 'notWantReceiveRelevantAdvice')}
                  required
                  label="I give instructions concerning investments by way of telecommunication (by telephone). Therefore, I do not want to receive any relevant advice pursuant to a separate agreement"
                  name="notWantReceiveRelevantAdvice"
                />
                <RadioGroupField
                  newLineForAnswers
                  options={selectValues(schema, 'confirmInformed')}
                  required
                  label="I hereby confirm that I have been informed about all important facts about the market and about the risks (especially potential price/exchange risks and the possibility of changes in tax legislation)"
                  name="confirmInformed"
                />
              </div>
            </div>
          </Scrollbar>
        </div>
      </div>
    </div>
  );
}


Step9.propTypes = {
  schema: PropTypes.object.isRequired,
};


const mapStateToProps = createStructuredSelector({
  schema: makeSelectPersonSchema(9),
  initialValues: makeSelectInitialValues(9),
});


const form = reduxForm({
  form: 'verificationFormStep9',
  validate: formValidator,
  destroyOnUnmount: false,
  forceUnregisterOnUnmount: true,
})(Step9);

export default connect(mapStateToProps, null)(form);
