import React from 'react';
import PropTypes from 'prop-types';
import ImmutableProptypes from 'react-immutable-proptypes';
// import moment from 'moment';
import { FormattedHTMLMessage, FormattedMessage } from 'react-intl';
// import Dropzone from 'react-dropzone';
import { reduxForm, FieldArray } from 'redux-form/immutable';
import { createStructuredSelector } from 'reselect';
import { connect } from 'react-redux';

import Scrollbar from 'Components/Scrollbar';
import { BtnGroupField, RadioGroupField, InputField, DateRangeInputField } from '../FormComponents';
// import { CommunicationMethodSelector } from '../CommonFields';

import '../../style.css';
import messages from '../../messages';

import { makeSelectPersonSchema, makeSelectInitialValues } from '../../selectors';

import { formValidator } from '../../../../utils/jsonSchemaUtils';

const currencies = [
  { value: 'USD', label: 'USD' },
  { value: 'EUR', label: 'EUR' },
];


const yesNo = [
  { value: 'Yes', label: 'Yes' },
  { value: 'No', label: 'No' },
];

function FamilyMember({ fields, disabled }) {
  /* eslint-disable jsx-a11y/no-static-element-interactions */
  return (
    <div styleName="mini">
      {
        fields.map((member, index) =>
          (<div key={`member_${index + 1}`} styleName={`family-member ${index === 0 ? '' : 'family-member-bordered'}`}>
            {index > 0 && <span styleName={`delete-icon ${disabled ? 'disabled' : ''}`} onClick={() => disabled ? {} : fields.remove(index)}><i className="icon-icon-cancel"></i></span>}
            <div>
              <InputField disabled={disabled} mini name={`${member}.fullName`} label="Full name" required />
              <InputField disabled={disabled} mini name={`${member}.intership`} label="Relationship" required placeholder="Fill in the relationship for this person" />
            </div>
          </div>))
      }
      <div styleName={`add-person ${disabled ? 'disabled' : ''}`}>
        <span onClick={() => disabled ? {} : fields.push({})}><FormattedMessage {...messages.addPerson} /></span>
      </div>
    </div>
  );
  /* eslint-enable */
}

FamilyMember.propTypes = {
  fields: PropTypes.object,
  disabled: PropTypes.bool,
};

function FamilyMembers(props) {
  return (<FieldArray component={FamilyMember} {...props} />);
}


function Step3({ handleSubmit, formValues }) {
  return (
    <form styleName="form-step-3" onSubmit={handleSubmit}>
      <p className="text-center italic"><FormattedHTMLMessage {...messages.fieldsRequiredNote} /></p>
      <p className="text-center italic"><FormattedMessage {...messages.fatcaInfoNote} /></p>
      <div styleName="form-step-container">
        <div styleName="form-step">
          <Scrollbar style={{ position: 'static' }}>
            <div styleName="form-fields-container">
              <BtnGroupField options={currencies} name="fatcaCurrency" label="Specify all amounts in answers in" />
              <RadioGroupField newLineForAnswers options={yesNo} name="usCitizen" required label="Are you a United States citizen or permanent resident?" />
              <RadioGroupField newLineForAnswers options={yesNo} name="usIncome" required label="Do you pay your U.S. annual income?">
                <InputField disabled={formValues.get('usIncome') !== 'Yes'} placeholder="Fill in TIN" name="usIncomeDetails" mini required label="TIN" />
              </RadioGroupField>
              <div styleName="full-width">
                <RadioGroupField newLineForAnswers options={yesNo} name="usNative" required label="Are you a United States of America native?">
                  <span styleName="sub-text"><FormattedMessage {...messages.specifyUsCitizenship} /></span>
                  <InputField disabled={formValues.get('usNative') !== 'Yes'} name="usNativeDetails" placeholder="Provide the explanation" />
                </RadioGroupField>
              </div>
              <RadioGroupField newLineForAnswers name="greenCard" options={yesNo} required label="Have you applied for the Green Card or requested an American citizenship?" />
              <RadioGroupField newLineForAnswers name="usFinancialInterest" options={yesNo} required label="Do you have any financial or economic interests in the United States of America?">
                <InputField disabled={formValues.get('usFinancialInterest') !== 'Yes'} mini name="usFinancialInterestNature" label="Nature" required placeholder="Fill in the nature of your interest" />
                <InputField disabled={formValues.get('usFinancialInterest') !== 'Yes'} mini name="usFinancialInterestEstimatedValue" label="Estimated value" required postfix={formValues.get('fatcaCurrency')} />
                <InputField disabled={formValues.get('usFinancialInterest') !== 'Yes'} mini name="usFinancialInterestAnnualReturns" label="Annual returns" required postfix={formValues.get('fatcaCurrency')} />
                <InputField disabled={formValues.get('usFinancialInterest') !== 'Yes'} mini name="usFinancialInterestProfitsResulting" label="Profits resulting" required postfix={formValues.get('fatcaCurrency')} />
              </RadioGroupField>
              <RadioGroupField newLineForAnswers name="usFamilyCitizen" options={yesNo} required label="Does one of your family members (husband, wife, parents, brother, sister etc.) hold the American citizenship?">
                <FamilyMembers disabled={formValues.get('usFamilyCitizen') !== 'Yes'} name="usFamilyCitizens" />
              </RadioGroupField>
              <RadioGroupField newLineForAnswers name="usVisited" options={yesNo} required label="Have you visited the United States during the five last years?">
                <InputField disabled={formValues.get('usVisited') !== 'Yes'} name="usVisitReason" label="Reason" required placeholder="Fill in the reason for the visit" />
              </RadioGroupField>
              <RadioGroupField newLineForAnswers name="usReside" options={yesNo} required label="Did you reside in the United States?">
                <DateRangeInputField disabled={formValues.get('usReside') !== 'Yes'} name="usResideDuration" label="Duration" required />
                <InputField disabled={formValues.get('usReside') !== 'Yes'} name="usResideState" label="State" required placeholder="Fill in the state for full mailing address" />
                <InputField disabled={formValues.get('usReside') !== 'Yes'} name="usResideStreet" label="Street" required placeholder="Fill in the street for full mailing address" />
                <InputField disabled={formValues.get('usReside') !== 'Yes'} name="usResideProvince" label="Province" required placeholder="Fill in the province for full mailing address" />
              </RadioGroupField>
            </div>
          </Scrollbar>
        </div>
      </div>
    </form>
  );
}

Step3.propTypes = {
  handleSubmit: PropTypes.func.isRequired,
  formValues: ImmutableProptypes.map,
  // schema: PropTypes.object,
};

const mapStateToProps = createStructuredSelector({
  schema: makeSelectPersonSchema(3),
  initialValues: makeSelectInitialValues(3, {
    fatcaCurrency: 'USD',
    usFamilyCitizens: [{}],
  }),
});

const form = reduxForm({
  form: 'verificationFormStep3',
  validate: formValidator,
  destroyOnUnmount: false,
  forceUnregisterOnUnmount: true,
})(Step3);

export default connect(mapStateToProps, null)(form);
