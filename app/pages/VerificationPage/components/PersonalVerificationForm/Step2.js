import React from 'react';
import ImmutableProptypes from 'react-immutable-proptypes';
import PropTypes from 'prop-types';
import { Map } from 'immutable';
import { FormattedHTMLMessage, FormattedMessage } from 'react-intl';
import { reduxForm } from 'redux-form/immutable';
import { createStructuredSelector } from 'reselect';
import { connect } from 'react-redux';

import Scrollbar from 'Components/Scrollbar';
import { InputField, RadioGroupField, CheckboxField, SelectInputField } from '../FormComponents';
import TabsField from '../Tabs';
import '../../style.css';
import messages from '../../messages';

import { makeSelectFormErrors, makeSelectPersonSchema, makeSelectInitialValues } from '../../selectors';

import { selectValues, formValidator } from '../../../../utils/jsonSchemaUtils';

const REASON_AVAILABLE_VALUE = 'The Account Holder is otherwise unable to obtain a TIN or equivalent number.';

const commentsForOptions = [
  {
    forValue: 'No TIN is required.',
    text: 'Only select this reason if the authorities of the country of tax residence you chose do not require the TIN to be disclosed.',
  },
];

function CrsCountry({ field, schema, formValues = new Map() }) {
  const tinNotAvailableReasonOptions = selectValues(schema, 'crsCountry.items.properties.tinNotAvailableReason');

  return (
    <div>
      <SelectInputField name={`${field}.country`} label="Country of tax residence" required disableClearable options={selectValues(schema, 'crsCountry.items.properties.country')} />
      <InputField placeholder="Fill in TIN" disabled={formValues.get('tinNotAvailable')} name={`${field}.tin`} label="TIN" required />
      <div styleName="no-label-checkbox">
        <CheckboxField name={`${field}.tinNotAvailable`} label="No TIN is available" />
      </div>
      <RadioGroupField
        disabled={!formValues.get('tinNotAvailable')}
        name={`${field}.tinNotAvailableReason`}
        label="If no TIN is available provide the reason"
        options={tinNotAvailableReasonOptions}
        commentsForOptions={commentsForOptions}
        required
      >
        <div styleName="input-inner">
          <div styleName="sub-text" style={{ padding: '0 0 5px 30px' }}>
            Please explain why you are unable to obtain a TIN below.
          </div>
          <InputField
            disabled={!formValues.get('tinNotAvailable') || formValues.get('tinNotAvailableReason') !== REASON_AVAILABLE_VALUE}
            name={`${field}.tinNotAvailableExplanation`}
            placeholder="Provide the explanation"
          />
        </div>
      </RadioGroupField>
    </div>
  );
}

CrsCountry.propTypes = {
  field: PropTypes.string.isRequired,
  schema: PropTypes.object,
  formValues: ImmutableProptypes.map,

};

function Step2({
  handleSubmit, schema, formValues, errors,
}) {
  return (
    <form styleName="form-step-2" onSubmit={handleSubmit}>
      <p className="text-center italic"><FormattedHTMLMessage {...messages.fieldsRequiredNote} /></p>
      <p className="text-center italic"><FormattedMessage {...messages.tinInfoNote} /></p>
      <div styleName="form-step-container">
        <div styleName="form-step">
          <Scrollbar style={{ position: 'static' }}>
            <div styleName="form-fields-container">
              <TabsField
                tabPrefix="Country"
                name="crsCountry"
                errors={errors.crsCountry}
                renderTab={(field, idx) => <CrsCountry formValues={formValues.getIn(['crsCountry', idx])} schema={schema} field={field} />}
              />
            </div>
          </Scrollbar>
        </div>
      </div>
    </form>
  );
}

Step2.propTypes = {
  handleSubmit: PropTypes.func.isRequired,
  schema: PropTypes.object,
  formValues: ImmutableProptypes.map,
  errors: PropTypes.object,
};

const mapStateToProps = createStructuredSelector({
  schema: makeSelectPersonSchema(2),
  initialValues: makeSelectInitialValues(2, {
    crsCountry: [{}],
  }),
  errors: makeSelectFormErrors(2),
});

const form = reduxForm({
  form: 'verificationFormStep2',
  validate: formValidator,
  destroyOnUnmount: false,
  forceUnregisterOnUnmount: true,
})(Step2);

export default connect(mapStateToProps, null)(form);
