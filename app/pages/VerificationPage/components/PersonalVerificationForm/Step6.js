import React from 'react';
import ImmutableProptypes from 'react-immutable-proptypes';
import PropTypes from 'prop-types';
import { FormattedHTMLMessage } from 'react-intl';
import { connect } from 'react-redux';
import { reduxForm } from 'redux-form/immutable';
import { createStructuredSelector } from 'reselect';

import Scrollbar from 'Components/Scrollbar';
import { CheckboxGroupField, BtnGroupField, RadioGroupField, InputField } from '../FormComponents';

import {} from '../../style.css';
import messages from '../../messages';

import { makeSelectPersonSchema, makeSelectInitialValues } from '../../selectors';

import { selectValues, formValidator } from '../../../../utils/jsonSchemaUtils';

const valueSymbols = {
  USD: '$',
  EUR: '€',
};

function Step6({ schema, formValues }) {
  return (
    <div styleName="form-step-6">
      <p className="text-center italic"><FormattedHTMLMessage {...messages.fieldsRequiredNote} /></p>
      <div styleName="form-step-container">
        <div styleName="form-step">
          <Scrollbar style={{ position: 'static' }}>
            <div styleName="form-fields-container">
              <BtnGroupField options={selectValues(schema, 'investmentCurrency')} name="investmentCurrency" label="Specify all amounts in answers in" />
              <CheckboxGroupField required name="investmentObjectives" options={selectValues(schema, 'investmentObjectives.items')} label="Investment objectives" />
              <RadioGroupField required multiRow options={selectValues(schema, 'frequencyOfProspective')} name="frequencyOfProspective" label="Frequency of prospective financial transaction" />
              <RadioGroupField required multiRow options={selectValues(schema, 'intendedAmount', { currency: valueSymbols[formValues.get('investmentCurrency')] })} name="intendedAmount" label="Intended to invest amount" />
              <RadioGroupField required multiRow options={selectValues(schema, 'accountTurnover', { currency: valueSymbols[formValues.get('investmentCurrency')] })} name="accountTurnover" label="Anticipated account turnover" />
              <RadioGroupField required multiRow options={selectValues(schema, 'riskLevel')} name="riskLevel" label="Level of risk willing to take" />
              <InputField multiRow name="anyInvestments" label="Any investments to avoid" placeholder="Fill in investments you would like to avoid" />
            </div>
          </Scrollbar>
        </div>
      </div>
    </div>
  );
}

Step6.propTypes = {
  schema: PropTypes.object.isRequired,
  formValues: ImmutableProptypes.map,
};


const mapStateToProps = createStructuredSelector({
  schema: makeSelectPersonSchema(6),
  initialValues: makeSelectInitialValues(6, {
    familyMembersInGovHeldPositionPrevYear: [{}],
    familyMembersInGovHoldPosition: [{}],
    investmentCurrency: 'USD',
  }),
});


const form = reduxForm({
  form: 'verificationFormStep6',
  validate: formValidator,
  destroyOnUnmount: false,
  forceUnregisterOnUnmount: true,
})(Step6);

export default connect(mapStateToProps, null)(form);
