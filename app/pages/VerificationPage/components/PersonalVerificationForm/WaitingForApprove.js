import React from 'react';
import { connect } from 'react-redux';
import { createStructuredSelector } from 'reselect';
import ImmutableProptypes from 'react-immutable-proptypes';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';
import { reduxForm } from 'redux-form/immutable';
import { QUESTIONNARIE_STATUS } from 'Model/enums';


import '../../style.css';
import FileInputField from '../FileInput';
import { makeSelectFiles } from '../../selectors';
import { loadFiles } from '../../actions';

class WaitingForApprove extends React.PureComponent {
  state = {};
  renderDefault() {
    const { status } = this.props;
    return status === QUESTIONNARIE_STATUS.VERIFICATION_IN_PROGRESS
      ? <div>
        <p styleName="on-verification-head-text">PROCESSING DATA</p>
        <p styleName="on-verification-text">We are verifying your information.<br />The process can take up to 15 days.</p>
        <p styleName="on-verification">
          <i className="icon-icon-in-verification"></i>
          Your account is on Verification
        </p>
      </div>
      : <div>
        <div styleName="rejected">
          <div>Your questionary rejected.<br />Please contact support to receive details.</div>
        </div>
      </div>;
  }

  renderFileUpload() {
    const { files, additionalFilesForm } = this.props;
    return <div>
      <p styleName="on-verification-head-text">UPLOAD ADDITIONAL<br />DOCUMENTS</p>
      <form>
        <FileInputField
          savedIds={additionalFilesForm.get('additionalFiles')}
          savedFiles={files}
          name="additionalFiles"
          onFileLoaded={this.props.loadFiles}
        />
      </form>
    </div>;
  }

  render() {
    const { upload } = this.state;
    return (
      <div styleName="form">
        <div styleName="verification-in-process">
          <div styleName="logo-container"></div>
          {!upload ? this.renderDefault() : this.renderFileUpload()}
          <div styleName="logout-link">
            <Link to="/logout">Log out</Link>
          </div>
        </div>
      </div>
    );
  }
}

WaitingForApprove.propTypes = {
  status: PropTypes.number.isRequired,
  loadFiles: PropTypes.func,
  additionalFilesForm: ImmutableProptypes.map,
  files: ImmutableProptypes.map,
};
const mapStateToProps = createStructuredSelector({
  files: makeSelectFiles(),
});

const mapDispatchToProps = (dispatch) => ({
  loadFiles: (items) => dispatch(loadFiles.success({ items })),
});
const form = reduxForm({
  form: 'onVerificationForm',
  forceUnregisterOnUnmount: true,
})(WaitingForApprove);

export default connect(mapStateToProps, mapDispatchToProps)(form);
