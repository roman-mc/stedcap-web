import React from 'react';
import PropTypes from 'prop-types';
import ImmutableProptypes from 'react-immutable-proptypes';
import { FormattedMessage, FormattedHTMLMessage } from 'react-intl';
import { connect } from 'react-redux';
import { createStructuredSelector } from 'reselect';

import { reduxForm, FieldArray } from 'redux-form/immutable';

import Scrollbar from 'Components/Scrollbar';
import { RadioGroupField, InputField, DateRangeInputField } from '../FormComponents';

import '../../style.css';
import messages from '../../messages';

import { makeSelectPersonSchema, makeSelectInitialValues } from '../../selectors';

import { selectValues, formValidator } from '../../../../utils/jsonSchemaUtils';

function FamilyMember({ fields, disabled }) {
  /* eslint-disable jsx-a11y/no-static-element-interactions */
  return (
    <div styleName="mini">
      {
        fields.map((member, index) =>
          (<div key={`member_${index + 1}`} styleName={`family-member ${index === 0 ? '' : 'family-member-bordered'}`}>
            {index > 0 && <span styleName={`delete-icon ${disabled ? 'disabled' : ''}`} onClick={() => disabled ? {} : fields.remove(index)}><i className="icon-icon-cancel"></i></span>}
            <div>
              <InputField disabled={disabled} mini name={`${member}.fullName`} label="Full name" required />
              <InputField disabled={disabled} mini name={`${member}.position`} label="Position" required />
              <DateRangeInputField disabled={disabled} mini name={`${member}.duration`} label="Duration" required />
            </div>
          </div>))
      }
      <div styleName={`add-person ${disabled ? 'disabled' : ''}`}>
        <a onClick={() => disabled ? {} : fields.push({})}><FormattedMessage {...messages.addPerson} /></a>
      </div>
    </div>
  );
  /* eslint-enable */
}

FamilyMember.propTypes = {
  fields: PropTypes.object,
  disabled: PropTypes.bool,
};

function FamilyMembers(props) {
  return (<FieldArray component={FamilyMember} {...props} />);
}

function Step5({ schema, formValues }) {
  return (
    <div styleName="form-step-5">
      <p className="text-center italic"><FormattedHTMLMessage {...messages.fieldsRequiredNote} /></p>
      <div styleName="form-step-container">
        <div styleName="form-step">
          <Scrollbar style={{ position: 'static' }}>
            <div styleName="form-fields-container">
              <RadioGroupField options={selectValues(schema, 'familyInGovHeldPositionPrevYear')} newLineForAnswers required name="inGovPrevYear" label="Did you hold a position in any governmental body during the previous one year?" />
              <RadioGroupField options={selectValues(schema, 'familyInGovHeldPositionPrevYear')} newLineForAnswers required name="familyInGovHoldPosition" label="Do any of your immediate family members or close associates hold a position in any governmental body?">
                <FamilyMembers disabled={formValues.get('familyInGovHoldPosition') !== 'Yes'} name="familyMembersInGovHoldPosition" />
              </RadioGroupField>
              <RadioGroupField options={selectValues(schema, 'familyInGovHeldPositionPrevYear')} newLineForAnswers required name="familyInGovHeldPositionPrevYear" label="Did any of your immediate family members or close associates held a position in any governmental body during the previous year?">
                <FamilyMembers disabled={formValues.get('familyInGovHeldPositionPrevYear') !== 'Yes'} name="familyMembersInGovHeldPositionPrevYear" />
              </RadioGroupField>
            </div>
          </Scrollbar>
        </div>
      </div>
    </div>
  );
}

Step5.propTypes = {
  schema: PropTypes.object.isRequired,
  formValues: ImmutableProptypes.map,
};


const mapStateToProps = createStructuredSelector({
  schema: makeSelectPersonSchema(5),
  initialValues: makeSelectInitialValues(5, {
    familyMembersInGovHeldPositionPrevYear: [{}],
    familyMembersInGovHoldPosition: [{}],
  }),
});


const form = reduxForm({
  form: 'verificationFormStep5',
  validate: formValidator,
  destroyOnUnmount: false,
  forceUnregisterOnUnmount: true,
})(Step5);

export default connect(mapStateToProps, null)(form);
