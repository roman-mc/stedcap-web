import React from 'react';
import ImmutableProptypes from 'react-immutable-proptypes';
import { FormattedHTMLMessage } from 'react-intl';
import { connect } from 'react-redux';
import { createStructuredSelector } from 'reselect';
import { reduxForm } from 'redux-form/immutable';
import PropTypes from 'prop-types';

import Scrollbar from 'Components/Scrollbar';
import { BtnGroupField, RadioGroupField, CheckboxGroupField, InputField, TextAreaField } from '../FormComponents';
// import { CurrencySelector } from '../CommonFields';
import '../../style.css';
import messages from '../../messages';

import { makeSelectPersonSchema, makeSelectInitialValues } from '../../selectors';

import { selectValues, formValidator } from '../../../../utils/jsonSchemaUtils';

const valueSymbols = {
  USD: '$',
  EUR: '€',
};

function Step4({ schema, formValues }) {
  return (
    <div styleName="form-step-4">
      <p className="text-center italic"><FormattedHTMLMessage {...messages.fieldsRequiredNote} /></p>
      <div styleName="form-step-container">
        <div styleName="form-step">
          <Scrollbar style={{ position: 'static' }}>
            <div styleName="form-fields-container">
              <BtnGroupField options={selectValues(schema, 'currency')} name="currency" label="Specify all amounts in answers in" />
              <RadioGroupField multiRow options={selectValues(schema, 'employmentStatus')} required label="Employment status" name="employmentStatus" />
              <InputField disabled={formValues.get('employmentStatus') === 'Unemployed' || formValues.get('employmentStatus') === 'Self-employed'} required label="Name of employer" name="nameOfEmployer" placeholder="Fill in full name of the employer" />
              <InputField required label="Occupation" name="occupation" />
              <CheckboxGroupField multiRow options={selectValues(schema, 'educationLevel.items')} required label="Level of education" name="educationLevel" />
              <RadioGroupField multiRow options={selectValues(schema, 'wealthSize', { currency: valueSymbols[formValues.get('currency')] })} required label="Size of wealth (savings and brokerage accounts, financial assets, real estates)" name="wealthSize" />
              <CheckboxGroupField multiRow options={selectValues(schema, 'sourceOfFund.items')} required label="Source of funds and/or wealth" name="sourceOfFund" />
              <TextAreaField required label="Further description of source of funds and/or wealth" name="sourceOfFundOther" placeholder="Fill in further description" />
              <RadioGroupField multiRow options={selectValues(schema, 'totalAnnualIncome', { currency: valueSymbols[formValues.get('currency')] })} required label="Total annual income" name="totalAnnualIncome" />
            </div>
          </Scrollbar>
        </div>
      </div>
    </div>
  );
}

Step4.propTypes = {
  schema: PropTypes.object,
  formValues: ImmutableProptypes.map,
};

const mapStateToProps = createStructuredSelector({
  schema: makeSelectPersonSchema(4),
  initialValues: makeSelectInitialValues(4, {
    currency: 'USD',
  }),
});

const form = reduxForm({
  form: 'verificationFormStep4',
  validate: formValidator,
  destroyOnUnmount: false,
  forceUnregisterOnUnmount: true,
})(Step4);

export default connect(mapStateToProps, null)(form);
