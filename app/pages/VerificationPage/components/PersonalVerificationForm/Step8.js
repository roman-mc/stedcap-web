import { connect } from 'react-redux';
import ImmutableProptypes from 'react-immutable-proptypes';
import React from 'react';
import PropTypes from 'prop-types';
import { createStructuredSelector } from 'reselect';
import { reduxForm } from 'redux-form/immutable';
import { FormattedMessage, injectIntl, intlShape } from 'react-intl';


import Scrollbar from 'Components/Scrollbar';
import { BtnGroupField, InputField, CheckboxField } from '../FormComponents';
import FileInput from '../FileInput';
import { formValidator } from '../../../../utils/jsonSchemaUtils';
// import formValidator from './validator';

import {
  makeSelectPersonSchema,
  makeSelectInitialValues,
  makeSelectFiles,
  makeSelectIsPaymentMethodExist,
} from '../../selectors';
import { loadFiles } from '../../actions';

import messages from '../../messages';
import {} from '../../style.css';


const availableCurrencies = ['USD', 'EUR', 'RUB'];

export class Step8 extends React.PureComponent {
  renderCommonBank = () => {
    const { intl, isPaymentMethodExist } = this.props;
    const disabled = isPaymentMethodExist;
    return (
      <div>
        <InputField
          name="correspondentBankName"
          placeholder={intl.formatMessage(messages.correspondentBank)}
          label={<FormattedMessage {...messages.correspondentBank} />}
          disabled={disabled}
        />
        <InputField
          name="correspondentBankSwift"
          placeholder={intl.formatMessage(messages.swift)}
          label={<FormattedMessage {...messages.swift} />}
          disabled={disabled}
        />
        <InputField
          name="correspondentBankAccount"
          placeholder={intl.formatMessage(messages.account)}
          label={<FormattedMessage {...messages.account} />}
          disabled={disabled}
        />
        <InputField
          name="beneficiaryBankBeneficiary"
          placeholder={intl.formatMessage(messages.beneficiaryBank)}
          label={<FormattedMessage {...messages.beneficiaryBank} />}
          required
          disabled={disabled}
        />
        <InputField
          name="beneficiaryBankSwift"
          placeholder={intl.formatMessage(messages.swift)}
          label={<FormattedMessage {...messages.swift} />}
          required
          disabled={disabled}
        />
        <InputField
          name="beneficiaryBankName"
          placeholder={intl.formatMessage(messages.beneficiary)}
          label={<FormattedMessage {...messages.beneficiary} />}
          required
          disabled={disabled}
        />
        <InputField
          name="beneficiaryBankAccountNumber"
          placeholder={intl.formatMessage(messages.accountNumber)}
          label={<FormattedMessage {...messages.accountNumber} />}
          required
          disabled={disabled}
        />
        <InputField
          name="beneficiaryBankIbanCode"
          placeholder={intl.formatMessage(messages.iban)}
          label={<FormattedMessage {...messages.iban} />}
          required
          disabled={disabled}
        />
      </div>
    );
  }

  renderRusBank = () => {
    const { isPaymentMethodExist } = this.props;
    const disabled = isPaymentMethodExist;
    return (
      <div>
        <InputField
          name="rusBankBik"
          placeholder="BIK"
          label="BIK"
          required
          disabled={disabled}
        />
        <InputField
          name="rusBankBeneficiaryBank"
          placeholder="Beneficiary bank"
          label="Beneficiary bank"
          required
          disabled={disabled}
        />
        <InputField
          name="rusBankCorrBankAccount"
          placeholder="Corr. bank account"
          label="Corr. bank account"
          disabled={disabled}
        />
        <InputField
          name="rusBankBankAddress"
          placeholder="Bank address"
          label="Bank address"
          disabled={disabled}
        />
        <InputField
          name="rusBankBeneficiaryName"
          placeholder="Beneficiary name"
          label="Beneficiary name"
          required
          disabled={disabled}
        />
        <InputField
          name="rusBankBeneficiaryAddress"
          placeholder="Beneficiary address"
          label="Beneficiary address"
          required
          disabled={disabled}
        />
        <InputField
          name="rusBankBeneficiaryAccountNo"
          placeholder="Beneficiary account no"
          label="Beneficiary account no"
          required
          disabled={disabled}
        />
        <InputField
          name="rusBankBeneficiaryTaxCode"
          placeholder="Beneficiary tax code (INN)"
          label="Beneficiary tax code (INN)"
          required
          disabled={disabled}
        />
        <InputField
          name="rusBankKppCode"
          placeholder="KPP code"
          label="KPP code"
          disabled={disabled}
        />
        <InputField
          name="rusBankVoCode"
          placeholder="VO code"
          label="VO code"
          disabled={disabled}
        />
      </div>
    );
  }


  render() {
    const {
      intl, handleSubmit, formValues, files, loadFiles: doLoadFiles, isPaymentMethodExist,
    } = this.props;
    const disabled = isPaymentMethodExist;

    return (
      <form styleName="form-step-8" onSubmit={handleSubmit}>
        <p className="text-center italic">Adding payment method</p>
        {disabled
          && <p className="text-center italic">
                        You already provided payment details.<br />
                        If you want to add another payment method you can do it after successful verification.<br /><br />
          </p>
        }
        <div styleName="form-step-container">
          <div styleName="form-step">
            <Scrollbar style={{ position: 'static' }}>
              <div styleName="form-fields-container">
                <BtnGroupField
                  name="paymentType"
                  options={availableCurrencies.map((cur) => ({ label: cur, value: cur }))}
                  label={<FormattedMessage {...messages.currency} />}
                  required
                  disabled={disabled}
                />

                {(formValues && formValues.get('paymentType') === 'RUB')
                  && <div styleName="no-label-checkbox">
                    <CheckboxField
                      name="isRussianBank"
                      label="Bank is based in Russian Federation"
                      disabled={disabled}
                    />
                  </div>
                }
                {(formValues && formValues.get('paymentType') === 'RUB' && formValues.get('isRussianBank'))
                  ? this.renderRusBank()
                  : this.renderCommonBank()
                }
                <InputField
                  name="paymentDetails"
                  placeholder={intl.formatMessage(messages.paymentDetails)}
                  label={<FormattedMessage {...messages.paymentDetails} />}
                  required
                  disabled={disabled}
                />
              </div>
            </Scrollbar>
          </div>
          <div styleName="file-upload">
            <FileInput
              savedIds={formValues.get('documents')}
              savedFiles={files}
              name="documents"
              placeholder="Select files or drop them here"
              label={messages.proofOfAffiliation}
              description={messages.proofOfAffiliationDescription}
              required
              onFileLoaded={doLoadFiles}
              disabled={disabled}
            />
            <div styleName="fields-required-title">
                            Note that you can add only one payment right now. If you need several payment  methods, you can add them later in finance section.
            </div>
          </div>
        </div>
      </form>
    );
  }
}

Step8.propTypes = {
  handleSubmit: PropTypes.func.isRequired,
  intl: intlShape,
  formValues: ImmutableProptypes.map,
  files: ImmutableProptypes.map,
  loadFiles: PropTypes.func.isRequired,
  isPaymentMethodExist: PropTypes.bool,
};

const mapStateToProps = createStructuredSelector({
  schema: makeSelectPersonSchema('payment_method'),
  isPaymentMethodExist: makeSelectIsPaymentMethodExist(),
  files: makeSelectFiles(),
  initialValues: makeSelectInitialValues(8, {
    paymentType: 'USD',
    documents: [],
  }),
});


const mapDipsatchToProps = (dispatch) => ({
  loadFiles: (items) => dispatch(loadFiles.success({ items })),
});


const form = reduxForm({
  form: 'verificationFormStep8',
  validate: formValidator,
  destroyOnUnmount: false,
  forceUnregisterOnUnmount: true,
})(injectIntl(Step8));

export default connect(mapStateToProps, mapDipsatchToProps)(form);
