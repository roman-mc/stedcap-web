import React from 'react';
import PropTypes from 'prop-types';
import { FormattedMessage } from 'react-intl';

import '../style.css';


export default function Steps({
  titles, currentStep, stepsFilled, onGoToStep,
}) {
  /* eslint-disable */
  return (
    <div styleName="step-container">
      <div styleName="step-line"></div>
      <div styleName="steps">
        {titles.map((step, idx) =>
          (<div onClick={() => currentStep >= idx ? onGoToStep(idx) : {}} className={`step-${idx}`} styleName={`step ${(idx <= stepsFilled || currentStep === idx) ? 'active' : ''}`} key={`step_${idx}`}>
            <i styleName={`step-circle`}>
              { idx <= stepsFilled && <i className="icon-icon-check"></i>}
            </i>
            <span styleName={`step-text ${currentStep === idx ? 'active' : ''}`}><FormattedMessage {...step} /></span>
          </div>))}
      </div>
    </div>
  );
  /* eslint-enable */
}

Steps.propTypes = {
  titles: PropTypes.array,
  currentStep: PropTypes.number,
  stepsFilled: PropTypes.number.isRequired,
  onGoToStep: PropTypes.func.isRequired,
};
