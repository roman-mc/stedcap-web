import React from 'react';
import { Link } from 'react-router-dom';
import PropTypes from 'prop-types';
import { FormattedMessage, injectIntl, intlShape } from 'react-intl';

import { Button } from 'Components/AuthForm';

import messages from '../messages';

import '../style.css';

function FormWrapper({
  intl: { formatMessage }, verifyButton, titleMessage, onNextStep, renderSteps, children,
}) {
  return (
    <div styleName="form">
      <div styleName="header-wrapper">
        <header styleName="header">
          <Link to="/" styleName="logo">
            <div styleName="logo-img"></div>
          </Link>
          <span styleName="title">
            <FormattedMessage {...titleMessage} />
          </span>
          <span styleName="stub">
            <span styleName="logo">
              <div styleName="logo-img"></div>
              <div styleName="logo-text">
                unity
              </div>
            </span>
          </span>

        </header>
        {renderSteps()}
      </div>
      <div styleName="body">
        {children}
      </div>
      <div styleName="footer">
        <Button
          type="submit"
          className="open-account"
          text={formatMessage(verifyButton ? messages.verifyMe : messages.nextStepBtn)}
          onClick={onNextStep}
        />
        <div styleName="logout-link">
          <Link to="/logout">
            Log out
          </Link>
        </div>
      </div>
    </div>
  );
}

FormWrapper.propTypes = {
  intl: intlShape,
  verifyButton: PropTypes.bool.isRequired,
  titleMessage: PropTypes.object,
  onNextStep: PropTypes.func.isRequired,
  renderSteps: PropTypes.func.isRequired,
  children: PropTypes.node.isRequired,
};

export default injectIntl(FormWrapper);
