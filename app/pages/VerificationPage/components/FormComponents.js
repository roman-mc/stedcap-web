import React from 'react';
import PropTypes from 'prop-types';
import { Field } from 'redux-form/immutable';
import DatePicker from 'react-datepicker';
import ReactSelect from 'react-select';
import moment from 'moment';

import { STANDARD_DATE } from 'unity-frontend-core/utils/dateUtils';

import { } from '../style.css';

function ElementWrapper({
  label, disabled, children, wrapperClassName = '', required, meta: {
    touched, error, warning, submitFailed,
  } = {},
}) {
  /* eslint-disable jsx-a11y/label-has-for */
  const isError = Boolean(submitFailed && touched && (error || warning));

  return (
    <div className={`${isError ? 'has-error' : ''}`} styleName={`form-element-wrapper ${disabled ? 'disabled' : ''}  ${wrapperClassName}`}>
      <label styleName="label-wrapper">
        {label}
        {required && <span styleName="required-mark">
*
        </span>}
      </label>
      <div styleName={`input-wrapper ${wrapperClassName}`}>
        {children}
        {isError
          && <div styleName="input-error-text">
            {error}
          </div>
        }
      </div>
    </div>
  );
}

ElementWrapper.propTypes = {
  label: PropTypes.oneOfType([PropTypes.string, PropTypes.node]),
  children: PropTypes.node,
  wrapperClassName: PropTypes.string,
  required: PropTypes.bool,
  disabled: PropTypes.bool,
  meta: PropTypes.any,
};

function Input({
  input, label, required, type = 'text', disabled, mini, postfix, meta, placeholder,
}) {
  const miniClass = mini ? 'mini' : '';
  const hasPostfixClass = postfix ? 'has-postfix' : '';

  const placeholderText = placeholder || `Fill in ${label.toLowerCase()}`;

  return (
    <ElementWrapper wrapperClassName={`${miniClass} ${hasPostfixClass}`} disabled={disabled} label={label} required={required} meta={meta}>
      <input disabled={disabled} type={type} {...input} placeholder={placeholderText} styleName="form-input" />
      {postfix && <span styleName="postfix">
        {postfix}
      </span>}
    </ElementWrapper>
  );
}

Input.propTypes = {
  type: PropTypes.string,
  disabled: PropTypes.bool,
  mini: PropTypes.bool,
  input: PropTypes.object,
  label: PropTypes.oneOfType([PropTypes.string, PropTypes.node]),
  placeholder: PropTypes.string,
  postfix: PropTypes.string,
  required: PropTypes.bool,
  meta: PropTypes.any,
};

export function InputField(props) {
  return (
    <Field
      component={Input}
      {...props}
    />
  );
}

function TextArea({
  input, disabled, label, required, meta, placeholder,
}) {
  return (
    <ElementWrapper disabled={disabled} label={label} wrapperClassName="multi-row" required={required} meta={meta}>
      <textarea type="text" {...input} styleName="form-input" placeholder={placeholder} />
    </ElementWrapper>
  );
}

TextArea.propTypes = {
  input: PropTypes.object,
  label: PropTypes.oneOfType([PropTypes.string, PropTypes.node]),
  required: PropTypes.bool,
  disabled: PropTypes.bool,
  meta: PropTypes.any,
  placeholder: PropTypes.any,
};

export function TextAreaField(props) {
  return (
    <Field
      component={TextArea}
      {...props}
    />
  );
}

function RadioGroup({
  input, label: groupLabel, disabled, options, required, multiRow, newLineForAnswers, children, large, meta, commentsForOptions,
}) {
  const multiRowClass = multiRow ? 'multi-row' : '';
  const newLineClass = newLineForAnswers ? 'new-line' : '';
  const largeClass = large ? 'large' : '';

  return (
    <ElementWrapper label={groupLabel} disabled={disabled} wrapperClassName={`radio-group ${largeClass} ${multiRowClass} ${newLineClass}`} required={required} meta={meta}>

      {options.map(({ label, value }) => {
        const commentForOption = commentsForOptions ? commentsForOptions.find((comment) => comment.forValue === value) : undefined;

        return (
          <label styleName={`radio-group-label ${input.value === value ? 'active' : ''}`} key={`label_${value}`}>
            <input type="radio" disabled={disabled} {...input} onChange={() => input.onChange(value)} />
            {label}
            {commentForOption &&
              <p styleName="sub-text radio-option-comment">{commentForOption.text}</p>
            }
          </label>
        );
      })}

      {children &&
        <div styleName="radio-group-inner">
          {children}
        </div>
      }

    </ElementWrapper>
  );
}

RadioGroup.propTypes = {
  input: PropTypes.object,
  label: PropTypes.oneOfType([PropTypes.string, PropTypes.node]),
  required: PropTypes.bool,
  large: PropTypes.bool,
  disabled: PropTypes.bool,
  options: PropTypes.array,
  commentsForOptions: PropTypes.arrayOf(PropTypes.shape({
    forValue: PropTypes.any,
    text: PropTypes.string,
  })),
  multiRow: PropTypes.bool,
  newLineForAnswers: PropTypes.bool,
  children: PropTypes.node,
  meta: PropTypes.any,
};

export function RadioGroupField(props) {
  return (
    <Field
      component={RadioGroup}
      {...props}
    />
  );
}


function CheckboxGroup({
  input, label: groupLabel, disabled, options, required, children, meta,
}) {
  return (
    <ElementWrapper meta={meta} disabled={disabled} label={groupLabel} wrapperClassName="radio-group multi-row" required={required}>
      {options.map(({ label, value }) => (<label styleName={`checkbox-group-label ${input.value.indexOf(value) > -1 ? 'active' : ''}`} key={`label_${value}`}>
        <input
          type="checkbox"
          disabled={disabled}
          onChange={(event) => {
            const v = input.value.toArray ? input.value.toArray() : input.value;
            const newValue = [...(v || [])];
            if (event.target.checked) {
              newValue.push(value);
            } else {
              newValue.splice(newValue.indexOf(value), 1);
            }

            return input.onChange(newValue);
          }}
          checked={input.value.indexOf(value) > -1}
        />
        {label}
      </label>))}
      {children && <div styleName="radio-group-inner">
        {children}
      </div>}
    </ElementWrapper>
  );
}

CheckboxGroup.propTypes = {
  input: PropTypes.object,
  meta: PropTypes.object,
  label: PropTypes.oneOfType([PropTypes.string, PropTypes.node]),
  required: PropTypes.bool,
  disabled: PropTypes.bool,
  options: PropTypes.array,
  children: PropTypes.node,
};

export function CheckboxGroupField(props) {
  return (
    <Field
      component={CheckboxGroup}
      {...props}
    />
  );
}

function Checkbox({
  input, label, disabled, required, meta: {
    touched, error, warning, submitFailed,
  } = {},
}) {
  /* eslint-disable jsx-a11y/label-has-for */
  const isError = Boolean(submitFailed && touched && (error || warning));

  return (
    <div className={`${isError ? 'has-error' : ''}`} styleName="checkbox-container" disabled={disabled}>
      <label styleName={`checkbox-group-label ${input.value ? 'active' : ''}`}>
        <input
          type="checkbox"
          {...input}
          disabled={disabled}
          onChange={(event) => input.onChange(event.target.checked)}
        />
        {label}
        {required && <span styleName="required-mark">
*
        </span>}
      </label>
      {isError
        && <div styleName="input-error-text" className="input-error">
          {error}
        </div>
      }
    </div>
  );
}

Checkbox.propTypes = {
  input: PropTypes.object,
  label: PropTypes.oneOfType([PropTypes.string, PropTypes.node]),
  required: PropTypes.bool,
  disabled: PropTypes.bool,
  meta: PropTypes.object.isRequired,
};

export function CheckboxField(props) {
  return (
    <Field
      component={Checkbox}
      {...props}
    />
  );
}

function BtnGroup({
  input, label: groupLabel, disabled, options, required, meta,
}) {
  return (
    <ElementWrapper label={groupLabel} disabled={disabled} wrapperClassName="btn-group" required={required} meta={meta}>
      <div styleName="btn-group-container">
        {options.map(({ label, value }) => (<label styleName={`btn-group-label ${input.value === value ? 'active' : ''}`} key={`label_${value}`}>
          <input disabled={disabled} type="radio" {...input} onChange={() => input.onChange(value)} />
          {label}
        </label>))}
      </div>
    </ElementWrapper>
  );
}

BtnGroup.propTypes = {
  input: PropTypes.object,
  label: PropTypes.oneOfType([PropTypes.string, PropTypes.node]),
  required: PropTypes.bool,
  disabled: PropTypes.bool,
  options: PropTypes.array,
  meta: PropTypes.any,
};

export function BtnGroupField(props) {
  return (
    <Field
      component={BtnGroup}
      {...props}
    />
  );
}

function DateInput({
  input, micro, minDate, maxDate, disabled, label, required, mini, meta,
}) {
  const miniClass = mini ? 'mini' : '';
  const microClass = micro ? 'micro' : '';

  return (
    <ElementWrapper wrapperClassName={`${microClass} ${miniClass}`} disabled={disabled} label={label} required={required} meta={meta}>
      <DatePicker
        {...input}
        onChange={(d) => input.onChange(d.format(STANDARD_DATE))}
        dateFormat={STANDARD_DATE}
        selected={input.value ? moment(input.value, STANDARD_DATE) : null}
        peekNextMonth
        showMonthDropdown
        showYearDropdown
        dropdownMode="select"
        minDate={minDate}
        maxDate={maxDate}
        disabled={disabled}
        autoComplete={false}
      />
      <i styleName="input-icon" className="icon-icon-calendar"></i>
    </ElementWrapper>
  );
}

DateInput.propTypes = {
  input: PropTypes.object,
  label: PropTypes.oneOfType([PropTypes.string, PropTypes.node]),
  required: PropTypes.bool,
  micro: PropTypes.bool,
  disabled: PropTypes.bool,
  mini: PropTypes.bool,
  meta: PropTypes.any,
  minDate: PropTypes.object,
  maxDate: PropTypes.object,
};


export function DateInputField(props) {
  return (
    <Field
      component={DateInput}
      {...props}
    />
  );
}

function DateRangePickerSingle({
  disabled, input, minDate, maxDate, meta: {
    touched, error, warning, submitFailed,
  } = {},
}) {
  const isError = Boolean(submitFailed && touched && (error || warning));

  return (
    <div className={`${isError ? 'has-error' : ''}`}>
      <DatePicker
        {...input}
        onChange={(d) => input.onChange(d.format(STANDARD_DATE))}
        dateFormat={STANDARD_DATE}
        selected={input.value ? moment(input.value, STANDARD_DATE) : null}
        peekNextMonth
        showMonthDropdown
        showYearDropdown
        dropdownMode="select"
        minDate={minDate}
        maxDate={maxDate}
        disabled={disabled}
      />
      {isError
        && <div styleName="input-error-text" className="input-error">
          {error}
        </div>
      }
    </div>
  );
}

DateRangePickerSingle.propTypes = {
  input: PropTypes.object,
  minDate: PropTypes.object,
  maxDate: PropTypes.object,
  disabled: PropTypes.bool,
  meta: PropTypes.any,
};

function DateRangePickerSingleField(props) {
  return (
    <Field
      component={DateRangePickerSingle}
      {...props}
    />
  );
}

export function DateRangeInputField(props) {
  const miniClass = props.mini ? 'mini' : '';
  return (
    <ElementWrapper wrapperClassName={miniClass} label={props.label} disabled={props.disabled} required={props.required}>
      <div styleName="date-range-picker">
        <DateRangePickerSingleField disabled={props.disabled} name={`${props.name}Start`} />
        <i styleName="input-icon" className="icon-icon-calendar" style={{ paddingTop: '7px' }}></i>
        <div className="datepicker-right">
          <DateRangePickerSingleField disabled={props.disabled} name={`${props.name}End`} />
        </div>
        <i styleName="input-icon" className="icon-icon-calendar" style={{ paddingTop: '7px' }}></i>
      </div>
    </ElementWrapper>
  );
}

DateRangeInputField.propTypes = {
  label: PropTypes.oneOfType([PropTypes.string, PropTypes.node]).isRequired,
  required: PropTypes.bool,
  disabled: PropTypes.bool,
  name: PropTypes.string,
  mini: PropTypes.bool,
};

class SelectInput extends React.PureComponent {
  onChange = (value) => {
    this.props.input.onChange(value.value);
  }

  render() {
    const {
      options, label, disabled, input, disableClearable, required, meta,
    } = this.props;
    return (
      <ElementWrapper disabled={disabled} label={label} required={required} meta={meta}>
        <ReactSelect
          options={options}
          value={input.value}
          onChange={this.onChange}
          clearable={!disableClearable}
          disabled={disabled}
        />
      </ElementWrapper>
    );
  }
}

SelectInput.propTypes = {
  input: PropTypes.object,
  label: PropTypes.oneOfType([PropTypes.string, PropTypes.node]),
  required: PropTypes.bool,
  disabled: PropTypes.bool,
  options: PropTypes.array,
  disableClearable: PropTypes.bool,
  meta: PropTypes.any,
};

export function SelectInputField(props) {
  return (
    <Field
      component={SelectInput}
      {...props}
    />
  );
}
