import { createSelector } from 'reselect';
import { Map } from 'immutable';
import { makeSelectForms } from 'Containers/App/selectors';

const makeSelectState = () => (state) => state.get('verificationPage');

export const makeSelectStep = () => createSelector(
  makeSelectState(),
  (substate) => substate.get('step'),
);

export const makeSelectStepsFilled = () => createSelector(
  makeSelectState(),
  (substate) => substate.get('stepsFilled'),
);

export const makeSelectQuestionaryStatus = () => createSelector(
  makeSelectState(),
  (substate) => substate.get('status'),
);

export const makeSelectInitialValues = (step, defaultValues = new Map()) => createSelector(
  makeSelectState(),
  (substate) => substate.getIn(['initialValues', step], defaultValues),
);

export const makeSelectFiles = () => createSelector(
  makeSelectState(),
  (substate) => substate.get('files'),
);


export const makeSelectPersonSchema = (step) => createSelector(
  makeSelectState(),
  (substate) => substate.getIn(['personSchema', step]),
);

export const makeSelectFormValues = () => createSelector(
  makeSelectStep(),
  makeSelectForms,
  (step, forms) => forms.getIn([`verificationFormStep${step}`, 'values'], new Map())
);

export const makeSelectFormErrors = (step) => createSelector(
  makeSelectForms,
  (forms) => forms.getIn([`verificationFormStep${step}`, 'syncErrors'], {})
);
export const makeSelectAdditionalFilesFormValues = () => createSelector(
  makeSelectForms,
  (forms) => forms.getIn(['onVerificationForm', 'values'], new Map())
);

export const makeSelectIsPaymentMethodExist = () => createSelector(
  makeSelectState(),
  (substate) => substate.get('isPaymentMethodExist'),
);
