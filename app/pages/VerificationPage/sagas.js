import { delay } from 'redux-saga';
import { call, put, takeLatest, select, takeEvery } from 'redux-saga/effects';
import Raven from 'raven-js';

import * as verificationApi from 'unity-frontend-core/api/verificationApi';
import * as jsonSchemaApi from 'unity-frontend-core/api/jsonSchemaApi';
import * as paymentMethodsApi from 'unity-frontend-core/api/paymentMethodsApi';
import * as documentsApi from 'unity-frontend-core/api/documentsApi';

import { QUESTIONNARIE_STATUS } from 'Model/enums';

import { transformPaymentMethodData } from 'Utils/transformers';

import {
  savePersonalQuestionnaireStep, loadPersonSchema, loadDraftStep, loadQuestionaryStatus, setFormValue, loadFiles,
  setPaymentMethodExist,
} from './actions';
import { makeSelectStep, makeSelectInitialValues } from './selectors';


const SUBMIT_FAILED_ACTION = '@@redux-form/SET_SUBMIT_FAILED';

const QUESTIONARY_PERSON = 'person';
const PAYMENT_METHOD_STEP = 8;
const TOTAL_STEPS = 9;

function* savePersonalQuestionnaireStepHandler({ payload: { data } }) {
  try {
    const step = yield select(makeSelectStep());
    if (step > 0) {
      if (step === PAYMENT_METHOD_STEP) {
        const savedPm = yield select(makeSelectInitialValues(PAYMENT_METHOD_STEP));
        if (!savedPm || !savedPm.get('paymentType')) { // pm not saved yet
          const { id } = yield call(paymentMethodsApi.putPaymentMethod, transformPaymentMethodData(data));
          const payload = { pmId: id };
          yield call(verificationApi.submitStep, QUESTIONARY_PERSON, step, payload);
          yield put(setPaymentMethodExist({ isPaymentMethodExist: true }));
        }
      } else {
        yield call(verificationApi.submitStep, QUESTIONARY_PERSON, step, data);
      }
    }
    if (step === TOTAL_STEPS) {
      yield call(verificationApi.publishQuestionnary, QUESTIONARY_PERSON);
      yield put(loadQuestionaryStatus.success({ status: QUESTIONNARIE_STATUS.VERIFICATION_IN_PROGRESS }));
    } else {
      yield call(loadPersonSchemaSaga, step + 1);
      yield put(savePersonalQuestionnaireStep.success());
    }
  } catch (e) {
    console.warn(e);
    Raven.captureException(e);
  }
}


function* savePersonalQuestionnaireStepSaga() {
  yield takeLatest(savePersonalQuestionnaireStep.toString(), savePersonalQuestionnaireStepHandler);
}

function* loadDraftStepHandler() {
  try {
    const { status } = yield call(verificationApi.questionaryInfo, QUESTIONARY_PERSON);

    if (status === QUESTIONNARIE_STATUS.NEW || status === QUESTIONNARIE_STATUS.UPDATE_IS_NEEDED) {
      const files = yield call(documentsApi.getDocuments);
      yield put(loadFiles.success(files));
      const stepsCount = yield call(verificationApi.stepsCount, QUESTIONARY_PERSON);
      if (stepsCount > 0) {
        const steps = new Array(Math.min(stepsCount + 1, TOTAL_STEPS)).fill();
        yield steps.map((x, idx) => call(loadDataAndSchema, idx + 1, idx === steps.length - 1));
        yield put(loadDraftStep.success({ result: status === QUESTIONNARIE_STATUS.NEW ? stepsCount : 0 }));
      }
    }
    yield put(loadQuestionaryStatus.success({ status }));
  } catch (e) {
    yield put(loadDraftStep.success({ result: 0 }));
    yield put(loadQuestionaryStatus.success({ status: QUESTIONNARIE_STATUS.NEW }));
  }
}

function* loadDataAndSchema(step, skipDataLoading) {
  yield call(loadPersonSchemaSaga, step);
  if (!skipDataLoading) {
    try {
      const { content } = yield call(verificationApi.getStep, QUESTIONARY_PERSON, step);
      if (step === PAYMENT_METHOD_STEP) { // Load payment method schema
        const paymentMethod = yield call(paymentMethodsApi.getPaymentMethod, content.pmId);
        yield put(setFormValue({ step, data: paymentMethod.content }));
        yield put(setPaymentMethodExist({ isPaymentMethodExist: true }));
      } else {
        yield put(setFormValue({ step, data: content }));
      }
    } catch (e) {
      console.warn('load saved data error', e);
      Raven.captureException(e);
    }
  }
}

function* loadDraftStepSaga() {
  yield takeLatest(loadDraftStep.toString(), loadDraftStepHandler);
}

function* loadPersonSchemaSaga(step) {
  try {
    const schema = yield call(jsonSchemaApi.loadJsonSchema, 'questionary_person', step);
    yield put(loadPersonSchema.success({ step, schema }));
  } catch (e) {
    yield put(loadPersonSchema.error(e));
    Raven.captureException(e);
  }
}

function* loadPaymentMethodSchemaSaga() {
  try {
    const schema = yield call(jsonSchemaApi.loadJsonSchema, 'payment_method');
    yield put(loadPersonSchema.success({ step: 'payment_method', schema }));
  } catch (e) {
    yield put(loadPersonSchema.error(e));
    Raven.captureException(e);
  }
}

function* failedSubmitHandler() {
  yield call(delay, 90); // allow to render errors
  const container = document.querySelector('.scrollbar-container');
  const firstError = document.querySelector('.has-error');
  if (container && firstError) {
    container.scrollTo(0, firstError.offsetTop);
  }
}


function* failedSubmitSaga() {
  yield takeEvery(({ type, meta: { form = '' } = {} }) => type === SUBMIT_FAILED_ACTION && form.indexOf('verificationFormStep') === 0, failedSubmitHandler);
}

export default [
  savePersonalQuestionnaireStepSaga,
  loadDraftStepSaga,
  failedSubmitSaga,
  loadPaymentMethodSchemaSaga,
];
