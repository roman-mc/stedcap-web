import { createAction } from 'redux-act';
import createApiAction from '../../../../core/utils/createAction';

export const savePersonalQuestionnaireStep = createApiAction('app/containers/VerificationPage/SAVE_PERSONAL_QUESTIONNAIRE_STEP');
export const loadPersonSchema = createApiAction('app/containers/VerificationPage/LOAD_PERSON_SCHEMA');
export const loadPaymentMethodSchema = createApiAction('app/containers/VerificationPage/LOAD_PAYMENT_METHOD_SCHEMA');
export const loadDraftStep = createApiAction('app/containers/VerificationPage/LOAD_DRAFT_STEP');
export const loadQuestionaryStatus = createApiAction('app/containers/VerificationPage/LOAD_QUESTIONARY_STATUS');
export const goToStep = createApiAction('app/containers/VerificationPage/GO_TO_STEP');
export const setFormValue = createApiAction('app/containers/VerificationPage/SET_FORM_VALUE');
export const loadFiles = createApiAction('app/containers/VerificationPage/LOAD_FILES');

export const setPaymentMethodExist = createAction('app/containers/VerificationPage/SET_PAYMENT_METHOD_EXIST');
