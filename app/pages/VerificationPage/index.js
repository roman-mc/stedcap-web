import React from 'react';
import ImmutableProptyes from 'react-immutable-proptypes';
import { connect } from 'react-redux';
import { createStructuredSelector } from 'reselect';

import { makeSelectCurrentUser } from 'Containers/App/selectors';

import WithInjection from 'unity-frontend-core/containers/WithInjection';

import PersonalVerificationForm from './components/PersonalVerificationForm';
import CorporateVerificationForm from './components/CorporateVerificationForm';
import { PROFILE_TYPE } from '../../model/enums';

import reducer from './reducer';
import sagas from './sagas';

function VerificationPage({ currentUser }) {
  if (!currentUser || !currentUser.profileType) {
    return null;
  }
  return (
    currentUser.profileType === PROFILE_TYPE.corporate ? <CorporateVerificationForm /> : <PersonalVerificationForm />
  );
}

VerificationPage.propTypes = {
  currentUser: ImmutableProptyes.record,
};


const mapStateToProps = createStructuredSelector({
  currentUser: makeSelectCurrentUser(),
});

const Injected = WithInjection({ reducer, sagas, key: 'verificationPage' })(VerificationPage);

export default connect(mapStateToProps, null)(Injected);
