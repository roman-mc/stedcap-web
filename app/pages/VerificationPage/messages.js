import { defineMessages } from 'react-intl';

export default defineMessages({
  personVerification: {
    id: 'app.containers.VerificationPage.personVerification',
    defaultMessage: 'INDIVIDUAL VERIFICATION',
  },
  corporateVerification: {
    id: 'app.containers.VerificationPage.corporateVerification',
    defaultMessage: 'CORPORATE VERIFICATION',
  },
  openingAccount: {
    id: 'app.containers.VerificationPage.openingAccount',
    defaultMessage: 'Opening an account',
  },
  customerInformation: {
    id: 'app.containers.VerificationPage.customerInformation',
    defaultMessage: 'Customer information',
  },
  crsInformation: {
    id: 'app.containers.VerificationPage.crsInformation',
    defaultMessage: 'CRS self-certification',
  },
  fatcaInformation: {
    id: 'app.containers.VerificationPage.fatcaInformation',
    defaultMessage: 'FATCA self-certification',
  },
  education: {
    id: 'app.containers.VerificationPage.education',
    defaultMessage: 'Education and occupation',
  },
  politically: {
    id: 'app.containers.VerificationPage.politically',
    defaultMessage: 'Politically exposed persons',
  },
  investment: {
    id: 'app.containers.VerificationPage.investment',
    defaultMessage: 'Investment profile',
  },
  financialStatus: {
    id: 'app.containers.VerificationPage.financialStatus',
    defaultMessage: 'Financial status and experience',
  },
  paymentDetails: {
    id: 'app.containers.VerificationPage.paymentDetails',
    defaultMessage: 'Payment details',
  },
  step1DescriptionTitle1: {
    id: 'app.containers.VerificationPage.step1DescriptionTitle1',
    defaultMessage: 'Step 1: Online application',
  },
  step1DescriptionText1: {
    id: 'app.containers.VerificationPage.step1DescriptionText1',
    defaultMessage: 'Please fill in the electronic form with your personal and financial information, as applicable. Then, you will be asked to upload your proof of identity and proof of residency. This may take some time.',
  },
  step1DescriptionTitle2: {
    id: 'app.containers.VerificationPage.step1DescriptionTitle2',
    defaultMessage: 'Step 2: Verification process',
  },
  step1DescriptionText2: {
    id: 'app.containers.VerificationPage.step1DescriptionText2',
    defaultMessage: 'The verification process will be initiated once your online application and documents are submitted. It may take us up to 15 days to review your information provided. For any further clarifications required, a member of our team will contact you directly.',
  },
  step1DescriptionTitle3: {
    id: 'app.containers.VerificationPage.step1DescriptionTitle3',
    defaultMessage: 'Step 3: Electronic signing',
  },
  step1DescriptionText3: {
    id: 'app.containers.VerificationPage.step1DescriptionText3',
    defaultMessage: 'After the verification of your information, you will be provided with our terms and conditions. Once you carefully read and accept our terms, you may fund your account and start trading.',
  },
  step1DescriptionText3Part2: {
    id: 'app.containers.VerificationPage.step1DescriptionText3Part2',
    defaultMessage: 'Please note that we can accept or decline your application in establishing a business relationship with our Company without any explanations.',
  },
  corporateStep1DescriptionTitle1: {
    id: 'app.containers.VerificationPage.corporateStep1DescriptionTitle1',
    defaultMessage: 'Step 1: Online application',
  },
  corporateStep1DescriptionText1: {
    id: 'app.containers.VerificationPage.corporateStep1DescriptionText1',
    defaultMessage: 'Please fill in the electronic form with information regarding your Company, its beneficial owners and authorised persons, as applicable. You will also be asked to upload corporate documents, as well as documents for the proof of identification and residency of the said persons. This may take some time.',
  },
  corporateStep1DescriptionTitle2: {
    id: 'app.containers.VerificationPage.corporateStep1DescriptionTitle2',
    defaultMessage: 'Step 2: Verification process',
  },
  corporateStep1DescriptionText2: {
    id: 'app.containers.VerificationPage.corporateStep1DescriptionText2',
    defaultMessage: 'The verification process will be initiated once your online application and documents are submitted. It may take us up to 15 days to review your information provided. For any further clarifications required, a member of our team will contact you directly.',
  },
  corporateStep1DescriptionTitle3: {
    id: 'app.containers.VerificationPage.corporateStep1DescriptionTitle3',
    defaultMessage: 'Step 3: Electronic signing',
  },
  corporateStep1DescriptionText3: {
    id: 'app.containers.VerificationPage.corporateStep1DescriptionText3',
    defaultMessage: 'After the verification of your information, you will be provided with our terms and conditions. Once you carefully read and accept our terms, you may fund your account and start trading.',
  },
  nextStepBtn: {
    id: 'app.containers.VerificationPage.nextStepBtn',
    defaultMessage: 'Next step',
  },
  verifyMe: {
    id: 'app.containers.VerificationPage.verifyMe',
    defaultMessage: 'Verify me',
  },
  fieldsRequiredNote: {
    id: 'app.containers.VerificationPage.fieldsRequiredNote',
    defaultMessage: 'Please note that all fields that have an asterisk<span class="required-mark">*</span> are required in order to continue',
  },
  proofOfIdentity: {
    id: 'app.containers.VerificationPage.proofOfIdentity',
    defaultMessage: 'Proof of identity',
  },
  proofOfIdentityDescription: {
    id: 'app.containers.VerificationPage.proofOfIdentityDescription',
    defaultMessage: 'Upload scans or photos of passport or ID that prove your identity',
  },
  proofOfResidency: {
    id: 'app.containers.VerificationPage.proofOfResidency',
    defaultMessage: 'Proof of residency',
  },
  proofOfResidencyDescription: {
    id: 'app.containers.VerificationPage.proofOfResidencyDescription',
    defaultMessage: 'Upload scans or photos of documents that confirm your residential addressy',
  },
  selectFiles: {
    id: 'app.containers.VerificationPage.selectFiles',
    defaultMessage: 'Select files or drop them here',
  },
  addPerson: {
    id: 'app.containers.VerificationPage.addPerson',
    defaultMessage: 'Add one more person',
  },
  addTabPerson: {
    id: 'app.containers.VerificationPage.addTabPerson',
    defaultMessage: 'Add another one',
  },
  currentKnowledge: {
    id: 'app.containers.VerificationPage.currentKnowledge',
    defaultMessage: 'Current knowledge',
  },
  currentKnowledgeDescription: {
    id: 'app.containers.VerificationPage.currentKnowledgeDescription',
    defaultMessage: 'Do you know the characteristics and risks of the instrument?',
  },
  knowledgeSourceDescription: {
    id: 'app.containers.VerificationPage.knowledgeSourceDescription',
    defaultMessage: 'Where did you gain knowledge of the features of the instrument?',
  },
  knowledgeDurationDescription: {
    id: 'app.containers.VerificationPage.knowledgeDurationDescription',
    defaultMessage: 'How long have you been trading with such instrument?',
  },
  crypto: {
    id: 'app.containers.VerificationPage.crypto',
    defaultMessage: 'Cryptocurrencies',
  },
  amountDescription: {
    id: 'app.containers.VerificationPage.amountDescription',
    defaultMessage: 'What is the average amount you invested in this product over the last year?',
  },
  currentExperience: {
    id: 'app.containers.VerificationPage.currentExperience',
    defaultMessage: 'Current experience',
  },
  cryptocurrencies: {
    id: 'app.containers.VerificationPage.cryptocurrencies',
    defaultMessage: 'Cryptocurrencies',
  },
  options: {
    id: 'app.containers.VerificationPage.options',
    defaultMessage: 'Options, futures, swaps, forwards, other derivatives',
  },
  securities: {
    id: 'app.containers.VerificationPage.securities',
    defaultMessage: 'Securities',
  },
  interestRates: {
    id: 'app.containers.VerificationPage.interestRates',
    defaultMessage: 'Currencies, interest rates',
  },
  yields: {
    id: 'app.containers.VerificationPage.yields',
    defaultMessage: 'Yields',
  },
  derivatives: {
    id: 'app.containers.VerificationPage.derivatives',
    defaultMessage: 'Derivatives on commodities',
  },
  writerOfDerivatives: {
    id: 'app.containers.VerificationPage.writerOfDerivatives',
    defaultMessage: 'Writer of derivative products',
  },
  deposits: {
    id: 'app.containers.VerificationPage.deposits',
    defaultMessage: 'Deposits',
  },
  moneyMarkets: {
    id: 'app.containers.VerificationPage.moneyMarkets',
    defaultMessage: 'Money market instruments',
  },
  depositaryReceipts: {
    id: 'app.containers.VerificationPage.depositaryReceipts',
    defaultMessage: 'Depositary receipts',
  },
  bonds: {
    id: 'app.containers.VerificationPage.bonds',
    defaultMessage: 'Bonds or other securitised debt',
  },
  units: {
    id: 'app.containers.VerificationPage.units',
    defaultMessage: 'Units',
  },
  otherInstruments: {
    id: 'app.containers.VerificationPage.otherInstruments',
    defaultMessage: 'Other non-complex financial instruments',
  },
  explicitDeclrations: {
    id: 'app.containers.VerificationPage.explicitDeclrations',
    defaultMessage: 'Explicit declarations and documents',
  },
  corporateClientInfo: {
    id: 'app.containers.VerificationPage.corporateClientInfo',
    defaultMessage: 'Information about corporate client',
  },
  beneficialOwners: {
    id: 'app.containers.VerificationPage.beneficialOwners',
    defaultMessage: 'Beneficial owners',
  },
  authorizedPersons: {
    id: 'app.containers.VerificationPage.authorizedPersons',
    defaultMessage: 'Authorized persons',
  },
  investmentProfile: {
    id: 'app.containers.VerificationPage.investmentProfile',
    defaultMessage: 'Investment profile of the legal entity',
  },
  corporateKYCNote: {
    id: 'app.containers.VerificationPage.corporateKYCNote',
    defaultMessage: 'Note that we can request a copy of KYC procedures.',
  },
  signAuthorizedPersons: {
    id: 'app.containers.VerificationPage.signAuthorizedPersons',
    defaultMessage: 'Persons, authorized to sign agreements and orders confirmations',
  },
  deliveryAuthorizedPersons: {
    id: 'app.containers.VerificationPage.deliveryAuthorizedPersons',
    defaultMessage: 'Persons, authorized for delivery of statements and reports',
  },
  operateBasisAuthorizedPersons: {
    id: 'app.containers.VerificationPage.operateBasisAuthorizedPersons',
    defaultMessage: 'Persons authorized to operate on the basis of a power of attorney and/or authorized to give settlement instructions',
  },
  tinInfoNote: {
    id: 'app.containers.VerificationPage.tinInfoNote',
    defaultMessage: 'Provide the Taxpayer Identification Number (“TIN”) or Functional Equivalent issued by your country(ies) of tax residence. If a TIN or Functional Equivalent is not issued by the country(ies) of residence for tax purposes, please provide the appropriate reason.',
  },
  fatcaInfoNote: {
    id: 'app.containers.VerificationPage.fatcaInfoNote',
    defaultMessage: 'The Foreign Account Tax Compliance Act (FATCA) is a US Tax regulation enacted in 2010 in an effort to combat tax evasion by US investors, with offshore accounts and assets. The main objective of FATCA is to require non-US Financial Institutions (FFIs) to identify any US account holders and any US entities under US control and to inform the Internal Revenue Service (IRS) of their assets, income payments and trade flow during a financial year.',
  },
  logOut: {
    id: 'app.containers.VerificationPage.logOut',
    defaultMessage: 'Log out',
  },
  specifyUsCitizenship: {
    id: 'app.containers.VerificationPage.specifyUsCitizenship',
    defaultMessage: 'Specify if you abandoned American citizenship or have any intention in this regard.',
  },
  currency: {
    id: 'app.containers.VerificationPage.currency',
    defaultMessage: 'Currency',
  },
  correspondentBank: {
    id: 'app.containers.VerificationPage.correspondentBank',
    defaultMessage: 'Correspondent bank',
  },
  swift: {
    id: 'app.containers.VerificationPage.swift',
    defaultMessage: 'SWIFT',
  },
  beneficiary: {
    id: 'app.containers.VerificationPage.beneficiary',
    defaultMessage: 'Beneficiary',
  },
  beneficiaryBank: {
    id: 'app.containers.VerificationPage.beneficiaryBank',
    defaultMessage: 'Beneficiary bank',
  },
  accountNumber: {
    id: 'app.containers.VerificationPage.accountNumber',
    defaultMessage: 'Account number',
  },
  iban: {
    id: 'app.containers.VerificationPage.iban',
    defaultMessage: 'IBAN code',
  },
  account: {
    id: 'app.containers.VerificationPage.account',
    defaultMessage: 'Account',
  },
  documents: {
    id: 'app.containers.VerificationPage.documents',
    defaultMessage: 'Documents',
  },
  investmentAdvise: {
    id: 'app.containers.VerificationPage.investmentAdvise',
    defaultMessage: 'Investment advise',
  },
  proofOfAffiliation: {
    id: 'app.containers.VerificationPage.proofOfAffiliation',
    defaultMessage: 'Proof of affiliation',
  },
  proofOfAffiliationDescription: {
    id: 'app.containers.VerificationPage.proofOfAffiliationDescription',
    defaultMessage: 'Upload document to prove your affiliation with the provided banking account',
  },
});
