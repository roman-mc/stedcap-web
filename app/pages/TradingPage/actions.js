/*
 *
 * TradingPage actions
 *
 */
import { createAction } from 'redux-act';
import createApiAction from 'unity-frontend-core/utils/createAction';

export const placeOrder = createApiAction('app/TradingPage/PLACE_ORDER');
export const placeOrderRequest = createApiAction('app/TradingPage/PLACE_ORDER_REQUEST');

export const openOrderPopup = createApiAction('app/TradingPage/OPEN_ORDER_POPUP');
export const openOrderRequestPopup = createApiAction('app/TradingPage/OPEN_ORDER_REQUEST_POPUP');

export const openPopup = createApiAction('app/TradingPage/OPEN_POPUP');
export const closePopup = createApiAction('app/TradingPage/CLOSE_POPUP');

export const subscribeOrder = createApiAction('app/TradingPage/SUBSCRIBE_ORDER');
export const unsubscribeOrder = createApiAction('app/TradingPage/UNSUBSCRIBE_ORDER');

export const destroyPage = createAction('app/TradingPage/DESTROY_PAGE');
