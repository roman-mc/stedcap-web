import React from 'react';
import ImmutableProptypes from 'react-immutable-proptypes';
import { connect } from 'react-redux';
import { createStructuredSelector } from 'reselect';

import WithInjection from 'unity-frontend-core/containers/WithInjection';


import Panel from './components/Panel';
import TopBar from './components/TopBar';

import { makeSelectTradingTree } from './selectors';

import reducer from './reducer';
import sagas from './sagas';


import './style.css';

class Dashboard extends React.PureComponent {
  render() {
    const { panels, config } = this.props.config;

    if (!panels || !panels.size) {
      return null;
    }
    const activePanelId = config.get('activePanelId') || 0;
    const headerHidden = config.get('headerHidden') || false;
    const panel = panels.get(activePanelId);


    return (
      <div styleName={`dashboard ${headerHidden ? 'no-header' : ''}`}>
        <TopBar
          panels={panels}
          activePanelId={activePanelId}
          headerHidden={headerHidden}
        />
        <Panel widgets={panel.widgets} panelId={activePanelId} headerHidden={headerHidden} />
      </div>
    );
  }
}

Dashboard.propTypes = {
  config: ImmutableProptypes.record,
};

const mapDispatchToProps = createStructuredSelector({
  config: makeSelectTradingTree(),
});

const Connected = connect(mapDispatchToProps)(Dashboard);

export default WithInjection({ reducer, sagas, key: 'trading.tree' })(Connected);
