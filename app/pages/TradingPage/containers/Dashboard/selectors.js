import { createSelector } from 'reselect';
import { Map } from 'immutable';

export const makeSelectTradingTree = () => (state) => state.get('trading.tree');

export const makeSelectActivePanelId = () => createSelector(
  makeSelectTradingTree(),
  (tree) => tree.getIn(['config', 'activePanelId']),
);

export const makeSelectActivePanel = () => createSelector(
  makeSelectTradingTree(),
  makeSelectActivePanelId(),
  (tree, activePanelId) => tree.getIn(['panels', activePanelId]),
);

export const makeSelectMaxZIndex = () => createSelector(
  makeSelectTradingTree(),
  makeSelectActivePanelId(),
  (tree, activePanelId) => tree.getIn(['panels', activePanelId]).widgets.reduce((r, widget) => Math.max(widget.position.zIndex, r), 0),
);

export const makeSelectWidgetId = (component) => createSelector(
  makeSelectActivePanel(),
  (activePanel) => activePanel.widgets.findKey((w) => w.component === component)
);

export const makeSelectWidget = (component) => createSelector(
  makeSelectActivePanel(),
  makeSelectWidgetId(component),
  (activePanel, widgetId) => widgetId && activePanel.widgets.get(widgetId),
);

export const makeSelectWidgetConfig = (component) => createSelector(
  makeSelectWidget(component),
  (widget) => widget ? widget.config : new Map(),
);
