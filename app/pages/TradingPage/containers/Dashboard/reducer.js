
import { createReducer } from 'redux-act';

import Dashboard from 'Model/Dashboard';
import Panel from 'Model/Dashboard/Panel';

// import TreeApi from 'Api/TreeApi';

import {
  resize,
  loadConfig,
  addPanel,
  removePanel,
  setConfig,
  removeWidget,
  addWidget,
} from './actions';

function handleRemovePanel(state, { id }) {
  const panels = state.panels.delete(id);
  const newId = id - 1;
  return state
    .setConfig([], { activePanelId: panels.size <= newId ? newId - 1 : newId })
    .set('panels', panels);
}

export default createReducer({
  [loadConfig.success]: (_, tree) => tree,
  [addPanel]: (state, config) => state
    .update('panels', (panels) => panels.push(new Panel({ widgets: {}, config })))
    .setConfig([], { activePanelId: state.panels.size }),
  [removePanel]: handleRemovePanel,

  [addWidget]: (state, { panelId, widget }) => state.addWidget(panelId, widget),
  [removeWidget]: (state, { blockId = [] }) => state.removeWidget(blockId),
  [resize]: (state, { blockId = [], position = {} }) => state.setPosition(blockId, position),
  [setConfig]: (state, { blockId = [], config = {} }) => state.setConfig(blockId, config),
}, new Dashboard());
