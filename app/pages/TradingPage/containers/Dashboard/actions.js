/*
 *
 * TradingPage actions
 *
 */
import { createAction } from 'redux-act';
import createApiAction from 'unity-frontend-core/utils/createAction';

export const resize = createAction('app/TradingPage/Dashboard/RESIZE');


export const loadConfig = createApiAction('app/TradingPage/Dashboard/LOAD_CONFIG');
loadConfig.loadDefault = createAction('app/TradingPage/Dashboard/LOAD_CONFIG_DEFAULT');

export const removePanel = createAction('app/TradingPage/Dashboard/REMOVE_PANEL');
export const addPanel = createAction('app/TradingPage/Dashboard/ADD_PANEL');

export const setConfig = createAction('app/TradingPage/Dashboard/SET_CONFIG');
export const addWidget = createAction('app/TradingPage/Dashboard/ADD_WIDGET');
export const removeWidget = createAction('app/TradingPage/Dashboard/REMOVE_WIDGET');


export const updateTree = createAction('app/TradingPage/Dashboard/UPDATE_TREE');
export const queuedRequest = createAction('app/TradingPage/Dashboard/QUEUE_REQUEST');


export const addChat = createAction('app/TradingPage/Dashboard/ADD_ORDER_REQUEST_CHAT');
