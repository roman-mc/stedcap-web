import { defineMessages } from 'react-intl';

export default defineMessages({
  cancelButton: {
    id: 'app.containers.TradingPage.TopBar.cancel',
    defaultMessage: 'Cancel',
  },
  addButton: {
    id: 'app.containers.TradingPage.TopBar.addButton',
    defaultMessage: 'Add widget',
  },
  addWidgetTitle: {
    id: 'app.containers.TradingPage.TopBar.addWidgetTitle',
    defaultMessage: 'Add widget',
  },
  watchlist: {
    id: 'app.containers.TradingPage.TopBar.watchlist',
    defaultMessage: 'Watchlists',
  },
  positions: {
    id: 'app.containers.TradingPage.TopBar.positions',
    defaultMessage: 'Positions and orders',
  },
  overview: {
    id: 'app.containers.TradingPage.TopBar.overview',
    defaultMessage: 'Chart',
  },
  marketDepth: {
    id: 'app.containers.TradingPage.TopBar.marketDepth',
    defaultMessage: 'Market depth',
  },
  chooseWidget: {
    id: 'app.containers.TradingPage.TopBar.chooseWidget',
    defaultMessage: 'Choose widget type',
  },
  news: {
    id: 'app.containers.TradingPage.TopBar.news',
    defaultMessage: 'News',
  },
  ticket: {
    id: 'app.containers.TradingPage.TopBar.ticket',
    defaultMessage: 'Ticket',
  },
});
