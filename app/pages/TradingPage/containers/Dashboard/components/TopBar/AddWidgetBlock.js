import React from 'react';
import PropTypes from 'prop-types';
import ImmutableProptypes from 'react-immutable-proptypes';
import { createStructuredSelector } from 'reselect';

import { connect } from 'react-redux';

import { FormattedMessage } from 'react-intl';
import { Button } from 'Components/GUI';

import Modal, { actions } from 'Containers/Modal';
import { InstrumentsTree } from 'Containers/InstrumentsTree';

import { addWidget } from '../../actions';
import { makeSelectActivePanel } from '../../selectors';
import messages from './messages';

import { ListItem, CheckMark } from '../../../../../../components/List';


import SearchField from '../../../../../../components/SearchField';
import { H2 } from '../../../../../../components/Typography';


import { DEFAULT_OVERVIEW, DEFAULT_POSITION, DEFAULT_WATCHLIST, DEFAULT_MARKETDEPTH, DEFAULT_NEWS, DEFAULT_TICKET } from '../../../../../../config/dashboardConfig';

import './style.css';

const widgets = [
  {
    name: messages.watchlist,
    isInstrumentsEnabled: false,
    icon: 'icon-icon-widget-watchlist',
    widget: { ...DEFAULT_WATCHLIST },
  },
  {
    name: messages.positions,
    isInstrumentsEnabled: false,
    icon: 'icon-icon-widget-positions-orders',
    widget: { ...DEFAULT_POSITION },
  },
  {
    name: messages.overview,
    isInstrumentsEnabled: true,
    icon: 'icon-icon-widget-chart',
    widget: { ...DEFAULT_OVERVIEW },
  },
  {
    name: messages.news,
    isInstrumentsEnabled: false,
    icon: 'icon-icon-news',
    widget: { ...DEFAULT_NEWS },
  },
  {
    name: messages.marketDepth,
    isInstrumentsEnabled: true,
    icon: 'icon-icon-widget-market-depth',
    widget: { ...DEFAULT_MARKETDEPTH },
  },
  {
    name: messages.ticket,
    isInstrumentsEnabled: true,
    icon: 'icon-icon-ticket-widget',
    widget: { ...DEFAULT_TICKET },
  },
];

export class AddWidgetBlock extends React.PureComponent {
  state = {
    selectedWidget: null,
    selectedInstrumentId: [],
    search: '',
  }

  setInstrument = (instrumentId) => this.setState({ selectedInstrumentId: [instrumentId] })

  get widgetDescription() {
    return widgets.find((w) => w.widget.component === this.state.selectedWidget);
  }


  addWidget = () => {
    const { selectedInstrumentId } = this.state;
    const { widget, instrumentsDisabled } = this.widgetDescription;
    const instrumentToSave = instrumentsDisabled ? {} : { selectedInstrumentId: selectedInstrumentId[0] };
    const widgetToSave = { ...widget, config: { ...widget.config, ...instrumentToSave } };

    this.props.addWidget(this.props.panelId, widgetToSave);
    this.closeModal();
  }

  closeModal = () => {
    this.setState({ selectedWidget: null });
    this.props.closeModal();
  }

  findHandler = (search) => {
    this.setState({ search }, this.loadLevel);
  }

  render() {
    const { activePanel, openModal: doOpen } = this.props;
    const addedWidgets = activePanel.widgets.valueSeq().map((widget) => widget.component);
    const { widgetDescription } = this;

    return (
      <div>
        <Button onClick={doOpen} type="tertiary">
          <FormattedMessage {...messages.addButton} />
        </Button>
        <Modal
          modalId="add_widget_modal"
          renderHeader={() => 'Add widget'}
          renderFooter={() => [
            <Button
              onClick={this.closeModal}
              big
              type="tertiary"
              key="add_widget_modal_cancel"
              largePaddings
            >
              <FormattedMessage {...messages.cancelButton} />
            </Button>,
            <Button
              disabled={!widgetDescription || (widgetDescription && widgetDescription.isInstrumentsEnabled && !this.state.selectedInstrumentId.length)}
              big
              type="primary"
              key="add_widget_modal_add"
              onClick={this.addWidget}
              largePaddings
            >
              <FormattedMessage {...messages.addButton} />
            </Button>,
          ]}
        >
          <div styleName="add-widget-modal">
            <div styleName="widgets-list">
              <div styleName="widgets-list-header-container">
                <H2 centered><FormattedMessage {...messages.chooseWidget} /></H2>
                <SearchField placeholder="Search for widgets" value={this.state.search} onChange={this.findHandler} />
              </div>
              {
                widgets.map(({ name, icon, widget }) =>
                  <ListItem
                    key={`widget_${name.id}`}
                    disabled={addedWidgets.contains(widget.component)}
                    onClick={() => this.setState({ selectedWidget: widget.component })}
                    renderRight={() => this.state.selectedWidget === widget.component && <CheckMark />}
                  >
                    <span styleName={addedWidgets.contains(widget.component) ? 'disabled' : ''}>
                      <i className={`icon ${icon}`} styleName="margin-right"></i>
                      <FormattedMessage {...name} />
                    </span>
                  </ListItem>
                )
              }
            </div>
            <div styleName="instruments-list">
              <InstrumentsTree
                disabled={!widgetDescription || !widgetDescription.isInstrumentsEnabled}
                selectedInstruments={this.state.selectedInstrumentId}
                onSelect={this.setInstrument}
              />
            </div>
          </div>
        </Modal>
      </div>
    );
  }
}

AddWidgetBlock.propTypes = {
  openModal: PropTypes.func.isRequired,
  closeModal: PropTypes.func.isRequired,
  addWidget: PropTypes.func.isRequired,
  panelId: PropTypes.number.isRequired,
  activePanel: ImmutableProptypes.record.isRequired,
};

const mapStateToProps = createStructuredSelector({
  activePanel: makeSelectActivePanel(),
});


const mapDispatchToProps = (dispatch) => ({
  addWidget: (panelId, widget) => dispatch(addWidget({ panelId, widget })),
  openModal: () => dispatch(actions.openModal({ modalId: 'add_widget_modal' })),
  closeModal: () => dispatch(actions.closeModal({ modalId: 'add_widget_modal' })),
});


export default connect(mapStateToProps, mapDispatchToProps)(AddWidgetBlock);
