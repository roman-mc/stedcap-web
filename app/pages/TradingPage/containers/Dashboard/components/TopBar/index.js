import React from 'react';

import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import immutableTypes from 'react-immutable-proptypes';
import { Input, Button } from 'Components/GUI';

import { addPanel, setConfig } from '../../actions';

import AddButton, { AddBoardInput } from './components/AddButton';
import TopBarButton from './components/TopBarButton';
import AddWidgetBlock from './AddWidgetBlock';


import './style.css';

function TopBar({ headerHidden, setHeaderHidden, activePanelId, panels, ...props }) {
  return (
    <div styleName="top-bar-container">
      <div styleName="top-bar">
        <div styleName="box">
          <div styleName="button-container">
            <Button onClick={() => setHeaderHidden(!headerHidden)} type="primary">
              <i title={headerHidden ? 'Show header' : 'Hide header'} className={headerHidden ? 'icon-icon-header-show' : 'icon-icon-header-hide'}></i>
            </Button>
          </div>
          {
            panels.map((panel, i) =>
              (
                <TopBarButton
                  key={`tab_${i}`} // eslint-disable-line react/no-array-index-key
                  id={i}
                  content={panel.config.get('name')}
                  isActive={activePanelId === i}
                  isFirst={i === 0}
                  onClick={() => props.setCurrentPanel(i)}
                  setBoardName={props.setBoardName}
                />
              )
            )
          }
          <AddButton addBoard={props.addPanel} />
        </div>
        <div styleName="box">
          <AddWidgetBlock panelId={activePanelId} />
        </div>
      </div>
    </div>
  );
}


TopBar.propTypes = {
  headerHidden: PropTypes.bool,
  panels: immutableTypes.list,
  setPanel: PropTypes.func,
  addPanel: PropTypes.func.isRequired,
  setHeaderHidden: PropTypes.func.isRequired,
  setBoardName: PropTypes.func.isRequired,
  setCurrentPanel: PropTypes.func.isRequired,
  activePanelId: PropTypes.number.isRequired,
};

const mapDispatchToProps = (dispatch) => ({
  setCurrentPanel: (activePanelId) => dispatch(setConfig({ config: { activePanelId } })),
  addPanel: (name) => dispatch(addPanel({ name })),
  setBoardName: (id, name) => dispatch(setConfig({ blockId: [id], config: { name } })),
  setHeaderHidden: (headerHidden) => dispatch(setConfig({ config: { headerHidden } })),
});


AddBoardInput.propTypes = Input.propTypes;


export default connect(null, mapDispatchToProps)(TopBar);
