import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';

import { AddBoardInput } from '../AddButton';

import { removePanel } from '../../../../actions';

import './style.css';

class TopBarButton extends React.PureComponent {
  state = {
    input: false,
    inputValue: this.props.content,
  };


  onBlur = () => {
    const { inputValue } = this.state;
    const { id, content } = this.props;

    this.setState(
      { input: false },
      () => {
        this.props.setBoardName(id, inputValue || content);
        this.setState({ inputValue: inputValue || content });
      },
    );
  };


  onChange = (event) => this.setState({ inputValue: event.target.value });


  onKeyPress = (e) => {
    const { key } = e;
    if (key === 'Enter') { this.input.blur(); }
  };


  doubleClick = () => {
    this.setState(
      { input: true },
      () => this.input.focus(),
    );
  };

  removePanel = (e) => {
    e.stopPropagation();
    this.props.removePanel(this.props.id);
  }


  renderCross() {
    const { isActive, isFirst } = this.props;

    /* eslint-disable jsx-a11y/no-static-element-interactions */

    if (isActive && !isFirst) {
      return (
        <div onClick={this.removePanel} styleName="cross">
          <i className="icon-icon-cancel"></i>
        </div>
      );
    }

    /* eslint-enable */

    return null;
  }


  render() {
    const { input } = this.state;
    const {
      isActive, content, onClick, isFirst,
    } = this.props;

    if (input) {
      return (
        <AddBoardInput
          inputRef={(elm) => this.input = elm}
          value={this.state.inputValue}
          onBlur={this.onBlur}
          onChange={this.onChange}
          placeholder={content}
          onKeyPress={this.onKeyPress}
        />
      );
    }

    return (
      <button
        disabled={isActive}
        onClick={onClick}
        styleName={`button ${isActive ? 'active' : ''} ${isFirst ? 'first' : ''}`}
      >
        <span
          onDoubleClick={this.doubleClick}
          styleName="crop-area"
        >
          {content}
        </span>
        {this.renderCross()}
      </button>
    );
  }
}


TopBarButton.propTypes = {
  id: PropTypes.number,
  isFirst: PropTypes.bool,
  onClick: PropTypes.func,
  content: PropTypes.string,
  setBoardName: PropTypes.func.isRequired,
  removePanel: PropTypes.func.isRequired,
  isActive: PropTypes.bool.isRequired,
};


const mapDispatchToProps = (dispatch) => ({
  removePanel: (id) => dispatch(removePanel({ id })),
});

export default connect(null, mapDispatchToProps)(TopBarButton);
