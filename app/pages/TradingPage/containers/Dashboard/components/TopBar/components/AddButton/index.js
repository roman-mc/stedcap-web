import React from 'react';
import PropTypes from 'prop-types';
import { FormattedMessage, injectIntl, intlShape } from 'react-intl';

import { Input, Button } from 'Components/GUI';


import messages from './messages';
import './style.css';

export function AddBoardInput(props) {
  return (
    <div styleName="input">
      <Input {...props} />
    </div>
  );
}


class AddButton extends React.PureComponent {
  state = {
    input: false,
    inputValue: '',
  };


  onBlur = () => {
    const { inputValue } = this.state;
    this.setState({ input: false, inputValue: '' });

    this.props.addBoard(inputValue || this.props.intl.formatMessage(messages.tradingBoard));
  };


  onClick = () => this.setState(
    { input: true },
    () => this.input.focus(),
  );


  onChange = (event) => this.setState({ inputValue: event.target.value });


  onKeyPress = (e) => {
    const { key } = e;
    if (key === 'Enter') { this.input.blur(); }
  };


  renderInput() {
    const { input, inputValue } = this.state;
    const { formatMessage } = this.props.intl;

    if (!input) return null;

    return (
      <AddBoardInput
        inputRef={(elm) => this.input = elm}
        value={inputValue}
        onBlur={this.onBlur}
        onChange={this.onChange}
        placeholder={formatMessage(messages.tradingBoard)}
        onKeyPress={this.onKeyPress}
      />
    );
  }


  render() {
    return (
      <div styleName="add-board">
        {this.renderInput()}
        <Button onClick={this.onClick} type="tertiary">
          <FormattedMessage {...messages.addBoard} />
        </Button>
      </div>
    );
  }
}


AddButton.propTypes = {
  addBoard: PropTypes.func.isRequired,
  intl: intlShape,
};

export default injectIntl(AddButton);
