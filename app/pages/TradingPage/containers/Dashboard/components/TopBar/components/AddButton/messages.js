import { defineMessages } from 'react-intl';

export default defineMessages({
  tradingBoard: {
    id: 'app.containers.TradingPage.TopBar',
    defaultMessage: 'Trading Board',
  },
  addBoard: {
    id: 'app.containers.TradingPage.TopBar.addBoard',
    defaultMessage: 'Add Board',
  },
});
