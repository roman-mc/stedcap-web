import React from 'react';
import PropTypes from 'prop-types';
import ImmutableProptypes from 'react-immutable-proptypes';

import { connect } from 'react-redux';

import { actions } from 'Containers/Modal';
import MarketDepth, { MarketDepthHeader } from 'Containers/MarketDepth';

import './style.css';

import { setConfig } from '../../actions';

import Watchlist, { WatchlistHeader } from '../../../Watchlist';
import Positions, { PositionHeader } from '../../../Positions';
import Overview, { OverviewHeader } from '../../../Overview';
import News, { NewsHeader } from '../../../News';
import Chat, { ChatHeader } from '../../../Chat';
import Ticket, { TicketHeader } from '../../../Ticket';

import WidgetContainer from '../WidgetContainer';


const componentsByKey = {
  marketDepth: {
    component: MarketDepth,
    header: MarketDepthHeader,
    minWidth: 400,
    minSearchLabelShownWidth: 490,
    minHeight: 294,
  },
  watchlist: {
    component: Watchlist,
    header: WatchlistHeader,
    minWidth: 500,
    minHeight: 35,
  },
  positions: {
    component: Positions,
    header: PositionHeader,
    minWidth: 450,
    minHeight: 35,
  },
  overview: {
    component: Overview,
    header: OverviewHeader,
    minWidth: 700,
    minHeight: 35,
  },
  news: {
    component: News,
    header: NewsHeader,
    minWidth: 300,
    minHeight: 35,
  },
  chat: {
    component: Chat,
    header: ChatHeader,
    minWidth: 300,
    minHeight: 35,
  },
  ticket: {
    component: Ticket,
    header: TicketHeader,
    minWidth: 440,
    maxWidth: 440,
    minHeight: 300,
  },
};

class Panel extends React.PureComponent {
  state = {
    size: {
      width: 0,
      height: 0,
    },
  }

  componentDidMount() {
    this.calculateSizes();
    window.addEventListener('resize', this.calculateSizes);
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.headerHidden !== this.props.headerHidden) {
      setTimeout(this.calculateSizes, 350); // Wait for the animation finishing
    }
  }

  componentWillUnmount() {
    window.removeEventListener('resize', this.calculateSizes);
  }


  calculateSizes = () => {
    this.setState({ size: { height: this.container.clientHeight, width: this.container.clientWidth } }); // eslint-disable-line react/no-did-mount-set-state
  }

  renderNoData() {
    return (
      <div onClick={this.props.openModal} styleName="no-data-container">
        <i styleName="icon" className="icon-icon-block-view"></i>
        Click here to add your first widget
      </div>
    );
  }

  render() {
    const { panelId, widgets } = this.props;
    const { size } = this.state;

    const maxZindex = widgets.reduce((r, widget) => Math.max(widget.position.zIndex, r), 0);

    return (
      <div
        styleName="panel"
        ref={(container) => this.container = container}
      >
        {
          widgets.map((widget, uid) => /* eslint-disable react/no-array-index-key */
            <WidgetContainer
              {...componentsByKey[widget.component]}
              key={uid} // eslint-disable-line react/no-array-index-key
              uid={uid}
              panelId={panelId}
              config={widget.config}
              position={widget.position}
              containerSize={size}
              maxZindex={maxZindex}
              widgets={widgets}
            />
          ).valueSeq().toArray() /* eslint-enable */
        }
        {
          !widgets.size && this.renderNoData()
        }
      </div>
    );
  }
}

Panel.propTypes = {
  panelId: PropTypes.number.isRequired,
  openModal: PropTypes.func.isRequired,
  headerHidden: PropTypes.bool.isRequired,
  widgets: ImmutableProptypes.map.isRequired,
};


const mapDispatchToProps = (dispatch) => ({
  openModal: () => dispatch(actions.openModal({ modalId: 'add_widget_modal' })),
  setMode: (blockId, mode) => dispatch(setConfig({ blockId, config: { mode } })),
});

export default connect(null, mapDispatchToProps)(Panel);
