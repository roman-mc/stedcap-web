import React from 'react';
import PropTypes from 'prop-types';
import ImmutableProptypes from 'react-immutable-proptypes';
import Rnd from 'react-rnd';
import { connect } from 'react-redux';

import { WindowHeader, Window } from '../../../../../../components/Window';

import { resize, setConfig, removeWidget } from '../../actions';

import styles from './style.css';
const grid = [5, 5];

class WidgetContainer extends React.PureComponent {
  setConfig = (config) => {
    const { panelId, uid } = this.props;
    this.props.setConfig(panelId, uid, config);
  }

  handleResize = (e, direction, ref, delta, position) => {
    this.resizeWidget({ width: ref.offsetWidth, height: ref.offsetHeight }, position);
  }

  resizeWidget = (size, position) => {
    const { position: blockPosition, maxZindex, panelId, uid, containerSize } = this.props;
    this.props.resize(panelId, uid, {
      x: Math.max(0, (position.x / containerSize.width)) * 100,
      y: Math.max(0, (position.y / containerSize.height)) * 100,
      width: (size.width / containerSize.width) * 100,
      height: (size.height / containerSize.height) * 100,
      zIndex: maxZindex === blockPosition.zIndex ? maxZindex : maxZindex + 1,
    });
  }

  handleDragStart = () => {
    this.moveToTop();
  }

  handleMove = (e, d) => {
    const { maxZindex, position, panelId, uid, containerSize } = this.props;
    this.props.resize(panelId, uid, {
      x: Math.max(0, (d.x / containerSize.width)) * 100,
      y: Math.max(0, (d.y / containerSize.height)) * 100,
      zIndex: maxZindex === position.zIndex ? maxZindex : maxZindex + 1,
    });
  }

  handleMouseDown = (e) => {
    e.stopPropagation();
    this.moveToTop();
  }

  removeWidget = () => {
    this.props.removeWidget(this.props.panelId, this.props.uid);
  }

  moveToTop() {
    const { position, maxZindex, panelId, uid } = this.props;
    this.props.resize(panelId, uid, {
      zIndex: maxZindex === position.zIndex ? maxZindex : maxZindex + 1,
    });
  }

  render() {
    const { minWidth, maxWidth, minHeight, header: HeaderComponent, component: Component = null, config, uid, panelId, containerSize, position, minSearchLabelShownWidth } = this.props;
    const size = { width: (position.width * containerSize.width) / 100, height: (position.height * containerSize.height) / 100 };
    const hideLabel = minSearchLabelShownWidth ? size.width < minSearchLabelShownWidth : false;
    return Component ? (
      <Rnd
        className={`${styles.rnd}`}
        size={size}
        z={position.zIndex}
        position={{ x: (position.x * containerSize.width) / 100, y: (position.y * containerSize.height) / 100 }}
        onDragStart={this.handleDragStart}
        onDrag={this.handleDrag}
        onDragStop={this.handleMove}
        onResize={this.handleResize}
        bounds="parent"
        resizeGrid={grid}
        dragGrid={grid}
        minWidth={minWidth}
        maxWidth={maxWidth}
        minHeight={minHeight}
      >
        <Window fullWidth>
          <HeaderComponent onConfigChange={this.setConfig} config={config} onWindowRemove={this.removeWidget} hideSearchLabel={hideLabel} />
          <div styleName="content-container" onMouseDown={this.handleMouseDown}>
            <Component
              size={size}
              position={position}
              config={config}
              onResize={this.resizeWidget}
              onConfigChange={this.setConfig}
              blockId={[panelId, uid]}
            />
          </div>
        </Window>
      </Rnd>
    ) : null;
  }
}

WidgetContainer.defaultProps = {
  header: WindowHeader,
};

WidgetContainer.propTypes = {
  setConfig: PropTypes.func.isRequired,
  resize: PropTypes.func.isRequired,
  removeWidget: PropTypes.func.isRequired,
  header: PropTypes.func,
  component: PropTypes.func,
  config: ImmutableProptypes.map,
  uid: PropTypes.string.isRequired,
  panelId: PropTypes.number.isRequired,
  containerSize: PropTypes.shape({
    width: PropTypes.number.isRequired,
    height: PropTypes.number.isRequired,
  }).isRequired,
  position: ImmutableProptypes.record,
  maxZindex: PropTypes.number.isRequired,
  minHeight: PropTypes.number.isRequired,
  minWidth: PropTypes.number.isRequired,
  maxWidth: PropTypes.number,
  minSearchLabelShownWidth: PropTypes.number,
};

const mapDispatchToProps = (dispatch) => ({
  resize: (panelId, uid, position) => dispatch(resize({ blockId: [panelId, uid], position })),
  setConfig: (panelId, uid, config) => dispatch(setConfig({ blockId: [panelId, uid], config })),
  removeWidget: (panelId, uid) => dispatch(removeWidget({ blockId: [panelId, uid] })),
});

export default connect(null, mapDispatchToProps)(WidgetContainer);
