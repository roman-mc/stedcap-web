// import { fromJS } from 'immutable';
import { lsGetConfig, lsSaveConfig } from 'Api/lsConfigManipulationApi';
import { takeEvery } from 'redux-saga';
import { throttle, select, call, put } from 'redux-saga/effects';

import Dashboard from 'Model/Dashboard';
import * as dashboardApi from 'Api/dashboardApi';
import * as userApi from 'unity-frontend-core/api/authApi';


import {
  addPanel,
  loadConfig,
  removePanel,
  removeWidget,
  queuedRequest,
  updateTree,
  setConfig,
  addWidget,
  resize,
  addChat,
} from './actions';

import { VERSION as CONFIG_VERSION } from '../../../../config/dashboardConfig';

import { makeSelectTradingTree } from './selectors';

function* runDashboardSagas() {
  yield takeEvery(removeWidget.toString(), updateTreeHandler);
  yield takeEvery(addPanel.toString(), updateTreeHandler);
  yield takeEvery(removePanel.toString(), updateTreeHandler);
  yield takeEvery(updateTree.toString(), updateTreeHandler);
  yield takeEvery(setConfig.toString(), updateTreeHandler);
  yield takeEvery(addWidget.toString(), updateTreeHandler);
  yield takeEvery(resize.toString(), updateTreeHandler);
}

function* loadDefaultConfig() {
  const response = yield call(dashboardApi.getDefaultDashboard);
  const tree = new Dashboard(response.default);

  return yield put(loadConfig.success(tree));
}

function* loadConfigHandler() {
  const lsConfig = yield call(lsGetConfig);

  if (lsConfig && lsConfig.version === CONFIG_VERSION) {
    const tree = new Dashboard(lsConfig);
    return yield put(loadConfig.success(tree));
  }

  try {
    const profileConfig = yield call(userApi.getDashboard);

    if (profileConfig && profileConfig.version === CONFIG_VERSION) {
      const tree = new Dashboard(profileConfig);
      return yield put(loadConfig.success(tree));
    }

    return yield call(loadDefaultConfig);
  } catch (e) {
    return yield call(loadDefaultConfig);
  }
}


function* loadConfigSaga() {
  return yield takeEvery(loadConfig.toString(), loadConfigHandler);
}

function* addChatSaga() {
  return yield takeEvery(addChat.toString(), addChatHandler);
}


function* addChatHandler({ payload: { panelId, widget } }) {
  const config = yield select(makeSelectTradingTree());
  const sameWidgetExists = config.getIn(['panels', panelId]).widgets.findKey((w) => w.component === widget.component);
  if (sameWidgetExists) {
    yield put(setConfig({ blockId: [panelId, sameWidgetExists], config: widget.config }));
  } else {
    yield put(addWidget({ panelId, widget }));
  }
}

function* updateTreeHandler() {
  const config = yield select(makeSelectTradingTree());
  yield call(lsSaveConfig, config.toJS());
  yield put(queuedRequest(config));
}


function* saveDashboarHandler({ payload }) {
  yield call(userApi.updateDashboard, payload.toJS());
}

function* saveDashboardSaga() {
  yield throttle(10 * 1000, queuedRequest.toString(), saveDashboarHandler);
}

export default [
  loadConfigSaga,
  runDashboardSagas,
  saveDashboardSaga,
  addChatSaga,
];
