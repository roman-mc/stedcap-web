import React from 'react';
import PropTypes from 'prop-types';
import { throttle } from 'lodash';
import ImmutableProptypes from 'react-immutable-proptypes';
import { FormattedMessage } from 'react-intl';
import { createStructuredSelector } from 'reselect';
import { connect } from 'react-redux';

import WithInjection from 'unity-frontend-core/containers/WithInjection';


import { ORDER_TYPE, GOOD_TILL_TYPE, TRADE_DIRECTION } from 'Model/enums';
import { parseNumeral } from 'Utils/form/normalization';

import InstrumentMarketData from 'unity-frontend-core/containers/InstrumentMarketData';

import { makeSelectCurrentUser } from 'Containers/App/selectors';

import Popup from 'Components/Popup';

import { placeOrder, subscribeOrder, unsubscribeOrder } from '../../../actions';
import TradeForm from './TradeForm';

import InstrumentInfo from './components/InstrumentInfo';
import InstrumentPrices from './components/InstrumentPrices';

import LoadingIndicator from '../../../../../components/LoadingIndicator';


import sagas from './sagas';

import {
  makeSelectInstrumentId,
  makeSelectFormData,
  makeSelectDirection,
  makeSelectTradingDetails,
} from './selectors';
import { makeSelectIsConnectionBad, makeSelectNetworkDisconnected } from '../../../../AuthWrapper/selectors';

import messages from './messages';

import './style.css';


class Ticket extends React.PureComponent {
  componentDidMount() {
    const { instrumentId, initialValues } = this.props;
    if (initialValues && initialValues.get('id')) {
      const subscription = { ...initialValues.toJS(), instrumentId };
      this.props.subscribeOrder(subscription);
    }
  }

  componentWillUnmount() {
    this.props.unsubscribeOrder();
  }

  onOrderChanges = throttle((changes) => {
    const { instrumentId, formValues } = this.props;
    const subscription = { ...formValues.toJS(), ...changes, instrumentId };
    this.props.subscribeOrder(subscription);
  }, 400)


  get amount() {
    const { formValues } = this.props;
    const amountValue = (formValues && formValues.get('amount')) || '';

    if (typeof amountValue === 'number') {
      return amountValue;
    }
    return parseFloat(amountValue.replace(/[,]/g, ''));
  }

  placeOrder = (direction) => {
    const { instrumentId, formValues } = this.props;

    const entity = {
      ...formValues.toJS(),
      amount: parseFloat(parseNumeral(formValues.get('amount'))),
      price: parseFloat(parseNumeral(formValues.get('price'))),
      direction,
      orderType: formValues.get('orderType'),
      goodTillType: formValues.get('goodTillType'),
      instrumentId,
    };
    this.props.placeOrder(entity);
  }


  renderPopup = (marketData, id, { instrumentInfo, displayedCurrency }) => {
    const { networkDisconnected, isConnectionBad, loading, initialValues, tradingDetails, direction, formValues } = this.props;
    // console.log('formValues', formValues);
    const initial = initialValues ? initialValues.toJS() : {};


    return (
      <div styleName="ticket">
        {instrumentInfo && <InstrumentInfo instrument={instrumentInfo} />}
        {marketData
          ? <div>
            {instrumentInfo && <InstrumentPrices data={marketData} />}
            <TradeForm
              tradingDetails={tradingDetails}
              onOrderChanges={this.onOrderChanges}
              info={instrumentInfo}
              onOrderPlace={this.placeOrder}
              data={marketData}
              direction={direction}
              formValues={formValues}
              isConnectionBad={isConnectionBad}
              networkDisconnected={networkDisconnected}
              initialValues={{
                goodTillType: GOOD_TILL_TYPE.G_T_C.id,
                orderType: ORDER_TYPE.LIMIT.id,
                price: ((marketData.bid + marketData.ask) / 2).toFixed(5),
                ...initial,
              }}
              loading={loading}
              displayedCurrency={displayedCurrency}
            />
          </div> : <div styleName="loading">
            <LoadingIndicator loading />
          </div>
        }
      </div>
    );
  }

  render() {
    const { instrumentId, direction, currentUser } = this.props;

    const sellOrBuyTitle = direction === TRADE_DIRECTION.BUY ? messages.buyTicketHeader : messages.sellTicketHeader;

    const title = isNaN(parseInt(direction, 10)) ? messages.ticketHeader : sellOrBuyTitle;

    return (
      <Popup
        popupKey="placeOrder"
        key="placeOrder"
        header={<FormattedMessage {...title} />}
        removePopup={this.props.onRemove}
      >
        <InstrumentMarketData
          id={instrumentId}
          withInstrumentInfo
          render={this.renderPopup}
          amount={this.amount}
          displayedCurrency={currentUser.settings.displayedCurrency}
        />
      </Popup>
    );
  }
}


Ticket.propTypes = {
  direction: PropTypes.oneOf(Object.values(TRADE_DIRECTION)),
  placeOrder: PropTypes.func.isRequired,
  subscribeOrder: PropTypes.func.isRequired,
  unsubscribeOrder: PropTypes.func.isRequired,
  instrumentId: PropTypes.number.isRequired,
  formValues: ImmutableProptypes.map,
  initialValues: PropTypes.oneOfType([ImmutableProptypes.map, ImmutableProptypes.record]),
  tradingDetails: ImmutableProptypes.record,
  onRemove: PropTypes.func.isRequired,
  loading: PropTypes.bool,
  isConnectionBad: PropTypes.bool.isRequired,
  networkDisconnected: PropTypes.bool.isRequired,
  currentUser: ImmutableProptypes.record,
};

function mapDispatchToProps(dispatch) {
  return {
    placeOrder: (payload) => dispatch(placeOrder(payload)),
    subscribeOrder: (order) => dispatch(subscribeOrder({ order })),
    unsubscribeOrder: () => dispatch(unsubscribeOrder()),
  };
}

const mapStateToPros = createStructuredSelector({
  currentUser: makeSelectCurrentUser(),
  instrumentId: makeSelectInstrumentId(),
  formValues: makeSelectFormData(),
  direction: makeSelectDirection(),
  tradingDetails: makeSelectTradingDetails(),
  isConnectionBad: makeSelectIsConnectionBad(),
  networkDisconnected: makeSelectNetworkDisconnected(),
});

const Connected = connect(mapStateToPros, mapDispatchToProps)(Ticket);

export default WithInjection({ sagas })(Connected);
