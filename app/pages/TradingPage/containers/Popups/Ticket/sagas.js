import { takeEvery } from 'redux-saga';
import { call, put, take, apply, race } from 'redux-saga/effects';

import { createChannel } from 'Utils/sagaUtils';
import socketApi from 'Api/socketApi';

import {
  subscribeOrder,
  unsubscribeOrder,
} from '../../../actions';


function createSocket() {
  return socketApi;
}


export function* orderPricesHandler({ payload: { order } }) {
  const socket = yield call(createSocket);
  const channel = yield call(createChannel, socket, 'orderTicket');
  yield apply(socket, socket.subscribeOrder, [order]);
  try {
    while (true) { // eslint-disable-line no-constant-condition
      const { message, cancel, unsubscribe } = yield race({
        message: take(channel),
        cancel: take(subscribeOrder.toString()),
        unsubscribe: take(unsubscribeOrder.toString()),
      });
      if (message && message.data) {
        yield put(subscribeOrder.success(message.data));
      }
      if (cancel || unsubscribe) {
        throw new Error('Cancel subscription');
      }
    }
  } catch (e) {
    console.info('Cancel order subscription');
  }
}

export function* orderPricesSaga() {
  yield takeEvery(subscribeOrder.toString(), orderPricesHandler);
}

function* unsubscribeOrderHandler() {
  const socket = yield call(createSocket);
  yield apply(socket, socket.unsubscribeOrder);
}

export function* unsubscribeOrderSaga() {
  yield takeEvery(unsubscribeOrder.toString(), unsubscribeOrderHandler);
}


export default [
  orderPricesSaga,
  unsubscribeOrderSaga,
];
