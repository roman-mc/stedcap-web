import { createSelector } from 'reselect';
import { Map } from 'immutable';

import {
  makeSelectPopups,
} from '../../../selectors';

const makeSelectForm = () => (state) => state.getIn(['form', 'tradeTicketForm'], new Map());

const makeSelectPopup = () => createSelector(
  makeSelectPopups(),
  (popups) => popups.get('placeOrder'),
);

export const makeSelectInstrumentId = () => createSelector(
  makeSelectPopup(),
  (popup) => popup.getIn(['payload', 'instrumentId']),
);

export const makeSelectDirection = () => createSelector(
  makeSelectPopup(),
  (popup) => popup.getIn(['payload', 'direction']),
);

export const makeSelectTradingDetails = () => createSelector(
  makeSelectPopup(),
  (popup) => popup.get('tradingDetails'),
);

export const makeSelectFormData = () => createSelector(
  makeSelectForm(),
  (form) => form.get('values'),
);
