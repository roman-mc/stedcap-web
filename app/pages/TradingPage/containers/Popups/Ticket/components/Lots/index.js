import React from 'react';
import ImmutableProptypes from 'react-immutable-proptypes';
import PropTypes from 'prop-types';
import { Field } from 'redux-form/immutable';

import { Input } from 'Components/ReduxFields';

import amountFormat from 'unity-frontend-core/utils/amountFormat';


import { Button as GUIButton } from 'Components/GUI';

import './style.css';

class LotsRenderer extends React.PureComponent {
  state = {
    lotsExpanded: true,
  }

  onLotClick = (lotValue) => (e) => {
    e.preventDefault();
    this.props.input.onChange(lotValue);
  }

  onEnterClick = () => {
    this.setState({ lotsExpanded: false });
  }

  renderLot = (lotValue) => {
    const { input: { value }, disabled } = this.props;

    return this.renderButton(lotValue, value === lotValue ? 'primary' : 'tertiary', disabled);
  }

  renderButton(lotValue, type, disabled) {
    const button = (
      <div styleName="lot__inner">
        {amountFormat(lotValue)}
      </div>
    );
    return (
      <div styleName="lot" key={`lot_value_${lotValue}`}>
        <GUIButton
          fullWidth
          type={type}
          onClick={this.onLotClick(lotValue)}
          disabled={disabled}
        >
          {button}
        </GUIButton>
      </div>
    );
  }

  render() {
    const { lots, label } = this.props;
    const { lotsExpanded } = this.state;
    return (
      <div styleName="lots">
        <div styleName="label">{label || 'Lots'}</div>
        {lotsExpanded
          && <div styleName="wrapper">
            {lots.map(this.renderLot)}
          </div>}
        {!lotsExpanded
          && <div>
            <Input />
          </div>}
      </div>
    );
  }
}

LotsRenderer.propTypes = {
  lots: ImmutableProptypes.listOf(PropTypes.number).isRequired,
  label: PropTypes.any,
  disabled: PropTypes.bool,
  input: PropTypes.shape({
    value: PropTypes.any,
    onChange: PropTypes.func,
  }),
};


export default function Lots(props) {
  return (
    <Field
      component={LotsRenderer}
      {...props}
    />
  );
}
