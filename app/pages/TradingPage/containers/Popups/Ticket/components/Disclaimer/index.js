import React from 'react';
import PropTypes from 'prop-types';

import { FormattedMessage } from 'react-intl';
import messages from '../../messages';

import './style.css';

export default function Disclaimer({ supportedCurrencies }) {
  return (
    <div styleName="disclaimer">
      <div styleName="warning">
        <i className="icon-icon-error"></i>
      </div>
      <div styleName="text">
        <FormattedMessage
          {...messages.currencyNotSupported}
          values={{ currencies: supportedCurrencies.join(', ') }}
        />
      </div>
    </div>
  );
}

Disclaimer.propTypes = {
  supportedCurrencies: PropTypes.array,
};
