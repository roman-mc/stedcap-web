
import React from 'react';
import ImmutableProptypes from 'react-immutable-proptypes';
import PropTypes from 'prop-types';
import { FormattedNumber } from 'react-intl';

import { TRADE_DIRECTION } from 'Model/enums';

import './style.css';


export default function TradingDetails({ details, direction, displayedCurrency }) {
  const directionUndefined = isNaN(parseInt(direction, 10));

  return (
    details
      ? <table styleName="table">
        <tbody>
          <tr styleName="row">
            <th styleName="header white first">Trading details</th>
            {(directionUndefined || direction === TRADE_DIRECTION.BUY) && <th styleName="header">Buy ({displayedCurrency})</th>}
            {(directionUndefined || direction === TRADE_DIRECTION.SELL) && <th styleName="header">Sell ({displayedCurrency})</th>}
          </tr>
          <tr styleName="row">
            <th styleName="header first">Cost</th>
            {(directionUndefined || direction === TRADE_DIRECTION.BUY) && <td styleName="cell"><FormattedNumber value={details.buy.cost} /></td>}
            {(directionUndefined || direction === TRADE_DIRECTION.SELL) && <td styleName="cell"><FormattedNumber value={details.sell.cost} /></td>}
          </tr>
          <tr styleName="row">
            <th styleName="header first">Margin impact</th>
            {(directionUndefined || direction === TRADE_DIRECTION.BUY) && <td styleName="cell"><FormattedNumber value={details.buy.marginImpact} /></td>}
            {(directionUndefined || direction === TRADE_DIRECTION.SELL) && <td styleName="cell"><FormattedNumber value={details.sell.marginImpact} /></td>}
          </tr>
          <tr styleName="row">
            <th styleName="header first">Margin after trade</th>
            {(directionUndefined || direction === TRADE_DIRECTION.BUY) && <td styleName="cell"><FormattedNumber value={details.buy.afterTradeMarginAvailable} /></td>}
            {(directionUndefined || direction === TRADE_DIRECTION.SELL) && <td styleName="cell"><FormattedNumber value={details.sell.afterTradeMarginAvailable} /></td>}
          </tr>
        </tbody>
      </table>
      : null
  );
}

TradingDetails.propTypes = {
  direction: PropTypes.oneOf(Object.values(TRADE_DIRECTION)),
  details: ImmutableProptypes.record,
  displayedCurrency: PropTypes.string.isRequired,
};
