import React from 'react';

import './style.css';

export function ExchangeDetails() {
  return (
    <table styleName="table">
      <tbody>
        <tr styleName="row">
          <th styleName="header _white _first">Exchange details</th>
          <th styleName="header"></th>
        </tr>
        <tr styleName="row">
          <th styleName="header _first">Exchage rate</th>
          <td styleName="cell">1 EUR = 1.13 USD</td>
        </tr>
        <tr styleName="row">
          <th styleName="header _first">Fee</th>
          <td styleName="cell">No fee</td>
        </tr>
      </tbody>
    </table>
  );
}
