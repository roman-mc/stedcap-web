import React from 'react';
import { FormattedNumber } from 'react-intl';
import ImmutableProptypes from 'react-immutable-proptypes';

import QuotValue from 'Components/QuotValue';
import './style.css';


export default function InstrumentPrices({ data }) {
  const {
    bid, ask,
  } = data;

  return (
    <div styleName="prices">
      <div styleName="bid">
        <div styleName="value">
          <QuotValue value={bid} />
        </div>
        <div styleName="text">Bid</div>
      </div>

      <div styleName="spread">
        <div styleName="value">
          <FormattedNumber value={data.spread} maximumFractionDigits={1} />
        </div>
        <div styleName="text">Spread</div>
      </div>

      <div styleName="ask">
        <div styleName="value">
          <QuotValue value={ask} />
        </div>
        <div styleName="text">Ask</div>
      </div>
    </div>
  );
}

InstrumentPrices.propTypes = {
  data: ImmutableProptypes.record,
};
