import React from 'react';
import ImmutableProptypes from 'react-immutable-proptypes';

import InstrumentCategory from 'Components/InstrumentCategory';

import { Cell } from '../Rows';

import './style.css';

export default function InstrumentInfo({ instrument }) {
  return (
    <div styleName="instrument-info">
      <Cell>
        <div styleName="instrument-label">
          <InstrumentCategory size="35px" instrument={instrument} fontSize="18px" />
        </div>
        <div>
          <div styleName="instrument-name-id">
            {instrument.displayName}
          </div>
          {/* <div styleName="instrument-name-details">
                        AMEX
                    </div> */}
        </div>
      </Cell>
      {/* <Cell>
                {instrument.supportedCurrencies.map((instrument) =>
                    <div
                        styleName="other-instrument"
                        key={`supported_currencies_${instrument}`}
                    >
                        <GrayLabel
                            text={instrument}
                            size="30px"
                            fontSize="13px"
                        />
                    </div>
                )}
            </Cell> */}
    </div>
  );
}

InstrumentInfo.propTypes = {
  instrument: ImmutableProptypes.record,
};
