import React from 'react';
import PropTypes from 'prop-types';

import './style.css';

export function Cell({ children }) {
  return (
    <div styleName="cell">{children}</div>
  );
}

Cell.propTypes = {
  children: PropTypes.node.isRequired,
};
