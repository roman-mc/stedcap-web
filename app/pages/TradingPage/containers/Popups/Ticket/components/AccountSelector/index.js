import React from 'react';
import PropTypes from 'prop-types';
import { FormattedNumber, FormattedMessage } from 'react-intl';
import ImmutableProptypes from 'react-immutable-proptypes';
import { FormSelector } from 'Components/ReduxFields';

import messages from '../../messages';
import './style.css';

export default class AccountSelector extends React.PureComponent {
  optionRenderer = ({ id, balance, currency }) =>
    (<div styleName="label">
      <div styleName="info">
        {`${currency} account ${id}`}
      </div>
      <div styleName="bill">
        <FormattedNumber value={balance} />
        {currency}
      </div>
    </div>)


  render() {
    const { accounts, name, validate } = this.props;
    return (
      <FormSelector
        validate={validate}
        text={<FormattedMessage {...messages.accountLabel} />}
        options={accounts.toArray()}
        name={name}
        disableClearable
        valueRenderer={this.optionRenderer}
        optionRenderer={this.optionRenderer}
      />
    );
  }
}


AccountSelector.propTypes = {
  accounts: ImmutableProptypes.listOf(ImmutableProptypes.record),
  name: PropTypes.string.isRequired,
  validate: PropTypes.array,
};
