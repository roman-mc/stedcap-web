import React from 'react';
import PropTypes from 'prop-types';
import { FormattedMessage } from 'react-intl';

import './style.css';

import { Cell } from '../Rows';

import messages from '../../messages';

export default function MarketInfo(props) {
  const { name, status } = props;
  const marketStatus = status ? 'opened' : 'closed';

  return (
    <div styleName="market">
      <Cell>
        <div><FormattedMessage {...messages.realtimePrices} /></div>
      </Cell>
      <Cell>
        <div styleName="status">
          <div styleName={`dot ${marketStatus}`}></div>
          <div>{name}</div>
        </div>
      </Cell>
    </div>
  );
}


MarketInfo.propTypes = {
  name: PropTypes.string.isRequired,
  status: PropTypes.bool.isRequired,
};
