import React from 'react';
import PropTypes from 'prop-types';
import ImmutableProptypes from 'react-immutable-proptypes';
import { FormattedMessage } from 'react-intl';
import { reduxForm } from 'redux-form/immutable';

import { Row, Column } from 'Components/Popup/components';
import { Input, FormSelector } from 'Components/ReduxFields';
import { Button } from 'Components/GUI';
// import { FormSelector } from 'Components/ReduxFields';

import { requireValidate } from 'Utils/form/validation';
import { formatNumeral } from 'Utils/form/normalization';
import { GOOD_TILL_TYPE, ORDER_TYPE, TRADE_DIRECTION } from 'Model/enums';

import './style.css';

// import Disclaimer from './components/Disclaimer';
import Lots from './components/Lots';

import TradingDetails from './components/TradingDetails';

import messages from './messages';

function Ticket({
  loading, onOrderPlace, tradingDetails, onOrderChanges, handleSubmit, info, invalid, formValues, direction, change, isConnectionBad, networkDisconnected, displayedCurrency,
}) {
  const isDirectionUndefined = isNaN(parseInt(direction, 10));

  const isPoorConnection = networkDisconnected || isConnectionBad;
  const poorConnectionText = networkDisconnected ? messages.noConnectionTitle : messages.badConnectionTitle;

  const orderType = formValues && formValues.get('orderType');
  const isEdit = formValues && !!formValues.get('id');

  const goodTillTypes = Object
    .values(GOOD_TILL_TYPE)
    .filter(({ orderTypes }) => (formValues && orderTypes.indexOf(orderType) >= 0))
    .map(({ id, name }) => ({ label: name, value: id }));

  const details = tradingDetails && (direction === TRADE_DIRECTION.BUY ? tradingDetails.buy : tradingDetails.sell);
  return (
    <form onSubmit={handleSubmit}>

      {/* <MarketInfo {...marketMock} /> */}
      <Row>
        <Input
          name="amount"
          format={formatNumeral}
          validate={[requireValidate]}
          text={<FormattedMessage {...messages.amount} />}
          onChange={(e, amount) => onOrderChanges({ amount: parseInt(amount, 10) })}
        />
      </Row>

      <Row withoutBorder>
        <Lots
          validate={[requireValidate]}
          name="amount"
          lots={info.volumes}
          onChange={(e, amount) => onOrderChanges({ amount })}
          label="Amount"
        />
      </Row>

      <Row>
        <Column rightBackslash>
          <FormSelector
            validate={[requireValidate]}
            text={<FormattedMessage {...messages.orderTypeLabel} />}
            onChange={(e, ot) => {
              if (formValues.get('orderType') === ot) {
                return;
              }
              if (ot === ORDER_TYPE.LIMIT.id) {
                change('goodTillType', GOOD_TILL_TYPE.G_T_C.id);
              } else {
                change('goodTillType', GOOD_TILL_TYPE.FILL_OR_KILL.id);
              }
            }}
            options={Object.values(ORDER_TYPE).map(({ id, name }) => ({ label: name, value: id }))}
            name="orderType"
            disableClearable
            disabled={isEdit}
          />
        </Column>
        <Column leftBackslash>
          <FormSelector
            validate={[requireValidate]}
            text={<FormattedMessage {...messages.goodTillTypeLabel} />}
            options={goodTillTypes}
            name="goodTillType"
            disableClearable
            disabled={isEdit}
          />
        </Column>
      </Row>

      {orderType !== ORDER_TYPE.MARKET.id
        && <Row>
          <Column>
            <Input
              name="price"
              format={formatNumeral}
              validate={[requireValidate]}
              text={<FormattedMessage {...messages.price} />}
            />
          </Column>
        </Row>
      }
      <Row>
        <TradingDetails
          details={tradingDetails}
          direction={direction}
          displayedCurrency={displayedCurrency}
        />
      </Row>

      {isPoorConnection
        && <div styleName={`tooltip ${networkDisconnected ? 'tooltip-error' : ''}`}>
          <i className="icon-icon-error"></i>
          <span styleName="tooltip-text">
            <FormattedMessage {...poorConnectionText} />
          </span>
        </div>
      }

      {isDirectionUndefined
        ? <Row withoutBorder>
          <Column rightBackslash>
            <Button
              btnType="button"
              fullWidth
              big
              type="primary"
              disabled={invalid || loading}
              onClick={() => onOrderPlace(TRADE_DIRECTION.BUY)}
            >

              {
                loading
                  ? <FormattedMessage {...messages.loading} />
                  : <FormattedMessage {...messages.orderButtonText} values={{ direction: TRADE_DIRECTION.BUY, isEdit }} />
              }

            </Button>
          </Column>
          <Column leftBackslash>
            <Button
              btnType="button"
              fullWidth
              big
              type="primary"
              disabled={invalid || loading}
              onClick={() => onOrderPlace(TRADE_DIRECTION.SELL)}
              dataButtonAction="place-order"
            >
              {
                loading
                  ? <FormattedMessage {...messages.loading} />
                  : <FormattedMessage {...messages.orderButtonText} values={{ direction: TRADE_DIRECTION.SELL, isEdit }} />
              }
            </Button>
          </Column>
        </Row>
        : <Row withoutBorder>
          <Column leftBackslash>
            <Button
              btnType="button"
              fullWidth
              big
              type="primary"
              disabled={invalid || !details || loading}
              onClick={() => onOrderPlace(direction)}
              dataButtonAction={isEdit ? 'edit-order' : 'place-order'}
            >

              {
                loading
                  ? <FormattedMessage {...messages.loading} />
                  : <FormattedMessage {...messages.orderButtonText} values={{ direction, isEdit }} />
              }

            </Button>
          </Column>
        </Row>
      }
    </form>
  );
}

Ticket.propTypes = {
  loading: PropTypes.bool,
  handleSubmit: PropTypes.func.isRequired,
  onOrderPlace: PropTypes.func.isRequired,
  onOrderChanges: PropTypes.func.isRequired,
  info: ImmutableProptypes.record,
  invalid: PropTypes.bool.isRequired,
  isConnectionBad: PropTypes.bool.isRequired,
  networkDisconnected: PropTypes.bool.isRequired,
  tradingDetails: ImmutableProptypes.record,
  formValues: ImmutableProptypes.map,
  change: PropTypes.func.isRequired,
  direction: PropTypes.oneOf(Object.values(TRADE_DIRECTION)),
  displayedCurrency: PropTypes.string.isRequired,
};


export default reduxForm({ form: 'tradeTicketForm' })(Ticket);
