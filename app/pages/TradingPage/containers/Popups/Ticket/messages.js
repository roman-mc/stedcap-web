import { defineMessages } from 'react-intl';

export default defineMessages({
  noAccounts: {
    id: 'app.containers.TradingPage.TicketPopup.noAccounts',
    defaultMessage: 'You don\'t have trading accounts. Please contact support.',
  },
  sellTicketHeader: {
    id: 'app.containers.TradingPage.TicketPopup.sellTicketHeader',
    defaultMessage: 'Sell order ticket',
  },
  buyTicketHeader: {
    id: 'app.containers.TradingPage.TicketPopup.buyTicketHeader',
    defaultMessage: 'Buy order ticket',
  },
  currencyNotSupported: {
    id: 'app.containers.TradingPage.TicketPopup.currencyNotSupported',
    defaultMessage: 'This instrument can only be traded in {currencies}. Choose your {currencies} account or money for this trade will be converted using our exchange rate.',
  },
  accountLabel: {
    id: 'app.containers.TradingPage.TicketPopup.accountLabel',
    defaultMessage: 'Account',
  },
  orderTypeLabel: {
    id: 'app.containers.TradingPage.TicketPopup.orderTypeLabel',
    defaultMessage: 'Type',
  },
  goodTillTypeLabel: {
    id: 'app.containers.TradingPage.TicketPopup.goodTillTypeLabel',
    defaultMessage: 'Duration',
  },
  realtimePrices: {
    id: 'app.containers.TradingPage.TicketPopup.realtimePrices',
    defaultMessage: 'Realtime prices',
  },
  ticketHeader: {
    id: 'app.containers.TradingPage.TicketPopup.ticketHeader',
    defaultMessage: 'Order ticket',
  },
  orderButtonText: {
    id: 'app.containers.TradingPage.TicketPopup.placeBuyOrder',
    defaultMessage: '{isEdit, select, true {Edit} other {Place}} {direction, select, 1 {sell} 0 { buy } other { }} order',
  },
  loading: {
    id: 'app.containers.TradingPage.TicketPopup.loading',
    defaultMessage: 'Sending order...',
  },
  price: {
    id: 'app.containers.TradingPage.TicketPopup.price',
    defaultMessage: 'Price',
  },
  amount: {
    id: 'app.containers.TradingPage.TicketPopup.amount',
    defaultMessage: 'Amount',
  },
  badConnectionTitle: {
    id: 'app.containers.TradingPage.badConnectionTitle',
    defaultMessage: 'Real money trades may be executed with delay due to poor connection. Check your settings to restore it or proceed carefully.',
  },
  noConnectionTitle: {
    id: 'app.containers.TradingPage.noConnectionTitle',
    defaultMessage: 'There is no stable connection with server. Data is probably displayed incorrectly and trades may carry out with errors. Please check you internet settings before executing trades.',
  },
});
