import { put } from 'redux-saga/effects';

import {
  generateListener,
} from 'unity-frontend-core/utils/sagaUtils';
import {
  createOrderRequest,
  updateOrderRequest,
} from 'unity-frontend-core/containers/OrderRequests/actions';

import { closeModal } from '../../../../../containers/Modal/actions';

function* createUpdateOrderRequestHandler() {
  yield put(closeModal({ modalId: 'tradingPageOrderRequestTicket' }));
}

export default [
  generateListener(createOrderRequest.success, createUpdateOrderRequestHandler),
  generateListener(updateOrderRequest.success, createUpdateOrderRequestHandler),
];
