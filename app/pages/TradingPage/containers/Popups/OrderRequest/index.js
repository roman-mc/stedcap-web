import React from 'react';
import PropTypes from 'prop-types';
import ImmutableProptypes from 'react-immutable-proptypes';
import { FormattedMessage } from 'react-intl';
import { createStructuredSelector } from 'reselect';
import { connect } from 'react-redux';

import WithInjection from 'unity-frontend-core/containers/WithInjection';

import OrderRequestHOC from 'unity-frontend-core/containers/OrderRequests';

import Modal from '../../../../../containers/Modal';
import TradeForm from './TradeForm';

import sagas from './sagas';

import {
  makeSelectFormData,
  makeSelectModalPayload,
  makeSelectOrderRequestId,
} from './selectors';

import messages from './messages';

import './style.css';
import * as actions from '../../../../../containers/Modal/actions';


class OrderRequest extends React.PureComponent {
  get amount() {
    const { formValues } = this.props;
    const amountValue = (formValues && formValues.get('amount')) || '';

    if (typeof amountValue === 'number') {
      return amountValue;
    }
    return parseFloat(amountValue.replace(/[,]/g, ''));
  }

  placeOrder = () => {
    const { id: orderRequestId, formValues } = this.props;

    const entity = {
      ...formValues.toJS(),
      amount: this.amount,
    };
    if (orderRequestId) {
      this.props.updateOrderRequest({ ...entity, orderRequestId });
    } else {
      this.props.createOrderRequest(entity);
    }
  }

  render() {
    const { initialValues: { symbolDescription, direction, expectedPrice, amount, comment } = {}, initialValues, id, loading, formValues } = this.props;
    return (
      <Modal
        modalId="tradingPageOrderRequestTicket"
        renderHeader={() => <FormattedMessage {...messages[id ? 'updateTicketHeader' : 'orderHeader']} />}
      >
        <div styleName="ticket">
          <div>
            {initialValues ? <TradeForm
              id={id}
              onOrderPlace={this.placeOrder}
              formValues={formValues}
              initialValues={{
                expectedPrice: expectedPrice || 'Market',
                symbolDescription,
                direction,
                amount,
                comment,
              }}
              onRemove={this.props.closeModal}
              loading={loading}
            /> : null}
          </div>
        </div>
      </Modal>
    );
  }
}


OrderRequest.propTypes = {
  createOrderRequest: PropTypes.func.isRequired,
  updateOrderRequest: PropTypes.func.isRequired,
  closeModal: PropTypes.func,
  formValues: ImmutableProptypes.map,
  initialValues: PropTypes.object,
  loading: PropTypes.bool,
  id: PropTypes.number,
};


const mapStateToPros = createStructuredSelector({
  initialValues: makeSelectModalPayload(),
  id: makeSelectOrderRequestId(),
  formValues: makeSelectFormData(),
});

const mapDispatchToProps = (dispatch) => ({
  closeModal: () => dispatch(actions.closeModal({ modalId: 'tradingPageOrderRequestTicket' })),
});
const Connected = OrderRequestHOC(connect(mapStateToPros, mapDispatchToProps)(OrderRequest));

export default WithInjection({ sagas })(Connected);
