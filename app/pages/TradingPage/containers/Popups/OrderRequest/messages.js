import { defineMessages } from 'react-intl';

export default defineMessages({
  orderHeader: {
    id: 'app.containers.TradingPage.OrderRequest.orderHeader',
    defaultMessage: 'Order request',
  },
  updateTicketHeader: {
    id: 'app.containers.TradingPage.OrderRequest.updateTicketHeader',
    defaultMessage: 'Edit order request',
  },
  loading: {
    id: 'app.containers.TradingPage.OrderRequest.loading',
    defaultMessage: 'Sending order...',
  },
  amount: {
    id: 'app.containers.TradingPage.OrderRequest.amount',
    defaultMessage: 'Amount',
  },
  amountPlaceholder: {
    id: 'app.containers.TradingPage.OrderRequest.amountPlaceholder',
    defaultMessage: 'Fill in exact amount',
  },
  symbol: {
    id: 'app.containers.TradingPage.orderRequest.symbol',
    defaultMessage: 'Symbol',
  },
  expectedPrice: {
    id: 'app.containers.TradingPage.orderRequest.expectedPrice',
    defaultMessage: 'Expected price',
  },
  message: {
    id: 'app.containers.TradingPage.orderRequest.message',
    defaultMessage: 'Additional message',
  },
  messagePlaceholder: {
    id: 'app.containers.TradingPage.orderRequest.messagePlaceholder',
    defaultMessage: 'Fill in your comment',
  },
  orderRequestButton: {
    id: 'app.containers.TradingPage.orderRequest.orderRequestButton',
    defaultMessage: 'Send request',
  },
  orderRequestButtonCancel: {
    id: 'app.containers.TradingPage.orderRequest.orderRequestButtonCancel',
    defaultMessage: 'Cancel',
  },
  orderRequestBuy: {
    id: 'app.containers.TradingPage.orderRequest.orderRequestBuy',
    defaultMessage: 'Buy',
  },
  orderRequestSell: {
    id: 'app.containers.TradingPage.orderRequest.orderRequestSell',
    defaultMessage: 'Sell',
  },
  orderRequestUpdate: {
    id: 'app.containers.TradingPage.orderRequest.orderRequestUpdate',
    defaultMessage: 'Update',
  },
});
