import { createSelector } from 'reselect';
import { Map } from 'immutable';

const makeSelectModal = () => (state) => state.getIn(['modal', 'tradingPageOrderRequestTicket'], new Map()) || new Map();
const makeSelectForm = () => (state) => state.getIn(['form', 'orderRequestForm'], new Map());

export const makeSelectModalPayload = () => createSelector(
  makeSelectModal(),
  (popups) => popups.get('payload'),
);

export const makeSelectOrderRequestId = () => createSelector(
  makeSelectModalPayload(),
  (popup) => popup ? popup.id : undefined,
);

export const makeSelectFormData = () => createSelector(
  makeSelectForm(),
  (form) => form.get('values'),
);
