import React from 'react';
import PropTypes from 'prop-types';
import ImmutableProptypes from 'react-immutable-proptypes';
import { FormattedMessage, injectIntl } from 'react-intl';
import { reduxForm } from 'redux-form/immutable';

import { Row, Column } from 'Components/Popup/components';
import { Input } from 'Components/ReduxFields';
import { Button } from 'Components/GUI';

import { requireValidate } from 'Utils/form/validation';
import { formatNumeral } from 'Utils/form/normalization';
import { TRADE_DIRECTION } from 'Model/enums';

import messages from './messages';

import styles from './style.css';

function Ticket({
  loading, onOrderPlace, initialValues, handleSubmit, invalid, formValues, change, onRemove, id, intl,
}) {
  const direction = formValues ? formValues.get('direction') : initialValues.direction;
  return (
    <form onSubmit={handleSubmit}>
      <Row withoutBorder>
        <Column rightBackslash>
          <Button
            btnType="button"
            fullWidth
            big
            type="secondary"
            disabled={!!id}
            onClick={() => change('direction', TRADE_DIRECTION.SELL)}
            className={TRADE_DIRECTION.SELL === direction ? styles.sellSelected : ''}
          >
            <FormattedMessage {...messages.orderRequestSell} />
          </Button>
        </Column>
        <Column leftBackslash>
          <Button
            btnType="button"
            fullWidth
            big
            disabled={!!id}
            type="secondary"
            onClick={() => change('direction', TRADE_DIRECTION.BUY)}
            className={TRADE_DIRECTION.BUY === direction ? styles.buySelected : ''}
          >
            <FormattedMessage {...messages.orderRequestBuy} />
          </Button>
        </Column>
      </Row>
      <Row withoutBorder>
        <Input
          disabled={!!id}
          name="symbolDescription"
          validate={[requireValidate]}
          text={<FormattedMessage {...messages.symbol} />}
        />
      </Row>
      <Row withoutBorder>
        <Column rightBackslash>
          <Input
            name="amount"
            format={formatNumeral}
            validate={[requireValidate]}
            text={<FormattedMessage {...messages.amount} />}
            placeholder={intl.formatMessage(messages.amountPlaceholder)}
          />
        </Column>
        <Column leftBackslash>
          <Input
            name="expectedPrice"
            validate={[requireValidate]}
            text={<FormattedMessage {...messages.expectedPrice} />}
          />
        </Column>
      </Row>
      <Row withoutBorder>
        <Input
          disabled={!!id}
          name="comment"
          text={<FormattedMessage {...messages.message} />}
          placeholder={intl.formatMessage(messages.messagePlaceholder)}
        />
      </Row>

      <Row withoutBorder>
        <Column rightBackslash>
          <Button
            btnType="button"
            fullWidth
            big
            type="secondary"
            onClick={onRemove}
          >
            <FormattedMessage {...messages.orderRequestButtonCancel} />
          </Button>
        </Column>
        <Column leftBackslash>
          <Button
            btnType="button"
            fullWidth
            big
            type="primary"
            disabled={invalid || loading}
            onClick={onOrderPlace}
          >
            <FormattedMessage {...messages[id ? 'orderRequestUpdate' : 'orderRequestButton']} />
          </Button>
        </Column>
      </Row>
    </form>
  );
}

Ticket.propTypes = {
  intl: PropTypes.object,
  loading: PropTypes.bool,
  handleSubmit: PropTypes.func.isRequired,
  onOrderPlace: PropTypes.func.isRequired,
  onRemove: PropTypes.func.isRequired,
  invalid: PropTypes.bool.isRequired,
  initialValues: PropTypes.oneOfType([ImmutableProptypes.map, ImmutableProptypes.record]),
  formValues: ImmutableProptypes.map,
  change: PropTypes.func.isRequired,
  id: PropTypes.number,
};


export default reduxForm({ form: 'orderRequestForm' })(injectIntl(Ticket));
