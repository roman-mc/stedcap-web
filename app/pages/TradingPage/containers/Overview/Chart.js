import React, { Fragment } from 'react';
import PropTypes from 'prop-types';

import withInstrumentLog from 'unity-frontend-core/containers/InstrumentLog';

import LoadingIndicator from 'Components/LoadingIndicator';
import CandleSticksChart from 'Components/Charts/CandleSticksChart';

import { } from './style.css';

class Chart extends React.Component {
  componentDidMount() {
    const { loadChartData, selectedInstrumentId, currentTimeframe } = this.props;
    loadChartData({ instrumentId: selectedInstrumentId, timeframe: currentTimeframe });
  }

  componentWillReceiveProps(nextProps) { // Listen for instrument changes from the Watchlist block
    const { loadChartData, selectedInstrumentId, stopChartDataLoading, currentTimeframe } = this.props;

    const prevInstrumentId = selectedInstrumentId;
    const newInstrumentId = nextProps.selectedInstrumentId;

    const prevTimeframe = currentTimeframe;
    const newTimeframe = nextProps.currentTimeframe;

    if ((newInstrumentId && prevInstrumentId !== newInstrumentId) || (newTimeframe && prevTimeframe !== newTimeframe)) {
      stopChartDataLoading({ instrumentId: selectedInstrumentId, timeframe: prevTimeframe });
      loadChartData({ instrumentId: newInstrumentId, timeframe: newTimeframe });
    }
  }

  componentWillUnmount() {
    const { stopChartDataLoading, selectedInstrumentId, currentTimeframe } = this.props;
    stopChartDataLoading({ instrumentId: selectedInstrumentId, timeframe: currentTimeframe });
  }

  componentDidCatch() {
    return null;
  }

  render() {
    const { loading, chartData, currentTheme, error, chartSize: { width, height } } = this.props;

    if (loading) {
      return (
        <div styleName="loader-style">
          <LoadingIndicator loading />
        </div>
      );
    }

    if (error) {
      return (
        <div styleName="data-error">Error loading instrument data. <br /> Please, try again later.</div>
      );
    }

    if (chartData.length <= 2 || !width || !height) {
      return (
        <div styleName="data-error">No data for selected instrument.</div>
      );
    }

    return (
      <Fragment>
        <CandleSticksChart height={height} width={width} data={chartData} type="hybrid" theme={currentTheme} />
      </Fragment>
    );
  }
}

Chart.propTypes = {
  selectedInstrumentId: PropTypes.number,
  currentTimeframe: PropTypes.number.isRequired,
  loadChartData: PropTypes.func.isRequired,
  stopChartDataLoading: PropTypes.func.isRequired,
  chartData: PropTypes.array,
  error: PropTypes.bool,
  currentTheme: PropTypes.string.isRequired,
  loading: PropTypes.bool.isRequired,
  chartSize: PropTypes.object.isRequired,
};

export default withInstrumentLog(Chart);
