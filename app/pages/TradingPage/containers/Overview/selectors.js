import { createSelector } from 'reselect';

import { makeSelectWidgetConfig } from '../Dashboard/selectors';

export const makeSelectCurrentInstrumentId = () => createSelector(
  makeSelectWidgetConfig('overview'),
  (config) => config.get('selectedInstrumentId'),
);
