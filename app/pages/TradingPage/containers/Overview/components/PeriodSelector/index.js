import React from 'react';
import PropTypes from 'prop-types';

const availablePeriods = ['1D', '1W', '1M', '3M', '6M', '1Y', '3Y', '5Y', 'YTD'];

export default function PeriodSelector({ period, onSelect }) {
  return (
    <div className="overview__time-range">
      <div className="overview__time-range__left-box">
        <div className="overview__time-range__text">Time range</div>
        <div className="overview__time-range__times">
          {
            availablePeriods.map((p) =>
              (<button onClick={() => onSelect(p)} key={p} className={`overview__time-range__times__segment ${period === p && '_active'}`}>
                <span>{p}</span>
              </button>))
          }
        </div>
      </div>
    </div>
  );
}

PeriodSelector.propTypes = {
  period: PropTypes.string.isRequired,
  onSelect: PropTypes.func.isRequired,
};
