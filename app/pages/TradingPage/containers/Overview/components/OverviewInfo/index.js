import React from 'react';
import PropTypes from 'prop-types';
import ImmutableTypes from 'react-immutable-proptypes';
import { FormattedMessage, FormattedNumber } from 'react-intl';

import WatchlistData from 'unity-frontend-core/Models/WatchlistData';

import QuotValue from 'Components/QuotValue';


import messages from '../../../../messages';

import './style.css';

export default function OverviewInfo({ data, pipSize }) {
  const {
    bid, ask, dayHigh, dayLow,
  } = data;

  return (
    <div styleName="info">
      <div styleName="row-wrapper">
        <div styleName="slot">
          <div styleName="text"><FormattedMessage {...messages.bid} /></div>
          <div styleName="number">
            <QuotValue size="big" value={bid} pipSize={pipSize} />
          </div>
        </div>

        <div styleName="slot">
          <div styleName="text"><FormattedMessage {...messages.spread} /></div>
          <div styleName="number">
            <FormattedNumber maximumFractionDigits={1} value={data.calculateSpread(pipSize)} />
          </div>
        </div>

        <div styleName="slot">
          <div styleName="text"><FormattedMessage {...messages.ask} /></div>
          <div styleName="number">
            <QuotValue size="big" value={ask} />
          </div>
        </div>

        <div styleName="slot">
          <div styleName="text"><FormattedMessage {...messages.netChange} /></div>
          <div styleName="number">
            -
          </div>
        </div>
      </div>

      <div styleName="row-wrapper">
        <div styleName="slot">
          <div styleName="text"><FormattedMessage {...messages.percChange} /></div>
          <div styleName="number">
            -
          </div>
        </div>

        <div styleName="slot">
          <div styleName="text"><FormattedMessage {...messages.low} /></div>
          <div styleName="number">
            <QuotValue size="big" value={dayLow} />
          </div>
        </div>

        <div styleName="slot">
          <div styleName="text"><FormattedMessage {...messages.high} /></div>
          <div styleName="number">
            <QuotValue size="big" value={dayHigh} />
          </div>
        </div>

        <div styleName="slot">
          <div styleName="text"><FormattedMessage {...messages.prevClose} /></div>
          <div styleName="number">
                        -
          </div>
        </div>
      </div>
    </div>
  );
}


OverviewInfo.propTypes = {
  data: ImmutableTypes.recordOf(WatchlistData),
  pipSize: PropTypes.number.isRequired,
};
