import React from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import ImmutableTypes from 'react-immutable-proptypes';
import { createStructuredSelector } from 'reselect';
import { FormattedMessage } from 'react-intl';
import Select from 'react-select';

import InstrumentMarketData from 'unity-frontend-core/containers/InstrumentMarketData';
import InstrumentResolver from 'unity-frontend-core/containers/InstrumentResolver';
import { makeSelectActiveAccountPermissions } from 'unity-frontend-core/containers/SelectAccount/selectors';

import { makeSelectTheme } from 'Containers/App/selectors';
import { Button } from 'Components/GUI';

import { TIMEFRAME_OPTIONS } from 'unity-frontend-core/Models/Enums';

import Chart from './Chart';
import { OverviewHeader } from './OverviewHeader';
import messages from './messages';

import { openOrderPopup } from '../../actions';

import {} from './style.css';

class Overview extends React.PureComponent {
  selectTimeframe = ({ value }) => {
    this.props.onConfigChange({ timeframe: value });
  };

  renderInstrumentInfo = (instrument, id, props) =>
    <InstrumentMarketData render={this.renderOverview} id={id} instrument={instrument} {...props} />

  renderOverview = (marketData, id, { instrument, size: { width } }) => {
    const { accountPermissions, config, currentTheme } = this.props;

    const height = this.instrumentInfoContainer ? this.instrumentInfoContainer.offsetHeight : 0;

    if (!instrument) return null;

    const timeframe = config.get('timeframe') || TIMEFRAME_OPTIONS[3].value;
    /* eslint-disable jsx-a11y/no-static-element-int */
    return (
      <div
        styleName="overview"
        style={this.props.style}
      >

        {/* {marketData && <OverviewInfo data={marketData} pipSize={instrument.pipSize} />} */}
        <div styleName="graphic" ref={(ref) => this.instrumentInfoContainer = ref}>
          <div styleName="interval-select-wrapper">
            <div styleName="filter-box">
              <span styleName="interval-select-label">
Time frame
              </span>
              <div styleName="interval-selector">
                <Select
                  value={timeframe}
                  options={TIMEFRAME_OPTIONS}
                  onChange={this.selectTimeframe}
                  styleName="interval-select"
                  clearable={false}
                  optionstyleName="interval-select-option"
                />
              </div>
            </div>
            <div styleName="btn-box">
              <Button
                disabled={accountPermissions.indexOf('TRADE') === -1 || !instrument.availableForUserTrade}
                type="colored"
                onClick={() => this.props.openOrderPopup(instrument.id, null)}
              >
                <div styleName="button">
                  <FormattedMessage {...messages.trade} />
                </div>
              </Button>
            </div>
          </div>
          <div styleName="graphic" ref={(ref) => this.instrumentInfoContainer = ref}>
            {config && config.get('selectedInstrumentId')
            && <Chart
              chartSize={{ width, height }}
              selectedInstrumentId={config.get('selectedInstrumentId')}
              currentTheme={currentTheme}
              currentTimeframe={timeframe}
            />
            }
          </div>
        </div>
        {/* <InstrumentInfo instrument={instrument} isContainerNarrow={isContainerNarrow} /> */}
      </div>
    );
  };

  render() {
    const { config, size } = this.props;
    const instrumentId = config && config.get('selectedInstrumentId');
    if (!instrumentId) {
      return (
        <div styleName="no-data">
          <i className="icon-icon-widget-chart" />
          No instrument selected.
        </div>
      );
    }


    return (
      <InstrumentResolver
        id={instrumentId}
        render={this.renderInstrumentInfo}
        size={size}
        overViewState={this.state} // Force InstrumentResolver update by this.state change
      />
    );
  }
}

const mapStateToProps = createStructuredSelector({
  accountPermissions: makeSelectActiveAccountPermissions(),
  currentTheme: makeSelectTheme(),
});

const mapDispatchToProps = (dispatch) => ({
  openOrderPopup: (instrumentId, direction) => dispatch(openOrderPopup({
    instrumentId,
    direction,
  })),
});

Overview.propTypes = {
  style: PropTypes.object,
  size: PropTypes.object.isRequired,
  accountPermissions: ImmutableTypes.list,
  openOrderPopup: PropTypes.func.isRequired,
  onConfigChange: PropTypes.func.isRequired,
  currentTheme: PropTypes.string.isRequired,
  config: ImmutableTypes.map,
};

export default connect(mapStateToProps, mapDispatchToProps)(Overview);

export { OverviewHeader };
