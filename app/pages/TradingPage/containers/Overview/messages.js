import { defineMessages } from 'react-intl';

export default defineMessages({
  trade: {
    id: 'app.containers.TradingPage.Overview.trade',
    defaultMessage: 'Trade',
  },
  realtimePrices: {
    id: 'app.containers.TradingPage.Overview.realtimePrices',
    defaultMessage: 'Realtime prices',
  },
  marketOpen: {
    id: 'app.containers.TradingPage.Overview.marketOpen',
    defaultMessage: 'Market open',
  },
});
