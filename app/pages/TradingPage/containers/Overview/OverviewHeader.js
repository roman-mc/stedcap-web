import React, { Fragment } from 'react';
import PropTypes from 'prop-types';
import ImmutableProptypes from 'react-immutable-proptypes';

import { InstrumentSelectorModal } from 'Containers/InstrumentsTree';

import { WindowHeader, WindowHeaderTitle } from '../../../../components/Window';


export class OverviewHeader extends React.PureComponent { // eslint-disable-line react/prefer-stateless-function
  render() {
    return (
      <WindowHeader
        {...this.props}
        renderTitle={() =>
          <Fragment>
            <WindowHeaderTitle key="overview_header" icon="icon-icon-widget-chart">Chart</WindowHeaderTitle>
            <InstrumentSelectorModal
              widgetType="overview"
              config={this.props.config}
              onChange={(selectedInstrumentId) => this.props.onConfigChange({ selectedInstrumentId })}
              key="overview_header_instrument"
            />
          </Fragment>
        }
      />
    );
  }
}

OverviewHeader.propTypes = {
  config: ImmutableProptypes.map,
  onConfigChange: PropTypes.func.isRequired,
};
