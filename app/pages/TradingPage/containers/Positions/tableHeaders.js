export default [
  {
    name: 'instrument',
    label: 'Instrument',
    sortable: true,
  },
  {
    name: 'amount',
    label: 'Amount',
    sortable: true,
  },
  {
    name: 'avp',
    label: 'AVP',
    sortable: true,
  },
  {
    name: 'breakeven',
    label: 'Breakeven',
    sortable: true,
  },
  {
    name: 'swapInCur',
    label: 'Swap',
    sortable: true,
  },
  {
    name: 'marketRate',
    label: 'Market Price',
    sortable: true,
  },

  {
    name: 'unrealizedPl',
    label: 'Unrealized P/L',
    sortable: true,
  },
  {
    name: 'realizedPl',
    label: 'Realized P/L',
    sortable: true,
  },
  {
    name: 'closedPl',
    label: 'Closed P/L',
    sortable: true,
  },
  {
    name: 'totalPl',
    label: 'Total P/L',
    sortable: true,
  },

  {
    name: 'fee',
    label: 'Fee',
    sortable: true,
  },

  {
    name: 'accruedInterest',
    label: 'Accrd Interest',
    sortable: true,
  },

];
