import React from 'react';
import PropTypes from 'prop-types';

import '../style.css';

export default function PlusMinus({ expanding }) {
  return (
    <button styleName="plus-minus">
      <span className="positions__open">{expanding ? '−' : '+'}</span>
    </button>
  );
}

PlusMinus.propTypes = {
  expanding: PropTypes.bool.isRequired,
};
