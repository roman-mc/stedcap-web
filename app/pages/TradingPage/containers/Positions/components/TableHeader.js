import React from 'react';
import PropTypes from 'prop-types';

import SortArrows, { SORT_ORDER } from 'Components/SortArrows';

import '../style.css';

export default class TableHeader extends React.PureComponent {
  sortData = (newColumn) => {
    const { sortColumn, sortOrder } = this.props;
    const orderForSelectedColumn = sortOrder === SORT_ORDER.ASC ? SORT_ORDER.DESC : SORT_ORDER.ASC;
    const newOrder = newColumn === sortColumn ? orderForSelectedColumn : SORT_ORDER.ASC;
    this.props.onSortChange({ sortColumn: newColumn, sortOrder: newOrder });
  }

  render() {
    const { tableHeaders, sortOrder, sortColumn } = this.props;

    return (
      <div>
        <div styleName="row">
          {
            tableHeaders.map((tableHeader, i) => {
              const arrowPosition = i === 0 ? 'after' : 'before';
              return (
                <div styleName={'header'} key={i} > {/* eslint-disable-line react/no-array-index-key */}
                  <SortArrows
                    sortOrder={sortOrder}
                    sortColumn={sortColumn}
                    arrowPosition={arrowPosition}
                    {...tableHeader}
                    onChange={this.sortData}
                  />
                </div>
              );
            })
          }
          <div styleName="header _buttonfirst"></div>
        </div>
      </div>
    );
  }
}

TableHeader.propTypes = {
  tableHeaders: PropTypes.arrayOf(PropTypes.shape({
    name: PropTypes.string.isRequired,
    sortable: PropTypes.bool.isRequired,
    label: PropTypes.string,
  })),
  sortColumn: PropTypes.string,
  sortOrder: PropTypes.oneOf(Object.values(SORT_ORDER)),
  onSortChange: PropTypes.func.isRequired,
};
