import React from 'react';
import ImmutableProptypes from 'react-immutable-proptypes';
import PropTypes from 'prop-types';
import { FormattedNumber, FormattedMessage } from 'react-intl';
import { format } from 'date-fns';

import { STANDARD_DATE_TIME } from 'unity-frontend-core/utils/dateUtils';

import { INSTRUMENT_COLORS } from 'Model/enums';
// import { Button } from 'Components/GUI';

import messages from './messages';

import './style.css';

export default class Deal extends React.PureComponent {
  renderTrade() {
    const { isLast, category, deal: { executionDate } } = this.props;
    const angleStyle = {
      color: INSTRUMENT_COLORS[category.toUpperCase()],
    };

    return (
      <div styleName="instrument">
        <div styleName="instrument-icon">
          <i style={angleStyle} className={isLast ? 'icon-icon-single-line' : 'icon-icon-double-line'}></i>
        </div>
        <div>
          <FormattedMessage {...messages.tradeTitle} values={{ date: format(new Date(executionDate), STANDARD_DATE_TIME) }} />
        </div>
      </div>
    );
  }


  render() {
    const { deal } = this.props;
    const {
      direction, rate, commission, amount,
    } = deal;

    return (
      <div styleName="deal">
        <div styleName="deal-id">{this.renderTrade()}</div>
        <div styleName="deal-number"><FormattedNumber value={(direction ? -1 : 1) * amount} /> </div>
        <div styleName="deal-number"><FormattedNumber minimumFractionDigits={5} value={rate} /> </div>
        <div styleName="deal-number"><FormattedNumber minimumFractionDigits={5} value={commission} /> </div>
      </div>
    );
  }
}

Deal.propTypes = {
  deal: ImmutableProptypes.record.isRequired,
  isLast: PropTypes.bool.isRequired,
  category: PropTypes.string.isRequired,
};
