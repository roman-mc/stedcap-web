import { defineMessages } from 'react-intl';

export default defineMessages({
  amount: {
    id: 'app.containers.TradingPage.Positions.Deals.amount',
    defaultMessage: 'Amount',
  },
  rate: {
    id: 'app.containers.TradingPage.Positions.Deals.rate',
    defaultMessage: 'Rate',
  },
  commission: {
    id: 'app.containers.TradingPage.Positions.Deals.commission',
    defaultMessage: 'Fee',
  },
  tradeTitle: {
    id: 'app.containers.TradingPage.Positions.Deals.tradeTitle',
    defaultMessage: 'Trade {date}',
  },
});
