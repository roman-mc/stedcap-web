import React from 'react';
import ImmutableProptypes from 'react-immutable-proptypes';
import PropTypes from 'prop-types';
import { FormattedMessage } from 'react-intl';

import Deal from './Deal';

import messages from './messages';
import './style.css';

export default class Deals extends React.PureComponent {
  removeRow(row) {
    // e.stopPropagation();
    this.props.onDelete(row);
  }

  render() {
    const {
      onDelete, deals, category, hasTradePermission, availableForUserTrade, openOrderRequestPopup,
    } = this.props;

    return (
      <div styleName="deals">
        <div>
          <div styleName="header">
            <div styleName="header-amount"><FormattedMessage {...messages.amount} /></div>
            <div styleName="header-number"><FormattedMessage {...messages.rate} /></div>
            <div styleName="header-number"><FormattedMessage {...messages.commission} /></div>
          </div>
          {
            deals.map((deal, i) => {
              const isLast = deals.size - 1 === i;

              return (
                <Deal
                  key={`deal_${(i + 1)}`}
                  deal={deal}
                  isLast={isLast}
                  category={category}
                  onDelete={onDelete}
                  hasTradePermission={hasTradePermission}
                  availableForUserTrade={availableForUserTrade}
                  openOrderRequestPopup={openOrderRequestPopup}
                />
              );
            })
          }
        </div>
      </div>
    );
  }
}

Deals.propTypes = {
  deals: ImmutableProptypes.listOf(ImmutableProptypes.record).isRequired,
  category: PropTypes.string.isRequired,
  onDelete: PropTypes.func.isRequired,
  openOrderRequestPopup: PropTypes.func,
  hasTradePermission: PropTypes.bool.isRequired,
  availableForUserTrade: PropTypes.bool,
};
