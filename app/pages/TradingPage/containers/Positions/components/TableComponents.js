import React from 'react';
import PropTypes from 'prop-types';
import { Button } from 'Components/GUI';

import '../style.css';

export function Cell({ subClass = '', children, style }) {
  const additionalClass = subClass ? `_${subClass}` : '';
  return (
    <div styleName={`cell ${additionalClass}`} style={style}>
      {children}
    </div>
  );
}

Cell.propTypes = {
  subClass: PropTypes.string,
  children: PropTypes.any,
  style: PropTypes.object,
};


export function SimpleValue(props) {
  const { cellSubClass, subClass = '', value, handleValue } = props;

  return (
    <Cell subClass={cellSubClass}>
      <div styleName={`simple-value ${subClass}`}>{handleValue(value)}</div>
    </Cell>
  );
}


SimpleValue.propTypes = {
  value: PropTypes.any,
  subClass: PropTypes.string,
  handleValue: PropTypes.func,
  cellSubClass: PropTypes.string,
};

SimpleValue.defaultProps = {
  handleValue: (value) => value,
};


export function Th({ className = '', children }) {
  return (<div styleName={`header ${className}`}><span styleName="header-wrap">{children}</span></div>);
}

Th.propTypes = {
  className: PropTypes.string,
  children: PropTypes.node,
};


export function MetaValue(props) {
  const { cellSubClass, value, subClass = '', handleValue } = props;
  return (
    <Cell subClass={cellSubClass}>
      <div styleName={`meta-value ${subClass}`}>
        {handleValue(value)}
      </div>
    </Cell>
  );
}

MetaValue.propTypes = {
  value: PropTypes.any,
  subClass: PropTypes.string,
  handleValue: PropTypes.func,
  cellSubClass: PropTypes.string,
};

MetaValue.defaultProps = {
  handleValue: (value) => value,
};


export function TableButton(props) {
  const { disabled, cellSubClass, buttonOnClick, children } = props;

  return (
    <Cell subClass={cellSubClass}>
      <Button disabled={disabled} onClick={buttonOnClick} type="primary">
        {children}
      </Button>
    </Cell>
  );
}

TableButton.propTypes = {
  buttonOnClick: PropTypes.func,
  cellSubClass: PropTypes.string.isRequired,
  children: PropTypes.any,
  disabled: PropTypes.bool,
};


export function Child(props) {
  const { children, colspan } = props;

  return (
    <div className="positions__child">
      <div colSpan={colspan} styleName="child-cell">
        {children}
      </div>
    </div>
  );
}

Child.propTypes = {
  children: PropTypes.any.isRequired,
  colspan: PropTypes.number.isRequired,
};
