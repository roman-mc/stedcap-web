import React from 'react';
import PropTypes from 'prop-types';
import ImmutableProptypes from 'react-immutable-proptypes';

import { FormattedNumber } from 'react-intl';

import InstrumentCategory from 'Components/InstrumentCategory';

import InstrumentResolver from 'unity-frontend-core/containers/InstrumentResolver';

import Deals from './Deals';
import PlusMinus from './PlusMinus';
import { SimpleValue, MetaValue, TableButton, Cell, Child } from './TableComponents';

import '../style.css';

const tableCells = {
  closedPl: (data) => (
    <MetaValue
      value={data.position.head.closedPl}
      key={data.i}
      handleValue={(value) => <FormattedNumber value={value} />}
    />
  ),
  totalPl: (data) => (
    <MetaValue
      value={data.position.head.totalPl}
      key={data.i}
      handleValue={(value) => <FormattedNumber value={value} />}
    />
  ),
  unrealizedPl: (data) => (
    <MetaValue
      value={data.position.head.unrealizedPl}
      key={data.i}
      handleValue={(value) => <FormattedNumber value={value} />}
    />
  ),
  realizedPl: (data) => (
    <MetaValue
      value={data.position.head.realizedPl}
      key={data.i}
      handleValue={(value) => <FormattedNumber value={value} />}
    />
  ),
  breakeven: (data) => (
    <MetaValue
      value={data.position.head.breakeven}
      key={data.i}
      handleValue={(value) => <FormattedNumber minimumFractionDigits={5} value={value} />}
    />
  ),
  avp: (data) => (
    <MetaValue
      value={data.position.head.avp}
      key={data.i}
      handleValue={(value) => <FormattedNumber minimumFractionDigits={5} value={value} />}
    />
  ),
  fee: (data) => (
    <MetaValue
      value={data.position.head.fee}
      key={data.i}
      handleValue={(value) => <FormattedNumber value={value} />}
    />
  ),
  swapInCur: (data) => (
    <MetaValue
      value={data.position.head.swapInCur}
      key={data.i}
      handleValue={(value) => <FormattedNumber value={value} />}
    />
  ),
  amount: (data) => {
    return (
      <SimpleValue
        key={data.i}
        value={(data.position.head.direction ? -1 : 1) * data.position.head.amount}
        handleValue={(value) => <FormattedNumber value={value} />}
      />
    );
  },
  marketRate: (data) => (
    <SimpleValue
      key={data.i}
      value={data.position.head.marketRate}
    />
  ),
  instrument: (data) => <Instrument
    key={data.i}
    instrumentId={data.position.head.instrumentId}
    dealsCount={data.position.deals.size}
    onToggle={() => console.log('onToggle', data.position.head.instrumentId) || data.togglePositionExpand(data.position.head.instrumentId)}
    dealsExpanded={data.position.isExpanded}
  />,
  accruedInterest: (data) => (
    <SimpleValue
      key={data.i}
      value={data.position.head.accruedInterest}
    />
  ),
};


class Instrument extends React.PureComponent {
  renderInstrument = (instrumentInfo, _, { onToggle, dealsExpanded }) => {
    return (
      <Cell subClass="instrument">
        <div styleName="instrument" onClick={onToggle}>
          <div>
            <PlusMinus expanding={dealsExpanded} />
          </div>
          <div styleName="instrument-label">
            {instrumentInfo && <InstrumentCategory instrument={instrumentInfo} size="28px" fontSize="16px" />}
          </div>
          <div styleName="instrument-text" data-position-instrument={instrumentInfo ? instrumentInfo.displayName : ''}>{instrumentInfo && instrumentInfo.displayName}</div>
        </div>
      </Cell>
    );
  }
  render() {
    const { onToggle, instrumentId, dealsExpanded } = this.props;
    return <InstrumentResolver render={this.renderInstrument} onToggle={onToggle} dealsExpanded={dealsExpanded} id={instrumentId} />;
  }
}

Instrument.propTypes = {
  instrumentId: PropTypes.number.isRequired,
  onToggle: PropTypes.func.isRequired,
  dealsExpanded: PropTypes.bool.isRequired,
};


export default class Position extends React.PureComponent {
  closePosition(e, { head: { instrumentId } }) {
    e.stopPropagation();
    this.props.onClose(instrumentId);
  }

  renderCells() {
    const { position, tableHeaders, dealsExpanded, togglePositionExpand } = this.props;

    return tableHeaders.map(({ name }, i) =>
      tableCells[name]({
        i,
        position,
        state: this.state,
        dealsExpanded,
        togglePositionExpand,
      })
    );
  }

  renderChildren(instrumentInfo) {
    const { position, tableHeaders, category, hasTradePermission } = this.props;

    if (!position.isExpanded) return null;

    return (
      <Child colspan={tableHeaders.length}>
        <Deals
          deals={position.deals}
          color={this.color}
          position={position}
          tableHeaders={tableHeaders}
          category={category}
          hasTradePermission={hasTradePermission}
          availableForUserTrade={instrumentInfo && instrumentInfo.availableForUserTrade}
          openOrderRequestPopup={this.props.openOrderRequestPopup}
        />
      </Child>
    );
  }

  renderPosition = (instrumentInfo) => {
    const { position, hasTradePermission, dealsExpanded, openOrderRequestPopup } = this.props;
    const rowSubClass = dealsExpanded ? '_active' : '';

    return (
      <div styleName="position">
        <div styleName="row" className={`${rowSubClass}`}>
          {this.renderCells(instrumentInfo)}
          <TableButton
            cellSubClass="buttonfirst"
            buttonOnClick={(e) => {
              if (instrumentInfo && !instrumentInfo.availableForUserTrade) {
                openOrderRequestPopup({
                  amount: position.head.amount,
                  expectedPrice: 'Market',
                  direction: position.head.direction ? 0 : 1,
                  symbolDescription: instrumentInfo.code,
                });
              } else {
                this.closePosition(e, position);
              }
            }}
            disabled={!hasTradePermission}
          >
            <div styleName="button-close">
              Close
            </div>
          </TableButton>
        </div>
        {this.renderChildren(instrumentInfo)}
      </div>
    );
  }

  render() {
    const { position } = this.props;

    return (
      <InstrumentResolver
        id={position.head.instrumentId}
        key={`poz_${position.head.instrumentId}`}
        render={this.renderPosition}
        {...this.props}
      />
    );
  }
}

Position.propTypes = {
  category: PropTypes.string.isRequired,
  position: ImmutableProptypes.record,
  tableHeaders: PropTypes.array.isRequired,
  onClose: PropTypes.func.isRequired,
  hasTradePermission: PropTypes.bool.isRequired,
  dealsExpanded: PropTypes.bool,
  togglePositionExpand: PropTypes.func.isRequired,
  openOrderRequestPopup: PropTypes.func,
};
