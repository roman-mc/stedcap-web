import React from 'react';
import PropTypes from 'prop-types';
import { Button } from 'Components/GUI';
import ImmutablePropTypes from 'react-immutable-proptypes';
import { connect } from 'react-redux';
import { createStructuredSelector } from 'reselect';
import { makeSelectInstrumentsMap } from 'unity-frontend-core/containers/InstrumentResolver/selectors';

import { SORT_ORDER } from 'Components/SortArrows';

import Position from './Position';
import PlusMinus from './PlusMinus';
import { Cell, Child } from './TableComponents';

import { } from '../style.css';

function InstrumentCategory({ positionsWithCategory, onVisibilityToggle }) {
  return (
    <Cell subClass="instrument">
      <div styleName="instrument" onClick={onVisibilityToggle}>
        <div>
          <PlusMinus expanding={positionsWithCategory.isExpanded} />
        </div>
        <div styleName="instrument-text">{positionsWithCategory.category}</div>
        <div styleName="instrument-count">{positionsWithCategory.positions.size}</div>
      </div>
    </Cell>
  );
}

InstrumentCategory.propTypes = {
  positionsWithCategory: ImmutablePropTypes.record.isRequired,
  onVisibilityToggle: PropTypes.func,
};


class Category extends React.PureComponent {
  toggleCategoryVisibility = (event) => {
    event.preventDefault();
    this.props.toggleCategoryExpand(this.props.positionsWithCategory.category);
  }

  removePositions = (e) => {
    e.stopPropagation();
    const { positionsWithCategory, onClose } = this.props;
    positionsWithCategory.positions.forEach(({ head: { instrumentId } }) => onClose(instrumentId));
  }

  preparePositions = (positions, sortOrder = SORT_ORDER.ASC, sortColumn = false) => {
    if (!sortColumn) {
      return positions;
    }
    const asc = sortOrder === SORT_ORDER.ASC;
    if (sortColumn === 'instrument') {
      const { instruments } = this.props;
      const sortedPositions = positions.sortBy((p) => instruments.get(p.head.instrumentId, { code: '' }).code);
      return asc ? sortedPositions : sortedPositions.reverse();
    }
    const sortedPositions = positions.sortBy((p) => {
      if (sortColumn === 'amount') {
        return (p.head.direction ? -1 : 1) * p.head[sortColumn];
      }
      return p.head[sortColumn];
    });
    return asc ? sortedPositions : sortedPositions.reverse();
  }

  renderColumns() {
    const { tableHeaders } = this.props;

    return tableHeaders
      .filter((tableHeader) => tableHeader.name !== 'instrument')
      .map((tableHeader, i) => <Cell subClass={name} key={`thh_${i + 1}`} />);
  }

  renderChildren() {
    const { hasTradePermission, positionsWithCategory, tableHeaders, onClose, sortOrder, sortColumn } = this.props;

    return positionsWithCategory.isExpanded && this.preparePositions(positionsWithCategory.positions, sortOrder, sortColumn).map((position, i) => (
      <Child colspan={tableHeaders.length + 1} key={`tcfp_${i + 1}`}>
        <Position
          category={positionsWithCategory.category}
          position={position}
          onClose={onClose}
          tableHeaders={tableHeaders}
          openOrderRequestPopup={this.props.openOrderRequestPopup}
          hasTradePermission={hasTradePermission}
          togglePositionExpand={this.props.togglePositionExpand}
          dealsExpanded={positionsWithCategory.isExpanded}
        />
      </Child>
    ));
  }

  render() {
    const { onCloseAll, hasTradePermission, positionsWithCategory, theme } = this.props;

    return <div styleName="row-group" className={`_${theme}`}>
      <div styleName="row">
        <InstrumentCategory
          positionsWithCategory={positionsWithCategory}
          positions={positionsWithCategory.positions}
          onVisibilityToggle={this.toggleCategoryVisibility}
        />
        {this.renderColumns()}
        <Cell subClass="buttonfirst">
          <Button type="tertiary" disabled={!hasTradePermission} onClick={onCloseAll}>
            Close All
          </Button>
        </Cell>
      </div>
      {this.renderChildren()}
    </div>;
  }
}


Category.propTypes = {
  theme: PropTypes.string.isRequired,
  tableHeaders: PropTypes.arrayOf(PropTypes.shape({
    name: PropTypes.string.isRequired,
    sortable: PropTypes.bool.isRequired,
    label: PropTypes.string,
  })),
  hasTradePermission: PropTypes.bool.isRequired,
  sortOrder: PropTypes.string,
  sortColumn: PropTypes.string,
  onCloseAll: PropTypes.func.isRequired,
  onClose: PropTypes.func.isRequired,
  positionsWithCategory: ImmutablePropTypes.record,
  instruments: ImmutablePropTypes.map,

  togglePositionExpand: PropTypes.func.isRequired,
  toggleCategoryExpand: PropTypes.func.isRequired,
  openOrderRequestPopup: PropTypes.func,
};

const mapStateToProps = createStructuredSelector({
  instruments: makeSelectInstrumentsMap(),
});

export default connect(mapStateToProps)(Category);
