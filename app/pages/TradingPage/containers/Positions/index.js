import React, { Fragment } from 'react';
import PropTypes from 'prop-types';
import ImmutableProptypes from 'react-immutable-proptypes';

import Positions from './containers/Positions';
import Orders from './containers/Orders';

import { WindowHeader, WindowHeaderTitle } from '../../../../components/Window';

export default class PositionsAndOrders extends React.PureComponent { // eslint-disable-line react/prefer-stateless-function
  render() {
    const { ...props } = this.props;
    return (
      this.props.config.get('mode') === 'positions'
        ? <Positions {...props} />
        : <Orders {...props} />
    );
  }
}

PositionsAndOrders.propTypes = {
  config: ImmutableProptypes.map,
};


export function PositionHeader({ onConfigChange, config, ...props }) {
  return (
    <WindowHeader
      {...props}
      renderTitle={() => <Fragment>
        <WindowHeaderTitle
          icon="icon-icon-positions"
          onClick={() => onConfigChange({ mode: 'positions' })}
          active={config.get('mode') === 'positions'}
          dataWindowName="positions"
        >
          Positions
        </WindowHeaderTitle>
        <WindowHeaderTitle
          onClick={() => onConfigChange({ mode: 'orders' })}
          active={config.get('mode') === 'orders'}
          icon="icon-icon-orders"
          dataWindowName="orders"
        >
          Orders
        </WindowHeaderTitle>
      </Fragment>
      }
    />
  );
}

PositionHeader.propTypes = {
  onConfigChange: PropTypes.func.isRequired,
  config: ImmutableProptypes.map,
};
