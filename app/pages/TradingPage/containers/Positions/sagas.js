import { take, call, put, select, race } from 'redux-saga/effects';
import { takeEvery, delay } from 'redux-saga';
import Raven from 'raven-js';

import * as ordersApi from 'unity-frontend-core/api/ordersApi';
// import { callApi, generateListener } from 'Utils/sagaUtils';
import { generateListener } from 'Utils/sagaUtils';

import { loadOrderRequests } from 'unity-frontend-core/containers/OrderRequests/actions';
import { apiError } from 'unity-frontend-core/actions';
import { selectAccount } from 'unity-frontend-core/containers/SelectAccount/actions';
import { makeSelectActiveAccount } from 'unity-frontend-core/containers/SelectAccount/selectors';
import { makeSelectCurrentUser } from 'Containers/App/selectors';
import { USER_SETTINGS } from 'unity-frontend-core/Models/Enums';
import { loadWidgetSetting } from 'unity-frontend-core/containers/UserAccountSettings/actions';

// import { confirmSaga } from '../../../../containers/ConfirmationDialog/sagas';
import { doLogout } from '../../../LogoutPage/actions';

import {
  // closePositions,

  // closeAll,
  loadOrders,
  cancelOrder,
} from './actions';


import {
  makeSelectOrders,
} from '../../selectors';

// import messages from './messages';

export function* loadOrdersHandler({ payload }) {
  const ordersLoaded = yield select(makeSelectOrders());
  const restart = payload && payload.restart;
  if (!ordersLoaded.size || restart) {
    while (true) {
      yield call(loadOrdersApiRequest);
      try {
        const { loggedOut } = yield race({
          delay: call(delay, 10 * 1000),
          loggedOut: take(doLogout.toString()),
        });
        if (loggedOut) {
          throw new Error('User logged out, stop orders refresh');
        }
      } catch (e) {
        return null;
      }
    }
  }
  return null;
}

function* loadOrdersApiRequest() {
  try {
    const { orders } = yield call(ordersApi.list);
    yield put(loadOrders.success({ orders }));
  } catch (e) {
    yield put(loadOrders.error(e));
    yield put(apiError(e));
  }
}

function* cancelOrderHandler({ payload: { orderId } }) {
  try {
    const result = yield call(ordersApi.cancel, { id: orderId });
    yield put(cancelOrder.success(result));
    yield put(loadOrders());
  } catch (e) {
    yield put(cancelOrder.error(e));
    Raven.captureException(e);
  }
}

function* loadOrdersSaga() {
  yield call(takeEvery, loadOrders.toString(), loadOrdersHandler);
}

function* cancelOrderSaga() {
  yield call(takeEvery, cancelOrder.toString(), cancelOrderHandler);
}

export function* updateAccountHandler() {
  yield put(loadOrderRequests());
}

function* loadAccountSettingsSaga() {
  const account = yield select(makeSelectActiveAccount());
  const user = yield select(makeSelectCurrentUser());
  yield put(loadWidgetSetting({
    name: USER_SETTINGS.CATEGORIES,
    multiAccountId: account,
    profileId: user.id,
  }));
}

export default [
  loadOrdersSaga,
  cancelOrderSaga,
  generateListener(selectAccount.success, updateAccountHandler),
  generateListener(selectAccount.success, loadAccountSettingsSaga),
];
