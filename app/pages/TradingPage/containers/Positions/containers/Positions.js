import React from 'react';
import PropTypes from 'prop-types';
import ImmutablePropTypes from 'react-immutable-proptypes';
import { Map, List } from 'immutable';
import { SortableContainer, SortableElement } from 'react-sortable-hoc';

import { connect } from 'react-redux';
import { FormattedMessage } from 'react-intl';
import { createStructuredSelector } from 'reselect';

import PositionsCore, { actions } from 'unity-frontend-core/containers/Positions';
import UserAccountWidgetSettings from 'unity-frontend-core/containers/UserAccountSettings';

import { USER_SETTINGS } from 'unity-frontend-core/Models/Enums';
import { makeSelectActiveAccountPermissions, makeSelectActiveAccount } from 'unity-frontend-core/containers/SelectAccount/selectors';

import LoadingIndicator from 'Components/LoadingIndicator';
import ScrollBar from 'Components/Scrollbar';
import { makeSelectCurrentUser } from 'Containers/App/selectors';

import Category from '../components/Category';
import TableHeader from '../components/TableHeader';

import tableHeaders from '../tableHeaders';

import { openOrderRequestPopup } from '../../../actions';
import { setConfig } from '../../Dashboard/actions';

import { askForConfirmation } from '../../../../../containers/ConfirmationDialog/actions';

import messages from '../messages';

import styles from '../style.css';

const SortableCategory = SortableElement(Category);
export class Positions extends React.PureComponent {
  componentDidMount() {
    const { currentUser: { id: profileId }, account: multiAccountId } = this.props;
    this.props.loadPositions();
    this.props.startPositionsWatching();

    this.props.loadWidgetSetting({
      name: USER_SETTINGS.CATEGORIES,
      multiAccountId,
      profileId,
    });
  }

  onSortChange = ({ sortColumn, sortOrder }) => {
    const { blockId } = this.props;
    this.props.changeSortColumn({ blockId, sortColumn, sortOrder });
  }


  renderTableData() {
    const { positions, accountPermissions, config, widget = new Map(), currentUser: { settings: { theme = '' } } } = this.props;
    const categoriesOrder = widget.getIn(['value', 'categories'], new List());
    return positions.sortBy((pos) => categoriesOrder.indexOf(pos.category)).map((positionsWithCategory, index) =>
      <SortableCategory
        index={index}
        key={`category_${positionsWithCategory.category}`}
        tableHeaders={tableHeaders}
        onClose={this.props.closePosition}
        onCloseAll={this.props.closeAll}
        positionsWithCategory={positionsWithCategory}
        togglePositionExpand={this.props.togglePositionExpand}
        toggleCategoryExpand={this.props.toggleCategoryExpand}
        hasTradePermission={accountPermissions.includes('TRADE')}
        sortOrder={config.get('sortOrder')}
        sortColumn={config.get('sortColumn')}
        theme={theme}
        openOrderRequestPopup={this.props.openOrderRequestPopup}
      />
    );
  }


  renderNoData() {
    return (
      <div styleName="no-data" style={this.props.style}>
        <div styleName="no-data-wrapper">
          <i className="icon-icon-positions"></i>
          <p><FormattedMessage {...messages.noData} /></p>
        </div>
      </div>
    );
  }


  render() {
    const { loading, config } = this.props;
    if (loading) {
      return (
        <div style={{ position: 'relative', height: '100%' }}>
          <LoadingIndicator loading />
        </div>

      );
    }

    if (!this.props.positions.size) {
      return this.renderNoData();
    }

    return (
      <ScrollBar>
        <div styleName="positions">
          <TableHeader
            tableHeaders={tableHeaders}
            sortOrder={config.get('sortOrder')}
            sortColumn={config.get('sortColumn')}
            onSortChange={this.onSortChange}
          />
          <SortContainer
            distance={20}
            lockAxis="y"
            className={styles['positions-container']}
            onSortEnd={({ oldIndex, newIndex }) => {
              const { positions, currentUser: { id: profileId }, account: multiAccountId, widget = new Map() } = this.props;
              const currentCategories = widget.getIn(['value', 'categories']);
              const currentPositionCategories = positions.map((_) => _.category);

              const categories = currentCategories
                ? currentPositionCategories.sortBy((pos) => currentCategories.indexOf(pos))
                : currentPositionCategories.keySeq();

              const oldValue = categories.get(oldIndex);
              this.props.setWidgetSetting({
                systemPropertyName: USER_SETTINGS.CATEGORIES,
                profileId,
                multiAccountId,
                value: {
                  categories: categories.splice(oldIndex, 1).splice(newIndex, 0, oldValue).toJS(),
                },
              });
            }}
          >
            {this.renderTableData()}
          </SortContainer>
        </div>
      </ScrollBar>
    );
  }
}

const Container = ({ children, className = '' }) => {
  return <ScrollBar className={className}>{children}</ScrollBar>;
};
Container.propTypes = {
  className: PropTypes.string,
  children: PropTypes.any,
};

const SortContainer = SortableContainer(Container);
Positions.propTypes = {
  blockId: PropTypes.array.isRequired,
  style: PropTypes.object,
  widget: PropTypes.object,
  currentUser: PropTypes.object,
  account: PropTypes.number,
  config: ImmutablePropTypes.map,
  loadWidgetSetting: PropTypes.func,
  setWidgetSetting: PropTypes.func,
  openOrderRequestPopup: PropTypes.func,
  accountPermissions: ImmutablePropTypes.list,
  positions: ImmutablePropTypes.list,
  loading: PropTypes.bool,
  loadPositions: PropTypes.func.isRequired,
  startPositionsWatching: PropTypes.func.isRequired,
  closePosition: PropTypes.func.isRequired,
  changeSortColumn: PropTypes.func.isRequired,
  closeAll: PropTypes.func.isRequired,
  toggleCategoryExpand: PropTypes.func.isRequired,
  togglePositionExpand: PropTypes.func.isRequired,
  // ...Container.propTypes,
};


const mapStateToProps = createStructuredSelector({
  accountPermissions: makeSelectActiveAccountPermissions(),
  currentUser: makeSelectCurrentUser(),
  account: makeSelectActiveAccount(),
});


function mapDispatchToProps(dispatch) {
  return {
    openOrderRequestPopup: (order) => dispatch(openOrderRequestPopup(order)),
    changeSortColumn: ({ blockId, sortColumn, sortOrder }) => dispatch(setConfig({ blockId, config: { sortColumn, sortOrder } })),
    closePosition: (instrumentId) => {
      dispatch(askForConfirmation({
        messages: {
          header: messages.closePositionHeader,
          confirm: messages.closePositionOkBtn,
        },
        onSuccess: actions.closePosition({ instrumentId }),
      }));
    },
    closeAll: () => {
      dispatch(askForConfirmation({
        messages: {
          header: messages.closePositionHeader,
          confirm: messages.closePositionOkBtn,
        },
        onSuccess: actions.closeAll(),
      }));
    },
  };
}

export default PositionsCore(UserAccountWidgetSettings(connect(mapStateToProps, mapDispatchToProps)(Positions), USER_SETTINGS.CATEGORIES));
