import React from 'react';
import { FormattedMessage, FormattedNumber } from 'react-intl';
import ImmutablePropTypes from 'react-immutable-proptypes';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import moment from 'moment';
import { createStructuredSelector } from 'reselect';

import { STANDARD_DATE_TIME_SECONDS } from 'unity-frontend-core/utils/dateUtils';
import OrderRequests from 'unity-frontend-core/containers/OrderRequests';
import InstrumentResolver from 'unity-frontend-core/containers/InstrumentResolver';
import { makeSelectActiveAccountPermissions } from 'unity-frontend-core/containers/SelectAccount/selectors';

import InstrumentCategory from 'Components/InstrumentCategory/index';
import SortArrows from 'Components/SortArrows/index';
import { Button } from 'Components/GUI/index';
import ScrollBar from 'Components/Scrollbar';

import { Cell } from '../components/TableComponents';
import { DEFAULT_CHAT } from '../../../../../config/dashboardConfig';
import messages from '../messages';
import styles from '../style.css';

import { makeSelectOrders } from '../../../selectors';

import { loadOrders, cancelOrder } from '../actions';
import { openOrderPopup, openOrderRequestPopup } from '../../../actions';

import { TRADE_DIRECTION } from '../../../../../model/enums';
import { makeSelectTradingTree } from '../../Dashboard/selectors';
import { addChat } from '../../Dashboard/actions';


class Orders extends React.PureComponent {
  componentDidMount() {
    this.props.loadOrders();
    this.props.loadOrderRequests();
    this.props.startOrderRequestsWatching();
  }

  componentWillUnmount() {
    this.props.stopOrderRequestsWatching();
  }


  openChat = (order) => () => {
    const activePanelId = this.props.config.getIn(['config', 'activePanelId']);
    const widgetToSave = { ...DEFAULT_CHAT, config: { id: order.id, symbolDescription: order.symbolDescription } };
    if (order.unreadByClient) {
      this.props.readOrderRequestChatMessage(order.id);
    }
    this.props.addChat(activePanelId, widgetToSave);
  }

  countUnread = ({ unreadByClient }) => {
    return unreadByClient ? <span styleName="unread">{unreadByClient}</span> : null;
  }

  hasChars = (str) => {
    return str.search(/[^.,\d]/igm) > -1;
  }

  renderData() {
    const { orders, orderRequests } = this.props;

    return (
      <ScrollBar>
        <div className={`${styles.positions} ${styles.orders}`}>
          <div>
            <div styleName="row">
              <div styleName="header _instrument"><SortArrows label="Instrument" /></div>
              <div styleName="header"><SortArrows label="Date" /></div>
              <div styleName="header"><div className="pull-right"><SortArrows label="Amount" /></div></div>
              <div styleName="header"><div className="pull-right"><SortArrows label="Price" /></div></div>
              <div styleName="header"><div className="pull-right"><SortArrows label="Total" /></div></div>
              <div styleName="header _buttonfirst grow"></div>
            </div>
          </div>
          <div>
            <React.Fragment>
              {orders.map((order) => {
                return (
                  <InstrumentResolver
                    id={order.instrumentId}
                    key={`ord_${order.id}`}
                    render={this.renderRow}
                    order={order}
                    {...this.props}
                  />
                );
              })}
              {orderRequests.map((order) => this.renderRow({ displayName: order.symbolDescription }, null, { order }))}
            </React.Fragment>
          </div>
        </div>
      </ScrollBar>
    );
  }

  renderTotalPrice = (order) => {
    if (order.price) {
      return <FormattedNumber value={order.amount * order.price} minimumSignificantDigits={4} />;
    }
    if (Number(order.amount) && !this.hasChars(order.expectedPrice) && Number(parseFloat(order.expectedPrice))) {
      return <FormattedNumber value={order.amount * parseFloat(order.expectedPrice)} minimumSignificantDigits={4} />;
    }
    return '-';
  }

  renderPrice = (order) => {
    if (order.price) {
      return <FormattedNumber value={order.price} minimumSignificantDigits={4} />;
    }
    if (Number(order.expectedPrice)) {
      return <FormattedNumber value={parseFloat(order.expectedPrice)} minimumSignificantDigits={4} />;
    }
    return order.expectedPrice;
  }

  renderNoData() {
    return (
      <div styleName="no-data">
        <div styleName="no-data-wrapper">
          <i className="icon-icon-orders"></i>
          <p><FormattedMessage {...messages.noOrdersData} /></p>
        </div>
      </div>
    );
  }

  renderRow = (instrumentInfo, id, { order }) => {
    const { accountPermissions } = this.props;

    if (!instrumentInfo) {
      return null;
    }

    return (
      <div styleName="row" key={order.id}>
        <Cell subClass="instrument">
          <div styleName="instrument">
            {id && <div styleName="instrument-category">
              <InstrumentCategory instrument={instrumentInfo} size="28px" fontSize="16px" />
            </div>}
            {instrumentInfo.displayName}
          </div>
        </Cell>
        <Cell>{moment(order.created).format(STANDARD_DATE_TIME_SECONDS)}</Cell>
        <Cell>
          <div className="pull-right">
            {order.direction === TRADE_DIRECTION.SELL ? '-' : ''}
            <FormattedNumber value={order.amount} minimumSignificantDigits={4} />
          </div>
        </Cell>
        <Cell><div className="pull-right">{this.renderPrice(order)}</div></Cell>
        <Cell><div className="pull-right">{this.renderTotalPrice(order)}</div>
        </Cell>
        <Cell style={{ flexGrow: '1.8' }}>
          <div styleName="orders-button-container">
            {!id && <Button onClick={this.openChat(order)} type="with-margin primary" disabled={!(accountPermissions.includes('TRADE'))}>
              Chat{this.countUnread(order)}
            </Button>}
            <Button onClick={() => order.price ? this.props.openOrderPopup(order) : this.props.openOrderRequestPopup(order.toJS())} type="with-margin primary" disabled={!(accountPermissions.includes('TRADE'))}>
                Edit
            </Button>
            <Button onClick={() => order.price ? this.props.cancelOrder(order.id) : this.props.cancelOrderRequest(order.id)} type="primary" disabled={!(accountPermissions.includes('TRADE'))}>
                Cancel
            </Button>
          </div>
        </Cell>
      </div>
    );
  }

  render() {
    const { orders, orderRequests } = this.props;
    return orders.size > 0 || (orderRequests && orderRequests.size > 0) ? this.renderData() : this.renderNoData();
  }
}

Orders.propTypes = {
  openOrderPopup: PropTypes.func.isRequired,
  openOrderRequestPopup: PropTypes.func.isRequired,
  cancelOrder: PropTypes.func.isRequired,
  cancelOrderRequest: PropTypes.func.isRequired,
  loadOrders: PropTypes.func.isRequired,
  loadOrderRequests: PropTypes.func.isRequired,
  startOrderRequestsWatching: PropTypes.func.isRequired,
  stopOrderRequestsWatching: PropTypes.func.isRequired,
  readOrderRequestChatMessage: PropTypes.func.isRequired,
  addChat: PropTypes.func.isRequired,
  orders: ImmutablePropTypes.listOf(ImmutablePropTypes.record).isRequired,
  orderRequests: PropTypes.any,
  config: ImmutablePropTypes.record,
  accountPermissions: ImmutablePropTypes.list,
};

const mapStateToProps = createStructuredSelector({
  orders: makeSelectOrders(),
  config: makeSelectTradingTree(),
  accountPermissions: makeSelectActiveAccountPermissions(),
});

const mapDispatchToProps = (dispatch) => ({
  addChat: (panelId, widget) => dispatch(addChat({ panelId, widget })),
  loadOrders: () => dispatch(loadOrders()),
  cancelOrder: (orderId) => dispatch(cancelOrder({ orderId })),
  openOrderPopup: (order) => dispatch(openOrderPopup(order)),
  openOrderRequestPopup: (order) => dispatch(openOrderRequestPopup(order)),
});

export default OrderRequests(connect(mapStateToProps, mapDispatchToProps)(Orders));
