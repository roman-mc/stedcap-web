import createApiAction from 'unity-frontend-core/utils/createAction';

export const loadOrders = createApiAction('app/TradingPage/LOAD_ORDERS');
export const cancelOrder = createApiAction('app/TradingPage/CANCEL_ORDER');
