import { List, Map } from 'immutable';
import { createReducer } from 'redux-act';

import Order from 'Model/Order';

import {
  loadOrders,
} from './actions';

const initialState = new Map({
  orders: new List(),
});

export default createReducer({
  [loadOrders.success]: (state, { orders }) => state.set('orders', new List(orders.map((o) => new Order(o)))),
}, initialState);
