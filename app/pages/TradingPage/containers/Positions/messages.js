import { defineMessages } from 'react-intl';

export default defineMessages({
  noData: {
    id: 'containers.TradingPage.Positions.noData',
    defaultMessage: 'Your active positions will be listed here',
  },
  noOrdersData: {
    id: 'containers.TradingPage.Positions.noOrdersData',
    defaultMessage: 'Your active orders will be listed here',
  },

  closePositionHeader: {
    id: 'containers.TradingPage.Positions.closePositionHeader',
    defaultMessage: 'Are you sure?',
  },

  closeAllPositionsHeader: {
    id: 'containers.TradingPage.Positions.closeAllPositionsHeader',
    defaultMessage: 'Close all positions',
  },

  closePositionsText: {
    id: 'containers.TradingPage.Positions.closePositionsText',
    defaultMessage: 'This action can not be undone',
  },
  closePositionOkBtn: {
    id: 'containers.TradingPage.Positions.closePositionOkBtn',
    defaultMessage: 'Close',
  },
});
