import React from 'react';
import PropTypes from 'prop-types';
import ImmutablePropTypes from 'react-immutable-proptypes';
import { connect } from 'react-redux';
import { format } from 'date-fns';
import { injectIntl } from 'react-intl';
import { Form, reduxForm } from 'redux-form/immutable';

import OrderRequests from 'unity-frontend-core/containers/OrderRequests';
import { STANDARD_DATE, STANDARD_TIME } from 'unity-frontend-core/utils/dateUtils';
import { requireValidate } from 'unity-frontend-core/utils/form/validation';
import { INBOX_AUTHOR } from 'unity-frontend-core/Models/Enums';

import ScrollBar from 'Components/Scrollbar';
import ImmutableListView from 'Components/ImmutableListView';
import { InlineInput, Button } from 'Components/ReduxFields';

import { ChatHeader } from './ChatHeader';
import styles from './style.css';
import { removeWidget } from '../Dashboard/actions';
import messages from './messages';

class Chat extends React.PureComponent {
  componentDidUpdate({ orderRequests: prevOrderRequests }) {
    const { orderRequests, loaded, blockId, config } = this.props;
    if (loaded) {
      const exists = orderRequests.find((request) => (request.id === config.get('id')));
      if (!exists) {
        this.props.removeWidget({ blockId });
      } else {
        const prevSize = prevOrderRequests.find((request) => (request.id === config.get('id')))
          ? prevOrderRequests.find((request) => (request.id === config.get('id'))).messages.size : 0;
        if (exists.messages.size !== prevSize) {
          const { height } = this.chatContainer.firstChild.getBoundingClientRect();
          this.chatContainer.scrollTop = height + 100;
          this.props.readOrderRequestChatMessage(config.get('id'));
        }
      }
    }
  }

  setUnread = () => {
    const { orderRequests, config } = this.props;
    const order = orderRequests.find((request) => (request.id === config.get('id'))) || false;
    if (order.unreadByClient) {
      this.props.readOrderRequestChatMessage(order.id);
    }
  }

  submit = (value) => {
    const { config, reset } = this.props;
    this.props.sendOrderRequestChatMessage({ orderRequestId: config.get('id'), text: value.get('message') });
    reset();
  }

  renderMessage = (message) => {
    return <div key={message.created} styleName={`message-row ${message.author === INBOX_AUTHOR ? 'inbox' : 'outbox'}`}>
      <div styleName="message">{message.text}</div>
      <div styleName="date">{ format(message.created, STANDARD_TIME)}</div>
    </div>;
  }

  renderMessages = ({ messages }) => ( // eslint-disable-line no-shadow
    <ImmutableListView
      immutableData={messages.groupBy((message) => format(message.created, STANDARD_DATE))}
      renderSection={(key) => <div
        key={key}
        styleName="date-row"
      >
        {key}
      </div>}
      renderRow={this.renderMessage}
    />
  );

  render() {
    const { orderRequests, config, handleSubmit, intl, sending, invalid } = this.props;
    const order = orderRequests.find((request) => (request.id === config.get('id'))) || false;
    return order
      ? <div styleName="chat">
        <ScrollBar
          style={{ position: 'static' }}
          containerRef={(el) => { this.chatContainer = el; }}
        >
          <div>{this.renderMessages(order)}</div>
        </ScrollBar>
        <Form onSubmit={handleSubmit(this.submit)}>
          <div styleName="footer">
            <InlineInput
              name="message"
              placeholder={intl.formatMessage(messages.message)}
              hideLabel
              hideError
              validate={[requireValidate]}
              onFocus={this.setUnread}
            />
            <Button
              type="submit"
              smaller
              subClass={styles['small-btn']}
              disabled={sending || invalid}
            >Send</Button>
          </div>
        </Form>
      </div> : null;
  }
}

Chat.propTypes = {
  blockId: PropTypes.array,
  orderRequests: PropTypes.any,
  handleSubmit: PropTypes.func,
  removeWidget: PropTypes.func,
  readOrderRequestChatMessage: PropTypes.func,
  sending: PropTypes.bool,
  loaded: PropTypes.bool,
  invalid: PropTypes.bool,
  sendOrderRequestChatMessage: PropTypes.func,
  reset: PropTypes.func,
  intl: PropTypes.object,
  config: ImmutablePropTypes.map,
};

const mapDispatchToProps = (dispatch) => ({
  removeWidget: ({ blockId = [] }) => dispatch(removeWidget({ blockId })),
});

const ChatWithForm = reduxForm({
  form: 'chatForm',
})(injectIntl(Chat));
export default OrderRequests(connect(null, mapDispatchToProps)(ChatWithForm));

export { ChatHeader };
