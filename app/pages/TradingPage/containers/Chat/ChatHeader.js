import React, { Fragment } from 'react';
import ImmutablePropTypes from 'react-immutable-proptypes';

import { WindowHeader, WindowHeaderTitle } from '../../../../components/Window';

import {} from './style.css';

export class ChatHeader extends React.PureComponent { // eslint-disable-line react/prefer-stateless-function
  render() {
    const { config } = this.props;
    return (
      <WindowHeader
        {...this.props}
        renderTitle={() => (
          <Fragment>
            <WindowHeaderTitle key="chat_header" icon="icon-icon-chat">Order request chat</WindowHeaderTitle>
            <div styleName="symbol">
              {config && config.get('symbolDescription')}
            </div>
          </Fragment>
        )}
      />
    );
  }
}

ChatHeader.propTypes = {
  config: ImmutablePropTypes.map,
};
