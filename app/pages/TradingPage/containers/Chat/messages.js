/*
 * TradingPage Messages
 *
 * This contains all the text for the TradingPage component.
 */
import { defineMessages } from 'react-intl';

export default defineMessages({
  message: {
    id: 'app.containers.ChatWidget.message',
    defaultMessage: 'Start writing message here...',
  },

});
