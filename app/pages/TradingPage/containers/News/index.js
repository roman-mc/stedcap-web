import React from 'react';
import PropTypes from 'prop-types';

import connectNews from 'unity-frontend-core/containers/News';

import ScrollBar from 'Components/Scrollbar';
import LoadingIndicator from 'Components/LoadingIndicator';
import ImmutableListView from 'Components/ImmutableListView';
import { NewsListItem, NewsListHeader, NewsDetails } from 'Components/News';

import { NewsHeader } from './NewsHeader';

import {} from './style.css';

class News extends React.PureComponent {
  state = {
    currentItem: null,
    isDetailsVisible: false,
  };

  componentDidMount() {
    this.props.loadNews();
    this.props.startNewsLoading();
  }

  componentWillUnmount() {
    this.props.stopNewsLoading();
  }

  showDetails = (item) => {
    this.setState({ currentItem: item, isDetailsVisible: true });
  };

  hideDetails = () => {
    this.setState({ isDetailsVisible: false });
  };

  loadMore = () => {
    const { newsTotalCount, newsLoadedCount, newsLoading } = this.props;
    if (newsTotalCount && newsLoadedCount < newsTotalCount && !newsLoading) {
      this.props.loadNews({ loadMore: true });
    }
  };

  render() {
    const { news, newsLoading } = this.props;
    const { currentItem, isDetailsVisible } = this.state;

    return (
      <div styleName="news-list">
        {newsLoading && <div styleName="loader">
          <LoadingIndicator loading />
        </div>}
        <ScrollBar
          style={{ position: 'static' }}
          containerRef={(ref) => this.newsListScrollRef = ref}
          onYReachEnd={this.loadMore}
        >
          <ImmutableListView
            immutableData={news}
            renderSection={(section) => <NewsListHeader title={section} />}
            renderRow={(item, i) => <NewsListItem
              key={`news_list_${i}_${item.id}`}
              item={item}
              selected={currentItem && item.id === currentItem.id}
              onClick={this.showDetails}
            />
            }
          />
        </ScrollBar>
        {(currentItem && isDetailsVisible)
          && <div styleName="news-details-wrapper">
            <div styleName="news-details-header">
              <div styleName="news-details-back" onClick={this.hideDetails}>
                <i className="icon-icon-back" />
                <span>
Back to news list
                </span>
              </div>
            </div>
            <div styleName="news-details-content">
              <NewsDetails item={currentItem} />
            </div>
          </div>
        }
      </div>
    );
  }
}

News.propTypes = {
  news: PropTypes.object.isRequired,
  newsTotalCount: PropTypes.number.isRequired,
  newsLoadedCount: PropTypes.number.isRequired,
  newsLoading: PropTypes.bool,
  loadNews: PropTypes.func.isRequired,
  startNewsLoading: PropTypes.func.isRequired,
  stopNewsLoading: PropTypes.func.isRequired,
};

export default connectNews(News);

export { NewsHeader };
