import React from 'react';

import { WindowHeader, WindowHeaderTitle } from '../../../../components/Window';

export class NewsHeader extends React.PureComponent { // eslint-disable-line react/prefer-stateless-function
  render() {
    return (
      <WindowHeader
        {...this.props}
        renderTitle={() => [
          <WindowHeaderTitle key="news_header" icon="icon-icon-news">News</WindowHeaderTitle>,
        ]}
      />
    );
  }
}

NewsHeader.propTypes = {
};
