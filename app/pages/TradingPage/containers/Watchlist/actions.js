import { createAction } from 'redux-act';
import createApiAction from 'unity-frontend-core/utils/createAction';

export const selectInstrument = createAction('app/containers/TradingPage/Watchlist/SELECT_INSTRUMENT');

export const createWatchlist = createApiAction('app/containers/TradingPage/CREATE_WATCHLIST');
export const replaceWatchlistInstrument = createApiAction('app/containers/TradingPage/REPLACE_WATCHLIST_INSTRUMENT');
export const updateWatchlist = createApiAction('app/containers/TradingPage/UPDATE_WATCHLIST');
export const loadWacthlists = createApiAction('app/containers/TradingPage/LOAD_WATCHLISTS');
export const deleteWatchlist = createApiAction('app/containers/TradingPage/DELETE_WATCHLIST');

export const drugOrderInstruments = createApiAction('app/containers/TradingPage/DRAG_ORDER_INSTRUMENTS');

export const startWatchChanges = createApiAction('app/containers/TradingPage/START_WATCH_CHANGES');
export const stopWatchChanges = createApiAction('app/containers/TradingPage/STOP_WATCH_CHANGES');

export const setVolume = createAction('app/containers/TradingPage/SET_VOLUME');

export const openPopup = createAction('app/containers/TradingPage/OPEN_WATCHLIST_POPUP');
export const closePopup = createAction('app/containers/TradingPage/CLOSE_WATCHLIST_POPUP');
