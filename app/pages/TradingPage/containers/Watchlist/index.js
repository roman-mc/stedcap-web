import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import ImmutableProptypes from 'react-immutable-proptypes';
import { createStructuredSelector } from 'reselect';
import numeral from 'numeral';

import WithInjection from 'unity-frontend-core/containers/WithInjection';

import { makeSelectActiveAccountPermissions } from 'unity-frontend-core/containers/SelectAccount/selectors';

import ScrollBar from '../../../../components/Scrollbar';
import TabsHead from '../../../../components/TabsHead';
import RadioButtons from '../../../../components/URadioButtons';
import { Button } from '../../../../components/GUI';

import SingleRow from './components/SingleRow';
import TradingBoard from './components/TradingBoard';
import TopbarButtons from './components/TopbarButtons';
import CreateWatchlistModal from './CreateWatchlist';

import {
  makeSelectWatchlists,
  makeSelectSelectedWatchlist,
  makeSelectWatchlistPopupOpened,
  makeSelectLoading,
} from './selectors';

import { makeSelectCurrentInstrumentId } from '../Overview/selectors';

import {
  openOrderPopup,
  openOrderRequestPopup,
} from '../../actions';

import {
  loadWacthlists,
  selectInstrument as selectInstrumentAction,
  createWatchlist,
  updateWatchlist,
  startWatchChanges,
  stopWatchChanges,
  drugOrderInstruments,
} from './actions';

import { styleNames } from '../../../../utils/styles';

import sagas from './sagas';
import reducer from './reducer';

import ContextMenu from './components/Menu';
import { makeSelectOpened } from './components/Menu/selectors';
import { closeMenu } from './components/Menu/actions';
import styles from './style.css';


const oneK = 1000;
const oneM = oneK * oneK;

const amountOptions = [
  {
    value: 500000,
    label: '500K',
  },
  {
    value: oneM,
    label: '1M',
  },
  {
    value: 2 * oneM,
    label: '2M',
  },
  {
    value: 3 * oneM,
    label: '3M',
  },
  {
    value: 5 * oneM,
    label: '5M',
  },
  {
    value: 10 * oneM,
    label: '10M',
  },
];

const BOOKMARK_MODE = {
  ROW: 'singlerow',
  TILE: 'tradingboard',
};

class Watchlist extends React.PureComponent {
  state = {
    selectedAmount: undefined,
    amountInput: '',
  };

  componentDidMount() {
    this.props.loadWacthlists();
    this.props.startWatchChanges();
  }

  componentWillUnmount() {
    this.props.stopWatchChanges();
  }

  createOrUpdateWatchlist = (values) => {
    const { instruments, ...rest } = values.toJS();
    const instrumentsWithAmount = instruments.map((instrumentId) => {
      return { instrument: instrumentId, amount: 1000 };
    });
    const result = { instruments: instrumentsWithAmount, ...rest };
    const method = result.id ? this.props.updateWatchlist : this.props.createWatchlist;

    method(result, this.props.blockId);
  }

  /**
   * Resize block for tile-style rows - fit 3 columns(880px)
   * Resize block for row-style rows - no big gap between title and quotation(600px)
   *
   */

  changeWatchlist = (selectedWatchlist) => {
    this.props.onConfigChange({ selectedWatchlistId: selectedWatchlist.id });
  };

  changeView = (view) => {
    this.optimalResizeBlock(view);
    this.props.onConfigChange({ view });
  };

  toggleAmountPanel = () => {
    const { config } = this.props;
    const isAmountPanelHidden = config.get('isAmountPanelHidden') || false;

    this.props.onConfigChange({ isAmountPanelHidden: !isAmountPanelHidden });
  }

  optimalResizeBlock(bookmarkMode) {
    const { position, size, onResize, config } = this.props;

    const currentBookmark = config.get('view') || BOOKMARK_MODE.TILE;

    if (currentBookmark === bookmarkMode) return;

    const newWidth = bookmarkMode === BOOKMARK_MODE.ROW ? 600 : 800;

    onResize({ width: newWidth, height: size.height }, position);
  }

  handleAmountInputChange = (e) => {
    const { value } = e.target;
    const numeralValue = numeral(value);

    this.setState({ amountInput: numeralValue.format('0,0') });
  }

  handleAmountInputBlur = (e) => {
    const { value } = e.target;
    const numValue = numeral(value).value();

    if (!numValue) {
      this.setState({
        selectedAmount: undefined,
        amountInput: '',
      });
    } else {
      this.setState({
        selectedAmount: numValue,
      });
    }
  }

  handleAmountInputEnter = (e) => {
    if (e.key === 'Enter') {
      e.target.blur();
    }
  }

  handleAmountOptionChange = (value) => {
    const isUnset = this.state.selectedAmount === value;

    this.setState({
      // handle second click on a same button
      selectedAmount: isUnset ? undefined : value,
      amountInput: isUnset ? '' : numeral(value).format('0,0'),
    });
  }

  renderNoData() {
    return (
      <div styleName="no-data-container">
        <i className="icon-icon-new-watchlist" styleName="no-data-icon" />
        <span styleName="no-data-text">Create your first watchlist to begin trading</span>

        {/* do not remove modal or do refactor */}
        <CreateWatchlistModal
          onSubmit={this.createOrUpdateWatchlist}
        />
      </div>
    );
  }

  render() {
    const {
      userWatchlitsts,
      selectedInstrument,
      selectInstrument,
      size,
      accountPermissions,
      config,
      selectedWatchlist,
      isLoading,
    } = this.props;

    if (!isLoading && !selectedWatchlist) {
      // There is no watchlist in userWatchlitsts
      return this.renderNoData();
    } if (!selectedWatchlist) {
      return null;
    }

    if (!selectedWatchlist) return null;

    const currentView = config.get('view') || BOOKMARK_MODE.TILE;
    const isAmountPanelHidden = config.get('isAmountPanelHidden') || false;

    const sortableProps = {
      distance: 20,
      onSortEnd: ({ oldIndex, newIndex }) => {
        const { instruments } = selectedWatchlist;
        const oldValue = instruments.get(oldIndex).toJS();
        const newWatchlist = selectedWatchlist.set(
          'instruments', instruments.splice(oldIndex, 1).splice(newIndex, 0, oldValue)
        );
        this.props.drugOrderInstruments(newWatchlist.toJS());
      },
    };
    const { selectedAmount, amountInput } = this.state;

    const globalAmountLabelVisible = size.width > 640;
    const inputStyles = styleNames(
      'amount-input',
      amountInput.length === 0 && 'inactive',
    );

    const radioButtonStyles = styleNames(
      styles.radioButtons,
      !globalAmountLabelVisible && styles.radioButtonsShrinked
    );

    return (
      <div styleName="watchlist">
        <div styleName="backslash">
          <div styleName="top-bar">
            <TabsHead
              tabs={userWatchlitsts.toArray()}
              selectedTab={selectedWatchlist}
              onTabClick={this.changeWatchlist}
              size={size}
            />
            <div styleName="right-box">
              {currentView === BOOKMARK_MODE.TILE
                && <Button
                  smaller
                  type="primary"
                  className={styles.amountButton}
                  onClick={this.toggleAmountPanel}
                >
                  <i styleName="amounts-icon" className="icon-icon-amounts-panel"></i>
                </Button>
              }

              <TopbarButtons selectedView={currentView} onViewChange={this.changeView} />
            </div>

          </div>
        </div>
        {currentView === BOOKMARK_MODE.TILE && !isAmountPanelHidden &&
          <div styleName="amounts-container">
            {globalAmountLabelVisible &&
              <span styleName="amounts-label">
                Set amounts to
              </span>}
            <RadioButtons
              options={amountOptions}
              selectedValue={selectedAmount}
              onChange={this.handleAmountOptionChange}
              className={radioButtonStyles}
            />
            <span styleName="amounts-text-delimiter">
              or
            </span>
            <input
              ref={(ref) => this.amountInput = ref}
              pattern="\d|,"
              styleName={inputStyles}
              placeholder="Enter amount"
              value={amountInput}
              onChange={this.handleAmountInputChange}
              onBlur={this.handleAmountInputBlur}
              onKeyUp={this.handleAmountInputEnter}
            />
          </div>
        }
        <div styleName={`content ${isAmountPanelHidden ? 'panel-hidden' : ''}`}>
          <ScrollBar
            style={{ position: 'static' }}
            onScrollY={this.props.closeMenu}
          >
            {
              currentView === BOOKMARK_MODE.ROW
              && <SingleRow
                placeOrder={this.props.openOrderPopup}
                placeOrderRequest={this.props.openOrderRequestPopup}
                onInstrumentSelect={selectInstrument}
                selectedInstrument={selectedInstrument}
                instruments={selectedWatchlist.instruments}
                watchlist={selectedWatchlist}
                hasTradePermissions={accountPermissions.indexOf('TRADE') >= 0}
                {...sortableProps}
              />
            }
            {
              currentView === BOOKMARK_MODE.TILE
              && <TradingBoard
                onInstrumentSelect={selectInstrument}
                placeOrder={this.props.openOrderPopup}
                placeOrderRequest={this.props.openOrderRequestPopup}
                selectedInstrument={selectedInstrument}
                watchlist={selectedWatchlist}
                hasTradePermissions={accountPermissions.indexOf('TRADE') >= 0}
                size={size}
                axis="xy"
                globalAmount={selectedAmount}
                {...sortableProps}
              />
            }
          </ScrollBar>
        </div>
        <ContextMenu widgetPosition={this.props.position} blockId={this.props.blockId} watchlist={selectedWatchlist} />
        <CreateWatchlistModal
          selectedWatchlist={this.props.selectedWatchlist.id}
          onSubmit={this.createOrUpdateWatchlist}
        />

      </div>
    );
  }
}


const mapStateToProps = createStructuredSelector({
  selectedInstrument: makeSelectCurrentInstrumentId(),
  isLoading: makeSelectLoading(),
  userWatchlitsts: makeSelectWatchlists(),
  selectedWatchlist: makeSelectSelectedWatchlist(),
  popupOpened: makeSelectWatchlistPopupOpened(),
  accountPermissions: makeSelectActiveAccountPermissions(),
  menuOpened: makeSelectOpened(),
});


const mapDispatchToPros = (dispatch) => ({
  openOrderPopup: (instrumentId, direction) => dispatch(openOrderPopup({ instrumentId, direction })),
  openOrderRequestPopup: (symbolDescription, direction) => dispatch(openOrderRequestPopup({ symbolDescription, direction })),
  selectInstrument: (instrumentId) => dispatch(selectInstrumentAction(instrumentId)),
  loadWacthlists: () => dispatch(loadWacthlists()),
  createWatchlist: (watchlist, blockId) => dispatch(createWatchlist({ watchlist, blockId })),
  updateWatchlist: (watchlist) => dispatch(updateWatchlist({ watchlist })),
  drugOrderInstruments: (watchlist) => dispatch(drugOrderInstruments(watchlist)),
  startWatchChanges: () => dispatch(startWatchChanges()),
  stopWatchChanges: () => dispatch(stopWatchChanges()),
  closeMenu: () => dispatch(closeMenu()),
});

Watchlist.propTypes = {
  onConfigChange: PropTypes.func.isRequired,
  position: ImmutableProptypes.record,
  size: PropTypes.object,
  config: ImmutableProptypes.map,
  onResize: PropTypes.func.isRequired,
  blockId: PropTypes.array.isRequired,

  updateWatchlist: PropTypes.func.isRequired,
  createWatchlist: PropTypes.func.isRequired,

  selectInstrument: PropTypes.func.isRequired,
  selectedInstrument: PropTypes.number,

  userWatchlitsts: ImmutableProptypes.list,
  selectedWatchlist: PropTypes.object,
  isLoading: PropTypes.bool.isRequired,

  closeMenu: PropTypes.func,

  openOrderPopup: PropTypes.func.isRequired,
  openOrderRequestPopup: PropTypes.func.isRequired,
  loadWacthlists: PropTypes.func.isRequired,
  startWatchChanges: PropTypes.func.isRequired,
  stopWatchChanges: PropTypes.func.isRequired,
  drugOrderInstruments: PropTypes.func.isRequired,

  accountPermissions: ImmutableProptypes.list,
};

const Connected = connect(mapStateToProps, mapDispatchToPros)(Watchlist);

export default WithInjection({ sagas, reducer, key: 'trading.watchlist' })(Connected);
export { default as WatchlistHeader } from './WatchlistHeader';
