import { takeEvery } from 'redux-saga';
import { call, put, select, apply, race, take } from 'redux-saga/effects';
import Raven from 'raven-js';

import Watchlist from 'unity-frontend-core/Models/Watchlist';

import { actions } from 'Containers/Modal';
import { confirmSaga } from 'Containers/ConfirmationDialog/sagas';

import { createChannel } from 'Utils/sagaUtils';

import * as watchlistApi from 'unity-frontend-core/api/watchlistApi';

import socketApi from 'Api/socketApi';
import { WIDGET_TYPES } from 'Model/enums';


import { setConfig } from '../Dashboard/actions';
import { changeInstrument } from '../Ticket/actions';
import { makeSelectWidgetId, makeSelectActivePanelId } from '../Dashboard/selectors';

import messages from './messages';


import {
  loadWacthlists,
  createWatchlist,
  updateWatchlist,
  deleteWatchlist,
  replaceWatchlistInstrument,
  selectInstrument,
  startWatchChanges,
  stopWatchChanges,
  drugOrderInstruments,
} from './actions';

import { makeSelectWatchlists } from './selectors';

function* replaceWatchlistInstrumentHandler({
  payload: {
    watchlist, blockId, previousInstrument, newInstrument,
  },
}) {
  const model = watchlist.toJS();
  const data = {
    ...model,
    instruments: model.instruments.map((instrument) => instrument.instrument === previousInstrument ? { ...instrument, instrument: newInstrument } : instrument),
  };
  yield put(updateWatchlist({ watchlist: data, selectedWatchlist: data.id, blockId }));
}

function* createWatchlistHandler({ payload: { watchlist, blockId } }) {
  try {
    const result = yield call(watchlistApi.create, { watchlist });
    const created = new Watchlist(result);

    yield put(createWatchlist.success(created));
    yield put(actions.closeModal({ modalId: 'edit_watchlist_modal' }));

    yield put(setConfig({ blockId, config: { selectedWatchlistId: created.id } }));
  } catch (e) {
    yield put(createWatchlist.error(e));
    Raven.captureException(e);
  }
}


function* updateWatchlistHandler({ payload: { watchlist } }) {
  try {
    const result = yield call(watchlistApi.update, { watchlistId: watchlist.id, watchlist });
    const updated = new Watchlist(result);

    yield put(updateWatchlist.success(updated));
    yield put(actions.closeModal({ modalId: 'edit_watchlist_modal' }));
  } catch (e) {
    console.warn('Error on watchlist creation', e);
    yield put(createWatchlist.error(e));
    Raven.captureException(e);
  }
}

function* loadWacthlistsHandler() {
  try {
    const result = yield call(watchlistApi.list);
    yield put(loadWacthlists.success(result.watchLists));
  } catch (e) {
    yield put(loadWacthlists.error(e));
    Raven.captureException(e);
  }
}

function* deleteWatchlistHandler({ payload: { watchlist, cb } }) {
  const confirmed = yield call(confirmSaga, {
    header: messages.deleteHeader,
    text: messages.deleteText,
    confirm: messages.deleteButton,
  });
  if (confirmed) {
    try {
      yield call(watchlistApi.remove, { watchlistId: watchlist.id });
      yield put(deleteWatchlist.success({ watchlistId: watchlist.id }));
      if (cb) {
        yield call(cb);
      }
    } catch (e) {
      yield put(deleteWatchlist.error(e));
      Raven.captureException(e);
    }
  }
}

function* selectInstrumentSaga({ payload: instrumentId }) {
  const activePanelId = yield select(makeSelectActivePanelId());

  // try to set overview panel instrument

  const overviewWidgetId = yield select(makeSelectWidgetId(WIDGET_TYPES.OVERVIEW));
  if (overviewWidgetId) {
    const blockId = [activePanelId, overviewWidgetId];
    yield put(setConfig({ blockId, config: { selectedInstrumentId: instrumentId } }));
  }

  // try to set market depth panel instrument
  const marketDepthWidgetId = yield select(makeSelectWidgetId(WIDGET_TYPES.MARKET_DEPTH));
  if (marketDepthWidgetId) {
    const blockId = [activePanelId, marketDepthWidgetId];
    yield put(setConfig({ blockId, config: { selectedInstrumentId: instrumentId } }));
  }

  // remove & add ticket widget because don't want update form initial values from component
  const ticketWidgetId = yield select(makeSelectWidgetId(WIDGET_TYPES.TICKET));
  if (ticketWidgetId) {
    yield put(changeInstrument({ instrumentId }));
  }


  // TODO: Reload instrument card on select
}

function* watchChangesSaga() {
  const channel = yield call(createChannel, socketApi, 'watchlistMdf');
  yield apply(socketApi, socketApi.subscribeWatchlistChanges);
  try {
    while (true) {
      const { message, stop } = yield race({
        message: take(channel),
        stop: take(stopWatchChanges.toString()),
      });

      if (message && message.data) {
        yield put(loadWacthlists());
      } else if (stop) {
        break;
      }
    }
  } finally {
    channel.close();
  }
}


function* orderInstrumentsHandler({ payload: { ...watchlist } }) {
  const prevState = yield select(makeSelectWatchlists());
  const prevWatchlist = prevState.find((w) => (w.id === watchlist.id));
  yield put(drugOrderInstruments.success(watchlist));
  try {
    yield call(watchlistApi.update, { watchlistId: watchlist.id, watchlist });
  } catch (e) {
    yield put(drugOrderInstruments.error(prevWatchlist.toJS()));
    console.warn('Error on watchlist sort, revert store', e);
    Raven.captureException(e);
  }
}


function* sagasLoader() {
  yield takeEvery(startWatchChanges.toString(), watchChangesSaga);
  yield takeEvery(loadWacthlists.toString(), loadWacthlistsHandler);
  yield takeEvery(createWatchlist.toString(), createWatchlistHandler);
  yield takeEvery(updateWatchlist.toString(), updateWatchlistHandler);
  yield takeEvery(deleteWatchlist.toString(), deleteWatchlistHandler);
  yield takeEvery(replaceWatchlistInstrument.toString(), replaceWatchlistInstrumentHandler);
  yield takeEvery(selectInstrument.toString(), selectInstrumentSaga);
  yield takeEvery(drugOrderInstruments.toString(), orderInstrumentsHandler);
}

export default [
  sagasLoader,
];
