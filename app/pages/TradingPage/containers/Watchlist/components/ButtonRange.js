import React from 'react';
import PropTypes from 'prop-types';

import amountFormat from 'unity-frontend-core/utils/amountFormat';
import { Button, Input } from 'Components/GUI';

export class ButtonRange extends React.PureComponent {
  state = {
    currentLot: null,
    rangeExpanded: true,
  }

  onLotClick = (lotValue, i) => {
    const { onSelect } = this.props;
    return () => this.setState(
      { currentLot: i },
      () => onSelect(lotValue)
    );
  }

  onEnterClick = () => {
    this.setState({ rangeExpanded: false });
  }

  renderLot = (buttonValue, i) => {
    const { currentLot } = this.state;

    if (currentLot === i) {
      return this.renderButton(buttonValue, 'primary', i);
    }

    return this.renderButton(buttonValue, 'secondary', i);
  }

  renderButton(value, type, i) {
    const buttonValue = (
      <div className="gui__button-range__button__inner">
        {amountFormat(value)}
      </div>
    );
    return (
      <div className="gui__button-range__button" key={i}>
        <Button
          type={type}
          onClick={this.onLotClick(buttonValue, i)}
        >
          {buttonValue}
        </Button>
      </div>
    );
  }

  render() {
    const { range } = this.props;
    const { rangeExpanded } = this.state;

    return (
      <div className="gui__button-range">
        {
          rangeExpanded && <div className="gui__button-range__wrapper">
            {range.map(this.renderLot)}
            <div className="gui__button-range__button">
              <Button type="secondary" onClick={this.onEnterClick}>
                <div className="gui__button-range__button__inner">Enter</div>
              </Button>
            </div>
          </div>
        }
        {!rangeExpanded
          && <div className="gui__button-range__input-wrapper">
            <Input />
          </div>}
      </div>
    );
  }
}


ButtonRange.propTypes = {
  range: PropTypes.arrayOf(PropTypes.number).isRequired,
  onSelect: PropTypes.func.isRequired,
};
