import React from 'react';
import PropTypes from 'prop-types';
import ImmutableProptypes from 'react-immutable-proptypes';
import { connect } from 'react-redux';
import { createStructuredSelector } from 'reselect';
import WithInjection from 'unity-frontend-core/containers/WithInjection';
import { WIDGET_TYPES } from 'Model/enums';

import './style.css';
import { changeWidgetInstrument, closeMenu, deleteInstrument } from './actions';
import { makeSelectOpened, makeSelectInstrument, makeSelectPosition, makeSelectSubMenu } from './selectors';
import reducer from './reducer';
import sagas from './sagas';

const MENU_WIDTH = 160;
const OFFSET_TOP = 180;


class ContextMenu extends React.PureComponent {
  componentWillMount() {
    window.addEventListener('click', this.closeMenu);
    window.addEventListener('mouseup', this.closeMenu);
  }
  componentWillUnmount() {
    window.removeEventListener('click', this.closeMenu);
    window.removeEventListener('mouseup', this.closeMenu);
  }
  closeMenu = () => {
    this.props.closeMenu();
  }

  change = (widget) => () => {
    const { instrumentId } = this.props;
    this.props.changeWidgetInstrument(instrumentId, widget);
  }

  delete = () => {
    const { instrumentId, watchlist } = this.props;
    const model = watchlist.toJS();
    const data = {
      ...model,
      instruments: model.instruments.filter(({ instrument }) => instrument !== instrumentId),
    };
    this.props.deleteInstrument(data);
  }

  stopPropagation = (e) => e.stopPropagation();

  render() {
    const { opened = false, position = {} } = this.props;
    return <div
      styleName={`context-menu ${opened ? 'opened' : ''}`}
      style={{ left: `${position.x - MENU_WIDTH}px`, top: `${OFFSET_TOP + position.y}px` }}
      onClick={this.stopPropagation}
      onMouseUp={this.stopPropagation}
    >
      <div styleName="menu-item">
        <div>
          <span>Select widget</span>
        </div>
        <div styleName="sub-menu">
          <div onClick={this.change(WIDGET_TYPES.OVERVIEW)}><i className="icon-icon-widget-chart" />Chart</div>
          <div onClick={this.change(WIDGET_TYPES.MARKET_DEPTH)}><i className="icon-icon-widget-market-depth" />Market depth</div>
          <div onClick={this.change(WIDGET_TYPES.TICKET)}><i className="icon-icon-ticket-widget" />Trade ticket</div>
        </div>
      </div>
      <div styleName="menu-item">
        <div styleName="item-label" onClick={this.delete}>
          <span>Delete instrument</span>
        </div>
      </div>
    </div>;
  }
}


ContextMenu.propTypes = {
  opened: PropTypes.bool,
  instrumentId: PropTypes.number,
  position: PropTypes.object,
  closeMenu: PropTypes.func,
  changeWidgetInstrument: PropTypes.func,
  deleteInstrument: PropTypes.func,
  watchlist: ImmutableProptypes.record,
};

const mapStateToProps = createStructuredSelector({
  opened: makeSelectOpened(),
  subMenu: makeSelectSubMenu(),
  instrumentId: makeSelectInstrument(),
  position: makeSelectPosition(),
});

const mapDispatchToProps = (dispatch) => ({
  closeMenu: () => dispatch(closeMenu()),
  deleteInstrument: (watchlist) => dispatch(deleteInstrument({ watchlist })),
  changeWidgetInstrument: (id, widget) => dispatch(changeWidgetInstrument({ id, widget })),
});

export default WithInjection({ sagas, reducer, key: 'trading.watchlist.contextMenu' })(connect(mapStateToProps, mapDispatchToProps)(ContextMenu));
