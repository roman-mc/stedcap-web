import { Map } from 'immutable';
import { createSelector } from 'reselect';

export const makeSelectSpace = () => (state) => state.get('trading.watchlist.contextMenu', new Map());

export const makeSelectOpened = () => createSelector(
  makeSelectSpace(),
  (contextMenu) => contextMenu.get('opened'),
);

export const makeSelectInstrument = () => createSelector(
  makeSelectSpace(),
  (contextMenu) => contextMenu.get('instrumentId'),
);

export const makeSelectPosition = () => createSelector(
  makeSelectSpace(),
  (contextMenu) => contextMenu.get('position'),
);

export const makeSelectSubMenu = () => createSelector(
  makeSelectSpace(),
  (contextMenu) => contextMenu.get('subMenu'),
);

