import { createAction } from 'redux-act';

export const openMenu = createAction('app/containers/TradingPage/Watchlist/ContextMenu/OPEN');
export const openSubMenu = createAction('app/containers/TradingPage/Watchlist/ContextMenu/OPEN_SUB_MENU');
export const closeMenu = createAction('app/containers/TradingPage/Watchlist/ContextMenu/CLOSE');

export const changeWidgetInstrument = createAction('app/containers/TradingPage/Watchlist/ContextMenu/CHANGE_WIDGET_INSTRUMENT');
export const deleteInstrument = createAction('app/containers/TradingPage/Watchlist/ContextMenu/DELETE_INSTRUMENT');
