import { put, select } from 'redux-saga/effects';
import { generateListener } from 'unity-frontend-core/utils/sagaUtils';
import { WIDGET_TYPES } from 'Model/enums';
import { changeWidgetInstrument, closeMenu, deleteInstrument } from './actions';
import {
  makeSelectActivePanelId,
  makeSelectWidgetId,
  makeSelectMaxZIndex,
  makeSelectWidget,
} from '../../../Dashboard/selectors';

import { setConfig, addWidget, resize } from '../../../Dashboard/actions';
import { changeInstrument } from '../../../Ticket/actions';

import { DEFAULT_OVERVIEW, DEFAULT_MARKETDEPTH, DEFAULT_TICKET } from '../../../../../../config/dashboardConfig';
import { updateWatchlist } from '../../actions';

const WidgetMap = {
  ticket: DEFAULT_TICKET,
  marketDepth: DEFAULT_MARKETDEPTH,
  overview: DEFAULT_OVERVIEW,
};

function* changeWidgetInstrumentHandler({ payload: { widget, id } }) {
  const activePanelId = yield select(makeSelectActivePanelId());
  const zIndex = yield select(makeSelectMaxZIndex());
  const widgetId = yield select(makeSelectWidgetId(widget));
  const widgetSettings = yield select(makeSelectWidget(widget));
  if (widgetId) {
    const blockId = [activePanelId, widgetId];
    if (widget === WIDGET_TYPES.TICKET) {
      // remove & add ticket widget because don't want update form initial values from component
      yield put(changeInstrument({ instrumentId: id }));
    } else {
      // try to set widget instrument
      yield put(setConfig({ blockId, config: { selectedInstrumentId: id } }));
      yield put(resize({ blockId, position: { ...widgetSettings.position.toJS(), zIndex: zIndex + 1 } }));
    }
    // set widget zIndex
  } else {
    const widgetToSave = { ...WidgetMap[widget], config: { ...WidgetMap[widget].config, selectedInstrumentId: id }, position: { ...WidgetMap[widget].position, zIndex: zIndex + 1 } };
    yield put(addWidget({ panelId: activePanelId, widget: widgetToSave }));
  }
  yield put(closeMenu());
}

function* deleteInstrumentHandler({ payload }) {
  yield put(updateWatchlist(payload));
  yield put(closeMenu());
}


export default [
  generateListener(changeWidgetInstrument, changeWidgetInstrumentHandler),
  generateListener(deleteInstrument, deleteInstrumentHandler),
];
