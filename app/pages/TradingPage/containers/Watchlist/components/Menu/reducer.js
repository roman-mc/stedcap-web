import { Map } from 'immutable';
import { createReducer } from 'redux-act';


import {
  openMenu,
  closeMenu,
  openSubMenu,
} from './actions';


const initialState = new Map({
  opened: false,
});

export default createReducer({
  [openMenu]: (state, { id, position }) => state
    .set('opened', true)
    .set('instrumentId', id)
    .set('position', position),
  [openSubMenu]: (state, sub) => state.set('subMenu', sub),
  [closeMenu]: (state) => state
    .set('opened', false)
    .set('subMenu', false),
}, initialState);
