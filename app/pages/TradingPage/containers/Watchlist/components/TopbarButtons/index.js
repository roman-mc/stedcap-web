import React from 'react';
import PropTypes from 'prop-types';

import './style.css';

const buttons = [
  { name: 'singlerow', icon: 'icon-icon-list-view' },
  { name: 'tradingboard', icon: ' icon-icon-block-view' },
];

export default function TopbarButtons(props) {
  const { selectedView, onViewChange } = props;

  return (
    <div styleName="view-selection">
      {
        buttons.map(({ name, icon }) => {
          const additionalClassName = selectedView === name ? 'active' : '';
          return (
            <button
              key={name}
              styleName={`icon ${additionalClassName}`}
              onClick={() => onViewChange(name)}
            >
              <i className={icon}></i>
            </button>
          );
        })
      }
    </div>
  );
}

TopbarButtons.propTypes = {
  selectedView: PropTypes.string.isRequired,
  onViewChange: PropTypes.func.isRequired,
};
