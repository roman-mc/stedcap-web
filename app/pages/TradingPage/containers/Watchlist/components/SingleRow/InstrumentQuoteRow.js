import React from 'react';
import PropTypes from 'prop-types';
import { FormattedNumber } from 'react-intl';

import InstrumentCategory from 'Components/InstrumentCategory';
import QuotValue from 'Components/QuotValue';

import InstrumentMarketData from 'unity-frontend-core/containers/InstrumentMarketData';


import { TRADE_DIRECTION } from 'Model/enums';
// import amountFormat from 'unity-frontend-core/utils/amountFormat';

import Arrow from '../../../../../../assets/svg/arrow.svg';
import './style.css';

class InstrumentQuoteRow extends React.PureComponent {
  placeOrder = (instrument, instrumentInfo, direction) => {
    const { placeOrder, placeOrderRequest } = this.props;
    if (instrument.availableForUserTrade) {
      placeOrder(instrumentInfo.instrumentKey.instrumentId, direction);
    } else {
      placeOrderRequest(instrument.code, direction);
    }
  }

  renderNumber(cell, dynamic, columnName, instrumentInfo, direction, instrument) {
    const { colorMode } = this.props;
    return (
      <button
        onClick={() => this.placeOrder(instrument, instrumentInfo, direction)}
        styleName={`${dynamic ? 'positive' : 'negative'} cell__${columnName}`} // cell__ask, cell_bid
        disabled={instrument.expired}
      >
        <div styleName="wrapper">
          <div styleName="value-top">
            <QuotValue size="medium" value={cell} pipSize={instrument.pipSize} />
          </div>
          <div styleName="arrow-container">
            {colorMode && instrumentInfo.active && <Arrow />}
          </div>
        </div>
      </button>
    );
  }


  renderInstrument = (marketData, _, { instrumentInfo }) => {
    const {
      onInstrumentSelect, instrumentId, theme,
    } = this.props;

    if (!instrumentInfo || !marketData) {
      return <tr></tr>;
    }

    /* eslint-disable jsx-a11y/no-static-element-interactions */
    return (
      <tr onClick={() => onInstrumentSelect(instrumentId)} styleName="row" className={`_${theme}`}>
        <td styleName="cell">
          <div>
            <div styleName="instrument-label">
              {<InstrumentCategory size="35px" instrument={instrumentInfo} fontSize="15px" />}
            </div>
            <div styleName="instrument-text">
              <span styleName="name">
                {instrumentInfo.displayName}
              </span>
            </div>
          </div>
        </td>
        <td styleName="cell">
          <div styleName="quotes-container">
            {/* QUESTION: Is there necessary to show positions ? */}
            {/* {data.positions && data.positions.length &&
              <span styleName="positions">
                {amountFormat(data.positions.reduce((acc, p) => acc + p.amount, 0))}
              </span>
            } */}
            {this.renderNumber(marketData.bid, marketData.dynamic.bid, 'bid', marketData, TRADE_DIRECTION.SELL, instrumentInfo)}
            <span styleName="spread">
              <FormattedNumber value={marketData.spread} />
            </span>
            {this.renderNumber(marketData.ask, marketData.dynamic.ask, 'ask', marketData, TRADE_DIRECTION.BUY, instrumentInfo)}
          </div>
        </td>
      </tr>
    );
    /* eslint-enable; */
  }

  render() {
    const { instrumentId, colorMode } = this.props;
    return (<InstrumentMarketData id={instrumentId} withInstrumentInfo render={this.renderInstrument} colorMode={colorMode} />);
  }
}

InstrumentQuoteRow.propTypes = {
  instrumentId: PropTypes.number,
  theme: PropTypes.string,
  onInstrumentSelect: PropTypes.func.isRequired,
  placeOrder: PropTypes.func.isRequired,
  placeOrderRequest: PropTypes.func.isRequired,
  colorMode: PropTypes.bool.isRequired,
};

export default InstrumentQuoteRow;
