import React from 'react';
import PropTypes from 'prop-types';
import ImmutableProptypes from 'react-immutable-proptypes';
import { createStructuredSelector } from 'reselect';
import { connect } from 'react-redux';

import { SortableContainer, SortableElement } from 'react-sortable-hoc';

// import { FormattedMessage } from 'react-intl';
import { makeSelectCurrentUser } from 'Containers/App/selectors';

// import messages from '../../messages';

import InstrumentQuoteRow from './InstrumentQuoteRow';
import './style.css';

//
const SortableRow = SortableElement(InstrumentQuoteRow);

class SingleRow extends React.PureComponent {
  render() {
    const { instruments = false, onInstrumentSelect, placeOrder, placeOrderRequest, currentUser: { settings: { theme } } } = this.props;
    return (
      <table styleName="table" style={this.props.style}>
        <tbody>
          {instruments && instruments.size > 0 && instruments.map(({ instrument }, index) => {
            return (
              <SortableRow
                key={`sort_row_${instrument}`}
                index={index}
                colorMode={false}
                {...{
                  instrumentId: instrument,
                  onInstrumentSelect,
                  placeOrder,
                  placeOrderRequest,
                  theme,
                }}
              />);
          })}
        </tbody>
      </table>
    );
  }
}

const SortableList = SortableContainer(SingleRow);

SingleRow.propTypes = {
  style: PropTypes.object,
  instruments: ImmutableProptypes.list.isRequired,
  onInstrumentSelect: PropTypes.func.isRequired,
  placeOrder: PropTypes.func.isRequired,
  placeOrderRequest: PropTypes.func.isRequired,
  currentUser: ImmutableProptypes.record,
};

const mapStateToProps = createStructuredSelector({
  currentUser: makeSelectCurrentUser(),
});

export default connect(mapStateToProps)(SortableList);
