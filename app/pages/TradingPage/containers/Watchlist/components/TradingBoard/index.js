import React from 'react';
import ImmutableProptypes from 'react-immutable-proptypes';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { SortableContainer, SortableElement } from 'react-sortable-hoc';
import { createStructuredSelector } from 'reselect';

import { makeSelectActiveAccount } from 'unity-frontend-core/containers/SelectAccount/selectors';

import { ORDER_TYPE, GOOD_TILL_TYPE } from 'Model/enums';

import { makeSelectCurrentUser } from 'Containers/App/selectors';

import InstrumentQuoteTile from './InstrumentQuoteTile';

import { placeOrder } from '../../../../actions';
import { openMenu } from '../Menu/actions';
import {} from './style.css';


const minWidth = 300;
const tradingBoardSizeData = (containerWidth) => {
  const ticketsInSingleRow = Math.floor(containerWidth / minWidth);
  return {
    ticketWidth: 100 / ticketsInSingleRow,
    ticketsInSingleRow,
  };
};

const SortableTile = SortableElement(InstrumentQuoteTile);


class TradingBoard extends React.PureComponent {

  quickTrade = (order) => {
    if (!order.amount) {
      return null;
    }

    const entity = {
      ...order,
      orderType: ORDER_TYPE.MARKET.id,
      goodTillType: GOOD_TILL_TYPE.FILL_OR_KILL.id,
    };

    return this.props.doQuickTrade(entity);
  };

  render() {
    const {
      watchlist,
      onInstrumentSelect,
      placeOrder: _placeOrder,
      placeOrderRequest,
      selectedInstrument,
      size,
      hasTradePermissions,
      currentUser,
      currentUser: { settings: { theme } },
      multiAccountId,
      globalAmount,
    } = this.props;

    const { ticketWidth } = tradingBoardSizeData(size.width);

    return (
      <div styleName="trading-board">
        <div className="full-width">
          {watchlist.instruments.map(({ instrument: instrumentId }, index) =>
            <SortableTile
              key={`quoute_tile_${instrumentId}`}
              onTicketClick={onInstrumentSelect}
              onOrderClick={_placeOrder}
              onOrderRequestClick={placeOrderRequest}
              onQuickTrade={this.quickTrade}
              isActive={selectedInstrument === instrumentId}
              width={ticketWidth}
              instrumentId={instrumentId}
              hasTradePermissions={hasTradePermissions}
              colorMode={currentUser.settings.colorMode}
              profileId={currentUser.id}
              multiAccountId={multiAccountId}
              index={index}
              theme={theme}
              globalAmount={globalAmount}
              openMenu={this.props.openMenu}
            />
          )}
        </div>
      </div>
    );
  }
}


TradingBoard.propTypes = {
  globalAmount: PropTypes.number,
  watchlist: ImmutableProptypes.record,
  size: PropTypes.object,
  selectedInstrument: PropTypes.number,
  placeOrder: PropTypes.func,
  placeOrderRequest: PropTypes.func,
  openMenu: PropTypes.func,
  doQuickTrade: PropTypes.func.isRequired,
  onInstrumentSelect: PropTypes.func.isRequired,
  hasTradePermissions: PropTypes.bool,
  multiAccountId: PropTypes.number.isRequired,
  currentUser: ImmutableProptypes.record,
};

const mapStateToProps = createStructuredSelector({
  currentUser: makeSelectCurrentUser(),
  multiAccountId: makeSelectActiveAccount(),
});

const mapDispatchToProps = (dispatch) => ({
  doQuickTrade: (order) => dispatch(placeOrder(order)),
  openMenu: (payload) => dispatch(openMenu(payload)),
});

const SortableBoard = SortableContainer(TradingBoard);

export default connect(mapStateToProps, mapDispatchToProps)(SortableBoard);
