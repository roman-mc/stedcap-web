import React from 'react';
import { FormattedNumber } from 'react-intl';
import PropTypes from 'prop-types';
import amountFormat from 'unity-frontend-core/utils/amountFormat';


import './style.css';


export default function AmountMultiplier({ multiplier }) {
  return (
    <span styleName="custom-amount">
      {amountFormat(multiplier)} x
      <span styleName="custom-amount-tooltip">
        You are using quick trade multiplier.
        <br />
        All amounts multiply by <FormattedNumber value={multiplier} />.
      </span>
    </span>
  );
}


AmountMultiplier.propTypes = {
  multiplier: PropTypes.number.isRequired,
};
