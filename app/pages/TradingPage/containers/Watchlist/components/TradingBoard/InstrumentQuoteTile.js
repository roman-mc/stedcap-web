import React from 'react';
import Select from 'react-select';
import PropTypes from 'prop-types';
import { FormattedNumber } from 'react-intl';
import { uniq } from 'lodash';
import InstrumentMarketData from 'unity-frontend-core/containers/InstrumentMarketData';
import { UserAccountSettingsResolver } from 'unity-frontend-core/containers/UserAccountSettings';
import amountFormat from 'unity-frontend-core/utils/amountFormat';


import Arrow from 'Svg/arrow.svg';
import InstrumentCategory from 'Components/InstrumentCategory';
import QuotValue from 'Components/QuotValue';
import LoadingIndicator from 'Components/LoadingIndicator';

import { USER_SETTINGS } from 'unity-frontend-core/Models/Enums';

import { TRADE_DIRECTION } from 'Model/enums';
import { Button } from 'Components/GUI';

import AmountMultiplier from './AmountMultiplier';

import {} from './style.css';

const loaderStyle = {
  marginTop: '0.5em',
};

class InstrumentQuoteTile extends React.PureComponent {
  state = {
    customAmounts: [],
    amount: null,
  };

  changeVolume = ({ value }) => {
    const amount = Number.parseInt(value, 10);
    const { customAmounts } = this.state;

    this.setState({ amount });
    if (customAmounts.indexOf(amount) === -1) {
      this.setState({ customAmounts: [...customAmounts, amount] });
    }
  }

  findNode = (node, className) => {
    let whileNode = node;
    while (whileNode !== document) {
      if (whileNode.classList.contains(className)) {
        return whileNode;
      }
      whileNode = whileNode.parentNode;
    }
    return false;
  }

  resolveMultiplier = (customAmounts, { instrumentInfo, profileId, multiAccountId, ...props }) => {
    return (
      <UserAccountSettingsResolver
        instrumentInfo={instrumentInfo}
        {...props}
        multiAccountId={multiAccountId}
        profileId={profileId}
        customAmountsSetting={customAmounts}
        instrumentCategory={instrumentInfo && instrumentInfo.category}
        systemPropertyName={USER_SETTINGS.QUICK_TRADE_MULTIPLIER}
        render={this.renderInstrument}
      />
    );
  }

  resolveAmounts = (marketData, id, { instrumentInfo, profileId, multiAccountId, ...props }) => {
    return (
      <UserAccountSettingsResolver
        marketData={marketData}
        instrumentInfo={instrumentInfo}
        {...props}
        multiAccountId={multiAccountId}
        profileId={profileId}
        instrumentCategory={instrumentInfo && instrumentInfo.category}
        systemPropertyName={USER_SETTINGS.INSTRUMENT_AMOUNTS}
        render={this.resolveMultiplier}
      />
    );
  }

  renderInstrument = (quickTradeMultiplier, { marketData = {}, width, instrumentInfo, colorMode, amount, globalAmount, customAmountsSetting, loaded }) => {
    const qtMultiplier = (quickTradeMultiplier && quickTradeMultiplier.get('value')) || 1;
    const qtAmounts = (customAmountsSetting && customAmountsSetting.get('value').toArray()) || [];

    if (!instrumentInfo) {
      return <div></div>;
    }

    const {
      hasTradePermissions,
      onQuickTrade,
      onOrderClick,
      onTicketClick,
      isActive,
      theme,
      onOrderRequestClick,
    } = this.props;
    const { customAmounts } = this.state;

    const {
      bid,
      ask,
      pipSize = instrumentInfo.pipSize,
      spread,
      active,
      dynamic = {},
    } = marketData;
    const instrumentName = instrumentInfo.displayName;

    const availableAmounts = uniq([...customAmounts, ...qtAmounts, globalAmount])
      .filter((_) => _)
      .map((value) => ({ value, label: amountFormat(value) }));

    /* eslint-disable jsx-a11y/no-static-element-interactions */
    return (
      <div
        onClick={() => onTicketClick ? onTicketClick(instrumentInfo.id) : null}
        styleName="trade-ticket"
        className={`_${theme}`}
        style={{ width: `${width}%` }}
        ref={(el) => { this.tile = el; }}
      >
        <div styleName={`trade-ticket-wrapper ${isActive ? 'active' : ''}`}>
          {/* Card header */}
          <div styleName="trade-ticket-row">
            <div styleName="trade-ticket-box">
              <div styleName="trade-ticket-label">
                <InstrumentCategory instrument={instrumentInfo} size={25} fontSize="11px" fontColor="#ffffff" />
              </div>
              <span styleName="trade-ticket-currency">
                {instrumentName}
              </span>
            </div>
            <div styleName="trade-ticket-right-box">
              <span styleName="trade-ticket-amount">
                Amount
                {instrumentInfo.availableForUserTrade && qtMultiplier > 1 && <AmountMultiplier multiplier={qtMultiplier} />}
              </span>
              <div styleName="trade-ticket-select-wrapper">
                <Select.Creatable
                  value={amount || globalAmount}
                  options={availableAmounts}
                  onChange={this.changeVolume}
                  styleName="trade-ticket-select"
                  clearable={false}
                  optionstyleName="trade-ticket-select-option"
                  disabled={!(bid > 0 && ask > 0)}
                />
              </div>
              <div styleName="context-menu">
                <i
                  className="icon-icon-wathclist-widget-menu"
                  onClick={(e) => {
                    const container = this.findNode(e.target, 'scrollbar-container');
                    const containerRect = container.getBoundingClientRect();
                    const targetRect = this.tile.getBoundingClientRect();
                    e.stopPropagation();
                    this.props.openMenu({
                      id: instrumentInfo.id,
                      position: {
                        x: (targetRect.x + targetRect.width) - containerRect.x,
                        y: targetRect.y - containerRect.y,
                      },
                    });
                  }}
                />
              </div>
            </div>
          </div>

          {/* Card body */}
          <div styleName="trade-ticket-row">
            <div styleName="trade-ticket-left-box">
              <div
                onClick={() => hasTradePermissions && instrumentInfo.availableForUserTrade && (amount || globalAmount)
                  ? onQuickTrade({
                    instrumentId: instrumentInfo.id,
                    price: bid,
                    direction: TRADE_DIRECTION.SELL,
                    amount: (amount || globalAmount) * qtMultiplier,
                  })
                  : {}
                }
                styleName={`trade-ticket-sell ${hasTradePermissions && instrumentInfo.availableForUserTrade && (amount || globalAmount) ? 'availableForQuickTrade' : ''}`}
              >
                <div styleName="trade-ticket-sell-text">
                  Sell
                </div>
                <div styleName={`trade-ticket-sell-arrow ${dynamic.bid ? '_positive' : '_negative'}`}>
                  {active && colorMode && <Arrow />}
                </div>
                <div styleName="trade-ticket-sell-price">
                  <div styleName="trade-ticket-sell-price-value">
                    <span styleName="trade-ticket-sell-price-value-part">
                      {loaded && bid
                        ? <QuotValue value={bid} pipSize={pipSize} />
                        : <span>
                          <LoadingIndicator loading small transparent style={loaderStyle} />
                        </span>
                      }
                    </span>
                  </div>
                </div>
              </div>
            </div>

            <div styleName="trade-ticket-ratio">
              <span styleName="trade-ticket-ratio-text">
                {spread ? <FormattedNumber value={spread} maximumFractionDigits={1} /> : null}
              </span>
            </div>

            <div styleName="trade-ticket-right-box">
              <div
                onClick={() => hasTradePermissions && instrumentInfo.availableForUserTrade && (amount || globalAmount)
                  ? onQuickTrade({
                    instrumentId: instrumentInfo.id,
                    price: ask,
                    direction: TRADE_DIRECTION.BUY,
                    amount: (amount || globalAmount) * qtMultiplier,
                  })
                  : {}
                }
                styleName={`trade-ticket-buy ${hasTradePermissions && instrumentInfo.availableForUserTrade && (amount || globalAmount) ? 'availableForQuickTrade' : ''}`}
              >
                <div styleName="trade-ticket-buy-text">
                  Buy
                </div>
                <div styleName={`trade-ticket-buy-arrow ${dynamic.ask ? '_positive' : '_negative'}`}>
                  {active && colorMode && <Arrow />}
                </div>
                <div styleName="trade-ticket-buy-price">
                  <div styleName="trade-ticket-sell-price-value">
                    <span styleName="trade-ticket-buy-price-value-part">
                      {loaded && ask
                        ? <QuotValue value={ask} pipSize={pipSize} />
                        : <span>
                          <LoadingIndicator loading small transparent style={loaderStyle} />
                        </span>
                      }
                    </span>
                  </div>
                </div>
              </div>
            </div>
          </div>

          {/* Card footer */}
          <div styleName="trade-ticket-row">
            <div styleName="trade-ticket-left-box">
              <Button
                fullWidth
                disabled={!hasTradePermissions || !onOrderClick || instrumentInfo.expired}
                type="primary"
                onClick={() => onOrderClick && instrumentInfo.availableForUserTrade ? onOrderClick(instrumentInfo.id, TRADE_DIRECTION.SELL) : onOrderRequestClick(instrumentInfo.code, TRADE_DIRECTION.SELL)}
                smaller
                dataButtonAction="sell-order"
              >
                Sell order
              </Button>
            </div>
            <div styleName="trade-ticket-right-box">
              <Button
                fullWidth
                disabled={!hasTradePermissions || !onOrderClick || instrumentInfo.expired}
                type="primary"
                onClick={() => onOrderClick && instrumentInfo.availableForUserTrade ? onOrderClick(instrumentInfo.id, TRADE_DIRECTION.BUY) : onOrderRequestClick(instrumentInfo.code, TRADE_DIRECTION.BUY)}
                smaller
                dataButtonAction={`buy-order:${instrumentInfo.displayName}`}
              >
                Buy order
              </Button>
            </div>
          </div>
        </div>
      </div>
    );
    /* eslint-enable */
  }

  render() {
    const { instrumentId, width, colorMode, multiAccountId, profileId, globalAmount } = this.props;
    const { amount } = this.state;

    return (
      <InstrumentMarketData
        withInstrumentInfo
        id={instrumentId}
        render={this.resolveAmounts}
        width={width}
        colorMode={colorMode}
        multiAccountId={multiAccountId}
        profileId={profileId}
        amount={amount}
        globalAmount={globalAmount}
      />
    );
  }
}


InstrumentQuoteTile.propTypes = {
  onQuickTrade: PropTypes.func,
  width: PropTypes.number,

  multiAccountId: PropTypes.number.isRequired,
  profileId: PropTypes.number.isRequired,
  onOrderClick: PropTypes.func,
  onOrderRequestClick: PropTypes.func,
  openMenu: PropTypes.func,
  onTicketClick: PropTypes.func,
  isActive: PropTypes.bool.isRequired,
  colorMode: PropTypes.bool.isRequired,
  hasTradePermissions: PropTypes.bool,
  instrumentId: PropTypes.number,
  theme: PropTypes.string,
  globalAmount: PropTypes.number,
};


export default InstrumentQuoteTile;
