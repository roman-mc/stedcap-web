/*
 *
 * CreateWatchlistPopup
 *
 */

import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { FormattedMessage } from 'react-intl';
import { createStructuredSelector } from 'reselect';
import Modal, { actions } from 'Containers/Modal';

import CreateWatchlistForm from './CreateWatchlistForm';


import messages from './messages';
import { makeSelectTheme } from '../../../../../containers/App/selectors';

function CreateWatchlist({ instruments, onSubmit, theme }) {
  const isDarkTheme = theme === 'dark-theme';

  return (
    <Modal
      modalId="edit_watchlist_modal"
      renderHeader={({ initialValues }) =>
        <FormattedMessage
          {...(initialValues && initialValues.id ? messages.editWatchlist : messages.addWatchlist)}
        />
      }
    >
      <CreateWatchlistForm
        instruments={instruments}
        onSubmit={onSubmit}
        isDarkTheme={isDarkTheme}
      />
    </Modal>
  );
}

const mapStateToProps = createStructuredSelector({
  theme: makeSelectTheme(),
});

const mapDispatchToProps = (dispatch) => ({
  closeModal: () => dispatch(actions.closeModal({ modalId: 'edit_watchlist_modal' })),
});


CreateWatchlist.propTypes = {
  instruments: PropTypes.any, // TODO: proper type
  onSubmit: PropTypes.func.isRequired,
  theme: PropTypes.string.isRequired,
};

export default connect(mapStateToProps, mapDispatchToProps)(CreateWatchlist);
