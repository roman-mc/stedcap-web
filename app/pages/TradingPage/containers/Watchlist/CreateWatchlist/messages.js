/*
 * CreateWatchlistPopup Messages
 *
 * This contains all the text for the CreateWatchlistPopup component.
 */
import { defineMessages } from 'react-intl';

export default defineMessages({
  remove: {
    id: 'app.containers.CreateWatchlistPopup.remove',
    defaultMessage: 'Remove',
  },
  noData: {
    id: 'app.containers.CreateWatchlistPopup.noData',
    defaultMessage: 'Instruments will be displayed here',
  },
  add: {
    id: 'app.containers.CreateWatchlistPopup.add',
    defaultMessage: 'Add',
  },
  cancelButton: {
    id: 'app.containers.CreateWatchlistPopup.cancelButton',
    defaultMessage: 'Cancel',
  },
  createButton: {
    id: 'app.containers.CreateWatchlistPopup.createButton',
    defaultMessage: 'Create watchlist',
  },
  updateButton: {
    id: 'app.containers.CreateWatchlistPopup.updateButton',
    defaultMessage: 'Update watchlist',
  },
  watchlistName: {
    id: 'app.containers.CreateWatchlistPopup.watchlistName',
    defaultMessage: 'Watchlist name',
  },
  enterWatchlistName: {
    id: 'app.containers.CreateWatchlistPopup.enterWatchlistName',
    defaultMessage: 'Name your new watchlist',
  },
  addWatchlist: {
    id: 'app.containers.TradingPage.WatchList.addWatchlist',
    defaultMessage: 'Create watchlist',
  },
  editWatchlist: {
    id: 'app.containers.TradingPage.WatchList.editWatchlist',
    defaultMessage: 'Edit watchlist',
  },

  addedSymbols: {
    id: 'app.containers.TradingPage.WatchList.addedSymbols',
    defaultMessage: 'Added symbols',
  },
});
