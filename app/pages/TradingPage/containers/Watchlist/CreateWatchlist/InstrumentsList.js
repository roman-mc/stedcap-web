import React from 'react';
import { List } from 'immutable';
import PropTypes from 'prop-types';
// import ImmutableProptypes from 'react-immutable-proptypes';
import { FormattedMessage } from 'react-intl';
import { SortableContainer, SortableElement, SortableHandle } from 'react-sortable-hoc';
import { InstrumentsTree } from 'Containers/InstrumentsTree';
import SearchField from 'Components/SearchField';
import InstrumentCategory from 'Components/InstrumentCategory';
import InstrumentResolver from 'unity-frontend-core/containers/InstrumentResolver';


import { ListItem } from '../../../../../components/List';

import messages from './messages';
import { styleNames } from '../../../../../utils/styles';

import styles from './style.css';

function NoData() {
  return (<div styleName="no-data"><FormattedMessage {...messages.noData} /></div>);
}

const DragHandle = SortableHandle(() => <span styleName="reorder-hadnler" />);

class InstrumentItem extends React.PureComponent {
  renderItem = (instrumentInfo) => {
    const { value, onClick, searchQuery, isDarkTheme } = this.props;
    if (!instrumentInfo) {
      return (
        <ListItem>
          Loading...
        </ListItem>
      );
    }

    // So hard because of BUG in react-sortable-hoc
    // root container couldn't be specified
    const instrumentRowTitleClasses = styleNames(
      styles['instrument-row-title'],
      !isDarkTheme && styles['instrument-row-title--light-theme'],
    );


    if (instrumentInfo.displayName.toLowerCase().indexOf(searchQuery) > -1) {
      return (
        <ListItem renderRight={() => <DragHandle />}>
          <span
            styleName="instrument-button"
            onClick={() => onClick(value)}
          >
            <i className="icon-icon-minus" />
          </span>
          <InstrumentCategory instrument={{ category: instrumentInfo.category }} size="28px" fontSize="11px" />
          <h4 className={instrumentRowTitleClasses} data-instrument-item-name={instrumentInfo.displayName}>{instrumentInfo.displayName}</h4>
        </ListItem>
      );
    }
    return <span />;
  }

  render() {
    const { value, searchQuery } = this.props;
    return (
      <InstrumentResolver id={value} render={this.renderItem} searchQuery={searchQuery} />
    );
  }
}

InstrumentItem.propTypes = {
  value: PropTypes.number.isRequired,
  searchQuery: PropTypes.string,
  onClick: PropTypes.func.isRequired,
  isDarkTheme: PropTypes.bool,
};

const SortableInstrumentItem = SortableElement(InstrumentItem);


class InstrumentsList extends React.PureComponent {
  state = {
    searchQuery: '',
  };

  onSearchChange = (searchQuery) => {
    this.setState({ searchQuery: searchQuery.toUpperCase() });
  };

  render() {
    const {
      items, title, onClick, disabled,
    } = this.props;
    const { searchQuery } = this.state;

    return (
      <div styleName="scroll-wrapper">
        <h2 styleName="instruments-title"><FormattedMessage {...title} /></h2>
        <div styleName="new-list-padding">
          <SearchField onChange={this.onSearchChange} value={searchQuery} />
        </div>
        <div styleName="text">
          {
            items.length
              ? items.map((instrument, idx) =>
                (<SortableInstrumentItem
                  disabled={disabled}
                  value={instrument}
                  onClick={onClick}
                  index={idx}
                  key={`create_watchlist_instrument_${instrument}`}
                  searchQuery={searchQuery.toLowerCase()}
                />))
              : <NoData />
          }
        </div>
      </div>
    );
  }
}

InstrumentsList.propTypes = {
  title: PropTypes.object,
  onClick: PropTypes.func.isRequired,
  items: PropTypes.array,
  disabled: PropTypes.bool,
};

const SortableList = SortableContainer(InstrumentsList);

export default class InstrumentsListField extends React.PureComponent {
  onReorder = ({ oldIndex, newIndex }) => {
    const { fields } = this.props;
    const oldValue = fields.getAll().get(oldIndex);
    fields.remove(oldIndex);
    fields.insert(newIndex, oldValue);
  };

  get fieldValue() {
    return this.props.fields.getAll() || new List();
  }

  removeInstrument = (instrument) => this.props.fields.remove((this.props.fields.getAll() || []).indexOf(instrument));

  addInstrument = (instrument) => this.props.fields.push(instrument);

  render() {
    const selectedInstruments = this.fieldValue.toArray();
    return (
      <div styleName="row">
        <div styleName="column-left withBorder">
          <SortableList
            items={selectedInstruments}
            title={messages.addedSymbols}
            onClick={this.removeInstrument}
            useDragHandle
            onSortEnd={this.onReorder}
            isDarkTheme={this.props.isDarkTheme}
          />
        </div>
        <div styleName="column-right withBorder">
          <InstrumentsTree
            selectedInstruments={selectedInstruments}
            onSelect={this.addInstrument}
            onRemove={this.removeInstrument}
          />
        </div>
      </div>
    );
  }
}

InstrumentsListField.propTypes = {
  fields: PropTypes.object.isRequired,
  isDarkTheme: PropTypes.bool.isRequired,
};
