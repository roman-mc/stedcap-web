import React from 'react';
import PropTypes from 'prop-types';
import { reduxForm, FieldArray } from 'redux-form/immutable';
import { FormattedMessage, injectIntl, intlShape } from 'react-intl';
import { Button, Input } from 'Components/ReduxFields';

import { requireValidate, nonEmptyArrayValidate } from 'Utils/form/validation';

import InstrumentsListField from './InstrumentsList';
import messages from './messages';

import style from './style.css';

export class CreateWatchlistForm extends React.PureComponent { // eslint-disable-line react/prefer-stateless-function
  render() {
    const {
      isDarkTheme,
      initialValues,
      handleSubmit,
      removePopup,
      invalid, intl: { formatMessage },
    } = this.props;

    return (
      <form styleName="create-watchlist" onSubmit={handleSubmit}>
        <div styleName="row">
          <div styleName="column-left">
            <div styleName="new-list-padding">
              <Input
                validate={[requireValidate]}
                name="name"
                text={formatMessage(messages.watchlistName)}
                placeholder={formatMessage(messages.enterWatchlistName)}
                maxlength={15}
                normalize={(val) => val.toUpperCase()}
              />
            </div>
          </div>
        </div>
        <FieldArray
          component={InstrumentsListField}
          name="instruments"
          validate={[nonEmptyArrayValidate]}
          isDarkTheme={isDarkTheme}
        />
        <div styleName="row justify-center">
          <Button
            big
            subClass={`${style.footerButton} ${style.first}`}
            onClick={removePopup}
          >
            <FormattedMessage {...messages.cancelButton} />
          </Button>
          <Button
            big
            disabled={invalid}
            type="submit"
            subClass={`${style.footerButton} ${style.last} ${style.button_right}`}
          >
            <span data-button-action="save-watchlist">
              <FormattedMessage {...(initialValues && initialValues.id ? messages.updateButton : messages.createButton)} />
            </span>
          </Button>
        </div>
      </form>
    );
  }
}

CreateWatchlistForm.propTypes = {
  handleSubmit: PropTypes.func.isRequired,
  removePopup: PropTypes.func.isRequired,
  invalid: PropTypes.bool.isRequired,
  intl: intlShape,
  initialValues: PropTypes.object,
  isDarkTheme: PropTypes.bool.isRequired,
};


export default reduxForm({ form: 'createWatchlistForm' })(injectIntl(CreateWatchlistForm));
