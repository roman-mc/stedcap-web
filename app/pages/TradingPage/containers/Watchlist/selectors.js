import { Map, List } from 'immutable';
import { createSelector } from 'reselect';

export const makeSelectSpace = () => (state) => state.get('trading.watchlist', new Map());

export const makeSelectLoading = () => createSelector(
  makeSelectSpace(),
  (watchlistState) => watchlistState.get('isLoading'),
);

export const makeSelectWatchlists = () => createSelector(
  makeSelectSpace(),
  (substate) => substate.get('watchlists', new List()),
);

export const makeSelectSelectedWatchlistId = () => (state, props) => props.config.get('selectedWatchlistId');

export const makeSelectVolumesForWatchlists = () => (state, props) => props.volumes || {};

export const makeSelectSelectedWatchlist = () => createSelector(
  makeSelectWatchlists(),
  makeSelectSelectedWatchlistId(),
  (watchlists, id) => watchlists.find((w) => w.id === id) || watchlists.find((_) => _),
);

export const makeSelectWatchlistPopupOpened = () => createSelector(
  makeSelectSpace(),
  (substate) => substate.get('watchlistPopupOpened'),
);
