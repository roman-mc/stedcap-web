import React, { Fragment, PureComponent } from 'react';
import { connect } from 'react-redux';
import { FormattedMessage } from 'react-intl';
import { createStructuredSelector } from 'reselect';
import PropTypes from 'prop-types';
import ImmutableProptypes from 'react-immutable-proptypes';
import { actions } from 'Containers/Modal';

import {
  makeSelectSelectedWatchlist,
  makeSelectWatchlists,
} from './selectors';

import messages from './messages';
import { WindowHeader, WindowHeaderTitle } from '../../../../components/Window';

import {
  deleteWatchlist,
} from './actions';

import styles from './style.css';

const prepareWatchlistToEdit = (watchlist) => {
  return watchlist.set('instruments', watchlist.instruments.map(({ instrument }) => instrument).toList());
};

class WatchlistHeader extends PureComponent {
  onWatchlistDelete = () => {
    const { selectedWatchlist, userWatchlitsts } = this.props;
    const newWatchlist = userWatchlitsts.findLast((w) => !w.disabled && w.id !== selectedWatchlist.id);
    this.changeWatchlist(newWatchlist);
  }

  changeWatchlist = (selectedWatchlist) => {
    this.props.onConfigChange({ selectedWatchlistId: selectedWatchlist.id });
  };

  deleteWatchlist = () => {
    const { selectedWatchlist } = this.props;
    this.props.deleteWatchlist(selectedWatchlist, this.onWatchlistDelete);
  };

  render() {
    const {
      selectedWatchlist,
      openWatchlistModal,
      ...windowHeaderProps
    } = this.props;

    return (
      <WindowHeader
        {...windowHeaderProps}
        renderTitle={() =>
          <Fragment>
            <WindowHeaderTitle dataWindowName="watchlist" icon="icon-icon-widget-watchlist">Watchlists</WindowHeaderTitle>
            <button
              onClick={() => openWatchlistModal()}
              type="button"
              className={styles.widgetHeaderButton}
              data-watchlist-button="new"
            >
              <span styleName="button-content-text" data-watchlist-button="edit">
                <i className={`icon-icon-new-watchlist ${styles.widgetHeaderButtonIcon}`}></i>
                <FormattedMessage {...messages.newWatchlist} />
              </span>
            </button>

            <div className={styles.headDelimiter} />

            <button
              onClick={() => openWatchlistModal(prepareWatchlistToEdit(selectedWatchlist))}
              disabled={!selectedWatchlist}
              type="button"
              className={styles.widgetHeaderButton}
            >
              <span styleName="button-content-text" data-watchlist-action="edit">
                <i className={`icon-icon-edit ${styles.widgetHeaderButtonIcon}`}></i>
                <FormattedMessage {...messages.editLayout} />
              </span>
            </button>

            <div className={styles.headDelimiter} />

            <button
              onClick={this.deleteWatchlist}
              disabled={!selectedWatchlist}
              type="button"
              className={styles.widgetHeaderButton}
            >
              <span styleName="button-content-text">
                <i className={`icon-icon-remove ${styles.widgetHeaderButtonIcon}`}></i>
                <FormattedMessage {...messages.deleteWatchlist} />
              </span>
            </button>
          </Fragment>
        }
      />
    );
  }
}

WatchlistHeader.propTypes = {
  userWatchlitsts: ImmutableProptypes.list,
  selectedWatchlist: PropTypes.object,

  openWatchlistModal: PropTypes.func.isRequired,
  deleteWatchlist: PropTypes.func.isRequired,
  onConfigChange: PropTypes.func.isRequired,
};

const mapStateToProps = createStructuredSelector({
  selectedWatchlist: makeSelectSelectedWatchlist(),
  userWatchlitsts: makeSelectWatchlists(),
});

const mapDispatchToProps = (dispatch) => ({
  openWatchlistModal: (initialValues) => dispatch(
    actions.openModal({ modalId: 'edit_watchlist_modal', payload: { initialValues } })
  ),
  deleteWatchlist: (watchlist, cb) =>
    dispatch(deleteWatchlist({ watchlist, cb })),
});


export default connect(mapStateToProps, mapDispatchToProps)(WatchlistHeader);
