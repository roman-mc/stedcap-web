import { Map, List } from 'immutable';
import { createReducer } from 'redux-act';

import Watchlist from 'unity-frontend-core/Models/Watchlist';


import { destroyPage } from '../../actions';

import {
  loadWacthlists,
  createWatchlist,
  updateWatchlist,
  deleteWatchlist,
  openPopup,
  drugOrderInstruments,
} from './actions';

const drugOrderInstrumentHandler = (state, watchlist) => (
  state
    .update(
      'watchlists',
      (watchlists) => watchlists.set(watchlists.findIndex((w) => w.id === watchlist.id), new Watchlist(watchlist))
    )
);

const initialState = new Map({
  watchlists: new List(),
  selectedWatchlist: null,
  watchlistPopupOpened: false,
  watchlistToEdit: null,
  instrumentsFound: null,
  isLoading: false,
});

export default createReducer({
  [openPopup]: (state, { watchlist }) =>
    state
      .set('watchlistToEdit', watchlist)
      .set('watchlistPopupOpened', true),
  [createWatchlist.success]: (state, payload) =>
    state.update('watchlists', (watchlists) => watchlists.push(payload)),
  [updateWatchlist.success]: (state, payload) =>
    state
      .update(
        'watchlists',
        (watchlists) => watchlists.set(watchlists.findIndex((w) => w.id === payload.id), payload)
      ),

  [loadWacthlists]: (state) => state.set('isLoading', true),
  [loadWacthlists.success]: (state, payload) =>
    state
      .set('watchlists', new List(payload.map((w) => new Watchlist(w))))
      .set('isLoading', false),
  [loadWacthlists.error]: (state) =>
    state.set('isLoading', false),

  [deleteWatchlist.success]: (state, { watchlistId }) =>
    state
      .update(
        'watchlists',
        (watchlists) =>
          watchlists.delete(watchlists.findIndex((w) => w.id === watchlistId))
      ),
  [destroyPage]: () => initialState,
  [drugOrderInstruments.success]: drugOrderInstrumentHandler,
  [drugOrderInstruments.error]: drugOrderInstrumentHandler,
}, initialState);
