/*
 * WatchList Messages
 *
 * This contains all the text for the WatchList component.
 */
import { defineMessages } from 'react-intl';

export default defineMessages({
  codeHeader: {
    id: 'app.containers.TradingPage.WatchList.instrument.header',
    defaultMessage: 'Instrument',
  },
  instrumentDescription: {
    id: 'app.containers.TradingPage.WatchList.instrument.description',
    defaultMessage: 'Description',
  },
  bidHeader: {
    id: 'app.containers.TradingPage.WatchList.bid.header',
    defaultMessage: 'Bid',
  },
  bidDescription: {
    id: 'app.containers.TradingPage.WatchList.bid.description',
    defaultMessage: 'Day high',
  },
  askHeader: {
    id: 'app.containers.TradingPage.WatchList.ask.header',
    defaultMessage: 'Ask',
  },
  askDescription: {
    id: 'app.containers.TradingPage.WatchList.ask.description',
    defaultMessage: 'Day low',
  },
  net_changeHeader: {
    id: 'app.containers.TradingPage.WatchList.net_change.header',
    defaultMessage: '% change',
  },
  net_changeDescription: {
    id: 'app.containers.TradingPage.WatchList.net_change.description',
    defaultMessage: 'Day high',
  },
  market_statusHeader: {
    id: 'app.containers.TradingPage.WatchList.market_status.header',
    defaultMessage: 'Market status',
  },
  market_statusDescription: {
    id: 'app.containers.TradingPage.WatchList.market_status.description',
    defaultMessage: 'Exchange',
  },
  delayHeader: {
    id: 'app.containers.TradingPage.WatchList.delay.header',
    defaultMessage: 'Delay',
  },
  delayDescription: {
    id: 'app.containers.TradingPage.WatchList.delay.description',
    defaultMessage: 'Last updated',
  },
  marketOpen: {
    id: 'app.containers.TradingPage.WatchList.marketOpen',
    defaultMessage: 'Open',
  },
  marketClosed: {
    id: 'app.containers.TradingPage.WatchList.marketClosed',
    defaultMessage: 'Closed',
  },
  newWatchlist: {
    id: 'app.containers.TradingPage.Watchlist.newWatchlist',
    defaultMessage: 'New',
  },
  deleteWatchlist: {
    id: 'app.containers.TradingPage.Watchlist.deleteWatchlist',
    defaultMessage: 'Delete',
  },
  editLayout: {
    id: 'app.containers.TradingPage.Watchlist.newLayout',
    defaultMessage: 'Edit',
  },
  quantity: {
    id: 'app.containers.TradingPage.Watchlist.quantity',
    defaultMessage: 'Quantity',
  },
  addInstruments: {
    id: 'app.containers.TradingPage.Watchlist.addInstruments',
    defaultMessage: 'Add instruments',
  },
  search: {
    id: 'app.containers.TradingPage.Watchlist.search',
    defaultMessage: 'Search',
  },

  deleteText: {
    id: 'app.containers.TradingPage.Watchlist.deleteText',
    defaultMessage: 'Are you sure? Symbols in this watchlist will be lost.',
  },
  deleteHeader: {
    id: 'app.containers.TradingPage.Watchlist.deleteHeader',
    defaultMessage: 'Delete watchlist',
  },
  deleteButton: {
    id: 'app.containers.TradingPage.Watchlist.deleteButton',
    defaultMessage: 'Delete',
  },
});
