import React from 'react';
import PropTypes from 'prop-types';
import { Map } from 'immutable';
import ImmutableProptypes from 'react-immutable-proptypes';
import { FormattedMessage } from 'react-intl';
import { reduxForm } from 'redux-form/immutable';

import { Row, Column } from 'Components/Popup/components';
import { Input, FormSelector, BtnGroup } from 'Components/ReduxFields';

import { requireValidate } from 'Utils/form/validation';
import { formatNumeral } from 'Utils/form/normalization';
import { GOOD_TILL_TYPE, ORDER_TYPE, TRADE_DIRECTION } from 'Model/enums';
import { Button } from 'Components/GUI';
import styles from './style.css';

import Lots from '../Popups/Ticket/components/Lots';

import messages from './messages';
import TradingDetails from '../Popups/Ticket/components/TradingDetails';

class Ticket extends React.PureComponent {
  componentDidMount() {
    const { initialValues = new Map(), info } = this.props;
    this.props.loadLastAmount({
      instrumentId: info.id,
      initialValues,
    });
  }
  render() {
    const {
      tradingDetails, hasTradePermission, onOrderChanges,
      placeOrder, invalid, handleSubmit, info,
      formValues, direction, change, loading,
      networkDisconnected, isConnectionBad,
      initialValues,
      displayedCurrency,
    } = this.props;

    const isPoorConnection = networkDisconnected || isConnectionBad;
    const poorConnectionText = networkDisconnected ? messages.noConnectionTitle : messages.badConnectionTitle;

    const orderType = parseFloat(formValues.get('orderType')) || parseFloat(initialValues.get('orderType'));
    const goodTillTypes = Object
      .values(GOOD_TILL_TYPE)
      .filter(({ orderTypes }) => (formValues && orderTypes.indexOf(orderType) >= 0))
      .map(({ id, name }) => ({ label: name, value: id }));
    return (
      <form onSubmit={handleSubmit}>
        <Row withoutBorder subClass={styles.row}>
          <BtnGroup
            validate={[requireValidate]}
            text={<FormattedMessage {...messages.orderTypeLabel} />}
            onChange={(e, ot) => {
              console.log(parseFloat(ot));
              if (orderType === parseFloat(ot)) {
                return;
              }
              // change('orderType', parseFloat(ot));
              if (parseFloat(ot) === ORDER_TYPE.LIMIT.id) {
                change('goodTillType', GOOD_TILL_TYPE.G_T_C.id);
              } else {
                change('goodTillType', GOOD_TILL_TYPE.FILL_OR_KILL.id);
              }
            }}
            hideLabel
            options={Object.values(ORDER_TYPE).map(({ id, name }) => ({ label: name, value: id }))}
            name="orderType"
            disableClearable
            disabled={!info.availableForUserTrade}
          />
        </Row>
        <Row withoutBorder subClass={styles.row}>
          <Input
            name="amount"
            format={formatNumeral}
            validate={[requireValidate]}
            text={<FormattedMessage {...messages.amount} />}
            onChange={(e, amount) => onOrderChanges({ amount: parseInt(amount, 10) })}
            disabled={!info.availableForUserTrade}
          />
        </Row>
        <Row withoutBorder subClass={styles.row}>
          {info && <Lots
            label={<FormattedMessage {...messages.amount} />}
            validate={[requireValidate]}
            name="amount"
            lots={info.volumes}
            onChange={(e, amount) => onOrderChanges({ amount })}
            disabled={!info.availableForUserTrade}
          />}
        </Row>
        <Row withoutBorder subClass={styles.row}>
          <Column rightBackslash>
            <FormSelector
              validate={[requireValidate]}
              text={<FormattedMessage {...messages.goodTillTypeLabel} />}
              options={goodTillTypes}
              name="goodTillType"
              disableClearable
              disabled={!info.availableForUserTrade}
            />
          </Column>
          {orderType !== ORDER_TYPE.MARKET.id && <Column leftBackslash>
            <Input
              name="price"
              format={formatNumeral}
              validate={[requireValidate]}
              text={<FormattedMessage {...messages.price} />}
              disabled={!info.availableForUserTrade}
            />
          </Column>}
        </Row>
        {isPoorConnection
          && <div styleName={`tooltip ${networkDisconnected ? 'tooltip-error' : ''}`}>
            <i className="icon-icon-error"></i>
            <span styleName="tooltip-text">
              <FormattedMessage {...poorConnectionText} />
            </span>
          </div>
        }
        <Row withoutBorder subClass={styles.row}>
          <Column rightBackslash>
            <Button
              btnType="button"
              fullWidth
              big
              type="secondary"
              className={styles.sell}
              disabled={!info.availableForUserTrade || invalid || !hasTradePermission || loading}
              onClick={placeOrder(TRADE_DIRECTION.SELL)}
            >
              <FormattedMessage {...messages[orderType === ORDER_TYPE.LIMIT.id ? 'marketSell' : 'ticketSell']} />
            </Button>
          </Column>
          <Column leftBackslash>
            <Button
              btnType="button"
              fullWidth
              big
              type="secondary"
              className={styles.buy}
              disabled={!info.availableForUserTrade || invalid || !hasTradePermission || loading}
              onClick={placeOrder(TRADE_DIRECTION.BUY)}
            >
              <FormattedMessage {...messages[orderType === ORDER_TYPE.LIMIT.id ? 'marketBuy' : 'ticketBuy']} />
            </Button>
          </Column>
        </Row>
        <Row subClass={styles.row}>
          {info && <TradingDetails
            details={tradingDetails}
            direction={direction}
            displayedCurrency={displayedCurrency}
          />}
        </Row>
      </form>
    );
  }
}

Ticket.propTypes = {
  handleSubmit: PropTypes.func.isRequired,
  onOrderChanges: PropTypes.func.isRequired,
  placeOrder: PropTypes.func.isRequired,
  loadLastAmount: PropTypes.func.isRequired,
  info: ImmutableProptypes.record,
  invalid: PropTypes.bool.isRequired,
  loading: PropTypes.bool,
  networkDisconnected: PropTypes.bool,
  isConnectionBad: PropTypes.bool,
  hasTradePermission: PropTypes.bool.isRequired,
  tradingDetails: ImmutableProptypes.record,
  formValues: ImmutableProptypes.map,
  initialValues: ImmutableProptypes.map,
  change: PropTypes.func.isRequired,
  direction: PropTypes.oneOf(Object.values(TRADE_DIRECTION)),
  displayedCurrency: PropTypes.string.isRequired,
};


export default reduxForm({ form: 'ticketWidgetForm' })(Ticket);
