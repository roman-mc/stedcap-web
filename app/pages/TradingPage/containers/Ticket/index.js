import React from 'react';
import connect from 'react-redux/es/connect/connect';
import { Map } from 'immutable';
import { createStructuredSelector } from 'reselect';
import ImmutableProptypes from 'react-immutable-proptypes';
import PropTypes from 'prop-types';
import { throttle } from 'lodash';

import InstrumentMarketData from 'unity-frontend-core/containers/InstrumentMarketData';
import TradeTicket from 'unity-frontend-core/containers/TradeTicket';
import WithInjection from 'unity-frontend-core/containers/WithInjection';
import { makeSelectActiveAccountPermissions } from 'unity-frontend-core/containers/SelectAccount/selectors';

import { GOOD_TILL_TYPE, ORDER_TYPE } from 'Model/enums';
import { parseNumeral } from 'Utils/form/normalization';

import ScrollBar from 'Components/Scrollbar';
import LoadingIndicator from 'Components/LoadingIndicator';
import TicketHeader from './TicketHeader';

import { makeSelectIsConnectionBad, makeSelectNetworkDisconnected } from '../../../AuthWrapper/selectors';
import { makeSelectSettings } from '../../../../containers/ProfileSettings/selectors';
import TicketForm from './TicketForm';
import { makeSelectFormData } from './selectors';
import { loadLastAmount } from './actions';
import sagas from './sagas';
import './style.css';

class TicketWidget extends React.PureComponent { // eslint-disable-line react/prefer-stateless-function
  componentWillUnmount() {
    this.props.unsubscribeTradeTicket();
  }
  onOrderChanges = throttle((changes) => {
    const { config, formValues } = this.props;
    const subscription = { ...formValues.toJS(), ...changes, instrumentId: config.get('selectedInstrumentId') };
    this.props.subscribeTradeTicket(subscription);
  }, 400)

  placeOrder = (direction) => () => {
    const { config, formValues } = this.props;
    const instrumentId = config && config.get('selectedInstrumentId');

    const entity = {
      ...formValues.toJS(),
      amount: parseFloat(parseNumeral(formValues.get('amount'))),
      price: parseFloat(parseNumeral(formValues.get('price'))),
      direction,
      orderType: parseFloat(formValues.get('orderType')),
      goodTillType: formValues.get('goodTillType'),
      instrumentId,
    };
    this.props.placeTradeTicket(entity);
  }

  renderNoData = () => (<div styleName="no-data">
    <i styleName="icon" className="icon-icon-ticket-widget"></i>
    <p>No order session for this instrument.<br />Please select another one.</p>
  </div>);

  renderTicket = (marketData, id, { instrumentInfo, displayedCurrency }) => {
    if (!instrumentInfo || !instrumentInfo.availableForUserTrade) {
      return this.renderNoData();
    }

    const { formValues = new Map() } = this.props;
    return marketData ?
      <TicketForm
        onOrderChanges={this.onOrderChanges}
        info={instrumentInfo}
        onOrderPlace={this.placeOrder}
        data={marketData}
        formValues={formValues}
        tradingDetails={this.props.tradingDetails}
        initialValues={{
          ...formValues.toJS(),
          price: ((marketData.bid + marketData.ask) / 2).toFixed(5),
          orderType: parseFloat(ORDER_TYPE.MARKET.id),
          goodTillType: GOOD_TILL_TYPE.FILL_OR_KILL.id,
        }}
        placeOrder={this.placeOrder}
        hasTradePermission={this.props.accountPermissions.includes('TRADE')}
        loading={this.props.loading}
        networkDisconnected={this.props.networkDisconnected}
        isConnectionBad={this.props.isConnectionBad}
        loadLastAmount={this.props.loadLastAmount}
        displayedCurrency={displayedCurrency}
      />
      : <div styleName="loading"><LoadingIndicator loading /></div>;
  }

  render() {
    const { config, formValues } = this.props;

    const instrumentId = config && config.get('selectedInstrumentId');

    return <ScrollBar style={{ position: 'static' }}>
      <InstrumentMarketData
        id={instrumentId}
        withInstrumentInfo
        render={this.renderTicket}
        formValues={formValues}
        tradingDetails={this.props.tradingDetails}
        displayedCurrency={this.props.settings.displayedCurrency}
      />
    </ScrollBar>;
  }
}

TicketWidget.propTypes = {
  config: ImmutableProptypes.map,
  formValues: ImmutableProptypes.map,
  tradingDetails: ImmutableProptypes.record,
  networkDisconnected: PropTypes.bool,
  isConnectionBad: PropTypes.bool,
  loading: PropTypes.bool,
  subscribeTradeTicket: PropTypes.func,
  unsubscribeTradeTicket: PropTypes.func,
  placeTradeTicket: PropTypes.func,
  loadLastAmount: PropTypes.func,
  accountPermissions: ImmutableProptypes.list,
  settings: ImmutableProptypes.record,
};


const mapStateToPros = createStructuredSelector({
  isConnectionBad: makeSelectIsConnectionBad(),
  networkDisconnected: makeSelectNetworkDisconnected(),
  formValues: makeSelectFormData(),
  accountPermissions: makeSelectActiveAccountPermissions(),
  settings: makeSelectSettings(),
});
function mapDispatchToProps(dispatch) {
  return {
    loadLastAmount: (payload) => dispatch(loadLastAmount(payload)),
  };
}
export default WithInjection({ sagas })(connect(mapStateToPros, mapDispatchToProps)(TradeTicket(TicketWidget)));

export {
  TicketHeader,
};
