import { createAction } from 'redux-act';

export const changeInstrument = createAction('app/TradingPage/CHANGE_TICKET_INSTRUMENT');
export const loadLastAmount = createAction('app/TradingPage/LOAD_LAST_AMOUNT');

