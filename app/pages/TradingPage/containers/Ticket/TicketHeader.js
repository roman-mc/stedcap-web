import React, { Fragment } from 'react';
import connect from 'react-redux/es/connect/connect';
import PropTypes from 'prop-types';
import ImmutableProptypes from 'react-immutable-proptypes';

import { InstrumentSelectorModal } from 'Containers/InstrumentsTree';

import { WindowHeader, WindowHeaderTitle } from 'Components/Window';

import { changeInstrument } from './actions';

class TicketHeader extends React.PureComponent { // eslint-disable-line react/prefer-stateless-function
  render() {
    return (
      <WindowHeader
        {...this.props}
        renderTitle={() =>
          <Fragment>
            <WindowHeaderTitle icon="icon-icon-ticket-widget">
              Ticket
            </WindowHeaderTitle>
            <InstrumentSelectorModal
              hideSearchLabel={this.props.hideSearchLabel}
              config={this.props.config}
              widgetType="ticket"
              onChange={(selectedInstrumentId) => this.props.changeInstrument(selectedInstrumentId)}
            />
          </Fragment>
        }
      />
    );
  }
}

TicketHeader.propTypes = {
  config: ImmutableProptypes.map,
  changeInstrument: PropTypes.func.isRequired,
  hideSearchLabel: PropTypes.bool,
};
function mapDispatchToProps(dispatch) {
  return {
    changeInstrument: (instrumentId) => dispatch(changeInstrument({ instrumentId })),
  };
}

export default connect(null, mapDispatchToProps)(TicketHeader);
