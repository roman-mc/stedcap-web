import { createSelector } from 'reselect';
import { Map } from 'immutable';

const makeSelectForm = () => (state) => state.getIn(['form', 'ticketWidgetForm'], new Map());

export const makeSelectFormData = () => createSelector(
  makeSelectForm(),
  (form) => form.get('values'),
);
