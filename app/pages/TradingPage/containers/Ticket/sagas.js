import { takeEvery } from 'redux-saga';
import { put, select, call } from 'redux-saga/effects';
import { initialize } from 'redux-form';
import { Map } from 'immutable';

import { makeSelectInstrumentsMap } from 'unity-frontend-core/containers/InstrumentResolver/selectors';
import { subscribeTradeTicket, placeTradeTicket } from 'unity-frontend-core/containers/TradeTicket/actions';
import * as OrdersApi from 'unity-frontend-core/api/ordersApi';
import {
  changeInstrument,
  loadLastAmount,
} from './actions';

import { makeSelectActivePanelId, makeSelectWidget, makeSelectWidgetId } from '../Dashboard/selectors';
import { addWidget, removeWidget } from '../Dashboard/actions';
import { loadOrders } from '../Positions/actions';

function* placeOrderHandler() {
  yield put(loadOrders());
}

export function* resetFormSaga() {
  yield takeEvery(placeTradeTicket.success.toString(), placeOrderHandler);
}

function* changeInstrumentHandler({ payload: { instrumentId } }) {
  const activePanelId = yield select(makeSelectActivePanelId());
  const ticketWidgetId = yield select(makeSelectWidgetId('ticket'));
  const ticketWidget = yield select(makeSelectWidget('ticket'));
  if (ticketWidgetId && ticketWidget.get('config').get('selectedInstrumentId') !== instrumentId) {
    // remove & add ticket widget because don't want update form initial values from component
    const blockId = [activePanelId, ticketWidgetId];
    yield put(removeWidget({ blockId }));
    yield put(addWidget({ panelId: activePanelId, widget: { ...ticketWidget.toJS(), config: { selectedInstrumentId: instrumentId } } }));
    yield put(loadLastAmount({ instrumentId }));
  }
}
export function* changeInstrumentSaga() {
  yield takeEvery(changeInstrument.toString(), changeInstrumentHandler);
}


export function* loadLastAmountHandler({ payload: { instrumentId, initialValues = new Map() } }) {
  try {
    const instruments = yield select(makeSelectInstrumentsMap());
    const instrument = instruments.get(instrumentId);
    const { amount } = yield call(OrdersApi.getLastAmount, instrument.category);
    const initial = { ...initialValues.toJS(), amount };
    yield put(initialize('ticketWidgetForm', initial));
    yield put(subscribeTradeTicket({ order: { ...initial, instrumentId } }));
  } catch (e) {
    console.log('error', e);
  }
}
export function* loadLastAmountSaga() {
  yield takeEvery(loadLastAmount.toString(), loadLastAmountHandler);
}

export default [
  resetFormSaga,
  changeInstrumentSaga,
  loadLastAmountSaga,
];
