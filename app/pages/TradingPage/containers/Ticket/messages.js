import { defineMessages } from 'react-intl';

export default defineMessages({
  orderTypeLabel: {
    id: 'app.containers.TradingPage.TicketWidget.orderTypeLabel',
    defaultMessage: 'Type',
  },
  goodTillTypeLabel: {
    id: 'app.containers.TradingPage.TicketWidget.goodTillTypeLabel',
    defaultMessage: 'Duration',
  },
  realtimePrices: {
    id: 'app.containers.TradingPage.TicketWidget.realtimePrices',
    defaultMessage: 'Realtime prices',
  },
  ticketHeader: {
    id: 'app.containers.TradingPage.TicketWidget.ticketHeader',
    defaultMessage: 'Order ticket',
  },
  orderButtonText: {
    id: 'app.containers.TradingPage.TicketWidget.placeBuyOrder',
    defaultMessage: '{isEdit, select, true {Edit} other {Place}} {direction, select, 1 {sell} 0 { buy } other { }} order',
  },
  loading: {
    id: 'app.containers.TradingPage.TicketWidget.loading',
    defaultMessage: 'Sending order...',
  },
  price: {
    id: 'app.containers.TradingPage.TicketWidget.price',
    defaultMessage: 'Price',
  },
  amount: {
    id: 'app.containers.TradingPage.TicketWidget.amount',
    defaultMessage: 'Amount',
  },
  badConnectionTitle: {
    id: 'app.containers.TradingPage.badConnectionTitle',
    defaultMessage: 'Real money trades may be executed with delay due to poor connection. Check your settings to restore it or proceed carefully.',
  },
  noConnectionTitle: {
    id: 'app.containers.TradingPage.noConnectionTitle',
    defaultMessage: 'There is no stable connection with server. Data is probably displayed incorrectly and trades may carry out with errors. Please check you internet settings before executing trades.',
  },
  ticketSell: {
    id: 'app.containers.TradingPage.ticketSell',
    defaultMessage: 'Sell',
  },
  ticketBuy: {
    id: 'app.containers.TradingPage.ticketBuy',
    defaultMessage: 'Buy',
  },
  marketSell: {
    id: 'app.containers.TradingPage.marketSell',
    defaultMessage: 'Sell order',
  },
  marketBuy: {
    id: 'app.containers.TradingPage.marketBuy',
    defaultMessage: 'Buy order',
  },
});
