/*
 *
 * TradingPage reducer
 *
 */
import { fromJS, Map } from 'immutable';
import { combineReducers } from 'redux-immutable';
import { createReducer } from 'redux-act';

import OrderData from 'Model/OrderData';

import {
  openPopup,
  placeOrder,
  closePopup,
  subscribeOrder,
} from './actions';

import positionsReducer from './containers/Positions/reducer';

import { doLogout } from '../LogoutPage/actions';

const placeOrderMap = new Map({
  direction: null,
  opened: false,
  payload: new Map(),
  tradingDetails: null,
  loading: false,
});

const popupsState = new Map({
  placeOrder: placeOrderMap,
  ticketOrder: placeOrderMap,
});

const popupsReducer = createReducer({
  [openPopup]: (state, { key, payload }) => state.set(key, fromJS(payload)),
  [closePopup]: (state, { key }) => state.set(key, new Map({ opened: false })),
  [placeOrder]: (state) => state.setIn(['placeOrder', 'loading'], true),
  [placeOrder.success]: (state) =>
    state
      .set('placeOrder', placeOrderMap),
  [subscribeOrder.success]: (state, payload) => state.setIn(['placeOrder', 'tradingDetails'], new OrderData(payload)),
}, popupsState);


const combined = combineReducers({
  positions: positionsReducer,
  popups: popupsReducer,
});


export default (state, action) => {
  if (action.type === doLogout.success.toString()) {
    return combined(undefined, action);
  }
  return combined(state, action);
};
