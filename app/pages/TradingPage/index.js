/*
 *
 * TradingPage
 *
 */

import React from 'react';
import ImmutableProptypes from 'react-immutable-proptypes';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { createStructuredSelector } from 'reselect';
import WithInjection from 'unity-frontend-core/containers/WithInjection';

// import LoadingIndicator from 'Components/LoadingIndicator';
import Helmet from 'Components/Helmet';

import TradeTicketPopup from './containers/Popups/Ticket';
import OrderRequestPopup from './containers/Popups/OrderRequest';
import Dashboard from './containers/Dashboard';

import messages from './messages';

import { makeSelectPopups } from './selectors';
import {
  openPopup,
  closePopup,
  destroyPage,
} from './actions';

import { loadConfig } from './containers/Dashboard/actions';

import './style.css';

import reducer from './reducer';
import sagas from './sagas';

export class TradingPage extends React.PureComponent {
  componentDidMount() {
    this.props.loadConfig();
  }

  componentWillUnmount() {
    this.props.destroyPage();
  }

  removePopup = (popupKey) => {
    this.props.closePopup(popupKey);
  }

  removeOrderPopup = () => this.removePopup('placeOrder')


  render() {
    const { popups } = this.props;

    return ([
      <Helmet key="tradingPageHelmet" pageTitle={messages.pageTitle} pageDescription={messages.pageDescription} />,
      <Dashboard key="tradingPageDashboard" />,
      popups.getIn(['placeOrder', 'opened'])
      && <TradeTicketPopup
        key="tradingPageOrdeTicket"
        onRemove={this.removeOrderPopup}
        initialValues={popups.getIn(['placeOrder', 'payload'])}
        direction={popups.getIn(['placeOrder', 'direction'])}
        loading={popups.getIn(['placeOrder', 'loading'])}
      />,
      <OrderRequestPopup
        key="tradingPageOrderRequestTicket"
      />,
    ]);
  }
}


TradingPage.propTypes = {
  popups: ImmutableProptypes.map,

  loadConfig: PropTypes.func.isRequired,
  closePopup: PropTypes.func.isRequired,
  destroyPage: PropTypes.func.isRequired,
};


const mapStateToProps = createStructuredSelector({
  popups: makeSelectPopups(),
});


function mapDispatchToProps(dispatch) {
  return {
    openPopup: (key) => dispatch(openPopup({ key, payload: { opened: true } })),
    closePopup: (key) => dispatch(closePopup({ key })),
    loadConfig: () => dispatch(loadConfig()),
    destroyPage: () => dispatch(destroyPage()),
  };
}

const Connected = connect(mapStateToProps, mapDispatchToProps)(TradingPage);

export default WithInjection({ reducer, sagas, key: 'tradingPage' })(Connected);
