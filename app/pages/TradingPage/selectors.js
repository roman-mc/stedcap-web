import { createSelector } from 'reselect';
/**
 * Direct selector to the tradingPage state domain
 */
const selectTradingPageDomain = () => (state) => state.get('tradingPage');

/**
 * Other specific selectors
 */

export const makeSelectTradingPage = () => createSelector(
  selectTradingPageDomain(),
  (substate) => substate
);

export const makeSelectConfig = () => (state, props) => props;

export const makeSelectPopups = () => createSelector(
  makeSelectTradingPage(),
  (substate) => substate.get('popups'),
);

export const makeSelectOrders = () => createSelector(
  makeSelectTradingPage(),
  (substate) => substate.getIn(['positions', 'orders']),
);

export default makeSelectTradingPage;
