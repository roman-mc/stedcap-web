/*
 * TradingPage Messages
 *
 * This contains all the text for the TradingPage component.
 */
import { defineMessages } from 'react-intl';

export default defineMessages({
  header: {
    id: 'app.containers.TradingPage.header',
    defaultMessage: 'This is TradingPage container !',
  },
  bid: {
    id: 'app.containers.TradingPage.bid',
    defaultMessage: 'Bid',
  },
  ask: {
    id: 'app.containers.TradingPage.ask',
    defaultMessage: 'Ask',
  },
  spread: {
    id: 'app.containers.TradingPage.spread',
    defaultMessage: 'Spread',
  },
  netChange: {
    id: 'app.containers.TradingPage.netChange',
    defaultMessage: 'Net change',
  },
  percChange: {
    id: 'app.containers.TradingPage.percChange',
    defaultMessage: '% change',
  },
  low: {
    id: 'app.containers.TradingPage.low',
    defaultMessage: 'Low',
  },
  high: {
    id: 'app.containers.TradingPage.high',
    defaultMessage: 'High',
  },
  prevClose: {
    id: 'app.containers.TradingPage.prevClose',
    defaultMessage: 'Previous close',
  },


  pageTitle: {
    id: 'app.containers.TradingPage.pageTitle',
    defaultMessage: 'Trading',
  },
  pageDescription: {
    id: 'app.containers.TradingPage.pageDescription',
    defaultMessage: 'Trading page',
  },

});
