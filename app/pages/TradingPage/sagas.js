// import { LOCATION_CHANGE } from 'react-router-redux';
import { call, fork, put, select } from 'redux-saga/effects';
import { delay } from 'redux-saga';
import Raven from 'raven-js';

import { generateListener } from 'Utils/sagaUtils';
import { USER_STATUS } from 'Model/enums';
import { actions } from 'Containers/Modal';

import * as ordersApi from 'unity-frontend-core/api/ordersApi';


import positionsSagas from './containers/Positions/sagas';

import { openPopup, openOrderPopup, placeOrder, closePopup, openOrderRequestPopup } from './actions';

import { loadOrders } from './containers/Positions/actions';


import { makeSelectCurrentUser } from '../../containers/App/selectors';

function* placeOrderHandler({ payload }) {
  try {
    const result = yield call(payload.id ? ordersApi.editOrder : ordersApi.createOrder, payload);
    yield put(placeOrder.success(result));
    yield put(closePopup({ key: 'placeOrder' }));
    yield call(delay, 500);
    yield put(loadOrders());
  } catch (e) {
    yield put(placeOrder.error(e));
    Raven.captureException(e);
  }
}

function* orderPopupHandler({ payload }) {
  const user = yield select(makeSelectCurrentUser());

  if (user.status === USER_STATUS.new) {
    yield put(openOrderPopup.errror('Account not verified!'));
    Raven.captureException(new Error('Not verified user!'));
  } else {
    yield put(openPopup({ key: 'placeOrder', payload: { payload, opened: true } }));
  }
}

function* orderRequestPopupHandler({ payload }) {
  const user = yield select(makeSelectCurrentUser());

  if (user.status === USER_STATUS.new) {
    yield put(openOrderPopup.errror('Account not verified!'));
    Raven.captureException(new Error('Not verified user!'));
  } else {
    yield put(actions.openModal({ modalId: 'tradingPageOrderRequestTicket', payload }));
  }
}

/**
 * Root saga manages watcher lifecycle
 */
export function* runAllSagas() {
  yield [
    ...positionsSagas,
    generateListener(openOrderPopup, orderPopupHandler),
    generateListener(openOrderRequestPopup, orderRequestPopupHandler),
    generateListener(placeOrder, placeOrderHandler),
  ].map(fork);
}


export default [
  runAllSagas,
];
