import { fromJS } from 'immutable';

import Watchlist from 'Model/Watchlist';
import Dashboard from 'Model/Dashboard';

import tradingPageReducer from '../reducer';

const selectedWatchlist = new Watchlist({
  instruments: [
    { instrument: 1, amount: 10000 },
    { instrument: 2, amount: 10000 },
    { instrument: 3, amount: 10000 },
  ],
  id: -1,
  name: 'Default',
});

const watchlists = [
  {
    created: undefined, modified: undefined, profileId: undefined, id: 0, disabled: true, name: 'Standard watchlists', instruments: [],
  },
  {
    created: undefined,
    modified: undefined,
    profileId: undefined,
    disabled: false,
    instruments: [
      {
        instrument: 1,
        amount: 10000,
      },
      {
        instrument: 2,
        amount: 10000,
      },
      {
        instrument: 3,
        amount: 10000,
      },
    ],
    id: -1,
    name: 'Default',
  },
  {
    id: 0, disabled: true, name: 'Custom watchlists', instruments: [], created: undefined, modified: undefined, profileId: undefined,
  },
];

const compareCase = fromJS({
  positions: [],
  tradingTree: new Dashboard({}),
  overview: {
    currentInstrumentId: null,
    chartData: {},
    period: '1m',
  },
  watchlist: {
    instrumentsData: {},
    watchlists,
    selectedWatchlist,
    watchlistPopupOpened: false,
    watchlistToEdit: null,
  },
  popups: {
    placeOrder: {
      opened: false,
      direction: null,
      payload: {},
      tradingDetails: null,
    },
  },
});


describe('tradingPageReducer', () => {
  it('returns the initial state', () => {
    expect(tradingPageReducer(undefined, {}).toJS())
      .toEqual(compareCase.toJS());
  });
});
