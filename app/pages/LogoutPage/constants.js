/*
 *
 * LogoutPage constants
 *
 */

export const DO_LOGOUT = 'app/LogoutPage/DO_LOGOUT';
export const DO_LOGOUT_SUCCESS = 'app/LogoutPage/DO_LOGOUT_SUCCESS';
export const DO_LOGOUT_ERROR = 'app/LogoutPage/DO_LOGOUT_ERROR';
