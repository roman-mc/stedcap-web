/*
 *
 * LogoutPage
 *
 */

import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';

import WithInjection from 'unity-frontend-core/containers/WithInjection';


import { doLogout } from './actions';

import sagas from './sagas';


export class LogoutPage extends React.PureComponent { // eslint-disable-line react/prefer-stateless-function
  componentDidMount() {
    this.props.logout();
  }

  render() {
    return (
      <div>
      </div>
    );
  }
}

LogoutPage.propTypes = {
  logout: PropTypes.func.isRequired,
};


function mapDispatchToProps(dispatch) {
  return {
    logout: () => dispatch(doLogout()),
  };
}


const connected = connect(null, mapDispatchToProps)(LogoutPage);

export default WithInjection({ sagas })(connected);
