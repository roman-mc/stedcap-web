/*
 *
 * LogoutPage actions
 *
 */

import { createAction } from 'redux-act';

import {
  DO_LOGOUT,
  DO_LOGOUT_ERROR,
  DO_LOGOUT_SUCCESS,
} from './constants';

export const doLogout = createAction(DO_LOGOUT);
doLogout.success = createAction(DO_LOGOUT_SUCCESS);
doLogout.fulfill = createAction(`${DO_LOGOUT}_FULFILL`);
doLogout.error = createAction(DO_LOGOUT_ERROR);
