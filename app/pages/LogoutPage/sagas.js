import { takeEvery } from 'redux-saga';
import { call, put, fork, apply } from 'redux-saga/effects';
import { push } from 'react-router-redux';


import { lsRemoveConfig } from 'Api/lsConfigManipulationApi';
import { authError } from 'unity-frontend-core/actions';

import socketApi from 'Api/socketApi';


import { doLogout } from './actions';
import { userLoggedOut } from '../../containers/App/actions';

// import { logout } from '../../api/authorizationApi';

import { removeToken } from '../../api/authorizationApi';

// Individual exports for testing
export function* doLogoutHandler() {
  yield call(removeToken);
  yield call(lsRemoveConfig);
  yield apply(socketApi, socketApi.stopSocket);
  yield put(userLoggedOut());
  yield put(doLogout.success());
  yield put(push('/login'));
  // yield put(doLogout.fulfill());
}

export function* doLogoutSaga() {
  return yield takeEvery(doLogout.toString(), doLogoutHandler);
}

function* authErrorHandler() {
  yield apply(socketApi, socketApi.stopSocket);
}

export function* authErrorSaga() {
  return yield takeEvery(authError.toString(), authErrorHandler);
}

/**
 * Root saga manages watcher lifecycle
 */
export function* runAllSagas() {
  yield [
    doLogoutSaga,
    authErrorSaga,
  ].map(fork);

  // yield take(LOCATION_CHANGE);
  // yield watchers.map((w) => w.cancel());
}

export default [
  runAllSagas,
];
