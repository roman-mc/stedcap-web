/*
 *
 * LoginPage reducer
 *
 */

import { fromJS } from 'immutable';

import { doLogin, doLoginVerify } from './actions';
import { userLoggedOut } from '../../containers/App/actions';

const initialState = fromJS({
  passwordVerified: false,
  loginError: false,
  userLoggedIn: false,
  loading: false,
});

function loginPageReducer(state = initialState, { type }) {
  switch (type) {
    case doLogin.toString():
      return state
        .set('loading', true)
        .set('loginError', false);
    case doLogin.success.toString():
      return state
        .set('loading', false)
        .set('passwordVerified', true);
    case doLogin.error.toString():
      return state
        .set('loading', false)
        .set('loginError', true);
    case doLoginVerify.error.toString():
      return state
        .set('loading', false);
    case doLoginVerify.toString():
      return state
        .set('loading', true);
    case userLoggedOut.toString(): // Need for logout - login
      return initialState;
    default:
      return state;
  }
}

export default loginPageReducer;
