import { takeEvery } from 'redux-saga';
import { call, put, fork, select } from 'redux-saga/effects';
import { login, loginVerify } from 'unity-frontend-core/api/authApi';

import { doLogin, doLoginVerify } from './actions';
import { userLoggedIn, temporaryTokenReceived, userTokenReceived } from '../../containers/App/actions';
import { makeSelectTemporaryToken } from '../../containers/App/selectors';

import {
  setToken,
} from '../../api/authorizationApi';

export function* doLoginHandler({ payload }) {
  try {
    const result = yield call(login, payload);
    const isTwoFa = !result.userInfo;

    if (isTwoFa) {
      yield put(temporaryTokenReceived({ token: result.token }));
    } else {
      const { token, userInfo } = result;

      yield call(setToken, token); // set to sharedSettings
      yield put(userTokenReceived({ token }));
      yield put(userLoggedIn(userInfo));
      yield put(doLoginVerify.success(result));
    }

    yield put(doLogin.success(result));
  } catch (e) {
    yield put(doLogin.error(e));
  }
}

export function* doLoginSaga() {
  return yield takeEvery(doLogin.toString(), doLoginHandler);
}

export function* doLoginVerifyHandler({ payload: { code } }) {
  const tempToken = yield select(makeSelectTemporaryToken());
  try {
    const result = yield call(loginVerify, { code }, tempToken);
    const { token, userInfo } = result;

    yield call(setToken, token); // set to sharedSettings
    yield put(userTokenReceived({ token }));
    yield put(userLoggedIn(userInfo));
    yield put(doLoginVerify.success(result));
  } catch (e) {
    yield put(doLoginVerify.error(e));
  }
}

export function* doLoginVerifySaga() {
  return yield takeEvery(doLoginVerify.toString(), doLoginVerifyHandler);
}

/**
 * Root saga manages watcher lifecycle
 */
export function* runAllSagas() {
  yield [
    doLoginSaga,
    doLoginVerifySaga,
  ].map(fork);

  // yield take(LOCATION_CHANGE);
  // yield watchers.map((w) => w.cancel());
}

export default [
  runAllSagas,
];
