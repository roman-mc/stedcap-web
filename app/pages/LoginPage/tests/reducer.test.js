
import loginPageReducer from '../reducer';

describe('loginPageReducer', () => {
  it('returns the initial state', () => {
    expect(loginPageReducer(undefined, {}).toJS())
      .toEqual({
        loading: false, loginError: false, passwordVerified: false, userLoggedIn: false,
      });
  });
});
