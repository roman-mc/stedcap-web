/*
 * LoginPage Messages
 *
 * This contains all the text for the LoginPage component.
 */
import { defineMessages } from 'react-intl';

export default defineMessages({
  liveButton: {
    id: 'app.containers.LoginPage.liveButton',
    defaultMessage: 'Live',
  },
  demoButton: {
    id: 'app.containers.LoginPage.demoButton',
    defaultMessage: 'Demo',
  },
  emailAddress: {
    id: 'app.containers.LoginPage.emailAddress',
    defaultMessage: 'Email address',
  },
  subTitle: {
    id: 'app.containers.LoginPage.subTitle',
    defaultMessage: 'Account log in',
  },

  resetPassword: {
    id: 'app.containers.LoginPage.resetPassword',
    defaultMessage: 'I forgot password',
  },
  loginButton: {
    id: 'app.containers.LoginPage.loginButton',
    defaultMessage: 'Log in',
  },
  openLiveAccount: {
    id: 'app.containers.LoginPage.openLiveAccount',
    defaultMessage: 'Open account',
  },
  signUpButton: {
    id: 'app.containers.LoginPage.signUpButton',
    defaultMessage: 'Sign up for demo account',
  },
  countryCode: {
    id: 'app.containers.LoginPage.countryCode',
    defaultMessage: 'Country code',
  },
  phoneNumber: {
    id: 'app.containers.LoginPage.phoneNumber',
    defaultMessage: 'Phone number',
  },
  password: {
    id: 'app.containers.LoginPage.password',
    defaultMessage: 'Password',
  },
  sendCode: {
    id: 'app.containers.LoginPage.sendCode',
    defaultMessage: 'Send code',
  },
  verifyCode: {
    id: 'app.containers.LoginPage.verifyCode',
    defaultMessage: 'SMS code',
  },
  confirmButton: {
    id: 'app.containers.LoginPage.confirmButton',
    defaultMessage: 'Confirm',
  },
  loginError: {
    id: 'app.containers.LoginPage.loginError',
    defaultMessage: 'Invalid phone or password',
  },
  helloUser: {
    id: 'app.containers.LoginPage.helloUser',
    defaultMessage: 'Hello, {name}!',
  },
  notMe: {
    id: 'app.containers.LoginPage.notMe',
    defaultMessage: 'It\'s not me',
  },

  accountLogin: {
    id: 'app.containers.LoginPage.accountLogin',
    defaultMessage: 'ACCOUNT LOG IN',
  },

  verificationCode: {
    id: 'app.containers.LoginPage.verificationCode',
    defaultMessage: 'Enter verification code from SMS',
  },
});
