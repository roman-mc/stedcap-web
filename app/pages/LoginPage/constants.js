/*
 *
 * LoginPage constants
 *
 */

export const DO_LOGIN = 'app/LoginPage/DO_LOGIN';
export const DO_LOGIN_VERIFY = 'app/LoginPage/DO_LOGIN_VERIFY';
