/*
 *
 * LoginPage
 *
 */
import React from 'react';
import { push } from 'react-router-redux';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { parsePhone } from 'Utils/phoneUtils';
import { createStructuredSelector } from 'reselect';
import { FormattedMessage, injectIntl } from 'react-intl';

import WithInjection from 'unity-frontend-core/containers/WithInjection';

import { AuthForm, FormGroup, FooterLink } from 'Components/AuthForm';

// import User from 'unity-frontend-core/Models/User';
import { makeSelectToken, makeSelectCurrentUser } from 'Containers/App/selectors';
import { userReset } from 'Containers/App/actions';

import messages from './messages';
import makeSelectLoginPage from './selectors';

import { doLogin, doLoginVerify } from './actions';

import LoginForm from './components/LoginForm';
import VerificationForm from './components/VerificationForm';


import reducer from './reducer';
import sagas from './sagas';


export class LoginPage extends React.PureComponent {
  state = {
    mode: 'Live',
  };

  componentWillMount() {
    this.checkAccess(this.props.token);
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.token !== this.props.token) {
      this.checkAccess(nextProps.token);
    }
  }

  onChangeUser = () => {
    this.props.userReset();
  }

  getInitialValues(user) {
    if (!user || !user.phone) {
      return {};
    }
    return { phone: user.phone.formValue };
  }

  verifyPhone = (values) => {
    this.props.doLoginVerify(values.get('code'));
  }

  sendCode = (values) => {
    const { currentUser } = this.props;
    const { phone: formPhone, ...rest } = values.toJS();
    const phone = currentUser && currentUser.phone ? currentUser.phone : parsePhone(formPhone);
    this.props.doLogin({ ...rest, phone });
  }

  checkAccess(token) {
    const { goTo } = this.props;
    if (token) {
      goTo('/trading');
    }
  }

  switcherOnClick(mode) {
    this.setState({ mode });
  }

  renderVerificationForm() {
    return (
      <AuthForm>
        <VerificationForm onSubmit={this.verifyPhone} loading={this.props.loginPage.loading} />
      </AuthForm>
    );
  }

  renderLoginForm() {
    const {
      props: { loginPage: { loading }, currentUser },
      state: { mode },
    } = this;

    return (
      <AuthForm subtitle={messages.subTitle}>
        <LoginForm
          loading={loading}
          currentUser={currentUser}
          onSubmit={this.sendCode}
          onChangeUser={this.onChangeUser}
        >
          <FormGroup>
            <FooterLink to="/register">
              {mode === 'Live'
                ? <FormattedMessage {...messages.openLiveAccount} />
                : <FormattedMessage {...messages.signUpButton} />
              }
            </FooterLink>
            <FooterLink to="/resetpassword">
              <FormattedMessage {...messages.resetPassword} />
            </FooterLink>
          </FormGroup>
        </LoginForm>
      </AuthForm>
    );
  }

  render() {
    const { loginPage: { passwordVerified } } = this.props;
    return passwordVerified ? this.renderVerificationForm() : this.renderLoginForm();
  }
}

LoginPage.propTypes = {
  doLogin: PropTypes.func.isRequired,
  doLoginVerify: PropTypes.func.isRequired,
  currentUser: PropTypes.object,
  goTo: PropTypes.func.isRequired,
  userReset: PropTypes.func.isRequired,
  token: PropTypes.string,
  loginPage: PropTypes.shape({
    passwordVerified: PropTypes.bool.isRequired,
    loading: PropTypes.bool.isRequired,
  }),
};

const mapStateToProps = createStructuredSelector({
  token: makeSelectToken(),
  loginPage: makeSelectLoginPage(),
  currentUser: makeSelectCurrentUser(),
});

function mapDispatchToProps(dispatch) {
  return {
    doLogin: (credentials) => dispatch(doLogin(credentials)),
    doLoginVerify: (code) => dispatch(doLoginVerify({ code })),
    userReset: () => dispatch(userReset()),
    goTo: (url) => dispatch(push(url)),
  };
}

const connected = connect(mapStateToProps, mapDispatchToProps)(injectIntl(LoginPage));

export default WithInjection({ reducer, sagas, key: 'loginPage' })(connected);
