import React from 'react';
import PropTypes from 'prop-types';
import { injectIntl, intlShape, FormattedMessage } from 'react-intl';
import { reduxForm } from 'redux-form/immutable';

import { Button, FormGroup, PhoneInput, Input } from 'Components/AuthForm';
import { phoneValidate, passwordValidate } from 'Utils/form/validation';

import messages from '../messages';

import './style.css';

function LoginForm({
  reset, invalid, currentUser, children, handleSubmit, loading, intl: { formatMessage }, onChangeUser,
}) {
  return (
    <form onSubmit={handleSubmit} autoComplete="off" name="loginFormFull">
      {currentUser
      /* eslint-disable jsx-a11y/no-static-element-interactions */
        ? <div styleName="auto-login">
          <FormattedMessage {...messages.helloUser} values={{ name: currentUser.firstName }} />
          <span styleName="forgot" onClick={() => { reset(); onChangeUser(); }}>
            <FormattedMessage {...messages.notMe} />
          </span>
        </div>
      /* eslint-enable */
        : <FormGroup>
          <PhoneInput
            id="phone"
            name="phone"
            type="text"
            placeholder={formatMessage(messages.phoneNumber)}
            validate={[phoneValidate]}
          />
        </FormGroup>}

      <FormGroup>
        <Input
          id="password"
          name="password"
          type="password"
          placeholder={formatMessage(messages.password)}
          validate={[passwordValidate]}
        />
      </FormGroup>
      <FormGroup>
        <Button
          type="submit"
          className="open-account"
          disabled={loading || invalid}
          text={formatMessage(messages.loginButton)}
        />
      </FormGroup>
      {children}
    </form>
  );
}


LoginForm.propTypes = {
  loading: PropTypes.bool.isRequired,
  invalid: PropTypes.bool.isRequired,
  handleSubmit: PropTypes.func.isRequired,
  intl: intlShape,
  children: PropTypes.node,
  currentUser: PropTypes.object,
  onChangeUser: PropTypes.func,
  reset: PropTypes.func,
};


export default reduxForm({
  form: 'loginFormStep1',
})(injectIntl(LoginForm));
