import React from 'react';
import PropTypes from 'prop-types';
import { injectIntl, intlShape } from 'react-intl';
import { reduxForm } from 'redux-form/immutable';

import { Button, FormGroup, Input } from 'Components/AuthForm';

import messages from '../messages';

import './style.css';

function VerificationForm({ loading, handleSubmit, intl: { formatMessage } }) {
  return (
    <form onSubmit={handleSubmit} name="verificationForm">
      <h1 styleName="header">
        {formatMessage(messages.accountLogin)}
      </h1>
      <div styleName="text">
        {formatMessage(messages.verificationCode)}
      </div>
      <FormGroup>
        <Input
          id="code"
          name="code"
          type="text"
          placeholder={formatMessage(messages.verifyCode)}
        />
      </FormGroup>

      <FormGroup>
        <Button
          type="submit"
          disabled={loading}
          className="open-account"
          text={formatMessage(messages.confirmButton)}
        />
      </FormGroup>
    </form>
  );
}

VerificationForm.propTypes = {
  loading: PropTypes.bool.isRequired,
  handleSubmit: PropTypes.func.isRequired,
  intl: intlShape,
};

export default reduxForm({ form: 'loginFormStep2' })(injectIntl(VerificationForm));
