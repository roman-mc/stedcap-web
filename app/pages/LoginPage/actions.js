/*
 *
 * LoginPage actions
 *
 */
import createApiAction from 'unity-frontend-core/utils/createAction';

import {
  DO_LOGIN,
  DO_LOGIN_VERIFY,
} from './constants';

export const doLogin = createApiAction(DO_LOGIN);
export const doLoginVerify = createApiAction(DO_LOGIN_VERIFY);
