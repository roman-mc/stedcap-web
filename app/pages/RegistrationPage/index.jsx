import React from 'react';
import PropTypes from 'prop-types';
import ImmutableProptypes from 'react-immutable-proptypes';
import { push } from 'react-router-redux';
import { connect } from 'react-redux';
import { createStructuredSelector } from 'reselect';
import Helmet from 'react-helmet';
import { injectIntl, intlShape, FormattedMessage } from 'react-intl';

import WithInjection from 'unity-frontend-core/containers/WithInjection';


import { parsePhone } from 'Utils/phoneUtils';
import { AuthForm, FormGroup, FooterLink } from 'Components/AuthForm';

import { makeSelectToken } from 'Containers/App/selectors';

import makeSelectRegisterPage, { makeSelectRegistrationFormData } from './selectors';

import { doCodeSend, doCodeVerify, doRegisterFinish } from './actions';

import RegistrationFormStep1 from './components/RegistrationFormStep1';
import RegistrationFormStep2 from './components/RegistrationFormStep2';

import messages from './messages';


import reducer from './reducer';
import sagas from './sagas';


export class RegistrationPage extends React.PureComponent {
  state = {
    accountType: 'live',
  };

  componentWillMount() {
    this.checkAccess(this.props.token);
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.token !== this.props.token) {
      this.checkAccess(nextProps.token);
    }
  }

  onFormSubmit = (values) => {
    const { accountType } = this.state;
    const { firstName, lastName, email, code, password, profileType } = values.toJS();
    this.props.doRegisterFinish({ firstName, lastName, accountType, profileType, email, code, password });
  }

  checkAccess(token) {
    const { goTo } = this.props;
    if (token) {
      goTo('/trading');
    }
  }

  sendCode = () => {
    const { phone } = this.props.formData.toJS();
    this.props.doCodeSend({ phone: parsePhone(phone) });
  }

  codeVerify = (values) => {
    const { code } = values.toJS();
    this.props.doCodeVerify({ code });
  }

    /*
    changeMode = (accountType) => {
        this.setState({ accountType });
    }
    */

  render() {
    const { intl: { formatMessage }, registrationPage: { codeSent, codeSentSuccess, codeVerified, registrationRequestSent } } = this.props;

    return (
      <AuthForm subtitle={messages.subTitle}>
        <Helmet
          title={formatMessage(messages.pageTitle)}
          meta={[
                        { name: 'description', content: formatMessage(messages.pageDescription) },
          ]}
        />
        <div>
          {!codeVerified &&
            <RegistrationFormStep1
              onSubmit={this.codeVerify}
              onCodeSend={this.sendCode}
              codeSent={codeSent}
              codeSentSuccess={codeSentSuccess}
              codeVerified={codeVerified}
            />}
          {codeVerified &&
            <RegistrationFormStep2
              registrationRequestSent={registrationRequestSent}
              initialValues={{ profileType: 1 }}
              onSubmit={this.onFormSubmit}
            />}
          <br /><br />
          <FormGroup>
            <FooterLink to="/login"><FormattedMessage {...messages.logIn} /></FooterLink>
          </FormGroup>
        </div>
      </AuthForm>
    );
  }
}

RegistrationPage.propTypes = {
  doCodeSend: PropTypes.func.isRequired,
  doCodeVerify: PropTypes.func.isRequired,
  doRegisterFinish: PropTypes.func.isRequired,
  goTo: PropTypes.func.isRequired,

  token: PropTypes.string,
  registrationPage: PropTypes.object,
  formData: ImmutableProptypes.map,

  intl: intlShape,
};

const mapStateToProps = createStructuredSelector({
  token: makeSelectToken(),
  registrationPage: makeSelectRegisterPage(),
  formData: makeSelectRegistrationFormData(),
});

function mapDispatchToProps(dispatch) {
  return {
    doCodeSend: (credentials) => dispatch(doCodeSend(credentials)),
    doCodeVerify: (payload) => dispatch(doCodeVerify(payload)),
    doRegisterFinish: (payload) => dispatch(doRegisterFinish(payload)),
    goTo: (url) => dispatch(push(url)),
  };
}

const connected = connect(mapStateToProps, mapDispatchToProps)(injectIntl(RegistrationPage));

export default WithInjection({ reducer, sagas, key: 'registrationPage' })(connected);
