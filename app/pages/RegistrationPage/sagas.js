import { takeEvery } from 'redux-saga';
import { call, put, fork, select } from 'redux-saga/effects';
import { stopSubmit } from 'redux-form';
import Raven from 'raven-js';
import { register, registerVerify, registerFinish } from 'unity-frontend-core/api/authApi';
import { userLoggedIn, userTokenReceived, temporaryTokenReceived, verifyTokenReceived } from 'Containers/App/actions';
import { makeSelectTemporaryToken, makeSelectVerifyToken } from 'Containers/App/selectors';

import { doCodeSend, doCodeVerify, doRegisterFinish } from './actions';


export function* doRegisterHandler({ payload }) {
  try {
    const result = yield call(register, payload);
    yield put(temporaryTokenReceived({ token: result.token }));
    yield put(doCodeSend.success(result));
  } catch (e) {
    yield put(doCodeSend.error(e));
    yield put(stopSubmit('registrationForm', {})); // https://github.com/erikras/redux-form/issues/2604#issuecomment-282670843
    Raven.captureException(e);
  }
}

export function* doRegisterSaga() {
  return yield takeEvery(doCodeSend.toString(), doRegisterHandler);
}

export function* doRegisterVerifyHandler({ payload }) {
  const tempToken = yield select(makeSelectTemporaryToken());
  try {
    const result = yield call(registerVerify, payload, tempToken);
    const { token } = result;
    yield put(doCodeVerify.success(result));
    yield put(verifyTokenReceived({ token }));
  } catch (e) {
    yield put(doCodeVerify.error(e));
    Raven.captureException(e);
  }
}

export function* doRegisterVerifySaga() {
  return yield takeEvery(doCodeVerify.toString(), doRegisterVerifyHandler);
}

export function* doRegisterFinishHandler({ payload }) {
  const verifyToken = yield select(makeSelectVerifyToken());
  try {
    const result = yield call(registerFinish, payload, verifyToken);
    const { token, userInfo } = result;
    yield put(doRegisterFinish.success(result));
    yield put(userTokenReceived({ token }));
    yield put(userLoggedIn(userInfo));
  } catch (e) {
    yield put(doRegisterFinish.error(e));
    Raven.captureException(e);
  }
}

export function* doRegisterFinishSaga() {
  return yield takeEvery(doRegisterFinish.toString(), doRegisterFinishHandler);
}

/**
 * Root saga manages watcher lifecycle
 */
export function* runAllSagas() {
  yield [
    doRegisterSaga,
    doRegisterVerifySaga,
    doRegisterFinishSaga,
  ].map(fork);

  // yield take(LOCATION_CHANGE);
  // yield watchers.map((w) => w.cancel());
}

export default [
  runAllSagas,
];
