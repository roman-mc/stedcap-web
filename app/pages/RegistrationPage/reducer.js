/*
 *
 * LoginPage reducer
 *
 */

import { fromJS } from 'immutable';
import { createReducer } from 'redux-act';
import { userLoggedOut } from 'Containers/App/actions';


import { doCodeSend, doCodeVerify, doRegisterFinish } from './actions';

const initialState = fromJS({
  codeSent: false,
  codeSentSuccess: false,
  codeVerified: false,
  registrationRequestSent: false,
});

export default createReducer({
  [doCodeSend]: (state) => state.set('codeSent', true),
  [doCodeSend.success]: (state) => state.set('codeSentSuccess', true),
  [doCodeSend.error]: (state) =>
    state
      .set('codeSent', false)
      .set('codeSentSuccess', false),
  [doCodeVerify.error]: (state) =>
    state
      .set('codeSent', false)
      .set('codeSentSuccess', false)
      .set('codeVerified', false),
  [doCodeVerify.success]: (state) => state.set('codeVerified', true),
  [doRegisterFinish]: (state) => state.set('registrationRequestSent', true),
  [doRegisterFinish.error]: (state) => state.set('registrationRequestSent', false),
  [userLoggedOut]: () => initialState,
}, initialState);
