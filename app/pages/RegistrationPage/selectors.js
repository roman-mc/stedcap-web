import { createSelector } from 'reselect';
import { makeSelectForms } from 'Containers/App/selectors';
/**
 * Direct selector to the loginPage state domain
 */
const selectRegisterPageDomain = () => (state) => state.get('registrationPage');

/**
 * Other specific selectors
 */


/**
 * Default selector used by LoginPage
 */

const makeSelectRegisterPage = () => createSelector(
  selectRegisterPageDomain(),
  (substate) => substate.toJS()
);

const makeSelectRegistrationFormData = () => createSelector(
  makeSelectForms,
  (substate) => substate.getIn(['registrationForm', 'values'])
);

const makeSelectRegistrationFormStep2Data = () => createSelector(
  makeSelectForms,
  (substate) => substate.getIn(['registrationFormStep2', 'values'])
);

export default makeSelectRegisterPage;
export {
  selectRegisterPageDomain,
  makeSelectRegistrationFormData,
  makeSelectRegistrationFormStep2Data,
};
