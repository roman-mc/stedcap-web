/*
 *
 * RegisterPage constants
 *
 */

export const DO_CODE_SEND = 'app/RegisterPage/DO_CODE_SEND';

export const DO_CODE_VERIFY = 'app/RegisterPage/DO_CODE_VERIFY';

export const DO_REGISTER_FINISH = 'app/RegisterPage/DO_REGISTER_FINISH';
