/*
 * RegistrationPage Messages
 *
 * This contains all the text for the RegistrationPage component.
 */
import { defineMessages } from 'react-intl';

export default defineMessages({
  firstName: {
    id: 'app.containers.RegistrationPage.firstName',
    defaultMessage: 'First name',
  },
  legalName: {
    id: 'app.containers.RegistrationPage.legalName',
    defaultMessage: 'Legal name',
  },
  verifyCode: {
    id: 'app.containers.RegistrationPage.verifyCode',
    defaultMessage: 'SMS verification code',
  },
  lastName: {
    id: 'app.containers.RegistrationPage.lastName',
    defaultMessage: 'Second name',
  },
  emailAddress: {
    id: 'app.containers.RegistrationPage.emailAddress',
    defaultMessage: 'Email address',
  },
  verificationCode: {
    id: 'app.containers.RegistrationPage.verificationCode',
    defaultMessage: 'Verification code',
  },
  password: {
    id: 'app.containers.RegistrationPage.password',
    defaultMessage: 'Password',
  },
  countryCode: {
    id: 'app.containers.RegistrationPage.countryCode',
    defaultMessage: 'Country code',
  },
  phoneNumber: {
    id: 'app.containers.RegistrationPage.phoneNumber',
    defaultMessage: 'Mobile phone number',
  },
  subTitle: {
    id: 'app.containers.RegistrationPage.subTitle',
    defaultMessage: 'Open account',
  },

  loginButton: {
    id: 'app.containers.RegistrationPage.loginButton',
    defaultMessage: 'Login',
  },
  logIn: {
    id: 'app.containers.RegistrationPage.logIn',
    defaultMessage: 'Already have an account? Log in',
  },
  signUpButton: {
    id: 'app.containers.RegistrationPage.signUpButton',
    defaultMessage: 'Sign up for demo account',
  },
  sendCode: {
    id: 'app.containers.RegistrationPage.sendCode',
    defaultMessage: 'Send code',
  },
  openAccount: {
    id: 'app.containers.RegistrationPage.openAccount',
    defaultMessage: 'Open account',
  },
  enterCodeText: {
    id: 'app.containers.RegistrationPage.enterCodeText',
    defaultMessage: 'Enter code from SMS to confirm phone number',
  },
  demo: {
    id: 'app.containers.RegistrationPage.demo',
    defaultMessage: 'Demo',
  },
  live: {
    id: 'app.containers.RegistrationPage.live',
    defaultMessage: 'Live',
  },
  pageTitle: {
    id: 'app.containers.RegistrationPage.pageTitle',
    defaultMessage: 'Registration',
  },
  pageDescription: {
    id: 'app.containers.RegistrationPage.pageDescription',
    defaultMessage: 'Registration page',
  },
  person: {
    id: 'app.containers.RegistrationPage.person',
    defaultMessage: 'Individual',
  },
  corporate: {
    id: 'app.containers.RegistrationPage.corporate',
    defaultMessage: 'Corporate',
  },
  nextStep: {
    id: 'app.containers.RegistrationPage.nextStep',
    defaultMessage: 'Next step',
  },
});
