import React from 'react';
import PropTypes from 'prop-types';

import { reduxForm } from 'redux-form/immutable';
import { PhoneInput, FormGroup, Text, Button, Input } from 'Components/AuthForm';
import { injectIntl, intlShape } from 'react-intl';

import {
  phoneValidate,
  requireValidate,
} from 'Utils/form/validation';

import messages from '../messages';

function RegistrationFormStep1(props) {
  const { invalid, submitting, onCodeSend, codeSent, codeSentSuccess, intl: { formatMessage }, handleSubmit } = props;

  return (
    <form autoComplete="off" onSubmit={handleSubmit}>
      <FormGroup>
        <PhoneInput
          id="phone"
          name="phone"
          type="text"
          placeholder={formatMessage(messages.phoneNumber)}
          validate={[phoneValidate]}
        />
      </FormGroup>
      <Text text={formatMessage(messages.enterCodeText)} />
      <FormGroup>
        <Button
          disabled={codeSent || invalid}
          basis={50}
          type="button"
          className="send-code"
          text={formatMessage(messages.sendCode)}
          onClick={onCodeSend}
        />
        <Input
          id="code"
          name="code"
          type="text"
          placeholder={formatMessage(messages.verificationCode)}
          validate={codeSent ? [requireValidate] : null}
        />
      </FormGroup>
      <FormGroup>
        <Button
          type="submit"
          className="open-account"
          disabled={!codeSentSuccess || invalid || submitting}
          text={formatMessage(messages.nextStep)}
        />
      </FormGroup>
    </form>
  );
}

RegistrationFormStep1.propTypes = {
  onCodeSend: PropTypes.func.isRequired,
  codeSent: PropTypes.bool.isRequired,
  codeSentSuccess: PropTypes.bool.isRequired,
  intl: intlShape,
  handleSubmit: PropTypes.func.isRequired,
  invalid: PropTypes.bool.isRequired,
  submitting: PropTypes.bool.isRequired,
  // mode: PropTypes.oneOf(['live', 'demo']),
  // onModeChange: PropTypes.func.isRequired,
};

export default reduxForm({ form: 'registrationForm' })(injectIntl(RegistrationFormStep1));
