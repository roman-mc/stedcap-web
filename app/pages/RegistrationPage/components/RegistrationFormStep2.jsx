import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { createStructuredSelector } from 'reselect';
import { reduxForm } from 'redux-form/immutable';
import { injectIntl, intlShape } from 'react-intl';

import { FormGroup, Input, Password, Button } from 'Components/AuthForm';

import {
  passwordValidate,
  requireValidate,
  emailValidate,
} from 'Utils/form/validation';

import { PROFILE_TYPE } from '../../../model/enums';

import messages from '../messages';
import { makeSelectRegistrationFormStep2Data } from '../selectors';
import AccountTypeSwitcher from '../components/AccountTypeSwitcher';

function RegistrationFormStep2(props) {
  const { invalid, registrationRequestSent, intl: { formatMessage }, handleSubmit, formData } = props;
  const profileType = (formData && formData.get('profileType', PROFILE_TYPE.person)) || PROFILE_TYPE.person;
  return (
    <form autoComplete="off" onSubmit={handleSubmit}>
      <FormGroup>
        <AccountTypeSwitcher name="profileType" validate={[requireValidate]} />
      </FormGroup>
      <FormGroup>
        <Input
          id="firstName"
          name="firstName"
          type="text"
          placeholder={profileType === PROFILE_TYPE.person ? formatMessage(messages.firstName) : formatMessage(messages.legalName)}
          validate={[requireValidate]}
        />
        {Boolean(profileType === PROFILE_TYPE.person) &&
          <Input
            id="lastName"
            name="lastName"
            type="text"
            placeholder={formatMessage(messages.lastName)}
            validate={[requireValidate]}
          />}
      </FormGroup>
      <FormGroup>
        <Input
          id="email"
          name="email"
          type="text"
          placeholder={formatMessage(messages.emailAddress)}
          validate={[requireValidate, emailValidate]}
        />
      </FormGroup>
      <FormGroup>
        <Password
          id="password"
          name="password"
          placeholder={formatMessage(messages.password)}
          validate={[passwordValidate]}
        />
      </FormGroup>
      <FormGroup>
        <Button
          type="submit"
          className="open-account"
          disabled={invalid || registrationRequestSent}
          text={formatMessage(messages.openAccount)}
        />
      </FormGroup>
    </form>
  );
}

RegistrationFormStep2.propTypes = {
  intl: intlShape,
  handleSubmit: PropTypes.func.isRequired,
  invalid: PropTypes.bool.isRequired,
  registrationRequestSent: PropTypes.bool.isRequired,
  formData: PropTypes.any,
};

const mapStateToProps = createStructuredSelector({
  formData: makeSelectRegistrationFormStep2Data(),
});

function mapDispatchToProps() {
  return {
  };
}

const connected = connect(mapStateToProps, mapDispatchToProps)(injectIntl(RegistrationFormStep2));

export default reduxForm({ form: 'registrationFormStep2' })(connected);
