import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import { Field } from 'redux-form/immutable';
import { injectIntl, intlShape } from 'react-intl';

import { PROFILE_TYPE } from '../../../model/enums';
import styles from '../style.css';
import messages from '../messages';

class SwitcherRenderer extends PureComponent {
  onButtonClick = (val) => {
    const { input: { onChange } } = this.props;
    onChange(val);
  }

  render() {
    const { input: { value }, intl: { formatMessage } } = this.props;
    return (
      <div styleName="switcherButtonsWrapper">
        <button
          onClick={() => this.onButtonClick(PROFILE_TYPE.person)}
          type="button"
          styleName="switcherButton"
          className={value === PROFILE_TYPE.person && styles.switcherButtonActive}
        >
          {formatMessage(messages.person)}
        </button>
        <button
          onClick={() => this.onButtonClick(PROFILE_TYPE.corporate)}
          type="button"
          styleName="switcherButton"
          className={value === PROFILE_TYPE.corporate && styles.switcherButtonActive}
        >
          {formatMessage(messages.corporate)}
        </button>
      </div>

    );
  }
}

SwitcherRenderer.propTypes = {
  intl: intlShape,
  input: PropTypes.shape({
    value: PropTypes.any,
  }),
};

const SwitcherRendererIntl = injectIntl(SwitcherRenderer);


export default function AccountTypeSwitcher({ id, name }) {
  return (
    <Field
      id={id}
      name={name}
      component={SwitcherRendererIntl}
    />
  );
}


AccountTypeSwitcher.propTypes = {
  id: PropTypes.string,
  name: PropTypes.string,
};
