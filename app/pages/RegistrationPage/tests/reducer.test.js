
import loginPageReducer from '../reducer';

describe('loginPageReducer', () => {
  it('returns the initial state', () => {
    expect(loginPageReducer(undefined, {}).toJS())
      .toEqual({
        codeSent: false, codeSentSuccess: false, codeVerified: false, registrationRequestSent: false,
      });
  });
});
