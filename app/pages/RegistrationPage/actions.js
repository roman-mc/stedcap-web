/*
 *
 * LoginPage actions
 *
 */
import createApiAction from 'unity-frontend-core/utils/createAction';

import {
  DO_CODE_SEND,
  DO_CODE_VERIFY,
  DO_REGISTER_FINISH,
} from './constants';

export const doCodeSend = createApiAction(DO_CODE_SEND);
export const doCodeVerify = createApiAction(DO_CODE_VERIFY);
export const doRegisterFinish = createApiAction(DO_REGISTER_FINISH);
