/*
 *
 * News reducer
 *
 */

import { Map, List } from 'immutable';
import { createReducer } from 'redux-act';

import News from 'unity-frontend-core/Models/News';

import { loadNews } from './actions';

const initialState = new Map({
  loading: false,
  totalCount: 0,
  news: new List(),
});

export default createReducer({
  [loadNews]: (state) => state
    .set('loading', true),
  [loadNews.error]: (state) => state.set('loading', false),
  [loadNews.success]: (state, { news, totalCount }) => {
    const newsList = new List(news.map((n) => new News(n)));

    return state
      .set('totalCount', totalCount)
      .set('news', newsList)
      .set('loading', false);
  },
}, initialState);
