/*
 *
 * News actions
 *
 */
// import { createAction } from 'redux-act';
import createApiAction from 'unity-frontend-core/utils/createAction';

export const loadNews = createApiAction('app/containers/NewsPage/LOAD_NEWS');
export const stopNewsLoading = createApiAction('app/containers/NewsPage/STOP_NEWS_LOADING');
