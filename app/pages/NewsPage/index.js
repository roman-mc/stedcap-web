import { connect } from 'react-redux';
import React, { Fragment } from 'react';
import PropTypes from 'prop-types';
import { createStructuredSelector } from 'reselect';

import WithInjection from 'unity-frontend-core/containers/WithInjection';

// import Select from 'react-select';
import moment from 'moment/moment';

import Helmet from 'Components/Helmet';
import { DateInput } from 'Components/FormInputs/DateInput';
import ScrollBar from 'Components/Scrollbar';
import LoadingIndicator from 'Components/LoadingIndicator';
import ImmutableListView from 'Components/ImmutableListView';
import { Button } from 'Components/GUI';
import { NewsListItem, NewsListHeader, NewsDetails } from 'Components/News';
import { Window } from 'Components/Window';

import { loadNews, stopNewsLoading } from './actions';
import { selectNews, selectNewsFirst, selectNewsLoading, selectNewsTotalCount } from './selectors';
import messages from './messages';

import { } from './style.css';

import reducer from './reducer';
import sagas from './sagas';


/*
const newsFilterTypes = [
  {
    label: 'All news',
    value: 0,
  },
  {
    label: 'Not all news',
    value: 1,
  },
];
*/

export class News extends React.PureComponent {
  state = {
    filterFrom: moment().startOf('day').add(-3, 'days'),
    filterTo: moment().endOf('day'),
    currentItem: this.props.newsFirst,
    offset: 0,
    limit: 100,
  }

  componentDidMount() {
    const { filterFrom, filterTo } = this.state;
    this.props.loadNews({ timeFrom: filterFrom.valueOf(), timeTo: filterTo.valueOf() });
  }

  componentWillReceiveProps(nextProps) {
    if (!this.props.newsFirst && !this.state.currentItem && nextProps.newsFirst) {
      this.setCurrentItem(nextProps.newsFirst);
    }
  }

  componentWillUnmount() {
    this.props.stopNewsLoading();
  }

  setCurrentItem = (item) => {
    this.setState({ currentItem: item });
  }

  changeFilterFrom = (option) => {
    const { filterTo } = this.state;
    const newTimeFrom = option.startOf('day');

    this.props.loadNews({ timeFrom: newTimeFrom.valueOf(), timeTo: filterTo.valueOf() });
    this.setState({ filterFrom: newTimeFrom, offset: 0 });
    this.newsListScrollRef.scrollTop = 0;
  }

  changeFilterTo = (option) => {
    const { filterFrom } = this.state;
    const newTimeTo = option.endOf('day');

    this.props.loadNews({ timeFrom: filterFrom.valueOf(), timeTo: newTimeTo.valueOf() });
    this.setState({ filterTo: newTimeTo, offset: 0 });
    this.newsListScrollRef.scrollTop = 0;
  }

  nextPage = () => {
    const { offset, limit } = this.state;
    const newOffset = offset + limit;

    this.changePageHandler(newOffset);
  }

  prevPage = () => {
    const { offset, limit } = this.state;
    const newOffset = offset >= limit ? offset - limit : 0;

    this.changePageHandler(newOffset);
  }

  changePageHandler = (newOffset) => {
    const { filterFrom, filterTo } = this.state;

    this.props.loadNews({ offset: newOffset, timeFrom: filterFrom.valueOf(), timeTo: filterTo.valueOf() });
    this.setState({ offset: newOffset });
    this.newsListScrollRef.scrollTop = 0;
  }

  renderNoData = (text) => {
    return (
      <div styleName="news-dummy-wrapper">
        <div styleName="news-dummy">
          <div
            className="icon-icon-news"
            styleName="news-dummy-icon"
          />
          <div styleName="news-dummy-text">
            {text}
          </div>
        </div>
      </div>
    );
  }

  renderPager = () => {
    const { newsTotalCount } = this.props;
    const { offset, limit } = this.state;

    return (
      <div styleName="pager">
        <div>
          {offset >= limit && <Button onClick={this.prevPage}>Previous page</Button>}
        </div>
        <div>
          {newsTotalCount >= (offset + limit) && <Button onClick={this.nextPage}>Next page</Button>}
        </div>
      </div>
    );
  }


  renderFilters = () => {
    const { filterTo, filterFrom } = this.state;

    return (
      <div styleName="filters">
        {/*
        <div styleName="filter">
          <div styleName="filter-label">Show</div>
          <div styleName="filter-input">
            <Select
              value={filterType}
              options={newsFilterTypes}
              onChange={this.changeFilterType}
              styleName="filter-select"
              clearable={false}
              optionstyleName="filter-select-option"
            />
          </div>
        </div>
        */}
        <div styleName="filter">
          <div styleName="filter-input">
            <DateInput
              value={filterFrom}
              onChange={this.changeFilterFrom}
              label="From"
              maxDate={filterTo}
            />
          </div>
        </div>
        <div styleName="filter">
          <div styleName="filter-input">
            <DateInput
              value={filterTo}
              onChange={this.changeFilterTo}
              label="To"
              minDate={filterFrom}
              maxDate={moment()}
            />
          </div>
        </div>
      </div>
    );
  }

  render() {
    const { news, newsLoading } = this.props;
    const { currentItem } = this.state;

    return (
      <div className="page-container">
        <Helmet pageTitle={messages.pageTitle} pageDescription={messages.pageDescription} />
        <Window headerHidden fullHeight fullWidth>
          <div styleName="news-panels">
            <div styleName="news-list-wrapper">
              {newsLoading && <div styleName="loader"><LoadingIndicator loading /></div>}
              {this.renderFilters()}
              <div styleName="news-list-scroll-wrap">
                <div styleName="news-list">
                  <ScrollBar style={{ position: 'static' }} containerRef={(ref) => this.newsListScrollRef = ref}>
                    <ImmutableListView
                      immutableData={news}
                      renderSection={(section) => <NewsListHeader title={section} />}
                      renderRow={(item, i) =>
                        <NewsListItem
                          key={`news_list_${i}_${item.id}`}
                          item={item}
                          selected={currentItem && item.id === currentItem.id}
                          onClick={this.setCurrentItem}
                        />
                      }
                    />
                    <div styleName="fadeout-scroll-spacer" />
                  </ScrollBar>
                </div>
                {Boolean(news.size)
                  && <Fragment>
                    <div styleName="fadeout" />
                    {this.renderPager()}
                  </Fragment>
                }
                {!news.size && this.renderNoData('There are no news')}
              </div>
            </div>
            <div styleName="news-full-wrapper">
              {currentItem && <NewsDetails item={currentItem} />}
            </div>
          </div>
        </Window>
      </div>
    );
  }
}

News.propTypes = {
  news: PropTypes.object.isRequired,
  newsFirst: PropTypes.object,
  newsTotalCount: PropTypes.number.isRequired,
  newsLoading: PropTypes.bool,
  loadNews: PropTypes.func.isRequired,
  stopNewsLoading: PropTypes.func.isRequired,
};

const mapStateToProps = createStructuredSelector({
  news: selectNews(),
  newsFirst: selectNewsFirst(),
  newsTotalCount: selectNewsTotalCount(),
  newsLoading: selectNewsLoading(),
});


const mapDispatchToProps = (dispatch) => ({
  loadNews: (params = {}) => dispatch(loadNews(params)),
  stopNewsLoading: () => dispatch(stopNewsLoading()),
});

const Connected = connect(mapStateToProps, mapDispatchToProps)(News);

export default WithInjection({ reducer, key: 'newsPage', sagas })(Connected);
