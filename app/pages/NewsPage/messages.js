/*
 * News Messages
 *
 * This contains all the text for the Accounts component.
 */
import { defineMessages } from 'react-intl';

export default defineMessages({
  pageTitle: {
    id: 'app.containers.NewsPage.pageTitle',
    defaultMessage: 'News',
  },
  pageDescription: {
    id: 'app.containers.NewsPage.pageDescription',
    defaultMessage: 'News overview',
  },
});
