import { delay } from 'redux-saga';
import { put, call, take, race } from 'redux-saga/effects';

import { apiError } from 'unity-frontend-core/actions';
import * as newsApi from 'unity-frontend-core/api/newsApi';

import { generateListener } from 'Utils/sagaUtils';

import { loadNews, stopNewsLoading } from './actions';

function* loadNewsHandler({ payload }) {
  try {
    while (true) { // eslint-disable-line no-constant-condition
      yield call(loadNewsRequest, { payload });
      const { timeout } = yield race({
        timeout: call(delay, 15 * 1000),
        changeFilters: take(loadNews.toString()),
        stopLoading: take(stopNewsLoading.toString()),
      });

      if (!timeout) {
        throw new Error('stop news reloading');
      }
    }
  } catch (e) {
    yield put(loadNews.error(e));
  }
}

function* loadNewsRequest({ payload: { currencyPair = [], limit = 100, offset = 0, timeFrom = null, timeTo = null } }) {
  try {
    const res = yield call(newsApi.getNews, { currencyPair, limit, offset, timeFrom, timeTo });
    yield put(loadNews.success(res));
  } catch (e) {
    yield put(loadNews.error(e));
    yield put(apiError(e));
  }
}


/**
 * Root saga manages watcher lifecycle
 */


export default [
  generateListener(loadNews, loadNewsHandler),
];
