import { createSelector } from 'reselect';
import { List } from 'immutable';
import moment from 'moment';

import { STANDARD_DATE } from 'unity-frontend-core/utils/dateUtils';

const selectNewsDomain = () => (state) => state.get('newsPage');

export const selectNews = () => createSelector(
  selectNewsDomain(),
  (substate) => substate.get('news', new List()).sortBy((n) => -n.time).groupBy((n) => moment(n.time).format(STANDARD_DATE))
);

export const selectNewsFirst = () => createSelector(
  selectNewsDomain(),
  (substate) => substate.get('news', new List()).first()
);

export const selectNewsTotalCount = () => createSelector(
  selectNewsDomain(),
  (substate) => substate.get('totalCount', 0)
);

export const selectNewsLoading = () => createSelector(
  selectNewsDomain(),
  (substate) => substate.get('loading', false)
);

export { selectNewsDomain };
