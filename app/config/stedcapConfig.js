import logo from 'Img/sec/sec-logo-dark-theme@1x.png';
import logoLarge from 'Img/sec/sec-logo-large-dark-theme@1x.png';


export default {
  logo,
  logoLarge,
  companyName: 'stone edge capital',
  projectTitle: 'Stone Edge Trader',
};
