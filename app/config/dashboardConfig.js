import uuid from 'uuid/v4';

import { TIMEFRAME_OPTIONS } from 'unity-frontend-core/Models/Enums';

export const VERSION = 16;

export const DEFAULT_POSITION = {
  component: 'positions',
  config: {
    mode: 'positions',
  },
  position: {
    x: 0,
    y: 50.5,
    width: 66,
    height: 49.5,
    zIndex: 1,
  },
};

export const DEFAULT_OVERVIEW = {
  component: 'overview',
  config: {
    timeframe: TIMEFRAME_OPTIONS[3].value,
  },
  position: {
    x: 50.5,
    y: 0,
    width: 49.5,
    height: 49.5,
    zIndex: 2,
  },
};

export const DEFAULT_MARKETDEPTH = {
  component: 'marketDepth',
  config: {
  },
  position: {
    x: 35.5,
    y: 20,
    width: 25,
    height: 35,
    zIndex: 4,
  },
};

export const DEFAULT_WATCHLIST = {
  component: 'watchlist',
  config: {
    currentBookmark: 'tradingboard',
  },
  position: {
    x: 0,
    y: 0,
    width: 49.5,
    height: 49.5,
    zIndex: 0,
  },
};

export const DEFAULT_NEWS = {
  component: 'news',
  config: {
  },
  position: {
    x: 67,
    y: 50.5,
    width: 33,
    height: 49.5,
    zIndex: 3,
  },
};

export const DEFAULT_CHAT = {
  component: 'chat',
  config: {
  },
  position: {
    x: 67,
    y: 50.5,
    width: 33,
    height: 49.5,
    zIndex: 3,
  },
};

export const DEFAULT_TICKET = {
  component: 'ticket',
  config: {
  },
  position: {
    x: 67,
    y: 50.5,
    width: 25,
    height: 49.5,
    zIndex: 5,
  },
};

export const defaultPanel = {
  config: { name: 'Trading Board' },
  widgets: {
    [uuid()]: { ...DEFAULT_WATCHLIST },
    [uuid()]: { ...DEFAULT_POSITION },
    [uuid()]: { ...DEFAULT_OVERVIEW },
    [uuid()]: { ...DEFAULT_NEWS },
  },
};

export default {
  version: VERSION,
  config: {
    activePanelId: 0,
  },
  panels: [
    defaultPanel,
  ],
};
