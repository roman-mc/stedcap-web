import logo from 'Img/unity/unity-logo-2-dark-theme@2x.png';
import logoLarge from 'Img/unity/unity-logo-3-dark-theme@2x.png';

export default {
  logo,
  logoLarge,
  companyName: 'unity',
  projectTitle: 'Unity Finance',
};
