import unityConfig from './unityConfig';
import stedcapConfig from './stedcapConfig';

const configs = {
  unity: unityConfig,
  sec: stedcapConfig,
};

export default configs[process.env.APP_ENV] || configs.unity;
