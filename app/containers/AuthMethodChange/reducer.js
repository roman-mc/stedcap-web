import { Map } from 'immutable';
import { createReducer } from 'redux-act';

import {
  openAuthMethodChangeModal, closeAuthMethodChangeModal,
  requestChangeAuthMethod,
  acceptChangeAuthMethod,
} from './actions';

const initialState = new Map({
  opened: false,

  // If There is token then show open input for sms code
  //      AND show send code again with timer(30 sec)
  token: undefined, // This is temp token used for 'acceptChangeAuthMethod'
  isLoading: false,
  // /**
  //  * @type {Map<authMethodId: number, token: string>}
  //  */
  tokensForAuthMethods: new Map(),
});

export default createReducer({
  [openAuthMethodChangeModal]: (state) => state.set('opened', true),
  [closeAuthMethodChangeModal]: () => initialState,


  [requestChangeAuthMethod]: (state) => state.set('isLoading', true),
  [requestChangeAuthMethod.success]: (state, payload) =>
    state
      .set('isLoading', false)
      .set('token', payload.token)
      .update('tokensForAuthMethods', (tokensForAuthMethods) => tokensForAuthMethods.set(payload.authMethodId, payload.token)),
  [requestChangeAuthMethod.error]: (state) => state.set('isLoading', false),


  [acceptChangeAuthMethod]: (state) => state.set('isLoading', true),
  [acceptChangeAuthMethod.success]: () =>
    initialState,

  [acceptChangeAuthMethod.error]: (state) =>
    state.set('isLoading', false),
}, initialState);
