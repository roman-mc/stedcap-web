import { call, put, select } from 'redux-saga/effects';

import * as userApi from 'unity-frontend-core/api/authApi';
import {
  requestChangeAuthMethod, acceptChangeAuthMethod,
} from './actions';
import { selectToken } from './selectors';

import { userDataReceived } from '../App/actions';
import { makeSelectCurrentUser } from '../App/selectors';
import { generateListener } from '../../utils/sagaUtils';

function* requestChangeAuthMethodSaga({ payload: authMethodId }) {
  const { phone } = yield select(makeSelectCurrentUser());

  try {
    const response = yield call(userApi.requestChangeAuthMethod, { phone, authMethodId });
    yield put(requestChangeAuthMethod.success({ token: response.token, authMethodId }));
  } catch (e) {
    yield put(requestChangeAuthMethod.error());
    console.warn(e);
  }
}

function* acceptChangeAuthMethodSaga({ payload: code }) {
  const token = yield select(selectToken);

  try {
    const response = yield call(userApi.acceptChangeAuthMethod, { confirmation: code }, token);
    yield put(acceptChangeAuthMethod.success());
    yield put(userDataReceived({ user: response }));
  } catch (e) {
    yield put(acceptChangeAuthMethod.error());
    console.warn(e);
  }
}

export default [
  generateListener(requestChangeAuthMethod, requestChangeAuthMethodSaga),
  generateListener(acceptChangeAuthMethod, acceptChangeAuthMethodSaga),
];
