import { createAction } from 'redux-act';
import createApiAction from 'unity-frontend-core/utils/createAction';

export const openAuthMethodChangeModal = createAction('app/containers/AuthMethodChange/OPEN_AUTH_METHOD_CHANGE_MODAL');
export const closeAuthMethodChangeModal = createAction('app/containers/AuthMethodChange/CLOSE_AUTH_METHOD_CHANGE_MODAL');

export const requestChangeAuthMethod = createApiAction('app/containers/AuthMethodChange/REQUEST_SMS_CHANGE');
export const acceptChangeAuthMethod = createApiAction('app/containers/AuthMethodChange/ACCEPT_SMS_CHANGE');
