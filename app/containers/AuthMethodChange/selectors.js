import { Map } from 'immutable';
import { createSelector } from 'reselect';
import { makeSelectForms } from '../App/selectors';

import { CONTAINER_DOMAIN } from './constants';

export const makeSelectSettingsState = () =>
  (state) => state.get(CONTAINER_DOMAIN);


export const selectOpened = createSelector(
  makeSelectSettingsState(),
  (settings) => settings.get('opened'),
);

export const selectLoading = createSelector(
  makeSelectSettingsState(),
  (container) => container.get('isLoading'),
);

export const selectToken = createSelector(
  makeSelectSettingsState(),
  (container) => container.get('token'),
);

export const makeSelectFormValues = () => createSelector(
  makeSelectForms,
  (forms) => forms.getIn(['authMethodChangeForm', 'values'], new Map()),
);

export const selectAuthMethodsForTokens = createSelector(
  makeSelectSettingsState(),
  (container) => container.get('tokensForAuthMethods')
);
