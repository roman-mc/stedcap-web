import React from 'react';
import PropTypes from 'prop-types';
import { reduxForm } from 'redux-form/immutable';

import { requireValidate } from 'Utils/form/validation';
import { AUTH_METHODS_OPTIONS } from 'unity-frontend-core/Models/Enums';


import styles from './styles.css';

import LinkButton from '../../components/ULinkButton';

import {
  InlineSelect,
  Button,
  Input,
} from '../../components/ReduxFields';

class AuthMethodChangeForm extends React.PureComponent {
  render() {
    const {
      isLoading,
      handleSubmit,
      requestChangeAuthMethod,
      close,
      invalid,
      formValues,
      initialValues,
      canSubmit,
    } = this.props;

    // Can not request change from old value to old value
    const canRequestChangeAuthMethod = formValues.get('authMethodId') !== initialValues.get('authMethodId');

    return (
      <form
        className={styles.formContainer}
        onSubmit={handleSubmit}
      >
        <div
          className={styles.formContainerTop}
        >
          <div className={styles.selectorContainer}>
            <InlineSelect
              name="authMethodId"
              options={AUTH_METHODS_OPTIONS}
              required
              text="Auth. method"
              validate={[requireValidate]}
              disabled={isLoading}
              disableClearable
            />
          </div>
          <div
            className={styles.sendSmsContainer}
          >
            <LinkButton
              onClick={requestChangeAuthMethod}
              disabled={!canRequestChangeAuthMethod || isLoading}
            >
              Send sms code
            </LinkButton>
            {canSubmit
              && (
                <div
                  className={styles.sendSmsButton}
                >
                  <Input validate={[requireValidate]} name="code" text="Sms code" />
                </div>
              )
            }
          </div>
        </div>
        <div className={styles.footer}>
          <Button big styleType="secondary" subClass={styles.btn} onClick={close}>
            CANCEL
          </Button>
          <Button big styleType="tertiary" type="submit" subClass={styles.btn} disabled={(isLoading || invalid) || !canSubmit}>
            SUBMIT
          </Button>
        </div>
      </form>
    );
  }
}

AuthMethodChangeForm.propTypes = {
  isLoading: PropTypes.bool.isRequired,
  invalid: PropTypes.bool.isRequired,
  canSubmit: PropTypes.bool.isRequired,

  // TODO: proper prop types
  initialValues: PropTypes.object,
  formValues: PropTypes.object,

  handleSubmit: PropTypes.func.isRequired,
  close: PropTypes.func.isRequired,
  requestChangeAuthMethod: PropTypes.func.isRequired,
};

export default reduxForm({ form: 'authMethodChangeForm' })(AuthMethodChangeForm);
