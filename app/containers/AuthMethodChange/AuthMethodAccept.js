import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { createStructuredSelector } from 'reselect';
import { Map } from 'immutable';
import ImmutableProptypes from 'react-immutable-proptypes';
import WithInjection from 'unity-frontend-core/containers/WithInjection';

import AuthMethodChangeForm from './AuthMethodChangeForm';
import {
  closeAuthMethodChangeModal,
  requestChangeAuthMethod, acceptChangeAuthMethod,
} from './actions';
import {
  selectOpened,
  selectLoading,
  selectToken,
  selectAuthMethodsForTokens,
  makeSelectFormValues,
} from './selectors';
import { CONTAINER_DOMAIN } from './constants';

import reducer from './reducer';
import sagas from './sagas';

import { makeSelectSettings } from '../ProfileSettings/selectors';
import Popup from '../../components/Popup';

const POP_UP_KEY = 'requestChangeAuthMethod';

class ProfileSettings extends React.PureComponent {
  close = () => {
    this.props.close();
  }

  requestChangeAuthMethod = () => {
    this.props.requestChangeAuthMethod(this.props.formValues.get('authMethodId'));
  }

  acceptChangeAuthMethod = (values) => {
    // BUG: if more than 2 methods will be, token and authMethodId will mismatch!
    // TODO: refactor me! pass in token for other 2fa methods
    this.props.acceptChangeAuthMethod(values.get('code'));
  }

  render() {
    const {
      opened,
      isLoading,
      settings,
      tokensForAuthMethods,
      formValues,
    } = this.props;

    if (!opened) {
      return null;
    }

    const canSubmit = Boolean(tokensForAuthMethods.get(formValues.get('authMethodId')));

    return (
      <Popup
        popupKey={POP_UP_KEY}
        key={POP_UP_KEY}
        header="Change authentication method"
        removePopup={this.close}
        backgroundTransparent
      >
        <AuthMethodChangeForm
          initialValues={new Map({ authMethodId: settings.authMethod })}
          requestChangeAuthMethod={this.requestChangeAuthMethod}
          close={this.close}
          onSubmit={this.acceptChangeAuthMethod}
          isLoading={isLoading}
          formValues={formValues}
          canSubmit={canSubmit}
        />
      </Popup>
    );
  }
}

ProfileSettings.propTypes = {
  opened: PropTypes.bool.isRequired,
  isLoading: PropTypes.bool.isRequired,
  settings: PropTypes.object,
  formValues: PropTypes.object,
  tokensForAuthMethods: ImmutableProptypes.map,

  close: PropTypes.func.isRequired,
  requestChangeAuthMethod: PropTypes.func.isRequired,
  acceptChangeAuthMethod: PropTypes.func.isRequired,
};

const mapStateToProps = createStructuredSelector({
  opened: selectOpened,
  isLoading: selectLoading,
  token: selectToken,
  formValues: makeSelectFormValues(),

  tokensForAuthMethods: selectAuthMethodsForTokens,
  settings: makeSelectSettings(),
});

const mapDispatchToProps = (dispatch) => ({
  close: () => dispatch(closeAuthMethodChangeModal()),
  requestChangeAuthMethod: (authMethodId) => dispatch(requestChangeAuthMethod(authMethodId)),
  acceptChangeAuthMethod: (code) => dispatch(acceptChangeAuthMethod(code)),
});

const Connected = connect(mapStateToProps, mapDispatchToProps)(ProfileSettings);

export default WithInjection({ reducer, sagas, key: CONTAINER_DOMAIN })(Connected);
