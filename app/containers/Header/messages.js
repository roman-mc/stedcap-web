/*
 * LoginPage Messages
 *
 * This contains all the text for the LoginPage component.
 */
import { defineMessages } from 'react-intl';

export default defineMessages({
  trading: {
    id: 'app.components.Header.trading',
    defaultMessage: 'Trading',
  },
  news: {
    id: 'app.components.Header.news',
    defaultMessage: 'News',
  },
  store: {
    id: 'app.components.Header.store',
    defaultMessage: 'Store',
  },
  account: {
    id: 'app.components.Header.account',
    defaultMessage: 'Account',
  },
  finances: {
    id: 'app.components.Header.finances',
    defaultMessage: 'Finances',
  },
});
