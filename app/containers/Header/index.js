import React from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';
import { FormattedMessage } from 'react-intl';
import { createStructuredSelector } from 'reselect';

import NotificationsCenter from '../NotificationsCenter';
import HeaderProfileDescription from '../HeaderProfileDescription';
import messages from './messages';

import { changeTheme } from '../App/actions';
import { makeSelectTheme } from '../App/selectors';

import { actions } from '../Modal';

import './style.css';

const links = [
  {
    name: 'trading',
    href: '/trading',
    text: messages.trading,
  },
  {
    name: 'finances',
    href: '/finances',
    text: messages.finances,
  },
  {
    name: 'news',
    href: '/news',
    text: messages.news,
  },
  // {
  //     name: 'store',
  //     href: '/store',
  //     text: messages.store,
  // },
  // {
  //     name: 'account',
  //     href: '/account',
  //     text: messages.account,
  // },
];

const themeSwitches = {
  'light-theme': 'dark-theme',
  'dark-theme': 'light-theme',
};


function Header({
  theme, changeTheme: doChangeTheme, openSettings, activeLinkName,
}) {
  /* eslint-disable jsx-a11y/no-static-element-interactions */

  return (
    <header styleName="header">
      <div styleName="flex-left">
        <Link to="/">
          <div styleName="logo-img"></div>
        </Link>

        <nav styleName="navigation">
          {
            links.map(({ name, href, text }, i) =>
              (<Link
                key={`ll_${i + 1}`}
                to={href}
                styleName={`navigation-link ${activeLinkName.indexOf(href) > -1 ? '_active' : ''}`}
              >
                <span data-menu-item={name}>
                  <FormattedMessage {...text} />
                </span>
              </Link>))
          }
        </nav>
      </div>

      <div styleName="flex-right">
        <div styleName="profile">
          <div styleName="profile-icons">
            <div onClick={() => doChangeTheme(themeSwitches[theme])} styleName="profile-icons-icon">
              <i className="icon-icon-theme"></i>
            </div>
            <NotificationsCenter />
            <span onClick={openSettings} styleName="profile-icons-icon">
              <i className="icon-icon-settings-desktop"></i>
            </span>
          </div>
          <HeaderProfileDescription />
        </div>
      </div>
    </header>
  );
  /* eslint-enable */
}


Header.propTypes = {
  activeLinkName: PropTypes.string,
  changeTheme: PropTypes.func.isRequired,
  openSettings: PropTypes.func.isRequired,
  theme: PropTypes.string.isRequired,
};


const mapDispatchToProps = (dispatch) => ({
  changeTheme: (theme) => dispatch(changeTheme({ theme, save: true })),
  openSettings: () => dispatch(actions.openModal({ modalId: 'profile_settings_popup' })),
});

const mapStateToProps = createStructuredSelector({
  theme: makeSelectTheme(),
});


export default connect(mapStateToProps, mapDispatchToProps)(Header);
