/*
 * LoginPage Messages
 *
 * This contains all the text for the LoginPage component.
 */
import { defineMessages } from 'react-intl';

export default defineMessages({
  logout: {
    id: 'app.components.HeaderProfileDescription.logout',
    defaultMessage: 'Log out',
  },
  account: {
    id: 'app.components.HeaderProfileDescription.account',
    defaultMessage: 'Account #{id}',
  },
});
