/*
 *
 * HeaderProfileDescription
 *
 */

import React from 'react';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';
import PropTypes from 'prop-types';
import { createStructuredSelector } from 'reselect';
import { FormattedMessage } from 'react-intl';

import { makeSelectActiveAccountInfo } from 'unity-frontend-core/containers/SelectAccount/selectors';

import { makeSelectCurrentUser } from '../App/selectors';
import SelectAccountDropdown from '../SelectAccountPopup/SelectAccountDropdown';

import messages from './messages';

import './style.css';

export class HeaderProfileDescription extends React.PureComponent {
  state = {
    popupOpened: false,
  }

  componentDidMount() {
    window.addEventListener('mousedown', this.keyPressListener);
  }

  componentWillUnmount() {
    window.removeEventListener('mousedown', this.keyPressListener);
  }

  keyPressListener = () => this.setState({ popupOpened: false })

  togglePopup = (e) => {
    e.stopPropagation();
    const { popupOpened } = this.state;
    this.setState({ popupOpened: !popupOpened });
  }

  render() {
    const { popupOpened } = this.state;
    const { activeAccount, currentUser: { userPic } } = this.props;
    if (!activeAccount) {
      return null;
    }
    return (
      <div styleName="profile-account-details">
        <div styleName={`profile-account-details-info ${popupOpened ? 'active' : ''}`} onMouseDown={this.togglePopup}>
          {userPic && <div styleName="profile-account-details-img">
            <img alt="userpic" src={`/rest/public/file?storage_type=userpic&path=${userPic}`} />
          </div>}
          <div styleName="profile-account-details-info-fullname">
            {activeAccount.accountAccessName || <FormattedMessage {...messages.account} values={{ id: activeAccount.multiAccountId }} />}
            <i className="icon-icon-dropdown-arrow" styleName={`${popupOpened ? 'popup-opened' : ''}`}></i>
          </div>
        </div>
        <Link to="/logout" styleName="profile-account-details-logout">
          <span styleName="profile-account-details-info-logout">
            <span styleName="profile-account-details-info-logout-text">
              <FormattedMessage {...messages.logout} />
            </span>
            <i className="icon-icon-logout"></i>
          </span>
        </Link>

        {(popupOpened)
          && <div styleName="profile-account-selector-dropdown">
            <SelectAccountDropdown />
          </div>
        }
      </div>
    );
  }
}

HeaderProfileDescription.propTypes = {
  activeAccount: PropTypes.object,
  currentUser: PropTypes.object,
};

const mapStateToProps = createStructuredSelector({
  activeAccount: makeSelectActiveAccountInfo(),
  currentUser: makeSelectCurrentUser(),
});

export default connect(mapStateToProps)(HeaderProfileDescription);
