import { createAction } from 'redux-act';

export const initProfileSubscription = createAction('app/container/AuthWrapper/INIT_PROFILE_SUBSCRIPTION');
