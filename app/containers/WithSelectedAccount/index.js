/*
 *
 * AuthWrapper
 *
 */

import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { createStructuredSelector } from 'reselect';

import WithInjection from 'unity-frontend-core/containers/WithInjection';
import { makeSelectActiveAccount } from 'unity-frontend-core/containers/SelectAccount/selectors';

import LoadingIndicator from 'Components/LoadingIndicator';
import SelectAccountPopup from 'Containers/SelectAccountPopup';
import { makeSelectCurrentUserStatus } from 'Containers/App/selectors';

import Wrapper from 'Containers/Wrapper';
import Footer from 'Containers/Footer';

import { initProfileSubscription } from './actions';

import sagas from './sagas';

/**
 * Authorization layer
 * Check access permissions, roles etc
 * For login/logout actions, token revalidation see `App`
 */

export class WithSelectedAccount extends React.Component { // eslint-disable-line react/prefer-stateless-function
  componentDidMount() {
    const { selectedAccountId } = this.props;

    if (selectedAccountId) {
      this.props.initProfileSubscription();
    }
  }

  componentWillReceiveProps(nextProps) {
    if (!this.props.selectedAccountId && nextProps.selectedAccountId) {
      this.props.initProfileSubscription();
    }
  }

  render() {
    const {
      location = {}, selectedAccountId, userStatus, children,
    } = this.props;

    if (!selectedAccountId && userStatus !== 'New') {
      return <SelectAccountPopup />;
    }

    return selectedAccountId
      ? <Wrapper activeLinkName={location.pathname}>
        {children}
        <Footer />
      </Wrapper>
      : <LoadingIndicator />;
  }
}


WithSelectedAccount.propTypes = {
  children: PropTypes.node.isRequired,
  location: PropTypes.any,
  userStatus: PropTypes.string,
  selectedAccountId: PropTypes.number,
  initProfileSubscription: PropTypes.func,
};

const mapStateToProps = createStructuredSelector({
  selectedAccountId: makeSelectActiveAccount(),
  userStatus: makeSelectCurrentUserStatus(),
});

const mapDispatchToProps = (dispatch) => ({
  initProfileSubscription: () => dispatch(initProfileSubscription()),
});

const Connected = connect(mapStateToProps, mapDispatchToProps)(WithSelectedAccount);

export default WithInjection({ sagas })(Connected);
