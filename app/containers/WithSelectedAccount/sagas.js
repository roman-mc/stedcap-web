import { call, put, fork, take, apply, cancel } from 'redux-saga/effects';
import Raven from 'raven-js';

import { createChannel, generateListener } from 'Utils/sagaUtils';

import {
  userLoggedOut,
  userInfoRefreshed,
  userBalanceRefreshed,
} from 'Containers/App/actions';

import socketApi from 'Api/socketApi';

import { initProfileSubscription } from './actions';

function createSocket() {
  return socketApi;
}

export function* profileSaga() {
  const socket = yield call(createSocket);
  const channel = yield call(createChannel, socket, 'userInfo');
  yield apply(socket, socket.subscribeProfile);
  try {
    while (true) { // eslint-disable-line no-constant-condition
      const { data } = yield take(channel);
      yield put(userInfoRefreshed(data));
    }
  } finally {
    channel.close();
  }
}

export function* accountsSaga() {
  const socket = yield call(createSocket);
  const channel = yield call(createChannel, socket, 'updatedAccounts');
  yield apply(socket, socket.subscribeAccounts);

  Raven.captureBreadcrumb({
    message: 'Accounts subscription activated',
  });

  try {
    while (true) { // eslint-disable-line no-constant-condition
      const { data } = yield take(channel);
      yield put(userBalanceRefreshed(data));
    }
  } finally {
    channel.close();
  }
}


export function* runSocketHandler() {
  const forks = yield [
    accountsSaga,
    profileSaga,
  ].map(fork);
  yield take(userLoggedOut.toString());
  yield forks.map((f) => cancel(f));
}


// All sagas to be loaded
export default [
  generateListener(initProfileSubscription, runSocketHandler),
];
