import notificationsCenterReducer from '../reducer';

describe('notificationsCenterReducer', () => {
  it('returns the initial state', () => {
    expect(notificationsCenterReducer(undefined, {}).toJS()).toEqual([]);
  });
});
