import { takeEvery } from 'redux-saga';
import { take, call, put, apply, select, fork, cancel } from 'redux-saga/effects';


import { createChannel } from 'Utils/sagaUtils';
import socketApi from 'Api/socketApi';
import * as notificationsApi from 'unity-frontend-core/api/notificationsApi';
import { NOTIFICATION_TYPE } from 'unity-frontend-core/Models/Enums';
import { makeSelectInstrumentsMap } from 'unity-frontend-core/containers/InstrumentResolver/selectors';
import { notifyTitleByType } from './';

import { actions as notifyActions } from '../Notifications';
import { messagesByType } from '../../components/NotificationMessage';

import { userInfoRefreshed } from '../App/actions';
import { doLogout } from '../../pages/LogoutPage/actions';

import {
  initSubscription,
  restMessageReceived,
  socketMessageReceived,
  removeMessages,
  updateNotificationTime,
  loadNotifications,
} from './actions';

function createSocket() {
  return socketApi;
}
const excludeHeaders = [
  NOTIFICATION_TYPE.dealExecuted,
];

function* loadNotificationsHandler({ payload: { loadMore = 0 } = {} }) {
  try {
    const data = yield call(notificationsApi.list, {
      limit: 50, offset: loadMore, deals: true, orders: true, other: true, dateFrom: 0, sortOrder: 'desc',
    });
    yield put(restMessageReceived({ data, loadMore }));
    yield put(loadNotifications.success());
  } catch (e) {
    yield put(loadNotifications.error());
    console.warn('load notifications error', e);
  }
}

export function* notificationsSaga() {
  yield put(loadNotifications());
  const socket = yield call(createSocket);
  const channel = yield call(createChannel, socket, 'notifications');
  yield apply(socket, socket.subscribeNotifications);
  try {
    while (true) { // eslint-disable-line no-constant-condition
      const { data } = yield take(channel);
      const instruments = yield select(makeSelectInstrumentsMap());
      const needRemove = yield data.filter((message) => message.deleted).map((r) => (`${r.created} ${r.message}`));
      yield data
        .filter((message) => !message.deleted && messagesByType[message.notificationType])
        .map((message) => put(notifyActions.success({
          defaultMessage: messagesByType[message.notificationType].general,
          detailsMessage: messagesByType[message.notificationType].details,
          defaultHeader: excludeHeaders.includes(message.notificationType) ? null : notifyTitleByType[message.notificationType].general,
          detailsValues: message.data,
          values: {
            direction: message.data.direction,
            price: message.data.price,
            amount: message.data.amount,
            currency: message.data.currency,
            symbol: instruments.get(message.data.instrumentId, { displayName: '' }).displayName,
            orderRequest: message.data.orderRequest,
          },
        })));
      yield put(socketMessageReceived(data));
      if (needRemove.length) {
        yield put(removeMessages(needRemove));
      }
    }
  } finally {
    channel.close();
  }
}

export function* runNotificationsSaga() {
  while (yield take(initSubscription.toString())) {
    const f = yield fork(notificationsSaga);
    yield take(doLogout.toString());
    yield cancel(f);
  }
}

export function* updateNotificatonSaga() {
  const result = yield call(notificationsApi.update, {});
  yield put(userInfoRefreshed(result));
  yield put(updateNotificationTime.success(result));
}

export function* updateNotificatonHandler() {
  yield call(takeEvery, updateNotificationTime.toString(), updateNotificatonSaga);
}

export function* loadNotificationsSaga() {
  yield call(takeEvery, loadNotifications.toString(), loadNotificationsHandler);
}


export default [
  runNotificationsSaga,
  updateNotificatonHandler,
  loadNotificationsSaga,
];
