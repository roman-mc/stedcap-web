import { createSelector } from 'reselect';


import { makeSelectCurrentUser } from '../App/selectors';

/**
 * Direct selector to the notificationsCenter state domain
 */
const selectNotificationsCenterDomain = () => (state) => state.getIn(['notificationsCenter']);

/**
 * Other specific selectors
 */


/**
 * Default selector used by NotificationsCenter
 */

const makeSelectNotificationsCenter = () => createSelector(
  selectNotificationsCenterDomain(),
  makeSelectNotificationTime(),
  (substate) => {
    return substate.get('notifications');
  },
);

export const makeSelectNotificationTime = () => createSelector(
  makeSelectCurrentUser(),
  (user) => user.notificationTime,
);

export const makeSelectLoading = () => createSelector(
  selectNotificationsCenterDomain(),
  (substate) => substate.get('loading')
);

export const makeSelectTotalCount = () => createSelector(
  selectNotificationsCenterDomain(),
  (substate) => substate.get('totalCount')
);


export default makeSelectNotificationsCenter;
export { selectNotificationsCenterDomain };
