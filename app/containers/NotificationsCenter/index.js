/*
 *
 * NotificationsCenter
 *
 */

import React from 'react';
import PropTypes from 'prop-types';
import ImmutableProptypes from 'react-immutable-proptypes';
import { connect } from 'react-redux';
import { createStructuredSelector } from 'reselect';
import { format } from 'date-fns';
import { FormattedMessage } from 'react-intl';

import { NOTIFICATION_TYPE } from 'unity-frontend-core/Models/Enums';
import { STANDARD_DATE } from 'unity-frontend-core/utils/dateUtils';
import WithInjection from 'unity-frontend-core/containers/WithInjection';

import Notification from 'Model/Notification';
import { Button } from 'Components/GUI';
import ImmutableListView from 'Components/ImmutableListView';
import NotificationMessage from 'Components/NotificationMessage';

import ScrollBar from 'Components/Scrollbar';
import LoadingIndicator from 'Components/LoadingIndicator';

import messages from './messages';

import makeSelectNotificationsCenter, { makeSelectNotificationTime, makeSelectLoading, makeSelectTotalCount } from './selectors';

import { initSubscription, loadNotifications, updateNotificationTime } from './actions';

import sagas from './sagas';
import reducer from './reducer';

import './style.css';

export const notifyTitleByType = {
  [NOTIFICATION_TYPE.dealExecuted]: {
    general: messages.dealExecuted,
  },
  [NOTIFICATION_TYPE.orderPlaced]: {
    general: messages.orderPlaced,
  },
  [NOTIFICATION_TYPE.orderExecuted]: {
    general: messages.orderExecuted,
  },
  [NOTIFICATION_TYPE.orderRejected]: {
    general: messages.orderRejected,
  },
  [NOTIFICATION_TYPE.orderIncorrect]: {
    general: messages.orderIncorrect,
  },
  [NOTIFICATION_TYPE.orderRequestCreated]: {
    general: messages.orderRequestCreated,
  },
  [NOTIFICATION_TYPE.orderRequestCanceled]: {
    general: messages.orderRequestCanceled,
  },
  [NOTIFICATION_TYPE.orderCanceled]: {
    general: messages.orderCanceled,
  },
  [NOTIFICATION_TYPE.orderUpdated]: {
    general: messages.orderUpdated,
  },
  [NOTIFICATION_TYPE.orderMessageReceived]: {
    general: messages.orderMessageReceived,
  },
  [NOTIFICATION_TYPE.withdrawalRequestDeclined]: {
    general: messages.withdrawalRequestDeclined,
  },
  [NOTIFICATION_TYPE.withdrawalRequestExecuted]: {
    general: messages.withdrawalRequestExecuted,
  },
  [NOTIFICATION_TYPE.marginWarning]: {
    general: messages.marginWarning,
  },
};

export class NotificationsCenter extends React.PureComponent {
  state = {
    opened: false,
  }

  componentWillMount() {
    this.props.initSubscription();
    window.addEventListener('click', this.closePopup);
  }

  componentWillUnmount() {
    window.removeEventListener('click', this.closePopup);
  }

  closePopup = () => this.setState({ opened: false })

  toggleVisibility = (e) => {
    e.stopPropagation();
    if (this.props.notifications.size) {
      const { opened } = this.state;
      this.setState({ opened: !opened });
    }
  }

  markMessagesAsSeen = () => {
    this.props.updateNotificationTime();
    this.closePopup();
  }

  loadMore = () => {
    const { totalCount, notifications } = this.props;
    const notificationsLength = notifications.size;
    if (totalCount > notificationsLength) {
      this.props.loadNotifications({ loadMore: notificationsLength });
    }
  }

  renderNotification = (notification, i) => {
    const { notificationTime } = this.props;
    const isReadstyleName = notification.isAfter(notificationTime) ? '' : 'seen';
    const notificationDate = new Date(notification.created);
    const message = notifyTitleByType[notification.notificationType];
    return message && (
      <div
        key={`${notification.data ? notification.data.orderId : notification.created}_${notification.notificationType}_${i}`}
        styleName={`message ${isReadstyleName}`}
      >
        <div styleName="time">
          {format(notificationDate, 'HH:mm')}
        </div>
        <div styleName="text">
          <h3 styleName="title">
            <FormattedMessage {...notifyTitleByType[notification.notificationType].general} />
          </h3>
          <NotificationMessage notification={notification} notificationTime={notificationTime} />
        </div>
      </div>
    );
  }

  renderPopup(unreadCount) {
    const { notifications, loading } = this.props;
    /* eslint-disable jsx-a11y/no-static-element-interactions */
    return (
      <div styleName="popup" onClick={(e) => e.stopPropagation()}>
        <div styleName="messages">
          {loading && <div styleName="loader">
            <LoadingIndicator loading />
          </div>}
          <ScrollBar
            style={{ position: 'static' }}
            containerRef={(ref) => this.notificationsListScrollRef = ref}
            onYReachEnd={this.loadMore}
          >
            <div>
              <ImmutableListView
                immutableData={notifications.groupBy((n) => format(n.created, STANDARD_DATE))}
                renderSection={(key) => <div
                  styleName="message small-text"
                >
                  {key}
                </div>}
                renderRow={this.renderNotification}
              />
            </div>
          </ScrollBar>
        </div>
        {unreadCount > 0
          && <div styleName="button">
            <Button fullWidth big onClick={this.markMessagesAsSeen} type="primary">
              OK (
              {unreadCount}
)
            </Button>
          </div>}
      </div>
    );
    /* eslint-enable */
  }

  render() {
    const { notifications, notificationTime } = this.props;
    const unreadCount = notifications.filter((notification) => notification.isAfter(notificationTime)).size;
    const hasMessagesClass = unreadCount ? 'has-messages' : '';
    const openedClass = this.state.opened ? 'active' : '';
    /* eslint-disable jsx-a11y/no-static-element-interactions */
    return (
      <span styleName="notification-center">
        <span onClick={this.toggleVisibility} className="notification__center__icon" styleName={`icon ${openedClass} ${hasMessagesClass}`}>
          <i className="icon-icon-notifications-desktop"></i>
        </span>
        {this.state.opened && this.renderPopup(unreadCount)}
      </span>
    );
    /* eslint-enable */
  }
}


NotificationsCenter.propTypes = {
  notifications: ImmutableProptypes.listOf(ImmutableProptypes.recordOf(Notification)),
  initSubscription: PropTypes.func.isRequired,
  loading: PropTypes.bool,
  totalCount: PropTypes.number,
  updateNotificationTime: PropTypes.func.isRequired,
  loadNotifications: PropTypes.func.isRequired,
  notificationTime: PropTypes.number.isRequired,
};

const mapStateToProps = createStructuredSelector({
  notifications: makeSelectNotificationsCenter(),
  notificationTime: makeSelectNotificationTime(),
  loading: makeSelectLoading(),
  totalCount: makeSelectTotalCount(),
});

function mapDispatchToProps(dispatch) {
  return {
    initSubscription: () => dispatch(initSubscription()),
    updateNotificationTime: () => dispatch(updateNotificationTime()),
    loadNotifications: (params) => dispatch(loadNotifications(params)),
  };
}

const Connected = connect(mapStateToProps, mapDispatchToProps)(NotificationsCenter);

export default WithInjection({ sagas, reducer, key: 'notificationsCenter' })(Connected);
