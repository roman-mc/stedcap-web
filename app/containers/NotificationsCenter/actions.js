/*
 *
 * NotificationsCenter actions
 *
 */
import { createAction } from 'redux-act';
import createApiAction from 'unity-frontend-core/utils/createAction';

export const loadNotifications = createApiAction('app/NotificationsCenter/LOAD_NOTIFICATIONS');
export const initSubscription = createAction('app/NotificationsCenter/INIT_SUBSCRIPTION');

export const socketMessageReceived = createAction('app/NotificationsCenter/SOCKET_MESSAGE_RECEIVED');
export const restMessageReceived = createAction('app/NotificationsCenter/REST_MESSAGE_RECEIVED');
export const removeMessages = createAction('app/NotificationsCenter/REMOVE_MESSAGES');

export const updateNotificationTime = createApiAction('app/NotificationsCenter/MESSAGE_RECEIVED');
