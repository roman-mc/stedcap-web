/*
 * TradingPage Messages
 *
 * This contains all the text for the TradingPage component.
 */
import { defineMessages } from 'react-intl';

export default defineMessages({
  tradeConfirmation: {
    id: 'app.notificationsCenter.tradeConfirmation',
    defaultMessage: 'Trade confirmation',
  },
  dealExecuted: {
    id: 'app.notificationsCenter.dealExecuted',
    defaultMessage: 'Trade confirmation',
  },
  orderPlaced: {
    id: 'app.notificationsCenter.orderPlaced',
    defaultMessage: 'Order placed',
  },
  orderExecuted: {
    id: 'app.notificationsCenter.orderExecuted',
    defaultMessage: 'Order executed',
  },
  orderRejected: {
    id: 'app.notificationsCenter.orderRejected',
    defaultMessage: 'Order rejected',
  },
  orderIncorrect: {
    id: 'app.notificationsCenter.orderIncorrect',
    defaultMessage: 'Order incorrect',
  },
  orderRequestCreated: {
    id: 'app.notificationsCenter.orderRequestCreated',
    defaultMessage: 'Order request created',
  },
  orderRequestCanceled: {
    id: 'app.notificationsCenter.orderRequestCanceled',
    defaultMessage: 'Order request canceled',
  },
  orderCanceled: {
    id: 'app.notificationsCenter.orderCanceled',
    defaultMessage: 'Order canceled',
  },
  orderUpdated: {
    id: 'app.notificationsCenter.orderUpdated',
    defaultMessage: 'Order was updated',
  },
  orderMessageReceived: {
    id: 'app.notificationsCenter.orderMessageReceived',
    defaultMessage: 'You have received a new message',
  },
  withdrawalRequestDeclined: {
    id: 'app.notificationsCenter.withdrawalRequestDeclined',
    defaultMessage: 'Withdrawal rejected',
  },
  withdrawalRequestExecuted: {
    id: 'app.notificationsCenter.withdrawalRequestExecuted',
    defaultMessage: 'Withdrawal executed',
  },
  marginWarning: {
    id: 'app.notificationsCenter.marginWarning',
    defaultMessage: 'Margin warning',
  },
});
