/*
 *
 * NotificationsCenter reducer
 *
 */

import { List, Map } from 'immutable';
import { createReducer } from 'redux-act';
import Notification from 'unity-frontend-core/Models/Notification';

import { restMessageReceived, socketMessageReceived, loadNotifications, removeMessages } from './actions';

import { doLogout } from '../../pages/LogoutPage/actions'; // TODO: remove

const emptyList = new List();
const initialState = new Map({
  totalCount: 0,
  loading: false,
  notifications: new List(),
});

export default createReducer({
  [loadNotifications]: (state) => state.set('loading', true),
  [loadNotifications.error]: (state) => state.set('loading', false),
  [loadNotifications.success]: (state) => state.set('loading', false),
  [socketMessageReceived]: (state, items) => {
    return state
      .update('notifications', (t) => new List(items.map((msg) => new Notification(msg))).concat(t))
      .update('totalCount', (t) => (t + items.length));
  },
  [restMessageReceived]: (state, { data: { items, totalCount }, loadMore }) => {
    return state
      .update('notifications', (notifications) => (loadMore ? notifications : emptyList).concat(new List(items.map((msg) => new Notification(msg)))))
      .set('totalCount', totalCount);
  },
  [removeMessages]: (state, removed) => {
    return state
      .update('notifications', (notifications) => notifications.filter((n) => !(removed.includes(`${n.created} ${n.message}`))))
      .update('totalCount', (t) => (t - removed.length));
  },
  [doLogout.succes]: () => initialState,
}, initialState);
