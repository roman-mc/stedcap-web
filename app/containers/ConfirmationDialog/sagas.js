import { take, put, race, call } from 'redux-saga/effects';
import { generateListener } from 'unity-frontend-core/utils/sagaUtils';
import * as actions from './actions';

export function* confirmSaga(messages) {
  yield put(actions.confirmRequest({ messages }));

  const { yes } = yield race({
    yes: take(actions.confirmApprove),
    no: take(actions.confirmDecline),
  });

  return !!yes;
}


function* confirmHandler({ payload: { messages, onSuccess = { type: 'SUCCESS' }, onError = { type: 'ERROR' } } }) {
  const result = yield call(confirmSaga, messages);
  yield put(result ? onSuccess : onError);
}

export default [
  generateListener(actions.askForConfirmation, confirmHandler),
];
