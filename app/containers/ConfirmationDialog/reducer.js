/*
 *
 * Accounts reducer
 *
 */

import { Map } from 'immutable';
import { createReducer } from 'redux-act';

import * as confirmActions from './actions';

const initialState = Map({
  messages: new Map(),
  opened: false,
});

export default createReducer({
  [confirmActions.confirmRequest]: (state, { messages = {} }) =>
    state
      .set('messages', new Map(messages))
      .set('opened', true),
  [confirmActions.confirmDecline]: () => initialState,
  [confirmActions.confirmApprove]: () => initialState,
}, initialState);
