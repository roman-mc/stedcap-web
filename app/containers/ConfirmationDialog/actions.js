import { createAction } from 'redux-act';


export const askForConfirmation = createAction('app/containers/ConfirmationDialog/ASK_FOR_CONFIRMATION');
export const confirmRequest = createAction('app/containers/ConfirmationDialog/CONFIRM_REQUEST');
export const confirmApprove = createAction('app/containers/ConfirmationDialog/CONFIRM_APPROVE');
export const confirmDecline = createAction('app/containers/ConfirmationDialog/CONFIRM_DECLINE');
