import { createSelector } from 'reselect';

const makeSelectConfirmState = () => (state) => state.get('confirmationPopup');

export const makeSelectMessages = () => createSelector(
  makeSelectConfirmState(),
  (confirm) => confirm.get('messages'),
);

export const makeSelectOpened = () => createSelector(
  makeSelectConfirmState(),
  (confirm) => confirm.get('opened'),
);
