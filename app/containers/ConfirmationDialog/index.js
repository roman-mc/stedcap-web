import React from 'react';
import PropTypes from 'prop-types';
import { FormattedMessage } from 'react-intl';
import { connect } from 'react-redux';
import { createStructuredSelector } from 'reselect';

import WithInjection from 'unity-frontend-core/containers/WithInjection';

import Popup from '../../components/Popup';
import { Button } from '../../components/GUI';

import messages from './messages';
import {
  makeSelectMessages,
  makeSelectOpened,
} from './selectors';

import * as actions from './actions';
import reducer from './reducer';
import sagas from './sagas';

import './style.css';

function ConfirmationDialog({
  confirmationMessages, confirm, cancel, opened,
}) {
  return (
    opened
      ? <Popup
        popupKey="confirmation_popup"
        removePopup={cancel}
        headerHidden
      >
        <div styleName="container">
          {confirmationMessages.get('header') && <h4 styleName="header"><FormattedMessage {...confirmationMessages.get('header')} /></h4>}
          {confirmationMessages.get('text') && <p styleName="text"><FormattedMessage {...(confirmationMessages.get('text'))} /></p>}
          <div styleName="buttons">
            <Button onClick={cancel} big type="tertiary">
              <FormattedMessage {...(confirmationMessages.get('cancel') || messages.cancel)} />
            </Button>
            <Button onClick={confirm} big type="primary">
              <FormattedMessage {...(confirmationMessages.get('confirm') || messages.confirm)} />
            </Button>
          </div>
        </div>
      </Popup>
      : <span></span>
  );
}

ConfirmationDialog.propTypes = {
  confirm: PropTypes.func.isRequired,
  cancel: PropTypes.func.isRequired,
  opened: PropTypes.bool.isRequired,
  confirmationMessages: PropTypes.object,
};

const mapStateToProps = createStructuredSelector({
  confirmationMessages: makeSelectMessages(),
  opened: makeSelectOpened(),
});

const mapDispatchToProps = (dispatch) => ({
  confirm: () => dispatch(actions.confirmApprove()),
  cancel: () => dispatch(actions.confirmDecline()),
});

const Connected = connect(mapStateToProps, mapDispatchToProps)(ConfirmationDialog);

export default WithInjection({ key: 'confirmationPopup', reducer, sagas })(Connected);
