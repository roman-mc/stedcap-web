import { defineMessages } from 'react-intl';

export default defineMessages({
  header: {
    id: 'app.containers.ConfirmationDialog.header',
    defaultMessage: 'Confirmation',
  },
  text: {
    id: 'app.containers.ConfirmationDialog.text',
    defaultMessage: 'Are you sure?',
  },
  cancel: {
    id: 'app.containers.ConfirmationDialog.cancel',
    defaultMessage: 'Cancel',
  },
  confirm: {
    id: 'app.containers.ConfirmationDialog.confirm',
    defaultMessage: 'OK',
  },
});
