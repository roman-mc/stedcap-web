import React, { Fragment } from 'react';
import PropTypes from 'prop-types';
import ImmutableProptypes from 'react-immutable-proptypes';

import { InstrumentSelectorModal } from 'Containers/InstrumentsTree';

import { WindowHeader, WindowHeaderTitle } from '../../components/Window';


export class MarketDepthHeader extends React.PureComponent { // eslint-disable-line react/prefer-stateless-function
  render() {
    return (
      <WindowHeader
        {...this.props}
        renderTitle={() =>
          <Fragment>
            <WindowHeaderTitle
              key="market_depth_header"
              icon="icon-icon-widget-market-depth"
            >
              Market depth
            </WindowHeaderTitle>
            <InstrumentSelectorModal
              hideSearchLabel={this.props.hideSearchLabel}
              config={this.props.config}
              widgetType="marketDepth"
              onChange={(selectedInstrumentId) => this.props.onConfigChange({ selectedInstrumentId })}
              key="market_depth_header_instrument"
            />
          </Fragment>
        }
      />
    );
  }
}

MarketDepthHeader.propTypes = {
  config: ImmutableProptypes.map,
  onConfigChange: PropTypes.func.isRequired,
  hideSearchLabel: PropTypes.bool,
};
