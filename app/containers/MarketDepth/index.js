import React from 'react';
import ImmutableProptypes from 'react-immutable-proptypes';
import PropTypes from 'prop-types';
import InstrumentMarketDepth from 'unity-frontend-core/containers/InstrumentMarketDepth';

import LoadingIndicator from 'Components/LoadingIndicator';
import { FormattedNumber } from 'react-intl';
import { MarketDepthHeader } from './MarketDepthHeader';

import './style.css';

const ROW_HEIGHT = 23; // by design


class MarketDepth extends React.PureComponent { // eslint-disable-line react/prefer-stateless-function
  renderAskStub = (_, idx) => {
    return (
      <tr key={`stub_ask_${idx}`}>
        <td styleName="empty"></td>
        <td></td>
        <td></td>
      </tr>
    );
  }

  renderBidStub = (_, idx) => {
    return (
      <tr key={`stub_bid_${idx}`}>
        <td></td>
        <td></td>
        <td styleName="empty"></td>
      </tr>
    );
  }

  renderData = (depth, height) => {
    const { config } = this.props;

    const rowCount = Math.floor(((height - ROW_HEIGHT) / ROW_HEIGHT) / 2);

    const instrumentId = config && config.get('selectedInstrumentId');

    const totalBidAmount = depth.bidBands.reduce((acc, bb) => acc + bb.amount, 0);
    const totalAskAmount = depth.askBands.reduce((acc, ab) => acc + ab.amount, 0);
    return (<table styleName="market-depth">
      <thead>
        <tr>
          <th styleName="nowrap">Bid (<FormattedNumber value={totalBidAmount} maximumFractionDigits={2} />)</th>
          <th>Price</th>
          <th styleName="nowrap">Ask (<FormattedNumber value={totalAskAmount} maximumFractionDigits={2} />)</th>
        </tr>
      </thead>
      <tbody>
        {new Array(Math.max(0, rowCount - depth.askBands.size)).fill().map(this.renderAskStub)}
        {depth.askBands.slice(0, rowCount).reverse().map((ab, idx) => /* eslint-disable react/no-array-index-key */
          <tr key={`md_${instrumentId}_ab_${idx}`}>
            <td styleName="empty"></td>
            <td>{ab.price}</td>
            <td><FormattedNumber value={ab.amount} maximumFractionDigits={2} /></td>
          </tr>
        )}
        {
          depth.bidBands.slice(0, rowCount).map((bb, idx) =>
            <tr styleName={`${idx === 0 ? 'row-first' : ''}`} key={`md_${instrumentId}_bb_${idx}`}>
              <td><FormattedNumber value={bb.amount} maximumFractionDigits={2} /></td>
              <td>{bb.price}</td>
              <td styleName="empty"></td>
            </tr>
          ) /* eslint-enable */
        }
        {new Array(Math.max(0, rowCount - depth.bidBands.size)).fill().map(this.renderBidStub)}
      </tbody>
    </table>);
  }

  renderLoading = () => {
    return <div styleName="loader">
      <LoadingIndicator loading />
    </div>;
  }

  renderDepth = (depth, _, { height }) => {
    return (
      <div styleName="market-depth-container">
        {depth ? this.renderData(depth, height) : this.renderLoading()}
      </div>
    );
  }

  // renderMarketData = (depth, id) => <InstrumentMarketData id={id} render={this.renderDepth} depth={depth} />

  render() {
    const { config } = this.props;
    const instrumentId = config && config.get('selectedInstrumentId');
    return (<InstrumentMarketDepth id={instrumentId} render={this.renderDepth} height={this.props.size.height} />);
  }
}

MarketDepth.propTypes = {
  config: ImmutableProptypes.map,
  size: PropTypes.object,
};

export default MarketDepth;

export { MarketDepthHeader };
