import React from 'react';
import PropTypes from 'prop-types';
import Dropzone from 'react-dropzone';

import {
  Button,
} from 'Components/ReduxFields';


import styles from './style.css';
import { H2, Italic } from '../../components/Typography';


class UserPic extends React.PureComponent {
  render() {
    const { userPic = false, openCropp } = this.props;
    return (
      <div styleName="row with-border">
        <H2>Userpic</H2>
        <div styleName="row-group user-pic">
          <div styleName="pic">{userPic ? <img alt="userpic" src={`/rest/public/file?storage_type=userpic&path=${userPic}`} /> : <i className="icon-icon-userpic" />}</div>
          <div>
            <Button onClick={() => { this.dropzoneRef.open(); }} subClass={styles.picBtn}>
              Upload userpic
            </Button>
          </div>
          <div styleName="accept"><Italic cleanColor>JPG, GIF or PNG</Italic></div>
        </div>
        <div style={{ display: 'none' }}>
          <Dropzone
            ref={(node) => { this.dropzoneRef = node; }}
            onDrop={(accepted) => {
              if (accepted[0]) {
                openCropp(accepted[0]);
              }
            }}
            accept="image/JPG,image/jpg,image/jpeg,image/png,image/gif"
          >
          </Dropzone>
        </div>
      </div>
    );
  }
}

UserPic.propTypes = {
  userPic: PropTypes.string,
  openCropp: PropTypes.func,
};

export default UserPic;
