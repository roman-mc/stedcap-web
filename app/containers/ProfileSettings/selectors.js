import { createSelector } from 'reselect';

import { Settings } from 'unity-frontend-core/Models/User';
import { makeSelectCurrentUser, makeSelectForms } from '../App/selectors';

export const makeSelectSettingsState = () =>
  (state) => state.get('profileSettings');

export const makeSelectSettings = () => createSelector(
  makeSelectCurrentUser(),
  (user) => user.settings,
);

export const makeSelectFormValues = () => createSelector(
  makeSelectForms,
  (forms) => forms.getIn(['profileSettingsForm', 'values'], new Settings()),
);

export const makeSelectAppearanceFormValues = () => createSelector(
  makeSelectForms,
  (forms) => forms.getIn(['appearanceSettingsForm', 'values'], new Settings()),
);

export const makeSelectResetFormValues = () => createSelector(
  makeSelectForms,
  (forms) => forms.getIn(['resetPasswordForm', 'values']),
);

export const makeSelectProfileFormValues = () => createSelector(
  makeSelectForms,
  (forms) => forms.getIn(['profileForm', 'values']),
);

export const selectLoading = createSelector(
  makeSelectSettingsState(),
  (container) => container.get('isLoading'),
);

export const selectToken = createSelector(
  makeSelectSettingsState(),
  (container) => container.get('token'),
);

export const selectAuthMethodsForTokens = createSelector(
  makeSelectSettingsState(),
  (container) => container.get('tokensForAuthMethods')
);

export const selectResetToken = createSelector(
  makeSelectSettingsState(),
  (container) => container.get('tokensForResetPassword'),
);

export const selectResetLoadin = createSelector(
  makeSelectSettingsState(),
  (container) => container.get('passResetLoading')
);
