import React from 'react';
import PropTypes from 'prop-types';
import { reduxForm } from 'redux-form/immutable';
import { FormattedMessage, injectIntl } from 'react-intl';

import {
  BtnGroup,
  Checkbox,
} from 'Components/ReduxFields';

import messages from './messages';
import './style.css';

const availableThemes = [
  { label: messages.themeDark, value: 'dark-theme' },
  { label: messages.themeLight, value: 'light-theme' },
];

function AppearanceForm({ intl: { formatMessage } }) {
  return (
    <form>
      <div styleName="row-group theme">
        <BtnGroup
          name="theme"
          options={availableThemes.map(({ label, ...rest }) => ({ ...rest, label: formatMessage(label) }))}
          text={<FormattedMessage {...messages.theme} />}
          // onChange={onThemeChange}
        />
      </div>
      <div styleName="colorized-quotes">
        {/* QUESTION: Why the fuck "colorized quotes" setting named as "colorMode" ? */}
        <div>
          <FormattedMessage {...messages.colorMode} />
        </div>
        <div>
          <Checkbox
            name="colorMode"
          />
        </div>
      </div>
    </form>
  );
}

AppearanceForm.propTypes = {
  intl: PropTypes.object,
};

const ReduxForm = reduxForm({
  form: 'appearanceSettingsForm',
  onChange: (values, dispatch, props) => {
    const { initialValues } = props;
    props.saveSettings({ ...initialValues.delete('smsCode').toJS(), ...values.toJS() });
  },
})(injectIntl(AppearanceForm));

class AppearanceSettingsTab extends React.PureComponent { // eslint-disable-line react/prefer-stateless-function
  render() {
    const { initialValues } = this.props;
    return (
      <div styleName="form">
        <div styleName="row">
          <div styleName="appearance-tab"><FormattedMessage {...messages.appearance} /></div>
          <ReduxForm
            saveSettings={this.props.saveSettings}
            initialValues={initialValues}
          />
        </div>
      </div>
    );
  }
}

AppearanceSettingsTab.propTypes = {
  initialValues: PropTypes.object,
  saveSettings: PropTypes.func,
};

export default AppearanceSettingsTab;
