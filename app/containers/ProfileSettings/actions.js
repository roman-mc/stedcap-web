import createApiAction from 'unity-frontend-core/utils/createAction';

export const saveSettings = createApiAction('app/containers/ProfileSettings/SAVE_SETTINGS');

export const requestChangeAuthMethod = createApiAction('app/containers/AuthMethodChange/REQUEST_SMS_CHANGE');
export const acceptChangeAuthMethod = createApiAction('app/containers/AuthMethodChange/ACCEPT_SMS_CHANGE');

export const requestChangePassword = createApiAction('app/containers/PasswordChange/REQUEST_CHANGE_PASSWORDS');
export const changePassword = createApiAction('app/containers/PasswordChange/CHANGE_PASSWORD');

export const uploadPic = createApiAction('app/containers/UserPic/UPLOAD_USERPIC');
