import React from 'react';
import PropTypes from 'prop-types';
import ImmutableProptypes from 'react-immutable-proptypes';


import ProfileSettingsForm from './PlatformSettingsForm';
import ResetPasswordForm from './ResetPasswordForm';

class PlatformSettingsTab extends React.PureComponent {
  render() {
    const {
      resetValues,
      onThemeChange,
      formValues,
      isLoading,
      token,
      tokensForAuthMethods,
      requestChangeAuthMethod,
      acceptChangeAuthMethod,
      settings,
      resetToken,
      resetLoading,
    } = this.props;

    return (
      <React.Fragment>
        <ProfileSettingsForm
          initialValues={settings}
          onThemeChange={onThemeChange}
          formValues={formValues}
          isLoading={isLoading}
          token={token}
          tokensForAuthMethods={tokensForAuthMethods}
          requestChangeAuthMethod={requestChangeAuthMethod}
          acceptChangeAuthMethod={acceptChangeAuthMethod}
          saveSettings={this.props.saveSettings}
        />
        <ResetPasswordForm
          formValues={resetValues}
          requestChangePassword={() => this.props.requestChangePassword(resetValues.delete('code'))}
          changePassword={() => {
            this.props.changePassword({ code: resetValues.get('code'), token: resetToken });
          }}
          resetToken={resetToken}
          resetLoading={resetLoading}
        />
      </React.Fragment>
    );
  }
}


PlatformSettingsTab.propTypes = {
  onThemeChange: PropTypes.func.isRequired,

  settings: ImmutableProptypes.record,
  formValues: ImmutableProptypes.record,
  requestChangeAuthMethod: PropTypes.func.isRequired,
  acceptChangeAuthMethod: PropTypes.func.isRequired,
  saveSettings: PropTypes.func.isRequired,
  requestChangePassword: PropTypes.func.isRequired,
  changePassword: PropTypes.func.isRequired,

  isLoading: PropTypes.bool.isRequired,
  resetLoading: PropTypes.bool.isRequired,
  token: PropTypes.string,
  resetToken: PropTypes.string,
  tokensForAuthMethods: ImmutableProptypes.map,
  resetValues: ImmutableProptypes.map,

};


export default PlatformSettingsTab;
