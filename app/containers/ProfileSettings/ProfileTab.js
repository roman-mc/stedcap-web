import React from 'react';
import PropTypes from 'prop-types';
import ImmutableProptypes from 'react-immutable-proptypes';

import base64ToFile from 'unity-frontend-core/utils/base64ToFile';
import {
  Button,
} from 'Components/ReduxFields';

import ProfileForm from './ProfileForm';
import UserPic from './UserPic';
import Cropper from './Cropper';
import styles from './style.css';
import { Italic } from '../../components/Typography';

class ProfileTab extends React.PureComponent {
  state = {
    cropp: false,
  }
  openCropp = (file) => {
    this.setState({
      file,
      cropp: true,
    });
  };

  closeCropp = () => {
    this.setState({
      file: null,
      cropp: false,
    });
  };

  crop = () => {
    // image in dataUrl
    const { file } = this.state;
    const dataURL = this.cropper.getCroppedCanvas().toDataURL();
    const formdata = new FormData();
    formdata.append('userpic', base64ToFile(dataURL, file, 'userpic'));
    this.props.uploadPic({
      formdata,
      success: () => {
        this.closeCropp();
      },
    });
  };

  renderTab = () => {
    const { cropp } = this.state;
    if (cropp) {
      return <div styleName="cropper">
        <div styleName="cropper-header">
          <Italic cleanColor>
            Resize and crop your image. After you finish editing click save button to set your new userpic.
          </Italic>
        </div>
        <Cropper
          cRef={(el) => { this.cropper = el; }}
          src={this.state.file ? this.state.file.preview : null}
          style={{ height: 'calc(100% - 48px)', width: 'calc(100% - 4px)', margin: '0 auto' }}
          // Cropper.js options
          aspectRatio={1 / 1}
          guides={false}
        />
        <div styleName="cropp-footer">
          <Button
            onClick={this.closeCropp}
            type="tertiary"
            subClass={styles.smallBtn}
            styleType="secondary"
          >
            Cancel
          </Button>
          <Button
            type="primary"
            onClick={this.crop}
            subClass={styles.smallBtn}
          >
            Save
          </Button>
        </div>
      </div>;
    }
    const {
      currentUser: { firstName, lastName, email, phone, userPic } = {},
      formValues,
    } = this.props;
    const initialValues = {
      firstName, lastName, email, phone: `+${phone.countryCode}${phone.phone}`,
    };
    return <React.Fragment>
      <UserPic
        userPic={userPic}
        openCropp={this.openCropp}
      />
      <ProfileForm
        initialValues={initialValues}
        formValues={formValues}
      />
    </React.Fragment>;
  };

  render() {
    const {
      currentUser = false,
    } = this.props;
    return currentUser ? this.renderTab() : null;
  }
}


ProfileTab.propTypes = {
  currentUser: ImmutableProptypes.record,
  formValues: ImmutableProptypes.map,
  uploadPic: PropTypes.func,
};


export default ProfileTab;
