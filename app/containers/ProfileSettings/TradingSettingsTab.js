import React from 'react';
import PropTypes from 'prop-types';
import { reduxForm } from 'redux-form/immutable';
import { connect } from 'react-redux';
import { throttle } from 'lodash';
import { createStructuredSelector } from 'reselect';
import { FormattedMessage } from 'react-intl';

import UserAccountSettings from 'unity-frontend-core/containers/UserAccountSettings';
import { USER_SETTINGS } from 'unity-frontend-core/Models/Enums';

import InstrumentCategoryIcon from '../../components/InstrumentCategoryIcon';
import { InlineSelect, BtnGroup } from '../../components/ReduxFields';

import { H2, Italic } from '../../components/Typography';
import AmountsSettings from './AmountsSettings';

import messages from './messages';
import './style.css';
import { INSTRUMENT_CATEGORIES } from '../../../../core/Models/Enums';
import { allValuesExists } from '../../utils/helpers';

const availableAmounts = [
  { value: 1, label: '1' },
  { value: 10, label: '10' },
  { value: 100, label: '100' },
  { value: 1000, label: '1K' },
  { value: 10000, label: '10K' },
  { value: 100000, label: '100K' },
  { value: 1000000, label: '1M' },
];
const availableCurrencies = ['USD', 'EUR', 'RUB', 'GBP'];

const SettingsForm = ({ initialValues }) => (
  <form>
    <div styleName="row with-border">
      <div styleName="row-group" className="select-up">
        {Object.keys(INSTRUMENT_CATEGORIES)
          .filter((cat) => (!INSTRUMENT_CATEGORIES[cat].disabled && INSTRUMENT_CATEGORIES[cat].isAvailableForUserTrade))
          .map((cat) => {
            const values = initialValues.getIn([cat, 'value', 'value']) ? initialValues.getIn([cat, 'value', 'value']) : false;
            return values ? (
              <InlineSelect
                disableClearable
                key={`${cat}.value.value`}
                name={`${cat}.value.value`}
                text={<div styleName="label"><InstrumentCategoryIcon fontSize="13px" category={cat} /><span styleName="label-text">{INSTRUMENT_CATEGORIES[cat].name}</span></div>}
                options={availableAmounts}
              />) : null;
          })
        }
      </div>
    </div>
  </form>
);


SettingsForm.propTypes = {
  initialValues: PropTypes.object,
};

const ReduxForm = reduxForm({
  form: 'tradingSettingsForm',
  onChange: throttle((values, _, props) => {
    values.forEach((value, instrumentCategory) => {
      const result = {
        systemPropertyName: USER_SETTINGS.QUICK_TRADE_MULTIPLIER,
        value: value.get('value').toObject(),
        instrumentCategory,
      };

      props.setWidgetSetting(result);
    });
  }, 500),
})(
  SettingsForm
);

class FormContainer extends React.PureComponent {
  componentDidMount() {
    Object.keys(INSTRUMENT_CATEGORIES).forEach((cat) => {
      if (!INSTRUMENT_CATEGORIES[cat].disabled && INSTRUMENT_CATEGORIES[cat].isAvailableForUserTrade) {
        this.props.loadWidgetSetting({
          name: USER_SETTINGS.QUICK_TRADE_MULTIPLIER,
          instrumentCategory: cat,
        });
      }
    });
  }

  render() {
    const { initialValues } = this.props;
    const availableCategories = Object.keys(INSTRUMENT_CATEGORIES).filter((cat) => (!INSTRUMENT_CATEGORIES[cat].disabled && INSTRUMENT_CATEGORIES[cat].isAvailableForUserTrade));
    return allValuesExists(initialValues, availableCategories) ? (
      <ReduxForm
        initialValues={initialValues}
        setWidgetSetting={this.props.setWidgetSetting}
      />
    ) : null;
  }
}

FormContainer.propTypes = {
  initialValues: PropTypes.object,
  loadWidgetSetting: PropTypes.func.isRequired,
  setWidgetSetting: PropTypes.func.isRequired,
};

const mapStateToProps = createStructuredSelector({
  initialValues: (_, props) => props.widget,
});
const SettingsFormContainer = UserAccountSettings(
  connect(mapStateToProps)(FormContainer),
  USER_SETTINGS.QUICK_TRADE_MULTIPLIER
);

function CurrencyForm() {
  return (
    <form>
      <div styleName="row-group">
        <BtnGroup
          name="displayedCurrency"
          options={availableCurrencies.map((cur) => ({ label: cur, value: cur }))}
          text={<FormattedMessage {...messages.currency} />}
        />
      </div>
    </form>
  );
}

const ReduxCurrencyForm = reduxForm({
  form: 'currencySettingsForm',
  onChange: (values, dispatch, props) => {
    const { initialValues } = props;
    props.saveSettings({ ...initialValues.delete('smsCode').toJS(), ...values.toJS() });
  },
})(CurrencyForm);

class TradingSettingsTab extends React.PureComponent { // eslint-disable-line react/prefer-stateless-function
  render() {
    const { settings } = this.props;
    return (
      <div>
        <div styleName="form">
          <div styleName="row with-border">
            <div>
              <H2><FormattedMessage {...messages.currencyTitle} /></H2>
            </div>
            <ReduxCurrencyForm initialValues={settings} saveSettings={this.props.saveSettings} />
          </div>
        </div>
        <div styleName="form">
          <div styleName="row">
            <div styleName="row-group">
              <H2>Trading multipliers</H2>
              <Italic cleanColor>
                Set preferred trading multipliers for each of the following
                trading instruments categories. All amounts will be multiplied
                by selected values.
              </Italic>
            </div>
          </div>
        </div>
        <SettingsFormContainer />
        <AmountsSettings />
      </div>
    );
  }
}

TradingSettingsTab.propTypes = {
  settings: PropTypes.object,
  saveSettings: PropTypes.func.isRequired,
};

export default TradingSettingsTab;
