import { Map } from 'immutable';
import { createReducer } from 'redux-act';

import {
  requestChangeAuthMethod,
  acceptChangeAuthMethod,
  requestChangePassword,
  changePassword,
} from './actions';
import { doLogin } from '../../pages/LoginPage/actions';

const initialState = new Map({
  isLoading: false,
  passResetLoading: false,
  // TODO: delete "token" and all its handling/selectors/etc, I cannot refactor now 31.07.18 3.30 p.m.
  //
  // If There is token then show open input for sms code
  //      AND show send code again with timer(30 sec)
  token: undefined, // This is temp token used for 'acceptChangeAuthMethod'
  /**
   * @type {Map<authMethodId: number, token: string>}
   */
  tokensForAuthMethods: new Map(),

  tokensForResetPassword: null,
});

export default createReducer({
  [requestChangeAuthMethod]: (state) => state.set('isLoading', true),
  [requestChangeAuthMethod.success]: (state, payload) =>
    state
      .set('isLoading', false)
      .set('token', payload.token)
      .update('tokensForAuthMethods', (tokensForAuthMethods) => tokensForAuthMethods.set(payload.authMethodId, payload.token)),
  [requestChangeAuthMethod.error]: (state) => state.set('isLoading', false),
  [acceptChangeAuthMethod]: (state) => state.set('isLoading', true),
  [acceptChangeAuthMethod.error]: (state) =>
    state.set('isLoading', false),
  [requestChangePassword]: (state) => state.set('passResetLoading', true),
  [requestChangePassword.error]: (state) => state.set('passResetLoading', false),
  [requestChangePassword.success]: (state, payload) =>
    state
      .set('passResetLoading', false)
      .set('tokensForResetPassword', payload.token),
  [changePassword.success]: (state) => state.set('tokensForResetPassword', null),
  [doLogin]: () => initialState,
}, initialState);
