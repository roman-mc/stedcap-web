import { takeEvery } from 'redux-saga';
import { call, put, select } from 'redux-saga/effects';
import Raven from 'raven-js';
import { reset } from 'redux-form';

import * as userApi from 'unity-frontend-core/api/authApi';
import * as profileApi from 'unity-frontend-core/api/profileApi';
import * as userpicApi from 'unity-frontend-core/api/userpicApi';

import { generateListener, createApiSaga } from 'unity-frontend-core/utils/sagaUtils';
import { reloadAccounts } from 'unity-frontend-core/containers/SelectAccount/actions';

import { makeSelectCurrentUser } from '../App/selectors';
import { userDataReceived, userInfoRefreshed } from '../App/actions';
import { closeModal } from '../Modal/actions';


import {
  saveSettings,
  requestChangeAuthMethod,
  acceptChangeAuthMethod,
  requestChangePassword,
  changePassword,
  uploadPic,
} from './actions';
import { selectToken } from './selectors';
import { doLogin } from '../../pages/LoginPage/actions';

function* saveSettingsHandler({ payload: { settings } }) {
  try {
    const result = yield call(userApi.saveSettings, settings);
    yield put(saveSettings.success(result));
    yield put(reloadAccounts());
  } catch (e) {
    yield put(saveSettings.error(e));
    Raven.captureException(e);
  }
}

function* saveSettingsSaga() {
  yield call(takeEvery, saveSettings.toString(), saveSettingsHandler);
}

function* requestChangeAuthMethodSaga({ payload: authMethodId }) {
  const { phone } = yield select(makeSelectCurrentUser());

  try {
    const response = yield call(userApi.requestChangeAuthMethod, { phone, authMethodId });
    yield put(requestChangeAuthMethod.success({ token: response.token, authMethodId }));
  } catch (e) {
    yield put(requestChangeAuthMethod.error());
    console.warn(e);
  }
}

function* acceptChangeAuthMethodSaga({ payload: code }) {
  const token = yield select(selectToken);

  try {
    const response = yield call(userApi.acceptChangeAuthMethod, { confirmation: code }, token);
    yield put(acceptChangeAuthMethod.success());
    yield put(userDataReceived({ user: response }));
  } catch (e) {
    yield put(acceptChangeAuthMethod.error());
    console.warn(e);
  }
}

function* clearResetForm() {
  yield put(reset('resetPasswordForm'));
}
function* doLoginSaga() {
  yield put(closeModal({ modalId: 'profile_settings_popup' }));
}

function* closeCropp(_, { success }) {
  const result = yield call(userApi.getUserInfo);
  yield put(userInfoRefreshed(result));
  if (success) {
    success();
  }
}

export default [
  saveSettingsSaga,
  generateListener(requestChangeAuthMethod, requestChangeAuthMethodSaga),
  generateListener(acceptChangeAuthMethod, acceptChangeAuthMethodSaga),
  generateListener(doLogin, doLoginSaga),
  createApiSaga(requestChangePassword, profileApi.requestChangePassword, (params) => [params]),
  createApiSaga(changePassword, profileApi.changePassword, ({ code, token }) => [{ code }, token], { onSuccess: clearResetForm }),
  createApiSaga(uploadPic, userpicApi.uploadPic, ({ formdata }) => [formdata], { onSuccess: closeCropp }),
];
