import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { createStructuredSelector } from 'reselect';
import ImmutableProptypes from 'react-immutable-proptypes';
import WithInjection from 'unity-frontend-core/containers/WithInjection';
import { FormattedMessage } from 'react-intl';
import { changeTheme } from '../App/actions';

import {
  makeSelectSettings,
  makeSelectFormValues,
  selectToken,
  selectAuthMethodsForTokens,
  selectLoading,
  makeSelectAppearanceFormValues,
  makeSelectResetFormValues,
  makeSelectProfileFormValues,
  selectResetLoadin, selectResetToken,
} from './selectors';
import { makeSelectCurrentUser } from '../App/selectors';
import {
  saveSettings,
  requestChangeAuthMethod as requestChangeAuthMethodAction,
  acceptChangeAuthMethod as acceptChangeAuthMethodAction,
  requestChangePassword, changePassword, uploadPic,
} from './actions';

import sagas from './sagas';
import reducer from './reducer';

import Scrollbar from '../../components/Scrollbar';

import Modal from '../Modal';
import PlatformSettingsTab from './PlatformSettingsTab';

import TradingSettingsTab from './TradingSettingsTab';
import AppearanceSettingsTab from './AppearanceSettingsTab';
import ProfileTab from './ProfileTab';

import messages from './messages';

import './style.css';

const PLATFORM_TAB = 'Security';
const TRADING_TAB = 'trading';
const APPEARANCE_TAB = 'appearance';
const PROFILE_TAB = 'profile';

const tabs = [
  { key: PROFILE_TAB, label: 'Profile' },
  { key: TRADING_TAB, label: 'Trading' },
  { key: PLATFORM_TAB, label: 'Security' },
  { key: APPEARANCE_TAB, label: 'Appearance' },
];


class ProfileSettings extends React.PureComponent {
  state = {
    activeTab: PROFILE_TAB,
  }

  setActiveTab = (activeTab) => this.setState({ activeTab })

  /**
   * Use either as callback to fields onChange, either as callback in lambda in onClick
   *
   * @param  {string?} authMethodId
   */
  requestChangeAuthMethod = (authMethodId) => {
    const authMethod = authMethodId !== undefined ? Number(authMethodId) : Number(this.props.formValues.authMethod);

    this.props.requestChangeAuthMethod(authMethod);
  }

  acceptChangeAuthMethod = () => {
    const { smsCode } = this.props.formValues;

    this.props.acceptChangeAuthMethod(smsCode);
  }


  changeTheme = ({ target: { value } }) =>
    this.props.changeTheme(value);

  render() {
    const {
      settings,
      formValues,
      resetValues,
      appearanceFormValues,
      isLoading,
      token,
      tokensForAuthMethods,
      resetToken,
      resetLoading,
    } = this.props;

    const { activeTab } = this.state;

    return (
      <Modal
        modalId="profile_settings_popup"
        renderHeader={() => <FormattedMessage {...messages.header} />}
      >
        <div styleName="settings">
          <div styleName="tabs">
            {tabs.map((tab) => <div key={`tab_${tab.key}`} onClick={() => this.setActiveTab(tab.key)} styleName={`tab ${activeTab === tab.key ? 'active' : ''}`}>{tab.label}</div>)}
          </div>
          <div styleName="sections">
            <Scrollbar>
              {activeTab === PLATFORM_TAB
                && <PlatformSettingsTab
                  settings={settings}
                  onThemeChange={this.changeTheme}
                  saveSettings={this.props.saveSettings}
                  formValues={formValues}
                  isLoading={isLoading}
                  token={token}
                  tokensForAuthMethods={tokensForAuthMethods}
                  requestChangeAuthMethod={this.requestChangeAuthMethod}
                  acceptChangeAuthMethod={this.acceptChangeAuthMethod}
                  resetValues={resetValues}
                  resetToken={resetToken}
                  resetLoading={resetLoading}
                  requestChangePassword={this.props.requestChangePassword}
                  changePassword={this.props.changePassword}
                />
              }
              {activeTab === TRADING_TAB
                && <TradingSettingsTab
                  settings={settings}
                  saveSettings={this.props.saveSettings}
                />
              }
              {activeTab === APPEARANCE_TAB
                && <AppearanceSettingsTab
                  formValues={appearanceFormValues}
                  initialValues={settings}
                  changeTheme={this.props.changeTheme}
                  saveSettings={this.props.saveSettings}
                />
              }
              {activeTab === PROFILE_TAB
                && <ProfileTab
                  currentUser={this.props.currentUser}
                  formValues={this.props.profileValues}
                  uploadPic={this.props.uploadPic}
                />
              }
            </Scrollbar>
          </div>
        </div>
      </Modal>
    );
  }
}

ProfileSettings.propTypes = {

  saveSettings: PropTypes.func.isRequired,
  requestChangeAuthMethod: PropTypes.func.isRequired,
  acceptChangeAuthMethod: PropTypes.func.isRequired,

  uploadPic: PropTypes.func.isRequired,
  requestChangePassword: PropTypes.func.isRequired,
  changePassword: PropTypes.func.isRequired,
  changeTheme: PropTypes.func.isRequired,
  settings: ImmutableProptypes.record,
  formValues: ImmutableProptypes.record,
  resetValues: ImmutableProptypes.map,
  appearanceFormValues: ImmutableProptypes.record,
  isLoading: PropTypes.bool.isRequired,
  resetLoading: PropTypes.bool.isRequired,
  token: PropTypes.string,
  resetToken: PropTypes.string,
  tokensForAuthMethods: ImmutableProptypes.map,
  profileValues: ImmutableProptypes.map,
  currentUser: ImmutableProptypes.record,
};


const mapStateToProps = createStructuredSelector({
  settings: makeSelectSettings(),
  formValues: makeSelectFormValues(),
  resetValues: makeSelectResetFormValues(),
  profileValues: makeSelectProfileFormValues(),
  appearanceFormValues: makeSelectAppearanceFormValues(),
  currentUser: makeSelectCurrentUser(),
  resetLoading: selectResetLoadin,
  resetToken: selectResetToken,
  isLoading: selectLoading,
  token: selectToken,

  tokensForAuthMethods: selectAuthMethodsForTokens,
});

const mapDispatchToProps = (dispatch) => ({
  changeTheme: (theme) => dispatch(changeTheme({ theme })),
  saveSettings: (settings) => dispatch(saveSettings({ settings })),

  requestChangeAuthMethod: (authMethodId) => dispatch(requestChangeAuthMethodAction(authMethodId)),
  acceptChangeAuthMethod: (code) => dispatch(acceptChangeAuthMethodAction(code)),
  requestChangePassword: (payload) => dispatch(requestChangePassword(payload)),
  changePassword: (payload) => dispatch(changePassword(payload)),

  uploadPic: (payload) => dispatch(uploadPic(payload)),
});

const Connected = connect(mapStateToProps, mapDispatchToProps)(ProfileSettings);

export default WithInjection({ reducer, sagas, key: 'profileSettings' })(Connected);
