import React from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import { reduxForm } from 'redux-form/immutable';
import { throttle } from 'lodash';
import { createStructuredSelector } from 'reselect';
import { intOnly, parseNumeral } from 'Utils/form/normalization';
import volumeFormat from 'unity-frontend-core/utils/amountFormat';
import UserAccountSettings from 'unity-frontend-core/containers/UserAccountSettings';
import { INSTRUMENT_CATEGORIES, USER_SETTINGS } from 'unity-frontend-core/Models/Enums';
import UTabButton from 'Components/UTabButton';

import { H2, Italic } from '../../components/Typography';
import { Input } from '../../components/ReduxFields';
import styles from './style.css';
import InstrumentCategoryIcon from '../../components/InstrumentCategoryIcon';
import { allValuesExists } from '../../utils/helpers';

class AmountsForm extends React.PureComponent {
  render() {
    return (
      <form>
        <div styleName="row-group instruments" className="select-up">
          {Object.keys(INSTRUMENT_CATEGORIES)
            .filter((cat) => (!INSTRUMENT_CATEGORIES[cat].disabled && INSTRUMENT_CATEGORIES[cat].isAvailableForUserTrade))
            .map((cat) => {
              const values = this.props.initialValues.getIn([cat, 'value', 'value']) ? this.props.initialValues.getIn([cat, 'value', 'value']) : false;
              return values ? (
                <div styleName="instrument" key={cat}>
                  <div styleName="instrument-label">
                    <InstrumentCategoryIcon fontSize="13px" category={cat} /><span>{INSTRUMENT_CATEGORIES[cat].name}</span>
                  </div>
                  <div styleName="values">
                    {values.map((value, i) => (
                      <div styleName="value" key={`${cat}.value.value.${i}`}>
                        <span styleName="amount"><UTabButton active className={styles.amountTab} label={volumeFormat(this.props.formValues && this.props.formValues.getIn([cat, 'value', 'value', i]))} /></span>
                        <Input
                          name={`${cat}.value.value.${i}`}
                          format={intOnly}
                        />
                      </div>
                    ))}
                  </div>
                </div>) : null;
            })
          }
        </div>
      </form>
    );
  }
}


AmountsForm.propTypes = {
  initialValues: PropTypes.object,
  formValues: PropTypes.object,
};

const ReduxForm = reduxForm({
  form: 'tradingInstrumentsAmountsForm',
  onChange: throttle((values, _, props) => {
    values.forEach((value, instrumentCategory) => {
      const resultValues = value.getIn(['value', 'value']).map((val) => parseInt(parseNumeral(val) || 0, 10)).toArray();
      if (!resultValues.includes(NaN)) {
        const result = {
          systemPropertyName: USER_SETTINGS.INSTRUMENT_AMOUNTS,
          value: { value: resultValues },
          instrumentCategory,
        };
        props.setWidgetSetting(result);
      }
    });
  }, 500),
})(
  AmountsForm
);

class AmountsContainer extends React.PureComponent {
  componentDidMount() {
    Object.keys(INSTRUMENT_CATEGORIES).forEach((cat) => {
      if (!INSTRUMENT_CATEGORIES[cat].disabled && INSTRUMENT_CATEGORIES[cat].isAvailableForUserTrade) {
        this.props.loadWidgetSetting({
          name: USER_SETTINGS.INSTRUMENT_AMOUNTS,
          instrumentCategory: cat,
        });
      }
    });
  }

  render() {
    const { values = false } = this.props;
    const availableCategories = Object.keys(INSTRUMENT_CATEGORIES).filter((cat) => (!INSTRUMENT_CATEGORIES[cat].disabled && INSTRUMENT_CATEGORIES[cat].isAvailableForUserTrade));
    return allValuesExists(values, availableCategories) ? (
      <div styleName="form">
        <div styleName="row with-border">
          <div styleName="row-group">
            <H2>Trading amounts</H2>
            <Italic cleanColor>
              Set preferred trading amounts for each of the following
              trading instruments categories.
            </Italic>
          </div>
          <ReduxForm
            initialValues={values}
            formValues={values}
            setWidgetSetting={this.props.setWidgetSetting}
          />
        </div>
      </div>
    ) : null;
  }
}

AmountsContainer.propTypes = {
  values: PropTypes.object,
  loadWidgetSetting: PropTypes.func.isRequired,
  setWidgetSetting: PropTypes.func.isRequired,
};


const mapStateToProps = createStructuredSelector({
  values: (_, props) => props.widget,
});

export default UserAccountSettings(
  connect(mapStateToProps)(AmountsContainer),
  USER_SETTINGS.INSTRUMENT_AMOUNTS
);
