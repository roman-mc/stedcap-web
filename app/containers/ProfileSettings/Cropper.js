import React from 'react';
import PropTypes from 'prop-types';
import Crop from 'react-cropper';
import 'cropperjs/dist/cropper.css';

export default class Cropper extends React.PureComponent {
  render() {
    return <Crop
      {...this.props}
      ref={this.props.cRef}
    />;
  }
}
Cropper.propTypes = {
  cRef: PropTypes.func.isRequired,
};
