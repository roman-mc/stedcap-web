import React from 'react';
import PropTypes from 'prop-types';
import { reduxForm } from 'redux-form/immutable';
import ImmutableProptypes from 'react-immutable-proptypes';
import IconResend from 'Svg/icon-resend.svg';
import {
  Button,
  InlineInput,
} from 'Components/ReduxFields';

import { requireValidate, minLength } from 'Utils/form/validation';

import styles from './style.css';
import { H2 } from '../../components/Typography';


function ResetPasswordForm(props) {
  const {
    invalid,
    formValues,
    requestChangePassword,
    changePassword,
    resetToken,
    resetLoading,
  } = props;

  const disablePassFields = !(formValues && formValues.get('oldPassword') && formValues.get('oldPassword').length > 5);
  const disableConfirm = !(formValues && formValues.get('newPassword') === formValues.get('repeatedPassword'));
  const disableCode = !(formValues && formValues.get('code') && formValues.get('code').length > 2);

  return (
    <form styleName="form">
      <div styleName="row">
        <H2>Change password</H2>
        <div styleName="row-group">
          <div styleName="reset">
            <InlineInput
              name="oldPassword"
              type="password"
              text="Current password"
              disabled={resetToken}
              placeholder="Fill in to change"
              validate={[requireValidate, minLength(6)]}
            />
            <InlineInput
              name="newPassword"
              text="New password"
              type="password"
              placeholder="Fill in new password"
              disabled={disablePassFields || resetToken}
              validate={[requireValidate, minLength(6)]}
            />
            <InlineInput
              name="repeatedPassword"
              text="Confirm new password"
              type="password"
              placeholder="Repeat new password"
              disabled={disablePassFields || resetToken}
              validate={[requireValidate, minLength(6)]}
            />
            {resetToken &&
              <InlineInput
                name="code"
                placeholder="Code"
                text={<div styleName="resend">
                  <span className={styles.text}>
                  SMS code
                  </span>
                  <button
                    className={styles.resendCodeButton}
                    type="button"
                    onClick={requestChangePassword}
                  >
                    <IconResend className={`${styles.resendCodeButtonIcon}`} />
                    <span className={`${styles.resendCodeButtonText}`}>Resend</span>
                  </button>
                </div>}
              />
            }
            <div styleName="right">
              <Button
                styleType="primary"
                disabled={invalid || resetLoading || disableConfirm || (resetToken && disableCode)}
                onClick={resetToken ? changePassword : requestChangePassword}
              >
                {resetToken ? 'Change password' : 'Confirm with code'}
              </Button>
            </div>
          </div>
        </div>
      </div>
    </form>
  );
}

ResetPasswordForm.propTypes = {
  invalid: PropTypes.bool.isRequired,
  formValues: ImmutableProptypes.record,
  requestChangePassword: PropTypes.func,
  changePassword: PropTypes.func,
  resetToken: PropTypes.string,
  resetLoading: PropTypes.bool,
};

export default reduxForm({
  form: 'resetPasswordForm',
  enableReinitialize: true, // Because of a settings change
})(ResetPasswordForm);
