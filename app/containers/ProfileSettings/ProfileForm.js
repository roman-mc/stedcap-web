import React from 'react';
import PropTypes from 'prop-types';
import { reduxForm } from 'redux-form/immutable';
import Flag from 'Components/Flag';

import {
  InlineInput,
} from 'Components/ReduxFields';

import styles from './style.css';
import { H2 } from '../../components/Typography';


function ProfileForm({ formValues }) {
  return (
    <form styleName="form">
      <div styleName="row">
        <H2>Personal information</H2>
        <div styleName="row-group">
          <div styleName="reset">
            <InlineInput
              name="firstName"
              text="First name"
              disabled
            />
            <InlineInput
              name="lastName"
              text="Last name"
              disabled
            />
            <InlineInput
              name="email"
              text="Email"
              disabled
            />
            <InlineInput
              name="phone"
              text="Mobile phone"
              disabled
              inputClassName={styles['left-padding']}
              leftIcon={<Flag
                phone={(formValues && formValues.get('phone')) ? formValues.get('phone') : ''}
                className={styles.flag}
              />}
            />
          </div>
        </div>
      </div>
    </form>
  );
}

ProfileForm.propTypes = {
  formValues: PropTypes.object,
};

export default reduxForm({
  form: 'profileForm',
  enableReinitialize: true,
})(ProfileForm);
