import { defineMessages } from 'react-intl';

export default defineMessages({
  header: {
    id: 'app.containers.ProfileSettings.header',
    defaultMessage: 'Settings',
  },
  theme: {
    id: 'app.containers.ProfileSettings.theme',
    defaultMessage: 'Theme',
  },
  autoLogout: {
    id: 'app.containers.ProfileSettings.autoLogout',
    defaultMessage: 'Automatic logout',
  },
  currency: {
    id: 'app.containers.ProfileSettings.currency',
    defaultMessage: 'I prefer trading in',
  },
  currencyTitle: {
    id: 'app.containers.ProfileSettings.currencyTitle',
    defaultMessage: 'Trading instrument categories',
  },
  cancel: {
    id: 'app.containers.ProfileSettings.cancel',
    defaultMessage: 'Cancel',
  },
  save: {
    id: 'app.containers.ProfileSettings.save',
    defaultMessage: 'Save',
  },
  themeDark: {
    id: 'app.containers.ProfileSettings.themeDark',
    defaultMessage: 'Dark',
  },
  themeLight: {
    id: 'app.containers.ProfileSettings.themeLight',
    defaultMessage: 'Light',
  },
  logoutTime: {
    id: 'app.containers.ProfileSettings.logoutTime',
    defaultMessage: '{value} minutes',
  },
  colorMode: {
    id: 'app.containers.ProfileSettings.colorMode',
    defaultMessage: 'Colorized quotes',
  },
  authMethod: {
    id: 'app.containers.ProfileSettings.authMethod',
    defaultMessage: '2-factor authentication',
  },
  appearance: {
    id: 'app.containers.ProfileSettings.appearance',
    defaultMessage: 'Color appearance',
  },
});
