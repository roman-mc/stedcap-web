import React from 'react';
import PropTypes from 'prop-types';
import { reduxForm } from 'redux-form/immutable';
import { FormattedMessage, injectIntl, intlShape } from 'react-intl';
import ImmutableProptypes from 'react-immutable-proptypes';

import {
  BtnGroup,
  Button,
  InlineSelect,
  Input,
} from 'Components/ReduxFields';

import { requireValidate } from 'Utils/form/validation';
import { AUTH_METHODS_OPTIONS } from 'unity-frontend-core/Models/Enums';
import IconResend from 'Svg/icon-resend.svg';
import messages from './messages';

import styles from './style.css';
import { H2 } from '../../components/Typography';

const availableLogoutTimes = [5, 10, 15, 30, 60];
const authOptions = AUTH_METHODS_OPTIONS.reverse();

function PlatformSettingsForm(props) {
  const {
    intl: { formatMessage },
    initialValues,
    formValues,
    requestChangeAuthMethod,
    acceptChangeAuthMethod,
    isLoading,
    tokensForAuthMethods,
  } = props;

  // Here is "tokensForAuthMethods" unnecessary, but further when there are 2+ auth methods this is necessary
  const canAcceptCode = Boolean(tokensForAuthMethods.get(Number(formValues.get('authMethod'))));

  const codeChangeFunctionalityDisabled = Number(formValues.authMethod) === initialValues.authMethod;
  const isInputCodeValid = formValues.smsCode && formValues.smsCode.length >= 3;

  return (
    <form styleName="form">
      <div styleName="row with-border">
        <H2>Authentication</H2>
        <div styleName="row-group">
          <BtnGroup
            name="authMethod"
            options={authOptions}
            text={<FormattedMessage {...messages.authMethod} />}
            onChange={(_, value) => requestChangeAuthMethod(value)}
          />
        </div>
        <div styleName="row-group textContainer">
          <div className={styles.codeTextContainer}>
            <span className={codeChangeFunctionalityDisabled ? styles.textDisabled : styles.text}>
                  SMS code
            </span>
            <button
              className={styles.resendCodeButton}
              type="button"
              onClick={() => requestChangeAuthMethod()}
              disabled={codeChangeFunctionalityDisabled || isLoading}
            >
              <IconResend className={`${styles.resendCodeButtonIcon}`} />
              <span className={`${styles.resendCodeButtonText}`}>Resend</span>
            </button>
          </div>

          <div className={styles.changeCodeContainer}>
            {/* These two input and button are skew */}
            <Input
              name="smsCode"
              placeholder="Enter code"
              className={codeChangeFunctionalityDisabled ? styles.inputDisabled : undefined}
              disabled={codeChangeFunctionalityDisabled || isLoading}
              maxlength={4}
            />
            <Button
              styleType="primary"
              onClick={acceptChangeAuthMethod}
              subClass={styles.changeCodeButton}
              disabled={codeChangeFunctionalityDisabled || !isInputCodeValid || !canAcceptCode || isLoading}
            >
              Change
            </Button>
          </div>
        </div>
      </div>
      <div styleName="row with-border">
        <H2>Automatic logout</H2>
        <div styleName="row-group">
          <InlineSelect
            disableClearable
            name="logoutMin"
            options={availableLogoutTimes.map((t) => ({ value: t, label: formatMessage(messages.logoutTime, { value: t }) }))}
            text={<FormattedMessage {...messages.autoLogout} />}
            validate={[requireValidate]}
            onChange={(e, v) => {
              props.saveSettings({ ...initialValues.delete('smsCode').toJS(), logoutMin: v });
            }}
          />
        </div>
      </div>
    </form>
  );
}

PlatformSettingsForm.propTypes = {
  saveSettings: PropTypes.func.isRequired,
  initialValues: ImmutableProptypes.record,
  formValues: ImmutableProptypes.record,

  intl: intlShape,
  requestChangeAuthMethod: PropTypes.func.isRequired,
  acceptChangeAuthMethod: PropTypes.func.isRequired,

  isLoading: PropTypes.bool.isRequired,
  tokensForAuthMethods: ImmutableProptypes.map,
};

export default reduxForm({
  form: 'profileSettingsForm',
  enableReinitialize: true, // Because of a settings change
})(injectIntl(PlatformSettingsForm));
