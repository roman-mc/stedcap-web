import { List } from 'immutable';
import { RNS_SHOW_NOTIFICATION, RNS_HIDE_NOTIFICATION } from 'react-notification-system-redux/lib/const';

export default function Notifications(state = List([]), action = {}) {
  switch (action.type) {
    case RNS_HIDE_NOTIFICATION:
      return state.filter((notification) => notification.uid !== action.uid);
    case RNS_SHOW_NOTIFICATION: {
      const { type, ...rest } = action; // eslint-disable-line no-unused-vars
      return state.push({ ...rest, uid: action.uid });
    }
    default:
      return state;
  }
}
