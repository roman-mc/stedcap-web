/*
 * TradingPage Messages
 *
 * This contains all the text for the TradingPage component.
 */
import { defineMessages } from 'react-intl';

export default defineMessages({
  TEST_MESSAGE: {
    id: 'app.notifications.testMessage',
    defaultMessage: 'This is TradingPage container !',
  },
});
