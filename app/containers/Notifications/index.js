import React from 'react';
import ImmutableProptypes from 'react-immutable-proptypes';
import { createStructuredSelector } from 'reselect';
import { connect } from 'react-redux';
import ReduxNotifications from 'react-notification-system-redux';
import { FormattedMessage } from 'react-intl';

import makeSelectNotifications from './selectors';
import * as actions from './actions';
import messages from './messages';

import './style.css';

const style = {
  NotificationItem: { // Override the notification item
    DefaultStyle: { // Applied to every notification, regardless of the notification level
      margin: '10px 5px 2px 1px',
      borderRadius: '8px',
      backgroundColor: '#2a2a2a',
      boxShadow: '0 2px 6px 2px rgba(0, 0, 0, 0.5)',
      fontSize: '16px',
      lineHeight: 1.31,
      textAlign: 'left',
      color: '#ffffff',
      border: 0,
    },
  },
  Dismiss: {
    DefaultStyle: {
      display: 'none',
    },
  },
};


function Notifications(props) {
  const notifications = props.notifications
    .toArray()
    .map(({
      message, header, detailsMessage, values, detailsValues, ...rest
    }) => ({
      ...rest,
      message:
  <span>
    <span className="notification__icon notification__icon__error"></span>
    {header && <div>
      <FormattedMessage {...header} />
    </div>}
    <span>
      <FormattedMessage {...message} values={{ ...values, ...detailsValues }} />
    </span>
    {detailsMessage && <div styleName="details"><FormattedMessage {...detailsMessage} values={detailsValues} /> </div>}
  </span>,
    }));

  return (
    <div>
      <ReduxNotifications style={style} notifications={notifications} />
    </div>
  );
}

Notifications.propTypes = {
  notifications: ImmutableProptypes.list,
};

const mapStateToProps = createStructuredSelector({
  notifications: makeSelectNotifications(),
});

export default connect(mapStateToProps)(Notifications);

export {
  actions,
  messages,
};
