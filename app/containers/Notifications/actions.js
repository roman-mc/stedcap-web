import Notifications from 'react-notification-system-redux';

const defaultOpts = {
  position: 'tr',
  autoDismiss: 5,
};

const generateUid = () => Math.round(Math.random() * 1000000);

export const success = (message, opts = {}) => {
  const { values = {} } = message;
  return Notifications.success({
    ...defaultOpts,
    ...opts,
    message: message.defaultMessage,
    header: message.defaultHeader,
    uid: generateUid(),
    values: {
      ...values,
      orderRequestAmount: values.orderRequest ? values.orderRequest.amount : undefined,
      symbolDescription: values.orderRequest ? values.orderRequest.symbolDescription : undefined,
      expectedPrice: values.orderRequest ? values.orderRequest.expectedPrice : undefined,
    },
    detailsMessage: message.detailsMessage,
    detailsValues: message.detailsValues,
  });
};
export const error = (message, opts = {}) => Notifications.error({
  ...defaultOpts, ...opts, message: message.defaultMessage, uid: generateUid(), values: message.values, detailsMessage: message.detailsMessage, detailsValues: message.detailsValues,
});
