const makeSelectNotifications = () => (state) => state.get('notifications');
export default makeSelectNotifications;
