import React from 'react';
import PropTypes from 'prop-types';

import Main from '../../components/Main';
import Header from '../Header';

export default function Wrapper(props) {
  const { activeLinkName, withoutFooter, children } = props;

  return (
    <div className="wrapper">
      <Header activeLinkName={activeLinkName} />
      <Main withoutFooter={withoutFooter}>
        {children}
      </Main>
      <div id="dragula-mirror-container"></div>
    </div>
  );
}


Wrapper.propTypes = {
  children: PropTypes.node,
  activeLinkName: PropTypes.string,
  withoutFooter: PropTypes.bool,
};
