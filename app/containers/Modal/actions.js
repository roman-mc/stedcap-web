import { createAction } from 'redux-act';

export const openModal = createAction('app/containers/Modal/OPEN_MODAL');
export const closeModal = createAction('app/containers/Modal/CLOSE_MODAL');
