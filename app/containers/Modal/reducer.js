import { Map } from 'immutable';
import { createReducer } from 'redux-act';

import { openModal, closeModal } from './actions';

const modalState = new Map({
  opened: false,
});

const initialState = new Map({
});

export default createReducer({
  [openModal]: (state, { modalId, payload }) => state
    .set(modalId, modalState)
    .setIn([modalId, 'opened'], true)
    .setIn([modalId, 'payload'], payload),
  [closeModal]: (state, { modalId }) => state.set(modalId, null),
}, initialState);
