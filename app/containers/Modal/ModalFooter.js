import React from 'react';
import PropTypes from 'prop-types';

import './style.css';

export default class ModalFooter extends React.PureComponent {
  render() {
    const { children } = this.props;
    return (
      <div styleName="footer">{children}</div>
    );
  }
}


ModalFooter.propTypes = {
  children: PropTypes.node.isRequired,
};
