import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { createStructuredSelector } from 'reselect';
import WithInjection from 'unity-frontend-core/containers/WithInjection';

import Popup from '../../components/Popup';
import ModalFooter from './ModalFooter';

import * as actions from './actions';
import { makeSelectIsModalOpened, makeSelectModalPayload } from './selectors';


import reducer from './reducer';

import './style.css';


class Modal extends React.PureComponent {
  closeModal = () => {
    if (this.props.onClose) {
      this.props.onClose();
    }
    this.props.closeModal(this.props.modalId);
  }

  render() {
    const { modalPayload, renderFooter, children, isModalOpened, modalId, renderHeader, portal = true, offset } = this.props;

    const childs = modalPayload && Object.keys(modalPayload).length > 0
      ? React.cloneElement(children, { ...modalPayload })
      : children;

    return isModalOpened ? (
      <Popup popupKey={modalId} header={renderHeader(modalPayload)} portal={portal} removePopup={this.closeModal} offset={offset}>
        {childs}
        {renderFooter && <ModalFooter>{renderFooter(this.closeModal)}</ModalFooter>}
      </Popup>
    ) : null;
  }
}

const mapDispatchToProps = (dispatch) => ({
  closeModal: (modalId) => dispatch(actions.closeModal({ modalId })),
});

const mapStateToProps = createStructuredSelector({
  isModalOpened: makeSelectIsModalOpened(),
  modalPayload: makeSelectModalPayload(),
});

Modal.propTypes = {
  children: PropTypes.node.isRequired,
  modalId: PropTypes.string.isRequired,
  renderHeader: PropTypes.func.isRequired,
  closeModal: PropTypes.func.isRequired,
  isModalOpened: PropTypes.bool,
  portal: PropTypes.bool,
  renderFooter: PropTypes.func,
  modalPayload: PropTypes.object,
  offset: PropTypes.array,
  onClose: PropTypes.func,
};


const Connected = connect(mapStateToProps, mapDispatchToProps)(Modal);

export default WithInjection({ reducer, key: 'modal' })(Connected);

export {
  actions,
};
