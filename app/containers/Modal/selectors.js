import { createSelector } from 'reselect';

const makeSelectSpace = () => (state) => state.get('modal');
const makeSelectModalId = () => (_, props) => props.modalId;

export const makeSelectModal = () => createSelector(
  makeSelectSpace(),
  makeSelectModalId(),
  (space, modalId) => space.get(modalId),
);

export const makeSelectIsModalOpened = () => createSelector(
  makeSelectModal(),
  (modalSpace) => modalSpace && modalSpace.get('opened'),
);

export const makeSelectModalPayload = () => createSelector(
  makeSelectModal(),
  (modalSpace) => modalSpace && modalSpace.get('payload'),
);
