/**
 *
 * App
 *
 * This component is the skeleton around the actual pages, and should only
 * contain code that should be seen on all pages. (e.g. navigation bar)
 */

import React from 'react';
import PropTypes from 'prop-types';
import Helmet from 'react-helmet';
import { injectIntl, intlShape } from 'react-intl';
import Raven from 'raven-js';

import WithInjection from 'unity-frontend-core/containers/WithInjection';

import config from '../../config';

import Notifications from '../Notifications';
import Blocker from './blocker';

import messages from './messages';

import sagas from './sagas';


import './style.css';


export class App extends React.PureComponent {
  componentDidCatch(e) {
    Raven.captureException(e);
  }
  render() {
    const { children, intl: { formatMessage } } = this.props;
    return (
      <div>
        <Helmet
          titleTemplate={formatMessage(messages.pageTitle, { projectTitle: config.projectTitle })}
          defaultTitle={formatMessage(messages.defaultPageTitle, { projectTitle: config.projectTitle })}
          meta={[
            { name: 'description', content: formatMessage(messages.pageDescription, { projectTitle: config.projectTitle }) },
          ]}
        />
        <Blocker />
        <div styleName="app-wrapper">
          {React.Children.toArray(children)}
        </div>
        <Notifications />
      </div>
    );
  }
}


App.propTypes = {
  children: PropTypes.node,
  intl: intlShape,
};

export default injectIntl(WithInjection({ sagas })(App));
