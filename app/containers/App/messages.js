import { defineMessages } from 'react-intl';

export default defineMessages({
  pageTitle: {
    id: 'app.containers.App.pageTitle',
    defaultMessage: '%s - {projectTitle}',
  },
  defaultPageTitle: {
    id: 'app.containers.App.defaultPageTitle',
    defaultMessage: '{projectTitle}',
  },
  pageDescription: {
    id: 'app.containers.App.pageDescription',
    defaultMessage: '{projectTitle}',
  },
  blockerText: {
    id: 'app.containers.App.blockerText',
    defaultMessage: `This site is not reachable from mobile devices.<br />
                Please download our mobile application from<br />
                market to achieve the best mobile experience.`,
  },
});
