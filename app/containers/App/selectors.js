/**
 * The global state selectors
 */

import { createSelector } from 'reselect';
import { Map } from 'immutable';

export const selectGlobal = (state) => state.get('global');

export const makeSelectCurrentUser = () => createSelector(
  selectGlobal,
  (globalState) => globalState.get('currentUser')
);

export const makeSelectBalance = () => createSelector(
  selectGlobal,
  (globalState) => globalState.get('balance')
);

export const makeSelectCurrentUserStatus = () => createSelector(
  makeSelectCurrentUser(),
  (user) => (user || new Map()).get('status')
);

export const makeSelectToken = () => createSelector(
  selectGlobal,
  (globalState) => globalState.get('token')
);

export const makeSelectTemporaryToken = () => createSelector(
  selectGlobal,
  (globalState) => globalState.get('temporaryToken')
);

export const makeSelectVerifyToken = () => createSelector(
  selectGlobal,
  (globalState) => globalState.get('verifyToken')
);

export const makeSelectLoading = () => createSelector(
  selectGlobal,
  (globalState) => globalState.get('loading')
);

export const makeSelectError = () => createSelector(
  selectGlobal,
  (globalState) => globalState.get('error')
);

export const makeSelectTheme = () => createSelector(
  makeSelectCurrentUser(),
  (user) => user && user.settings ? user.settings.theme : 'dark-theme',
);

export const makeSelectLogoutTime = () => createSelector(
  makeSelectCurrentUser(),
  (user) => user && user.settings ? user.settings.logoutMin * 1000 * 60 : 10 * 1000 * 60,
);

export const makeSelectForms = (state) => state.get('form');

export const makeSelectLocationState = () => {
  let prevRoutingState;
  let prevRoutingStateJS;

  return (state) => {
    const routingState = state.get('route'); // or state.route

    if (!routingState.equals(prevRoutingState)) {
      prevRoutingState = routingState;
      prevRoutingStateJS = routingState.toJS();
    }

    return prevRoutingStateJS;
  };
};
