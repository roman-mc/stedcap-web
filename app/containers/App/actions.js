/*
 * App Actions
 *
 * Actions change things in your application
 * Since this boilerplate uses a uni-directional data flow, specifically redux,
 * we have these actions which are the only way your application interacts with
 * your application state. This guarantees that your state is up to date and nobody
 * messes it up weirdly somewhere.
 *
 * To add a new Action:
 * 1) Import your constant
 * 2) Add a function like this:
 *    export function yourAction(var) {
 *        return { type: YOUR_ACTION_CONSTANT, var: var }
 *    }
 */

import { createAction } from 'redux-act';

import { userLoggedIn, userLoggedOut } from 'unity-frontend-core/actions';

import {
  USER_TOKEN_RECEIVED,
  USER_RESET,
  TEMPORARY_TOKEN_RECEIVED,
  VERIFY_TOKEN_RECEIVED,
  CHANGE_THEME,
} from './constants';


export const userDataReceived = createAction('app/containers/App/USER_DATA_RECEIVED');
export const userInfoRefreshed = createAction('app/containers/App/USER_INFO_REFRESHED');
export const userBalanceRefreshed = createAction('app/containers/App/USER_BALANCE_REFRESHED');

export const userTokenReceived = createAction(USER_TOKEN_RECEIVED);
export const verifyTokenReceived = createAction(VERIFY_TOKEN_RECEIVED);
export const temporaryTokenReceived = createAction(TEMPORARY_TOKEN_RECEIVED);
export const userReset = createAction(USER_RESET);
export const changeTheme = createAction(CHANGE_THEME);

export { userLoggedIn, userLoggedOut };
