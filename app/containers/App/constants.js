/*
 * AppConstants
 * Each action has a corresponding type, which the reducer knows and picks up on.
 * To avoid weird typos between the reducer and the actions, we save them as
 * constants here. We prefix them with 'yourproject/YourComponent' so we avoid
 * reducers accidentally picking up actions they shouldn't.
 *
 * Follow this format:
 * export const YOUR_ACTION_CONSTANT = 'yourproject/YourContainer/YOUR_ACTION_CONSTANT';
 */

export const DEFAULT_LOCALE = 'en';

export const USER_TOKEN_RECEIVED = 'app/container/App/USER_TOKEN_RECEIVED';
export const USER_RESET = 'app/container/App/USER_RESET';
export const TEMPORARY_TOKEN_RECEIVED = 'app/container/App/TEMPORARY_TOKEN_RECEIVED';
export const VERIFY_TOKEN_RECEIVED = 'app/container/App/VERIFY_TOKEN_RECEIVED';
export const CHANGE_THEME = 'app/TradingPage/CHANGE_THEME';
