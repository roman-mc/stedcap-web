/**
 *
 * App
 *
 * This component is the skeleton around the actual pages, and should only
 * contain code that should be seen on all pages. (e.g. navigation bar)
 */

import React from 'react';
import { FormattedHTMLMessage } from 'react-intl';
import AppStoreIcon from '../../assets/svg/appstore.svg';


import config from '../../config';

import messages from './messages';

import './style.css';

export default function Blocker() {
  return (
    <div styleName="blocker">
      <div styleName="blocker-logo">
        <img src={config.logoLarge} alt={config.companyName} />
      </div>
      <div styleName="blocker-text">
        <FormattedHTMLMessage {...messages.blockerText} />
      </div>
      <a
        href="https://itunes.apple.com/us/app/stone-edge-trader/id1342149731"
        target="_blank"
        styleName="blocker-ios"
      >
        <AppStoreIcon />
      </a>
    </div>
  );
}
