/*
 * AppReducer
 *
 * The reducer takes care of our data. Using actions, we can change our
 * application state.
 * To add a new action, add it to the switch statement in the reducer function
 *
 * Example:
 * case YOUR_ACTION_CONSTANT:
 *   return state.set('yourStateVariable', true);
 */

import { fromJS } from 'immutable';
import { createReducer } from 'redux-act';

import User from 'unity-frontend-core/Models/User';
import MultiAccountBalance from 'unity-frontend-core/Models/MultiAccountBalance';

import {
  userLoggedIn,
  userLoggedOut,
  userTokenReceived,
  userReset,
  temporaryTokenReceived,
  verifyTokenReceived,
  changeTheme,
  userInfoRefreshed,
  userDataReceived,
  userBalanceRefreshed,
} from './actions';

// The initial state of the App
const initialState = fromJS({
  loading: false,
  error: false,
  currentUser: null,
  token: null,
  temporaryToken: null,
  verifyToken: null,
  networkDisconnected: false,
  balance: null,
});

export default createReducer({
  [userLoggedIn]: (state, payload) => state.set('currentUser', new User(payload)),
  [userDataReceived]: (state, { user }) => state.set('currentUser', new User(user)),
  [userInfoRefreshed]: (state, userInfo) => state.update('currentUser', (u) => u.setInfo(userInfo)),
  [userBalanceRefreshed]: (state, balance) => state.set('balance', new MultiAccountBalance(balance)),
  [userTokenReceived]: (state, { token }) => state.set('token', token).set('temporaryToken', null),

  [temporaryTokenReceived]: (state, { token }) => state.set('temporaryToken', token),
  [verifyTokenReceived]: (state, { token }) => state.set('verifyToken', token),
  [changeTheme]: (state, payload) => state.setIn(['currentUser', 'settings', 'theme'], payload.theme),

  [userReset]: (state) => state.set('currentUser', null),
  [userLoggedOut]: (state) => state.set('token', null),
}, initialState);
