import { takeEvery, call, put, select } from 'redux-saga/effects';
import Raven from 'raven-js';

import * as userApi from 'unity-frontend-core/api/authApi';
import { apiError, networkConnected } from 'unity-frontend-core/actions';
import { loadInstruments } from 'unity-frontend-core/containers/InstrumentResolver/actions';
import { loadPositions } from 'unity-frontend-core/containers/Positions/actions';

import { loadNotifications } from '../NotificationsCenter/actions';

/* TODO: remove dat shit */
import { loadOrders } from '../../pages/TradingPage/containers/Positions/actions';
import { actions as notifyActions } from '../Notifications';

import { REST_ERRORS } from '../../model/enums';


import {
  userLoggedIn,
  userTokenReceived,
  userReset,
  changeTheme,
} from './actions';

import { makeSelectCurrentUser } from './selectors';

import {
  setUserData,
  getUserData,
  removeUserData,
  getToken,
  setToken,
} from '../../api/authorizationApi';

export function* authorizationHandler({ payload }) {
  yield call(setUserData, payload);
}

export function* authorizationSaga() {
  const authData = yield call(getUserData);

  if (authData && authData.id) {
    yield put(userLoggedIn(authData));
  }

  const token = yield call(getToken);
  if (token) {
    yield call(setToken, token); // set to sharedSettings
    yield put(userTokenReceived({ token, skipLs: true })); // Just save token to the store
  }
  yield takeEvery(userLoggedIn.toString(), authorizationHandler);
}


export function* userTokenHandler({ payload: { token, skipLs = false } }) {
  if (!skipLs) { // Do not save temporary token from 1st login/registration step and on app load
    yield call(setToken, token);
  }
}

export function* userTokenSaga() {
  yield takeEvery(userTokenReceived.toString(), userTokenHandler);
}

export function* userResetHandler() {
  yield call(removeUserData);
}

export function* userResetSaga() {
  yield takeEvery(userReset.toString(), userResetHandler);
}

function* changeThemeHandler({ payload: { save = false } }) {
  if (save) {
    const user = yield select(makeSelectCurrentUser());
    const settings = user.settings.toJS();
    yield call(userApi.saveSettings, settings);
  }
}

function* changeThemeSaga() {
  yield takeEvery(changeTheme.toString(), changeThemeHandler);
}


function* networkReconnectHandler() {
  yield put(loadInstruments());
  yield put(loadPositions({ restart: true }));
  yield put(loadOrders({ restart: true }));
  yield put(loadNotifications());
}

function* networkReconnectSaga() {
  yield takeEvery(networkConnected.toString(), networkReconnectHandler);
}


function* apiErrorHandler({ payload: { error = {}, isException } }) {
  const { status: { code = 0, message } = {} } = error;
  const err = REST_ERRORS[code] || REST_ERRORS[0];
  yield put(notifyActions.error({ defaultMessage: err, values: { message: message || error } }));

  if (isException) {
    Raven.captureException(error);
  }
}

function* apiErrorSaga() {
  yield takeEvery(apiError.toString(), apiErrorHandler);
}


export default [
  userTokenSaga,
  authorizationSaga,
  userResetSaga,
  changeThemeSaga,
  networkReconnectSaga,
  apiErrorSaga,
];
