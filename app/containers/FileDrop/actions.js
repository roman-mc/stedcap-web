/*
 *
 * Finances actions
 *
 */
// import { createAction } from 'redux-act';
import createApiAction from 'unity-frontend-core/utils/createAction';

export const uploadFiles = createApiAction('app/containers/FileInput/UPLOAD_FILES');
