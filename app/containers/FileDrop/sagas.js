import { call, put, takeLatest, all } from 'redux-saga/effects';
import Raven from 'raven-js';

import * as documentsApi from 'unity-frontend-core/api/documentsApi';

import { uploadFiles } from './actions';

export function* uploadFileHandler({ payload: { files, cb } }) {
  try {
    const result = yield all(files.map((file) => {
      const data = new FormData();
      data.append('file', file);
      return call(documentsApi.uploadDocument, data);
    }));
    yield call(cb, result);
    yield put(uploadFiles.success(result));
  } catch (e) {
    Raven.captureException(e);
    yield put(uploadFiles.error(e));
  }
}

/**
 * Root saga manages watcher lifecycle
 */
export function* runAllSagas() {
  yield takeLatest(uploadFiles.toString(), uploadFileHandler);
}


export default [
  runAllSagas,
];
