import React from 'react';
import PropTypes from 'prop-types';
import Dropzone from 'react-dropzone';
import { connect } from 'react-redux';

import WithInjection from 'unity-frontend-core/containers/WithInjection';

import sagas from './sagas';

import { uploadFiles } from './actions';

import { } from './style.css';

class FileDrop extends React.PureComponent {
  filesDrop = (acceptedFiles) => {
    this.props.uploadFiles({ files: acceptedFiles, cb: this.handleFilesUploaded });
  }

  handleFilesUploaded = (files) => {
    files.forEach((file) => {
      this.props.fields.push(file.id.toString());
    });
    this.props.setFiles(files);
  }

  render() {
    const {
      placeholder, maxFiles = 5, filesCount = 0, disabled,
    } = this.props;

    return (
      <div styleName="file-upload-dropzone">
        <Dropzone
          style={{
            position: 'relative', width: '100%', border: 0, cursor: 'pointer',
          }}
          onDrop={this.filesDrop}
          disabled={(filesCount >= maxFiles) || disabled}
        >
          {placeholder}
        </Dropzone>
      </div>
    );
  }
}

FileDrop.propTypes = {
  uploadFiles: PropTypes.func.isRequired,
  setFiles: PropTypes.func.isRequired,
  placeholder: PropTypes.oneOfType([PropTypes.string, PropTypes.array, PropTypes.object]),
  fields: PropTypes.any,
  maxFiles: PropTypes.any,
  filesCount: PropTypes.any,
  disabled: PropTypes.bool,
};

const mapDispatchToProps = (dispatch) => ({
  uploadFiles: ({ files, cb }) => dispatch(uploadFiles({ files, cb })),
});

const connected = connect(null, mapDispatchToProps)(FileDrop);

export default WithInjection({ sagas })(connected);
