import React, { Fragment } from 'react';
import ImmutableProptypes from 'react-immutable-proptypes';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { FormattedMessage } from 'react-intl';
import { createStructuredSelector } from 'reselect';
import { Button } from 'Components/GUI';
import InstrumentCategory from 'Components/InstrumentCategory';
import InstrumentCard from 'Components/InstrumentCard';
import InstrumentMarketData from 'unity-frontend-core/containers/InstrumentMarketData';

import { makeSelectRiskGroups } from '../DictionariesResolver/selectors';

import Modal, { actions } from '../Modal';

import InstrumentsTree from './InstrumentsTree';

import messages from './messages';

import './style.css';

const INSTRUMENT_CARD_WIDTH = 380;


class InstrumentSelectorModal extends React.PureComponent {
  state = {
    selectedInstrument: [],
    isCardOpened: false,
  }

  componentDidMount() {
    window.addEventListener('mouseup', this.hideCardVisibility);
  }

  componentWillUnmount() {
    window.removeEventListener('mouseup', this.hideCardVisibility);
  }


  onInstrumentChange = () => {
    this.saveSelection();
    this.props.closeModal(this.props.widgetType);
  }

  setInstrument = (instrumentId) => this.setState({ selectedInstrument: [instrumentId] })

  stopPropagation = (e) => {
    e.stopPropagation();
    e.preventDefault();
  }

  toggleCardVisibility = (e) => {
    e.stopPropagation();
    const { isCardOpened } = this.state;
    this.setState({ isCardOpened: !isCardOpened });
  }

  hideCardVisibility = () => this.setState({ isCardOpened: false })


  infoButtonRef = (ref) => this.infoButton = ref;

  saveSelection = () => {
    this.props.onChange(this.state.selectedInstrument[0]);
  }

  renderInstrumentCard = (instrument, marketData) => {
    const { riskGroups } = this.props;
    const offsetLeft = this.infoButton ? this.infoButton.offsetLeft : 0;
    const position = this.infoButton ? this.infoButton.getBoundingClientRect().x : 0;

    const windowWidth = window.innerWidth;

    return (
      <span
        styleName={`instrument-card-container ${position + INSTRUMENT_CARD_WIDTH > windowWidth ? 'bottom' : ''} `}
        style={{ left: `${offsetLeft}px` }}
      >
        <InstrumentCard marketData={marketData} instrument={instrument} riskGroups={riskGroups} />
      </span>
    );
  }

  renderInstrument = (marketData, _, { instrumentInfo, isCardOpened }) => {
    if (!instrumentInfo) {
      return null;
    }

    return (
      <Fragment>
        <div styleName="instrument-selector-container" onMouseUp={this.toggleCardVisibility}>
          <InstrumentCategory size="32px" instrument={instrumentInfo} fontSize="18px" />
          <span styleName="instrument-selector-instrument">{instrumentInfo.displayName}</span>
          <span styleName={`icon ${isCardOpened ? 'active' : ''}`} className="icon-icon-info"></span>
        </div>
        {isCardOpened && this.renderInstrumentCard(instrumentInfo, marketData)}
      </Fragment>

    );
  }

  render() {
    const { selectedInstrument, isCardOpened } = this.state;
    const { config, widgetType, hideSearchLabel } = this.props;
    const selectedInstrumentId = config && config.get('selectedInstrumentId');

    return (
      <Fragment>
        <div styleName="instrument-selector">
          {selectedInstrumentId
            && <InstrumentMarketData
              withInstrumentInfo
              id={selectedInstrumentId}
              render={this.renderInstrument}
              isCardOpened={isCardOpened}
            />
          }
          <button
            styleName="instrument-selector-button"
            onClick={() => this.props.openModal(widgetType)}
            ref={this.infoButtonRef}
          >
            <i className="icon-icon-search"></i>
            {!hideSearchLabel && <FormattedMessage {...messages.title} />}
          </button>

        </div>
        <Modal
          modalId={`select_instrument_modal_${widgetType}`}
          renderHeader={() => <FormattedMessage {...messages.title} />}
          renderFooter={() => <Fragment>
            <Button big type="tertiary" onClick={() => this.props.closeModal(this.props.widgetType)}>
              <FormattedMessage {...messages.cancel} />
            </Button>
            <Button disabled={selectedInstrument.length === 0} big type="primary" onClick={this.onInstrumentChange}>
              <FormattedMessage {...messages.change} />
            </Button>
          </Fragment>
          }
        >
          <InstrumentsTree
            hideTitle
            selectedInstruments={this.state.selectedInstrument}
            onSelect={this.setInstrument}
          />
        </Modal>
      </Fragment>

    );
  }
}

InstrumentSelectorModal.propTypes = {
  config: ImmutableProptypes.map,
  riskGroups: ImmutableProptypes.map,
  openModal: PropTypes.func.isRequired,
  closeModal: PropTypes.func.isRequired,
  onChange: PropTypes.func.isRequired,
  widgetType: PropTypes.string.isRequired,
  hideSearchLabel: PropTypes.bool,
};

const mapStateToProps = createStructuredSelector({
  riskGroups: makeSelectRiskGroups(),
});

const mapDispatchToProps = (dispatch) => ({
  openModal: (widgetType) => dispatch(actions.openModal({ modalId: `select_instrument_modal_${widgetType}` })),
  closeModal: (widgetType) => dispatch(actions.closeModal({ modalId: `select_instrument_modal_${widgetType}` })),
});

export default connect(mapStateToProps, mapDispatchToProps)(InstrumentSelectorModal);
