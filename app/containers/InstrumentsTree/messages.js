/*
 * CreateWatchlistPopup Messages
 *
 * This contains all the text for the CreateWatchlistPopup component.
 */
import { defineMessages } from 'react-intl';

export default defineMessages({
  noData: {
    id: 'app.containers.InstrumentsTree.noData',
    defaultMessage: 'Instruments will be displayed here',
  },
  availableSymbols: {
    id: 'app.containers.InstrumentsTree.availableSymbols',
    defaultMessage: 'Choose instrument',
  },
  cancel: {
    id: 'app.containers.InstrumentsTree.cancel',
    defaultMessage: 'Cancel',
  },
  change: {
    id: 'app.containers.InstrumentsTree.change',
    defaultMessage: 'Change',
  },
  title: {
    id: 'app.containers.InstrumentsTree.title',
    defaultMessage: 'Change instrument',
  },
});
