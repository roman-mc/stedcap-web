import React from 'react';
import PropTypes from 'prop-types';

import { INSTRUMENT_CATEGORIES } from 'unity-frontend-core/Models/Enums';

import InstrumentCategoryIcon from 'Components/InstrumentCategoryIcon';

import styles from './style.css';
import LoadingIndicator from '../../components/LoadingIndicator';

import { ListItem, CheckMark } from '../../components/List';

export default class InstrumentTreeLevel extends React.PureComponent {
  state = {
    expanded: false,
  }

  get isExpandable() {
    return !this.props.level.instrumentId;
  }

  get isInstrumentSelected() {
    const { selectedInstruments, level } = this.props;
    return !this.isExpandable && selectedInstruments.indexOf(level.instrumentId) >= 0;
  }

  toggleExpand = () => {
    const { level } = this.props;
    const { expanded } = this.state;
    if (level.instrumentId) {
      if (this.isInstrumentSelected) {
        return this.props.onRemove(this.props.level.instrumentId);
      }
      return this.props.onSelect(this.props.level.instrumentId);
    }

    if (!expanded) { // expand logic
      if (!level.children || !level.children.size) {
        this.props.onLevelLoad(level.path.toJS());
      }
    }
    return this.setState({ expanded: !expanded });
  }

  renderChild = () => {
    const { level, nestingLevel } = this.props;

    if (level.children.size === 0) {
      return (
        <div>
          <div
            className={`${styles[`new-instrument-row-level-${nestingLevel + 1}`]}`}
            styleName="new-instrument-row-loading-wrapper"
          >
            <div styleName="new-instrument-row-box">
              <div styleName="new-instrument-row-loading">
                <LoadingIndicator loading />
              </div>
            </div>
          </div>
        </div>
      );
    }

    return (
      level.children.size > 0 && level.children.map((item, index) => this.renderItem({ item, index }))
    );
  }

  renderItem = ({ item, index }) => {
    const { level, onLevelLoad, onSelect, onRemove, selectedInstruments, nestingLevel = 1 } = this.props;
    return (
      <InstrumentTreeLevel
        level={item}
        onRemove={onRemove}
        onSelect={onSelect}
        onLevelLoad={onLevelLoad}
        selectedInstruments={selectedInstruments}
        nestingLevel={nestingLevel + 1}
        isLast={index + 1 === level.children.size}
        key={`itl_${index}_${nestingLevel}`}
      />
    );
  }

  renderIcon = (isLast) => {
    const { level } = this.props;
    switch (level.id.nodeType) {
      case 'Category':
        return (<InstrumentCategoryIcon category={level.name} />);
      case 'Exchange':
        return (
          <div styleName="new-instrument-row-icon-wrap">
            <span
              styleName="new-instrument-row-icon-line"
              className={isLast ? 'icon-icon-single-line' : 'icon-icon-double-line'}
            />
            <span
              styleName="new-instrument-row-icon"
              className="icon-icon-market"
            />
          </div>
        );
      case 'Code':
      case 'Ticker':
        return (
          <div styleName="new-instrument-row-icon-wrap">
            <span
              styleName="new-instrument-row-icon-line"
              className={isLast ? 'icon-icon-single-line' : 'icon-icon-double-line'}
            />
            <span
              styleName="new-instrument-row-icon"
              className="icon-icon-asset"
            />
          </div>
        );
      case 'Expiration':
        return (
          <div styleName="new-instrument-row-icon-wrap">
            <span
              styleName="new-instrument-row-icon-line"
              className={isLast ? 'icon-icon-single-line' : 'icon-icon-double-line'}
            />
            <span
              styleName="new-instrument-row-icon"
              className="icon-icon-calendar"
            />
          </div>
        );
      default:
        return (
          <div styleName="new-instrument-row-icon-wrap">
            <span
              styleName="new-instrument-row-icon-line"
              className={isLast ? 'icon-icon-single-line' : 'icon-icon-double-line'}
            />
          </div>
        );
    }
  }

  render() {
    const { expanded } = this.state;
    const { disabled, level, nestingLevel = 1, isLast } = this.props;

    return (
      <div>
        <div
          className={`${this.isExpandable && styles['new-instrument-row-expandable']} ${styles[`new-instrument-row-level-${nestingLevel}`]}`}
        >
          <ListItem
            nestingLevel={nestingLevel}
            onClick={this.toggleExpand}
            disabled={disabled}
            renderRight={() =>
              <div styleName="new-instrument-row-right">
                <span styleName="new-instrument-row-count">
                  {!level.instrumentId && level.instrumentsCount}
                </span>

                {this.isInstrumentSelected && <CheckMark />}

                {this.isExpandable && <div className={`icon-icon-dropdown-arrow ${expanded && styles['new-instrument-row-arrow-up']}`} styleName="new-instrument-row-arrow" />}
              </div>
            }
          >
            <div styleName="new-instrument-row-left">
              {this.renderIcon(isLast)}
              <h4
                className={level.id.nodeType === 'Category' && styles['new-instrument-row-title-category']}
                styleName="new-instrument-row-title"
                data-instrument-level-name={level.name}
              >
                {level.id.nodeType === 'Category' ? INSTRUMENT_CATEGORIES[level.name].name : level.name}
              </h4>
            </div>

          </ListItem>
        </div>
        {expanded && this.renderChild()}
      </div>
    );
  }
}

InstrumentTreeLevel.propTypes = {
  level: PropTypes.object,
  onLevelLoad: PropTypes.func.isRequired,
  onSelect: PropTypes.func.isRequired,
  onRemove: PropTypes.func.isRequired,
  selectedInstruments: PropTypes.array.isRequired,
  nestingLevel: PropTypes.number,
  isLast: PropTypes.bool,
  disabled: PropTypes.bool,
};
