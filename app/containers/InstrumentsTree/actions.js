import createApiAction from 'unity-frontend-core/utils/createAction';

export const loadInstrumentLevel = createApiAction('app/containers/TradingPage/LOAD_INSTRUMENT_LEVEL');
export const toggleInstrumentSelection = createApiAction('app/containers/TradingPage/TOGGLE_INSTRUMENT_SELECTION');
export const clearInstrumentsTree = createApiAction('app/containers/TradingPage/CLEAR_INSTRUMENTS_TREE');
