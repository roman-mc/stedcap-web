import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { FormattedMessage } from 'react-intl';
import { createStructuredSelector } from 'reselect';
import { debounce } from 'lodash';

import SearchField from 'Components/SearchField';
import WithInjection from 'unity-frontend-core/containers/WithInjection';

import Loading from '../../components/LoadingIndicator';
import InstrumentTreeLevel from './InstrumentTreeLevel';

import {
  loadInstrumentLevel,
  clearInstrumentsTree,
  toggleInstrumentSelection,
} from './actions';

import { makeSelectInstrumentsFound } from './selectors';

import messages from './messages';

import reducer from './reducer';
import sagas from './sagas';

import { H2 } from '../../components/Typography';

import './style.css';

class InstrumentsTree extends React.PureComponent {
  state = {
    search: '',
  };

  componentDidMount() {
    this.loadLevel();
  }

  componentWillUnmount() {
    this.props.clearInstrumentsTree();
  }

  loadLevel = debounce((path = []) => {
    this.props.loadInstrumentLevel(path, this.state.search);
  }, 1000, { leading: true })


  findHandler = (search) => {
    this.setState({ search: search ? search.toUpperCase() : '' }, this.loadLevel);
  }

  render() {
    const { disabled, hideTitle, instrumentsFound, onSelect, onRemove, selectedInstruments } = this.props;
    const { search } = this.state;
    return (
      <div styleName={`scroll-wrapper ${hideTitle ? 'no-title' : ''} ${disabled ? 'disabled' : ''}`}>
        {!hideTitle && <H2 centered><FormattedMessage {...messages.availableSymbols} /></H2>}
        <div styleName="new-list-padding">
          <SearchField placeholder="Search for instruments" onChange={this.findHandler} value={search} />
        </div>
        <div styleName="text">
          {instrumentsFound && instrumentsFound.children
            ? instrumentsFound.children.map((item) => (
              <InstrumentTreeLevel
                level={item}
                onSelect={onSelect}
                onRemove={onRemove}
                disabled={disabled}
                onLevelLoad={(path) => this.loadLevel(path)}
                key={`create_watchlist_instrument_${item.id}`}
                selectedInstruments={selectedInstruments}
              />))
            : <Loading transparent />
          }
        </div>
      </div>
    );
  }
}

InstrumentsTree.propTypes = {
  onSelect: PropTypes.func.isRequired,
  onRemove: PropTypes.func,
  selectedInstruments: PropTypes.array,
  instrumentsFound: PropTypes.object,
  hideTitle: PropTypes.bool,
  disabled: PropTypes.bool,
  loadInstrumentLevel: PropTypes.func.isRequired,
  clearInstrumentsTree: PropTypes.func.isRequired,
};

InstrumentsTree.defaultProps = {
  onRemove: () => {},
};

const mapDispatchToProps = (dispatch) => ({
  toggleInstrumentSelection: (instrumentId) => dispatch(toggleInstrumentSelection({ instrumentId })),
  loadInstrumentLevel: (path = [], query) => dispatch(loadInstrumentLevel({ path, query, pagingOptions: { limit: 9000, offset: 0 } })),
  clearInstrumentsTree: () => dispatch(clearInstrumentsTree()),
});

const mapStateToProps = createStructuredSelector({
  instrumentsFound: makeSelectInstrumentsFound(),
});

const Connected = connect(mapStateToProps, mapDispatchToProps)(InstrumentsTree);

export default WithInjection({ reducer, sagas, key: 'instrumentsTree' })(Connected);
