import { takeEvery, call, put } from 'redux-saga/effects';

import * as instrumentsApi from 'unity-frontend-core/api/instrumentsApi';

import { loadInstrumentLevel } from './actions';

function* loadInstrumentLevelSaga({ payload }) {
  try {
    const result = yield call(instrumentsApi.searchInstruments, payload);
    yield put(loadInstrumentLevel.success({ result, path: payload.path }));
  } catch (e) {
    console.warn('error instruments loading', e);
    yield put(loadInstrumentLevel.error(e));
  }
}


function* sagasLoader() {
  yield takeEvery(loadInstrumentLevel.toString(), loadInstrumentLevelSaga);
}

export default [
  sagasLoader,
];
