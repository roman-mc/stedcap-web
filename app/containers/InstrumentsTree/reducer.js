import { Map } from 'immutable';
import { createReducer } from 'redux-act';

import RemoteInstrument from 'unity-frontend-core/Models/RemoteInstrument';


import {
  loadInstrumentLevel,
  clearInstrumentsTree,
  toggleInstrumentSelection,
} from './actions';


const initialState = new Map({
  instrumentsFound: null,
});

export default createReducer({
  [clearInstrumentsTree]: (state) => state.set('instrumentsFound', null),
  [toggleInstrumentSelection]: (state, { instrumentId }) => {
    const instruments = state.get('instrumentsFound');
    return state.set('instrumentsFound', instruments.toggleSelection(instrumentId));
  },
  [loadInstrumentLevel]: (state, { path }) => {
    if (path.length === 0) {
      return state.set('instrumentsFound', null);
    }
    return state;
  },
  [loadInstrumentLevel.success]: (state, { result, path }) => {
    const foundData = new RemoteInstrument(result);
    const oldData = state.get('instrumentsFound');

    return state.set('instrumentsFound', oldData === null || path.length === 0
      ? foundData
      : oldData.setByPath(path, foundData.getByPath(path).children)
    );
  },
}, initialState);
