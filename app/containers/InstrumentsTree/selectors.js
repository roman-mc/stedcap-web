import { createSelector } from 'reselect';

const makeSelectSpace = () => (state) => state.get('instrumentsTree');

export const makeSelectInstrumentsFound = () => createSelector(
  makeSelectSpace(),
  (substate) => substate.get('instrumentsFound'),
);
