import { defineMessages } from 'react-intl';

export default defineMessages({
  header: {
    id: 'app.containers.ConfirmationDialog.header',
    defaultMessage: 'No network connection',
  },
  text: {
    id: 'app.containers.ConfirmationDialog.text',
    defaultMessage: 'Check your connection or try again later',
  },
});
