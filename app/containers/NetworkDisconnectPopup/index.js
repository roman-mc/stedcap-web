import React from 'react';
import PropTypes from 'prop-types';
import { FormattedMessage } from 'react-intl';
import { connect } from 'react-redux';
import { createStructuredSelector } from 'reselect';

import Popup from '../../components/Popup';

import messages from './messages';

import {} from './style.css';
import { makeSelectNetworkDisconnected } from '../../pages/AuthWrapper/selectors';

function NetworkDisconnectPopup({ opened }) {
  return (
    opened
      ? <Popup
        popupKey="network_popup"
        key="network_popup"
        header={<FormattedMessage {...messages.header} />}
      >
        <div styleName="container">
          <div styleName="icon-wrapper">
            <i styleName="icon" className="icon-icon-no-network" />
          </div>
          <p styleName="text"><FormattedMessage {...messages.text} /></p>
        </div>
      </Popup>
      : <span />
  );
}

NetworkDisconnectPopup.propTypes = {
  opened: PropTypes.bool.isRequired,
};

const mapStateToProps = createStructuredSelector({
  opened: makeSelectNetworkDisconnected(),
});

export default connect(mapStateToProps, null)(NetworkDisconnectPopup);
