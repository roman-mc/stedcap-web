import React from 'react';
import PropTypes from 'prop-types';
import ImmutableProptypes from 'react-immutable-proptypes';
import { Link } from 'react-router-dom';
import { FormattedNumber, FormattedMessage } from 'react-intl';
import { connect } from 'react-redux';
import { createStructuredSelector } from 'reselect';

import { ACCOUNT_TYPES } from 'unity-frontend-core/Models/Enums';
import connectSelectAccount from 'unity-frontend-core/containers/SelectAccount';
import WithInjection from 'unity-frontend-core/containers/WithInjection';

import { USER_STATUS } from 'Model/enums';

import { AuthForm, Button } from 'Components/AuthForm';
import LoadingIndicator from 'Components/LoadingIndicator';
import IconCheck from '../../assets/svg/icon-check.svg';

import sagas from './sagas';

import messages from './messages';

import { makeSelectCurrentUser } from '../App/selectors';
import { } from './style.css';

function resolveTitle(firstAcc) {
  if (firstAcc.multiAccountType === ACCOUNT_TYPES.BROKERAGE) {
    return messages.tradingAccounts;
  }
  if (firstAcc.multiAccountType === ACCOUNT_TYPES.PORTFOLIO_MANAGEMENT) {
    return firstAcc.isMyAccount ? messages.underPmAccount : messages.pmAccount;
  }
  return messages.underPmAccount;
}

class SelectAccountPopup extends React.PureComponent {
  state = {
    activeAccount: null,
  }

  componentWillMount() {
    if (!this.props.selectedAccountId) {
      this.props.getAccountId();
    }
  }

  componentDidMount() {
    this.props.loadAccounts();
  }

  selectAccount = (activeAccount) => this.setState({ activeAccount })

  render() {
    const { accounts, loading, currentUser } = this.props;
    const { activeAccount } = this.state;
    if (loading) {
      return (
        <LoadingIndicator />
      );
    }

    if (!currentUser) {
      return null;
    }

    return (
      <AuthForm subtitle={messages.subtitle}>
        <div styleName="container">
          {currentUser.status === USER_STATUS.new
            ? <div>
              <div styleName="trade-button">
                <p styleName="not-verified">Thank you for completing your application!<br />We will now verify the information you have submitted.<br />The process can take up to 72 hours.<br /></p>
              </div>
            </div>
            : <div>
              {accounts.entrySeq().map(([k, accs]) => (
                <div key={`acc_group_${k}`} styleName="accounts-group">
                  <p styleName="label">
                    <FormattedMessage {...resolveTitle(accs.first())} />
                  </p>

                  {accs.valueSeq().map((acc) => (
                    <div key={`multiacc_${acc.multiAccountId}`}>
                      {/* eslint-disable jsx-a11y/no-static-element-interactions */}
                      <div
                        styleName={
                          `account-row ${acc.permissions.includes('VIEW') ? '' : 'disabled'} ${activeAccount === acc.multiAccountId ? 'active' : ''}`
                        }
                        onClick={() => {
                          if (acc.permissions.includes('VIEW')) {
                            this.selectAccount(acc.multiAccountId);
                          }
                        }}
                      >
                        <span
                          styleName={
                            `checkbox ${activeAccount === acc.multiAccountId ? 'active' : ''} ${acc.permissions.includes('VIEW') ? '' : 'hidden'}`
                          }
                        >
                          {activeAccount === acc.multiAccountId
                            && <IconCheck styleName="checkbox-icon" />
                          }
                        </span>
                        <span styleName="account-name">
                          {acc.accountAccessName || `#${acc.multiAccountId}`}
                        </span>
                        <span styleName="balance">
                          <FormattedNumber value={acc.assetValue} /> {acc.currency}
                        </span>
                      </div>
                      {/* eslint-enable */}
                    </div>
                  ))}
                </div>
              ))}
              <div styleName="trade-button">
                <Button
                  type="submit"
                  className="open-account"
                  text="Trade"
                  disabled={!activeAccount}
                  onClick={() => this.props.selectAccount({ activeAccount })}
                />
              </div>
            </div>
          }
          <div styleName="logout-link-container">
            <Link styleName="logout-link" to="/logout"><FormattedMessage {...messages.logout} /></Link>
          </div>
        </div>
      </AuthForm>
    );
  }
}

SelectAccountPopup.propTypes = {
  loadAccounts: PropTypes.func.isRequired,
  selectAccount: PropTypes.func.isRequired,
  accounts: ImmutableProptypes.map,
  currentUser: ImmutableProptypes.record,
  getAccountId: PropTypes.func.isRequired,
  loading: PropTypes.bool,
  selectedAccountId: PropTypes.number,
};

const mapStateToProps = createStructuredSelector({
  currentUser: makeSelectCurrentUser(),
});

const connected = connect(mapStateToProps, null)(SelectAccountPopup);

const withSagas = WithInjection({ sagas })(connected);

export default connectSelectAccount(withSagas);
