import React from 'react';
import PropTypes from 'prop-types';
import ImmutableProptypes from 'react-immutable-proptypes';
import { FormattedMessage } from 'react-intl';

import { ACCOUNT_TYPES } from 'unity-frontend-core/Models/Enums';
import connectSelectAccount from 'unity-frontend-core/containers/SelectAccount';

import AccountRowSmall from './AccontRowSmall';

import messages from './messages';

import './style.css';

function resolveTitle(firstAcc) {
  if (firstAcc.multiAccountType === ACCOUNT_TYPES.BROKERAGE) {
    return messages.tradingAccounts;
  }
  if (firstAcc.multiAccountType === ACCOUNT_TYPES.PORTFOLIO_MANAGEMENT) {
    return firstAcc.isMyAccount ? messages.underPmAccount : messages.pmAccount;
  }
  return messages.underPmAccount;
}

class SelectAccountDropdown extends React.PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      selectedAccountId: props.selectedAccountId,
    };
  }

  render() {
    const { accounts } = this.props;
    const { selectedAccountId } = this.state;
    return (
      <div>
        {accounts.entrySeq().map(([k, accs]) => (
          <div key={`acc_select_${k}`} styleName="account-wrapper">
            <p styleName="label-small">
              <FormattedMessage {...resolveTitle(accs.first())} />
            </p>
            {accs.valueSeq().map((acc) => (
              <AccountRowSmall
                key={`multiacc_${acc.multiAccountId}`}
                selectAccount={this.props.selectAccount}
                acc={acc}
                isActive={selectedAccountId === acc.multiAccountId}
                onAccountRename={this.props.renameAccount}
              />
            ))}
          </div>
        ))}
      </div>
    );
  }
}

SelectAccountDropdown.propTypes = {
  selectAccount: PropTypes.func.isRequired,
  renameAccount: PropTypes.func.isRequired,
  selectedAccountId: PropTypes.number.isRequired,
  accounts: ImmutableProptypes.map,
};

export default connectSelectAccount(SelectAccountDropdown);
