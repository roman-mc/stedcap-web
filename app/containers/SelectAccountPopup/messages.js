/*
 * LoginPage Messages
 *
 * This contains all the text for the LoginPage component.
 */
import { defineMessages } from 'react-intl';

export default defineMessages({
  subtitle: {
    id: 'app.containers.SelectAccountPopup.subtitle',
    defaultMessage: 'CHOOSE AN ACCOUNT',
  },
  tradingAccounts: {
    id: 'app.containers.SelectAccountPopup.tradingAccounts',
    defaultMessage: 'Trading accounts',
  },
  pmAccount: {
    id: 'app.containers.SelectAccountPopup.pmAccount',
    defaultMessage: 'Portfolio management',
  },
  underPmAccount: {
    id: 'app.containers.SelectAccountPopup.underPmAccount',
    defaultMessage: 'My accounts under portfolio management',
  },
  logout: {
    id: 'app.containers.SelectAccountPopup.logout',
    defaultMessage: 'Log out',
  },
  verificationFirst: {
    id: 'app.containers.SelectAccountPopup.verificationFirst',
    defaultMessage: 'Please verify your account to start trading or request portfolio management.',
  },
  startVerifiaction: {
    id: 'app.containers.SelectAccountPopup.startVerifiaction',
    defaultMessage: 'Start verification',
  },
});
