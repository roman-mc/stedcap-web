import { takeEvery } from 'redux-saga';
import { put, fork } from 'redux-saga/effects';
import Raven from 'raven-js';

import socketApi from 'Api/socketApi';
import { loadInstruments } from 'unity-frontend-core/containers/InstrumentResolver/actions';
import { changeTradingAccount } from 'unity-frontend-core/actions';
import { loadPositions } from 'unity-frontend-core/containers/Positions/actions';

/* TODO: remove dat shit */
import { loadNotifications } from '../NotificationsCenter/actions';
import { loadOrders } from '../../pages/TradingPage/containers/Positions/actions';

function* tradingAccountChangedHandler() {
  try {
    yield put(loadInstruments());
    yield put(loadPositions({ force: true }));
    yield put(loadOrders({ restart: true }));
    yield put(loadNotifications());
    socketApi.restart();
  } catch (e) {
    Raven.captureException(e);
  }
}

function* tradingAccountChangedSaga() {
  return yield takeEvery(changeTradingAccount.toString(), tradingAccountChangedHandler);
}

/**
 * Root saga manages watcher lifecycle
 */
export function* runAllSagas() {
  yield [
    tradingAccountChangedSaga,
  ].map(fork);
}

export default [
  runAllSagas,
];
