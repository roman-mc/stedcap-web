import React from 'react';
import PropTypes from 'prop-types';
import ImmutableProptypes from 'react-immutable-proptypes';
import { FormattedNumber } from 'react-intl';

import IconCheck from '../../assets/svg/icon-check.svg';
import './style.css';

export default class AccontRowSmall extends React.PureComponent {
  state = {
    isEditable: false,
    newName: '',
  }

  onChange = (e) => {
    this.setState({ newName: e.target.value });
  }

  startEdit = (e) => {
    e.stopPropagation();
    this.setState({ isEditable: true });
  }

  saveName = (e) => {
    e.stopPropagation();
    e.preventDefault();
    if (this.state.newName) {
      this.props.onAccountRename({ name: this.state.newName, multiAccountId: this.props.acc.multiAccountId });
    }
    this.setState({ isEditable: false, newName: '' });
  }

  render() {
    const { isEditable, newName } = this.state;
    const { isActive, acc, selectAccount } = this.props;
    return (
      <div
        styleName={`account-row-small ${acc.permissions.indexOf('VIEW') >= 0 ? '' : 'disabled'} ${isActive ? 'active' : ''}`}
        onMouseDown={() => {
          if (acc.permissions.indexOf('VIEW') >= 0) {
            selectAccount({ activeAccount: acc.multiAccountId, tradingAccountChanged: true });
          }
        }}
      >
        <span styleName={`checkbox-small ${isActive ? 'active' : ''} ${acc.permissions.indexOf('VIEW') > -1 ? '' : 'hidden'}`}>
          {
            isActive && <IconCheck styleName="checkbox-icon" />
          }
        </span>
        {
          isEditable
            ? (<span styleName="name-container">
              <form styleName="form" onSubmit={this.saveName}>
                <input
                  onMouseDown={(e) => e.stopPropagation()}
                  styleName="name-input"
                  placeholder={acc.accountAccessName || `#${acc.multiAccountId}`}
                  onChange={this.onChange}
                  value={newName}
                  maxLength={30}
                />
                <button type="submit" onMouseDown={this.saveName} styleName="name-ok-btn">OK</button>
              </form>
            </span>)
            : <span styleName="name-container">
              <span styleName="account-name">
                <span styleName="account-name-wrapper">
                  {acc.accountAccessName || `#${acc.multiAccountId}`}
                </span>
                <i
                  onMouseDown={(e) => this.startEdit(e, acc.multiAccountId)}
                  styleName="edit-icon"
                  className="icon-icon-edit"
                >
                </i>
              </span>
              <span styleName="balance">
                <span styleName="balance-sum">
                  <FormattedNumber value={acc.assetValue} />
                </span>
                {acc.currency}
              </span>
            </span>
        }
      </div>
    );
  }
}

AccontRowSmall.propTypes = {
  isActive: PropTypes.bool,
  acc: ImmutableProptypes.record,
  selectAccount: PropTypes.func.isRequired,
  onAccountRename: PropTypes.func.isRequired,
};
