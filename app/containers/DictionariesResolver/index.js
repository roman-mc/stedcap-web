import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { createStructuredSelector } from 'reselect';

import WithInjection from 'unity-frontend-core/containers/WithInjection';

import LoadingIndicator from 'Components/LoadingIndicator';
import { loadPaymentMethodSchema, loadRiskGroups } from './actions';
import { makeSelectRiskGroupsLoaded } from './selectors';

import reducer from './reducer';
import sagas from './sagas';


class DictionariesResolver extends React.Component {
  componentWillMount() {
    this.props.loadPaymentMethodSchema();
    this.props.loadRiskGroups();
  }

  render() {
    if (!this.props.riskGroupsLoaded) {
      return (
        <LoadingIndicator />
      );
    }
    return this.props.children;
  }
}

DictionariesResolver.propTypes = {
  children: PropTypes.node.isRequired,
  loadRiskGroups: PropTypes.func.isRequired,
  loadPaymentMethodSchema: PropTypes.func.isRequired,
  riskGroupsLoaded: PropTypes.bool,
};

const mapDispatchToProps = (dispatch) => ({
  loadRiskGroups: () => dispatch(loadRiskGroups()),
  loadPaymentMethodSchema: () => dispatch(loadPaymentMethodSchema()),
});

const mapStateToProps = createStructuredSelector({
  riskGroupsLoaded: makeSelectRiskGroupsLoaded(),
});

const Connected = connect(mapStateToProps, mapDispatchToProps)(DictionariesResolver);

export default WithInjection({ reducer, sagas, key: 'dictionaries' })(Connected);
