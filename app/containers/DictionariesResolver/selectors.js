import { createSelector } from 'reselect';

/**
 * Direct selector to the dictionaries state domain
 */
const makeSelectDictionariesDomain = () => (state) => state.get('dictionaries');


/**
 * Other specific selectors
 */

export const makeSelectPaymentMethodSchema = () => createSelector(
  makeSelectDictionariesDomain(),
  (substate) => substate.get('paymentMethodSchema'),
);

export const makeSelectRiskGroups = () => createSelector(
  makeSelectDictionariesDomain(),
  (substate) => substate.get('riskGroups'),
);

export const makeSelectRiskGroupsLoaded = () => createSelector(
  makeSelectDictionariesDomain(),
  (substate) => substate.get('riskGroupsLoaded'),
);
