import { simpleApiSaga } from 'Utils/sagaUtils';
import * as dictionariesApi from 'unity-frontend-core/api/dictionariesApi';
import * as jsonSchemaApi from 'unity-frontend-core/api/jsonSchemaApi';

import { loadPaymentMethodSchema, loadRiskGroups } from './actions';

export default [
  simpleApiSaga(loadRiskGroups, dictionariesApi.getRiskGroups),
  simpleApiSaga(loadPaymentMethodSchema, jsonSchemaApi.loadJsonSchema, () => ['payment_method']),
];
