import { createReducer } from 'redux-act';
import { Map, List } from 'immutable';

import RiskGroup from 'unity-frontend-core/Models/RiskGroup';

import { loadPaymentMethodSchema, loadRiskGroups } from './actions';

const initialState = new Map({
  instruments: new List(),
  instrumentsMap: new Map(),
  instrumentsCache: new Map(),
  initialDictionaryLoaded: false,
  riskGroupsLoaded: false,
  paymentMethodSchema: null,
  riskGroups: new Map(),
});

export default createReducer({
  [loadPaymentMethodSchema.success]: (state, { schema }) => state.set('paymentMethodSchema', schema),
  [loadRiskGroups.success]: (state, { items }) => {
    const riskGroups = new List(items.map((d) => new RiskGroup(d)));
    return state
      .set('riskGroupsLoaded', true)
      .set(
        'riskGroups',
        riskGroups
          .groupBy((x) => x.id)
          .toMap()
          .map((i) => i.first())
      );
  },
}, initialState);
