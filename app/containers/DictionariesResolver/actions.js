import createApiAction from 'unity-frontend-core/utils/createAction';

export const loadRiskGroups = createApiAction('app/DictionariesResolver/LOAD_RISK_GROUPS');
export const loadPaymentMethodSchema = createApiAction('app/DictionariesResolver/LOAD_PAYMENT_METHOD_SCHEMA');
