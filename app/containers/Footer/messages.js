import { defineMessages } from 'react-intl';

export default defineMessages({
  cashAvailable: {
    id: 'app.containers.Footer.cashAvailable',
    defaultMessage: 'Cash available',
  },
  accountValue: {
    id: 'app.containers.Footer.accountValue',
    defaultMessage: 'Account value',
  },
  equity: {
    id: 'app.containers.Footer.equity',
    defaultMessage: 'Equity',
  },
  fxUpl: {
    id: 'app.containers.Footer.fxUpl',
    defaultMessage: 'Unrealized P/L',
  },
  fxClosedPl: {
    id: 'app.containers.Footer.fxClosedPl',
    defaultMessage: 'Closed P/L',
  },
  marginAvailable: {
    id: 'app.containers.Footer.marginAvailable',
    defaultMessage: 'Margin available',
  },
  margin: {
    id: 'app.containers.Footer.margin',
    defaultMessage: 'Margin',
  },
  connection: {
    id: 'app.containers.Footer.connection',
    defaultMessage: 'Connection',
  },
  stable: {
    id: 'app.containers.Footer.stable',
    defaultMessage: 'Stable',
  },
  slow: {
    id: 'app.containers.Footer.slow',
    defaultMessage: 'Poor',
  },
  disconnected: {
    id: 'app.containers.Footer.disconnected',
    defaultMessage: 'Unstable',
  },
  connectionTitle: {
    id: 'app.containers.Footer.connectionTitle',
    defaultMessage: 'Connection: Stable',
  },
  badConnectionTitle: {
    id: 'app.containers.Footer.badConnectionTitle',
    defaultMessage: 'Real money trades may be executed with delay due to poor connection. Check your settings to restore it or proceed carefully.',
  },
  noConnectionTitle: {
    id: 'app.containers.Footer.noConnectionTitle',
    defaultMessage: 'There is no stable connection with server. Data is probably displayed incorrectly and trades may carry out with errors. Please check you internet settings before executing trades.',
  },
});
