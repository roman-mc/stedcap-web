import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { createStructuredSelector } from 'reselect';
import { FormattedNumber, FormattedMessage, injectIntl, intlShape } from 'react-intl';

import { makeSelectBalance } from '../App/selectors';
import { makeSelectIsConnectionBad, makeSelectNetworkDisconnected } from '../../pages/AuthWrapper/selectors';
import './style.css';

import messages from './messages';

// For that number there is no necessary for display fractions(e.g. pennies of dlloars)
const BIG_NUM = 100000;

const getNumFractions = (num = 0) => BIG_NUM > Math.abs(num) ? 2 : 0;

function Footer({ balance, intl: { formatMessage }, isConnectionBad, networkDisconnected }) {
  const isConnectionProblems = isConnectionBad || networkDisconnected;
  const badConnectionDescription = networkDisconnected ? messages.noConnectionTitle : messages.badConnectionTitle;

  // eslint-disable-next-line no-nested-ternary
  const connectionCircleStatusClass = isConnectionProblems
    ? (networkDisconnected ? 'circle-error' : 'circle-warn')
    : '';
  // eslint-disable-next-line no-nested-ternary
  const connectionStatusText = isConnectionProblems
    ? (networkDisconnected ? messages.disconnected : messages.slow)
    : messages.stable;

  return (
    <div styleName="footer">
      {balance
        && <div styleName="leftBox">
          <div styleName="element">
            <div styleName="info">
              <span styleName="text">
                <FormattedMessage {...messages.equity} />
              </span>
              <span styleName="value">
                <FormattedNumber minimumFractionDigits={getNumFractions(balance.assetValue)} maximumFractionDigits={getNumFractions(balance.assetValue)} value={balance.assetValue || 0} />
                &nbsp;
                {balance.currency || '-'}
              </span>
            </div>
          </div>

          <div styleName="element">
            <div styleName="info">
              <span styleName="text">
                <FormattedMessage {...messages.fxUpl} />
              </span>
              <span styleName="value">
                <FormattedNumber minimumFractionDigits={getNumFractions(balance.unrealizedPl)} maximumFractionDigits={getNumFractions(balance.unrealizedPl)} value={balance.unrealizedPl || 0} />
                &nbsp;
                {balance.currency || '-'}
              </span>
            </div>
          </div>

          <div styleName="element">
            <div styleName="info">
              <span styleName="text">
                <FormattedMessage {...messages.fxClosedPl} />
              </span>
              <span styleName="value">
                <FormattedNumber minimumFractionDigits={getNumFractions(balance.closedPl)} maximumFractionDigits={getNumFractions(balance.closedPl)} value={balance.closedPl || 0} />
                &nbsp;
                {balance.currency || '-'}
              </span>
            </div>
          </div>

          <div styleName="element">
            <div styleName="info">
              <span styleName="text">
                <FormattedMessage {...messages.marginAvailable} />
              </span>
              <span styleName="value">
                <FormattedNumber minimumFractionDigits={getNumFractions(balance.availableForTrading)} maximumFractionDigits={getNumFractions(balance.availableForTrading)} value={balance.availableForTrading || 0} />
                &nbsp;
                {balance.currency || '-'}
              </span>
            </div>
          </div>

          <div styleName="element">
            <div styleName="info">
              <span styleName="text">
                <FormattedMessage {...messages.margin} />
              </span>

              <div styleName="bar">
                <div
                  styleName="barFiller"
                  style={{
                    width: `${balance.marginUtilization * 100}%`,
                  }}
                ></div>
              </div>

              <span styleName="value">
                <FormattedNumber value={balance.marginUtilization * 100} />
%
              </span>
            </div>
          </div>
        </div>
      }
      <div styleName="rightBox">
        <div styleName="element">
          <div styleName="info">
            <i styleName={`circle ${connectionCircleStatusClass}`}>

              {isConnectionProblems
                && <div styleName={`tooltip ${networkDisconnected ? 'error' : ''}`}>
                  <i className="icon-icon-error"></i>
                  <span styleName="tooltip-text">
                    <FormattedMessage {...badConnectionDescription} />
                  </span>
                </div>
              }

            </i>
            <span styleName="text">
              <FormattedMessage {...messages.connection} />
            </span>
            <a
              title={formatMessage(messages.connectionTitle)}
              styleName="value"
            >
              <FormattedMessage {...connectionStatusText} />
            </a>
          </div>
        </div>
      </div>
    </div>
  );
}

Footer.propTypes = {
  balance: PropTypes.object,
  intl: intlShape,
  isConnectionBad: PropTypes.bool.isRequired,
  networkDisconnected: PropTypes.bool.isRequired,
};

const mapStateToProps = createStructuredSelector({
  balance: makeSelectBalance(),
  isConnectionBad: makeSelectIsConnectionBad(),
  networkDisconnected: makeSelectNetworkDisconnected(),
});


export default connect(mapStateToProps)(injectIntl(Footer));
