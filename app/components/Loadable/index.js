import React from 'react';
import Loadable from 'react-loadable';

import Raven from 'raven-js';

import LoadingIndicator from '../LoadingIndicator';

export default (loader) => Loadable({
  loader: () => loader().catch((e) => {
    console.warn('Error loading page', e);
    Raven.captureException(e);
  }),
  loading: () => <LoadingIndicator loading />,
});
