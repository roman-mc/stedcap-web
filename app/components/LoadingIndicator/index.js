import React from 'react';
import PropTypes from 'prop-types';

import {} from './style.css';

export default function LoadingIndicator({ loading, small, transparent, style = {} }) {
  if (!loading) {
    return null;
  }
  return (
    <div styleName={`loader ${transparent ? 'transparent' : ''}`} style={style}>
      <div styleName={`loader_animation ${small ? 'small' : ''}`} />
    </div>
  );
}

LoadingIndicator.defaultProps = {
  loading: true,
};

LoadingIndicator.propTypes = {
  loading: PropTypes.bool,
  small: PropTypes.bool,
  transparent: PropTypes.bool,
  style: PropTypes.object,
};
