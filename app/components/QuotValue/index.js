/**
*
* QuotValue
*
*/

import React from 'react';
import PropTypes from 'prop-types';

import './style.css';

function QuotValue({ value, pipSize = 0.0001, size = 'default' }) {
  if (pipSize === 1) {
    return (
      <span styleName={`quot ${size}`}>
        <span>
          {value.toFixed(2)}
        </span>
      </span>
    );
  }

  const pipCount = (pipSize.toString().split('.')[1] || []).length + 1;
  const strValue = value.toFixed(5).toString();
  const firstGroup = strValue.substr(0, strValue.toString().indexOf('.') + (pipCount - 2));
  const bigNumbersGroup = strValue.substr(strValue.toString().indexOf('.') + (pipCount - 2), 2);
  const lastGroup = strValue.substr(strValue.toString().indexOf('.') + pipCount, 1);


  return (
    <span styleName={`quot ${size}`}>
      <span styleName="base">
        {firstGroup}
      </span>
      <span styleName="increased">
        {bigNumbersGroup}
      </span>
      <span styleName="decreased">
        {lastGroup}
      </span>
    </span>
  );
}

QuotValue.propTypes = {
  value: PropTypes.number,
  pipSize: PropTypes.number,
  size: PropTypes.oneOf(['default', 'small', 'big', 'medium', 'plain']),
};

export default QuotValue;
