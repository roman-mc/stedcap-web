import React from 'react';
import PropTypes from 'prop-types';
import { Field } from 'redux-form/immutable';
import ReactSelect from 'react-select';

import InlineWrapper from './InlineWrapper';
import './style.css';

class SelectorField extends React.PureComponent {
  onChange = (value) => {
    this.props.input.onChange(value.value);
  }

  render() {
    const {
      text, required, placeholder, labelClassName, disableClearable, input: { value }, options, meta: {
        touched, error, warning, submitFailed,
      },
      optionRenderer, valueRenderer,
    } = this.props;

    const isError = Boolean(submitFailed && touched && (error || warning));


    return (
      <InlineWrapper {...{ isError, text, error, required, labelClassName }}>
        <div styleName="group">
          <ReactSelect
            placeholder={placeholder}
            clearable={!disableClearable}
            optionRenderer={optionRenderer}
            valueRenderer={valueRenderer}
            options={options}
            value={value}
            onChange={this.onChange}
          />
        </div>
      </InlineWrapper>
    );
  }
}

SelectorField.propTypes = {
  text: PropTypes.any,
  required: PropTypes.bool,
  disableClearable: PropTypes.any,
  meta: PropTypes.any,
  labelClassName: PropTypes.string,
  placeholder: PropTypes.any,
  input: PropTypes.shape({
    value: PropTypes.any,
    onChange: PropTypes.func,
  }),
  options: PropTypes.array,
  optionRenderer: PropTypes.func,
  valueRenderer: PropTypes.func,
};


export default function InlineSelect(props) {
  return (
    <Field
      component={SelectorField}
      {...props}
    />
  );
}
