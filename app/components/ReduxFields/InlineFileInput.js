import React from 'react';
import PropTypes from 'prop-types';
import { FieldArray } from 'redux-form/immutable';

import { formatFileSize } from '../../utils/helpers';
import FileDrop from '../../containers/FileDrop';

import InlineWrapper from './InlineWrapper';

import {} from './style.css';

class InputFileField extends React.PureComponent {
  state = {
    files: [],
  }

  render() {
    const { files } = this.state;
    const {
      text, required, labelClassName, meta: { error, warning, submitFailed }, subline, maxFiles,
    } = this.props;
    const isError = Boolean(submitFailed && (error || warning));

    return (
      <InlineWrapper {...{ isError, text, error, required, labelClassName }}>
        <div styleName="group">
          <FileDrop {...this.props} setFiles={(items) => this.setState({ files: files.concat(items) })} filesCount={files.length} maxFiles={maxFiles} />
          {!isError && <div styleName="input-subline">{subline}</div>}
          {isError
          && <div styleName="input-error">
            {error}
          </div>
          }
          {files.map((file, i) =>
            (<div key={`f_${i + 1}_${file.id}`} styleName="file-upload-uploaded">
              <span styleName="file-upload-uploaded-name">{file.name}</span>
              <span styleName="file-upload-uploaded-sz">({formatFileSize(file.size)})</span>
            </div>))}
        </div>
      </InlineWrapper>
    );
  }
}

InputFileField.propTypes = {
  text: PropTypes.any,
  meta: PropTypes.any,
  subline: PropTypes.any,
  maxFiles: PropTypes.any,
  required: PropTypes.any,
  labelClassName: PropTypes.string,
};


export default function InlineInput(props) {
  return (
    <FieldArray
      component={InputFileField}
      {...props}
    />
  );
}
