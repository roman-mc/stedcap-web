import React from 'react';
import PropTypes from 'prop-types';
import { Field } from 'redux-form/immutable';

import InlineWrapper from './InlineWrapper';
import {} from './style.css';

class TextareaField extends React.PureComponent {
  render() {
    const {
      text, input, placeholder, required, labelClassName, controlClassName, meta: {
        touched, error, warning, submitFailed,
      },
    } = this.props;
    const isError = Boolean(submitFailed && touched && (error || warning));

    return (
      <InlineWrapper {...{ isError, text, error, required, labelClassName }}>
        <div styleName="group">
          <textarea
            className={`redux__field__input ${controlClassName || ''}`}
            placeholder={placeholder}
            {...input}
          />
          {isError
          && <div styleName="input-error">
            {error}
          </div>
          }
        </div>
      </InlineWrapper>
    );
  }
}

TextareaField.propTypes = {
  placeholder: PropTypes.string,
  text: PropTypes.any,
  meta: PropTypes.any,
  required: PropTypes.any,
  labelClassName: PropTypes.string,
  controlClassName: PropTypes.string,
  input: PropTypes.shape({
    value: PropTypes.any,
  }),
};


export default function InlineTextarea(props) {
  return (
    <Field
      component={TextareaField}
      {...props}
    />
  );
}
