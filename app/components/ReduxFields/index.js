import React from 'react';
import PropTypes from 'prop-types';

import SelectorComponent from 'react-select';
import moment from 'moment';
import { uniqueId } from 'lodash';
import { Field } from 'redux-form/immutable';

import DatePicker from 'react-datepicker';
import 'react-datepicker/dist/react-datepicker.css';

import { STANDARD_DATE } from 'unity-frontend-core/utils/dateUtils';

import BtnGroup from './BtnGroup';
import InlineSelect from './InlineSelect';
import InlineInput from './InlineInput';
import InlineFileInput from './InlineFileInput';
import InlineTextarea from './InlineTextarea';

import WithLabel from './WithLabel';
import { Button as GuiButton } from '../GUI';

import './style.css';

export function Button({
  fullWidth, styleType = 'primary', big, children, onClick = () => { }, disabled = false, type = 'button', text, subClass = '', smaller,
}) {
  return (
    <GuiButton
      fullWidth={fullWidth}
      big={big}
      disabled={disabled}
      type={styleType}
      onClick={onClick}
      btnType={type}
      className={`redux__field__button ${subClass}`}
      smaller={smaller}
    >
      <span className={`${disabled ? 'redux__field__button__text__disabled' : ''}`}>{text || children}</span>
    </GuiButton>
  );
}

Button.propTypes = {
  big: PropTypes.bool,
  fullWidth: PropTypes.bool,
  type: PropTypes.string,
  styleType: PropTypes.string,
  text: PropTypes.string,
  subClass: PropTypes.string,
  children: PropTypes.node,
  disabled: PropTypes.bool,
  smaller: PropTypes.bool,
  onClick: PropTypes.func,
};


function InputField({
  placeholder,
  text,
  input,
  meta: {
    touched, error, warning, submitFailed,
  },
  className,
  maxlength,
  disabled,
}) {
  const isError = Boolean(submitFailed && touched && (error || warning));

  return (
    <WithLabel error={isError} text={text} disabled={disabled}>
      <input
        type="text"
        className={`redux__field__input ${className}`}
        placeholder={placeholder}
        maxLength={maxlength}
        disabled={disabled}
        {...input}
      />
    </WithLabel>
  );
}

InputField.propTypes = {
  placeholder: PropTypes.string,
  text: PropTypes.any,
  meta: PropTypes.any,
  input: PropTypes.shape({
    value: PropTypes.any,
  }),
  className: PropTypes.string,
  maxlength: PropTypes.number,
  disabled: PropTypes.bool,
};


export function Input(props) {
  return (
    <Field
      component={InputField}
      {...props}
    />
  );
}

class DateInputField extends React.Component {
  state = {}

  render() {
    const {
      placeholder, text, input, maxDate, meta: {
        touched, error, warning, submitFailed,
      },
    } = this.props;
    const isError = Boolean(submitFailed && touched && (error || warning));

    return (
      <WithLabel error={isError} text={text}>
        <DatePicker
          {...input}
          className="redux__field__input"
          placeholder={placeholder}
          dateFormat={STANDARD_DATE}
          selected={input.value ? moment(input.value, STANDARD_DATE) : null}
          peekNextMonth
          showMonthDropdown
          showYearDropdown
          dropdownMode="select"
          maxDate={maxDate || moment()}
        />
      </WithLabel>
    );
  }
}

DateInputField.propTypes = {
  placeholder: PropTypes.string,
  text: PropTypes.any,
  meta: PropTypes.any,
  maxDate: PropTypes.any,
  input: PropTypes.shape({
    value: PropTypes.any,
  }),
};

export function DateInput(props) {
  return (
    <Field
      component={DateInputField}
      {...props}
    />
  );
}

function RadioGroupField({
  input, options, meta: {
    touched, error, warning, submitFailed,
  }, text,
}) {
  const isError = Boolean(submitFailed && touched && (error || warning));
  const inputValue = input.value;

  const inputs = options.map((item, index) => {
    const id = uniqueId('__radio');
    const checked = inputValue.includes(item.value);

    return (
      <label className="redux__field__radio-label-wrapper" key={`radio__${index + 1}`} htmlFor={id}>
        <input
          id={id}
          type="radio"
          className="redux__field__radio"
          {...input}
          value={item.value}
          checked={checked}
        />
        <span className="redux__field__radio-label-wrapper__text">{item.label}</span>
      </label>
    );
  });

  return (
    <div className="redux__group__radio">
      <div className={`redux__group__radio-label required ${isError ? 'error' : ''}`}>
        {text}
      </div>
      <div className="redux__group__radio-wrapper">
        {inputs}
      </div>
    </div>
  );
}

RadioGroupField.propTypes = {
  options: PropTypes.any,
  meta: PropTypes.any,
  text: PropTypes.any,
  input: PropTypes.shape({
    value: PropTypes.any,
  }),
};

export function RadioGroup(props) {
  return (
    <Field
      component={RadioGroupField}
      {...props}
    />
  );
}

function CheckboxField({
  text,
  input: { value, onChange, onDragStart, onDrop, onFocus },
  meta: {
    touched, error, warning, submitFailed,
  },
}) {
  const isError = Boolean(submitFailed && touched && (error || warning));
  const id = uniqueId('__checkbox');

  return (
    <label className="redux__field__checkbox-label-wrapper" htmlFor={id}>
      <input
        id={id}
        type="checkbox"
        className="redux__field__checkbox"
        value={value}
        onChange={(e) => {
          onChange(e.target.checked);
        }}
        onDragStart={onDragStart}
        onDrop={onDrop}
        onFocus={onFocus}
        defaultChecked={Boolean(value)}
      />
      <span className={`redux__field__checkbox-label-wrapper__text ${isError && 'error'}`}>{text}</span>
    </label>
  );
}

CheckboxField.propTypes = {
  text: PropTypes.any,
  meta: PropTypes.any,
  input: PropTypes.shape({
    value: PropTypes.any,
  }),
};

export function Checkbox(props) {
  return (
    <Field
      component={CheckboxField}
      {...props}
    />
  );
}

function CheckboxGroupField({
  input, meta: {
    touched, error, warning, submitFailed,
  }, text, options,
}) {
  const {
    name, onChange, onBlur, onFocus,
  } = input;
  const isError = Boolean(submitFailed && touched && (error || warning));
  const inputValue = input.value;

  const checkboxes = options.map(({ label, value }, index) => {
    const handleChange = (event) => {
      const arr = [...inputValue];
      if (event.target.checked) {
        arr.push(value);
      } else {
        arr.splice(arr.indexOf(value), 1);
      }
      onBlur(arr.length ? arr : null);
      return onChange(arr.length ? arr : null);
    };
    const checked = inputValue.includes(value);
    const id = uniqueId('__checkbox');
    return (
      <label className="redux__field__checkbox-label-wrapper" key={`checkbox-${index + 1}`} htmlFor={id}>
        <input
          id={id}
          className="redux__field__checkbox"
          type="checkbox"
          name={`${name}[${index}]`}
          value={value}
          checked={checked}
          onChange={handleChange}
          onFocus={onFocus}
        />
        <span className="redux__field__checkbox-label-wrapper__text">{label}</span>
      </label>
    );
  });

  return (
    <div className="redux__group__checkbox">
      <div className={`redux__group__checkbox-label required ${isError ? 'error' : ''}`}>
        {text}
      </div>
      <div className="redux__group__checkbox-wrapper">
        {checkboxes}
      </div>
    </div>
  );
}

CheckboxGroupField.propTypes = {
  meta: PropTypes.any,
  value: PropTypes.any,
  options: PropTypes.any,
  text: PropTypes.any,
  input: PropTypes.shape({
    value: PropTypes.any,
  }),
};

export function CheckboxGroup(props) {
  return (
    <Field
      component={CheckboxGroupField}
      {...props}
    />
  );
}

const adaptFileEventToValue = (delegate) => (e) => delegate(e.target.files[0]);

/* eslint-disable no-unused-vars */
function FileField({
  input: { value: omitValue, onBlur, ...inputProps }, meta: { touched, error, warning }, fileLoaded, title, text, ...props
}) {
  const isError = touched && (error || warning);

  return (
    <div className="redux__field__file">
      <div className={`redux__field__file-heading ${isError ? 'error' : ''}`}>{title}</div>
      <div className="redux__field__file-row">
        {!fileLoaded && <div className="redux__field__file-icon"><i className="icon-icon-document"></i></div>}
        {fileLoaded && <div className="redux__field__file-icon loaded"><i className="icon-icon-check"></i></div>}
        <div className="redux__field__file-wrapper required">
          <GuiButton className="redux__field__file-button" type="primary">
            <div>Select file</div>
          </GuiButton>
          <input
            type="file"
            className="redux__field__input"
            onBlur={adaptFileEventToValue(onBlur)}
            {...inputProps}
            {...props}
          />
        </div>
        <div className="redux__field__file-text">
          {text}
        </div>
      </div>
    </div>
  );
}

FileField.propTypes = {
  meta: PropTypes.any,
  text: PropTypes.any,
  title: PropTypes.string,
  fileLoaded: PropTypes.bool,
  input: PropTypes.shape({
    value: PropTypes.any,
  }),
};


export function File(props) {
  return (
    <Field
      component={FileField}
      type="file"
      {...props}
    />
  );
}


export class SelectorField extends React.PureComponent {
  onChange = (value) => {
    this.props.input.onChange(value.value);
  }

  render() {
    const {
      valueRenderer, optionRenderer, text, disableClearable, input: { value }, disabled, options, meta: {
        touched, error, warning, submitFailed,
      },
    } = this.props;
    const isError = Boolean(submitFailed && touched && (error || warning));

    return (
      <WithLabel error={isError} text={text} disabled={disabled}>
        <div className="redux__field__selector-wrapper">
          <div className="redux__field__selector-wrapper__selector">
            <SelectorComponent
              clearable={!disableClearable}
              options={options}
              value={value}
              onChange={this.onChange}
              optionRenderer={optionRenderer}
              valueRenderer={valueRenderer}
              className="redux__field__selector-wrapper__selector__component"
              disabled={disabled}
            />
          </div>
        </div>
      </WithLabel>
    );
  }
}

SelectorField.propTypes = {
  text: PropTypes.any,
  disableClearable: PropTypes.any,
  meta: PropTypes.any,
  input: PropTypes.shape({
    value: PropTypes.any,
    onChange: PropTypes.func,
  }),
  options: PropTypes.array,
  valueRenderer: PropTypes.func,
  optionRenderer: PropTypes.func,
  disabled: PropTypes.bool,
};


export function FormSelector(props) {
  return (
    <Field
      component={SelectorField}
      {...props}
    />
  );
}


export {
  BtnGroup,
  InlineSelect,
  InlineInput,
  InlineFileInput,
  InlineTextarea,
};
