import React from 'react';
import PropTypes from 'prop-types';
import { uniqueId } from 'lodash';
import { Field } from 'redux-form/immutable';

import InlineWrapper from './InlineWrapper';
import './style.css';

function BtnGroupField({
  input, options, required, labelClassName, hideLabel, meta: {
    touched, error, warning, submitFailed,
  }, text,
}) {
  const isError = Boolean(submitFailed && touched && (error || warning));
  const inputValue = input.value;

  const inputs = options.map((item) => {
    const id = uniqueId('__radio');
    const checked = inputValue == item.value; // eslint-disable-line eqeqeq

    return (
      <label styleName={`input-label ${checked ? 'active' : ''}`} key={`radio__${id}`} htmlFor={id}>
        <input
          id={id}
          type="radio"
          {...input}
          value={item.value}
          checked={checked}
        />
        <span>{item.label}</span>
      </label>
    );
  });

  return (
    <InlineWrapper {...{ isError, text, error, required, hideLabel, labelClassName }}>
      <div styleName="group">
        <div styleName="btn-group">
          {inputs}
        </div>
      </div>
    </InlineWrapper>
  );
}

BtnGroupField.propTypes = {
  options: PropTypes.any,
  meta: PropTypes.any,
  text: PropTypes.any,
  required: PropTypes.any,
  labelClassName: PropTypes.string,
  hideLabel: PropTypes.bool,
  input: PropTypes.shape({
    value: PropTypes.any,
  }),
};


export default function BtnGroup(props) {
  return (
    <Field
      component={BtnGroupField}
      {...props}
    />
  );
}
