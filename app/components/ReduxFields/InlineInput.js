import React from 'react';
import PropTypes from 'prop-types';
import { Field } from 'redux-form/immutable';
import { FormattedMessage } from 'react-intl';
import InlineWrapper from './InlineWrapper';
import {} from './style.css';

class InputField extends React.PureComponent {
  render() {
    const {
      text, input, placeholder, required, labelClassName, inputClassName = '', meta: {
        touched, error, warning, submitFailed,
      }, hideLabel, hideError, type = 'text', disabled, leftIcon = null,
    } = this.props;
    const isError = Boolean(submitFailed && touched && (error || warning));

    return (
      <InlineWrapper {...{ isError, text, error, required, labelClassName, hideLabel, disabled }}>
        <div styleName="group" style={leftIcon ? { position: 'relative' } : {}}>
          {leftIcon}
          <input
            type={type}
            className={`redux__field__input ${inputClassName}`}
            placeholder={placeholder}
            disabled={disabled}
            {...input}
          />
          {isError && !hideError && <div styleName="input-error">
            {(typeof error === 'string') ? error : <FormattedMessage {...error} />}
          </div>}
        </div>
      </InlineWrapper>
    );
  }
}

InputField.propTypes = {
  placeholder: PropTypes.string,
  inputClassName: PropTypes.string,
  text: PropTypes.any,
  meta: PropTypes.any,
  leftIcon: PropTypes.any,
  required: PropTypes.any,
  disabled: PropTypes.bool,
  hideError: PropTypes.bool,
  hideLabel: PropTypes.bool,
  labelClassName: PropTypes.string,
  type: PropTypes.string,
  input: PropTypes.shape({
    value: PropTypes.any,
  }),
};


export default function InlineInput(props) {
  return (
    <Field
      component={InputField}
      {...props}
    />
  );
}
