import React from 'react';
import PropTypes from 'prop-types';

import {} from './style.css';

const InlineWrapper = ({ isError, hideLabel, text, required, children, labelClassName, disabled }) => (
  <div styleName="group-wrapper">
    {!hideLabel && <div styleName={`label ${isError ? 'error' : ''} ${disabled ? 'disabled' : ''}`} className={labelClassName || ''}>
      {text}{required && <span styleName="required-mark">*</span>}
    </div>}
    <div styleName="group">
      {children}
    </div>
  </div>
);

InlineWrapper.propTypes = {
  isError: PropTypes.bool,
  disabled: PropTypes.bool,
  required: PropTypes.bool,
  hideLabel: PropTypes.bool,
  text: PropTypes.any,
  labelClassName: PropTypes.string,
  children: PropTypes.any,
};

export default InlineWrapper;
