import React from 'react';
import PropTypes from 'prop-types';

export default function WithLabel({ children, text, error, disabled }) {
  return (
    <div className={`redux__field__label-wrapper ${disabled ? 'disabled' : ''}`}>
      <span className={`redux__field__label-wrapper__text ${error ? 'error' : ''}`}>{text}</span>
      {children}
    </div>
  );
}

WithLabel.propTypes = {
  error: PropTypes.bool,
  disabled: PropTypes.bool,
  text: PropTypes.any,
  children: PropTypes.node,
};
