import React from 'react';
import PropTypes from 'prop-types';
import PerfectScrollbar from 'react-perfect-scrollbar';

import 'react-perfect-scrollbar/dist/css/styles.css';

const defaultOptions = {
  wheelPropagation: true,
};

const CustomScrollbars = ({ children, options, ...props }) => (
  <PerfectScrollbar
    option={{
      ...defaultOptions,
      ...options,
    }}

    {...props}
  >
    {children}
  </PerfectScrollbar>
);

CustomScrollbars.defaultProps = {
  options: {},
};

CustomScrollbars.propTypes = {
  children: PropTypes.node.isRequired,
  options: PropTypes.object,

};

export default CustomScrollbars;
