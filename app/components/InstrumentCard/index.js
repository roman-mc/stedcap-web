import React from 'react';
import ImmutablePropTypes from 'react-immutable-proptypes';
import { connect } from 'react-redux';
import { FormattedNumber } from 'react-intl';
import { createStructuredSelector } from 'reselect';

import { INSTRUMENT_CATEGORY } from 'unity-frontend-core/Models/Enums';
import InstrumentInfoFields from 'unity-frontend-core/Components/InstrumentInfoFields';
import { makeSelectRiskGroups } from 'Containers/DictionariesResolver/selectors';

import QuotValue from '../QuotValue';

import './style.css';


/**
 * @param  {string|undefined|null|false} value
 * @param  {string} title
 * @return {Element|null}
 */
function renderRow(value, title) {
  if (typeof value === 'undefined') {
    return null;
  }

  return (
    <div styleName="info-row">
      <span styleName="cell">{title}</span>
      <span styleName="cell-right">{value}</span>
    </div>
  );
}

class InstrumentCard extends React.PureComponent {
  renderMarketData = (marketData, instrument) => {
    return (
      <div styleName="instrument-info-row-container">
        <div styleName="instrument-market-data">
          <div>
            <h4 styleName="instrument-market-data-title">Bid</h4>
            <QuotValue value={marketData.bid} pipSize={instrument.pipSize} size="plain" />
          </div>
          <div>
            <h4 styleName="instrument-market-data-title">Spread</h4>
            <span styleName="instrument-market-data-value"><FormattedNumber value={marketData.spread} maximumFractionDigits={5} /></span>
          </div>
          <div>
            <h4 styleName="instrument-market-data-title">Ask</h4>
            <QuotValue value={marketData.ask} pipSize={instrument.pipSize} size="plain" />
          </div>
        </div>
      </div>
    );
  }

  renderHeaderInfo = () => {
    const { instrument } = this.props;

    return (instrument.description || instrument.exchange)
      && (instrument.category !== INSTRUMENT_CATEGORY.FX)
      ? (
        <div
          styleName="instrument-info-row-container"
        >
          {instrument.description
            && <div styleName="info-row">
              <span styleName="cell header-cell">
                <span
                  className="icon-icon-asset"
                />
                <span styleName="instrument-info-row-description"> {instrument.description}</span>
              </span>
            </div>
          }
          {
            instrument.exchange
            && <div styleName="info-row">
              <span styleName="cell header-cell">
                <span
                  className="icon-icon-market"
                />
                <span styleName="instrument-info-row-description"> {instrument.exchange}</span>
              </span>
            </div>
          }
        </div>
      ) : null;
  }


  render() {
    const { instrument, riskGroups, marketData } = this.props;

    return (
      <div
        styleName="instrument-info"
      >
        {/* HEADER */}
        {marketData && instrument && this.renderMarketData(marketData, instrument)}
        {this.renderHeaderInfo()}

        {/* BODY */}
        <div styleName="instrument-info-row-container">
          <InstrumentInfoFields instrumentInfo={instrument} riskGroups={riskGroups} renderRow={renderRow} />
        </div>
        {/* NOTE: Market session info in footer */}
        {/* TODO: Green line  */}
        {/* FOOTER  */}
        {/* <div
          styleName="instrument-info__footer"
        >
          <div styleName="info-row">
            <span styleName="text-left">Main session</span>
            <span styleName="text-right">Next in 10:30</span>
          </div>

          <div styleName="session-line">

          </div>
          <div styleName="session-status">
            <div styleName="text-left">
              <i styleName="circle">

              </i>

              <span styleName="text">
                Main session
              </span>
            </div>
            <span styleName="text-right">
              03.00 - 03.00
            </span>
          </div>
        </div> */}
      </div>
    );
  }
}

InstrumentCard.propTypes = {
  marketData: ImmutablePropTypes.record,
  instrument: ImmutablePropTypes.record,
  riskGroups: ImmutablePropTypes.map,
};

const mapStateToProps = createStructuredSelector({
  riskGroups: makeSelectRiskGroups(),
});

export default connect(mapStateToProps)(InstrumentCard);
