import React from 'react';
import PropTypes from 'prop-types';

import './style.css';

export default class Button extends React.PureComponent {
  state = { isPressed: false };

  render() {
    const {
      fullWidth,
      type = 'primary',
      content,
      children,
      btnType,
      big,
      disabled,
      className = '',
      smaller,
      largePaddings,
      dataButtonAction,
      onClick,
    } = this.props;
    const { isPressed } = this.state;

    const typeClass = type ? `${type}` : '';
    const pressedClass = isPressed ? 'pressed' : '';
    const sizeClass = big ? 'big' : '';
    const fullWidthClass = fullWidth ? 'full-width' : '';
    const smallerClass = smaller ? 'smaller' : '';
    const largePaddingsClass = largePaddings ? 'large-paddings' : '';

    return (
      <button
        onClick={onClick}
        styleName={`button ${typeClass} ${largePaddingsClass} ${pressedClass} ${sizeClass} ${fullWidthClass} ${smallerClass}`}
        className={className}
        type={btnType}
        disabled={disabled}
        data-button-action={dataButtonAction}
      >
        {children}
        {content}
      </button>
    );
  }
}


Button.propTypes = {
  className: PropTypes.string,
  btnType: PropTypes.string,
  big: PropTypes.bool,
  fullWidth: PropTypes.bool,
  disabled: PropTypes.bool,
  type: PropTypes.string,
  dataButtonAction: PropTypes.string,
  onClick: PropTypes.func,
  content: PropTypes.any,
  children: PropTypes.any,
  smaller: PropTypes.bool,
  largePaddings: PropTypes.bool,
};
