import React from 'react';

import PropTypes from 'prop-types';

import Button from './Button';

export function Input({ inputRef = () => { }, value, onBlur, onChange, onKeyPress, placeholder }) {
  return (
    <div className="gui__input">
      <input
        ref={inputRef}
        type="text"
        value={value}
        onBlur={onBlur}
        onChange={onChange}
        onKeyPress={onKeyPress}
        placeholder={placeholder}
      />
    </div>
  );
}

Input.propTypes = {
  inputRef: PropTypes.func,
  value: PropTypes.any,
  onBlur: PropTypes.func,
  onChange: PropTypes.func,
  onKeyPress: PropTypes.func,
  placeholder: PropTypes.string,
};

export {
  Button,
};
