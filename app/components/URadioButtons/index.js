import React from 'react';
import PropTypes from 'prop-types';

import UTabButton from '../UTabButton';

import './styles.css';

// TODO: make component more customizable
//      1. Container styles
//      2. Button style
//      3. Active button style
class RadioButtons extends React.PureComponent {
  render() {
    const { options, selectedValue, onChange, buttonClassName, className } = this.props;

    return (
      <div styleName="radio-buttons-container" className={className}>
        {options.map((option) =>
          <UTabButton
            key={option.value}
            active={option.value === selectedValue}
            onClick={() => onChange(option.value)}
            label={option.label}
            className={buttonClassName}
          />
        )}
      </div>
    );
  }
}

RadioButtons.propTypes = {
  options: PropTypes.arrayOf(PropTypes.shape({
    value: PropTypes.oneOfType([PropTypes.number, PropTypes.string]),
    label: PropTypes.oneOfType([PropTypes.number, PropTypes.string]),
  })),
  selectedValue: PropTypes.oneOfType([
    PropTypes.number,
    PropTypes.string,
  ]),
  buttonClassName: PropTypes.string,
  onChange: PropTypes.func.isRequired,
  className: PropTypes.string,
};

export default RadioButtons;
