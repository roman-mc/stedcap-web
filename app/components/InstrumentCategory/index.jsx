import React from 'react';
import PropTypes from 'prop-types';
import ImmutableProptypes from 'react-immutable-proptypes';


import { INSTRUMENT_COLORS } from 'Model/enums';
import './style.css';

export default function InstrumentCategory({ instrument, modificator, size, fontColor, fontSize }) {
  return (
    <div styleName="label" className={modificator}>
      <div
        styleName="label-box"
        style={{
          backgroundColor: INSTRUMENT_COLORS[instrument.category.toUpperCase()],
          width: size,
          height: size,
          fontSize,
        }}
      >
        <span
          styleName="label-box-text"
          style={{
            color: fontColor,
          }}
        >
          {instrument.category}
        </span>
      </div>
    </div>
  );
}

const stringOrNumber = PropTypes.oneOfType([PropTypes.string, PropTypes.number]);


InstrumentCategory.propTypes = {
  instrument: PropTypes.oneOfType([ImmutableProptypes.record, PropTypes.object]).isRequired,
  size: stringOrNumber.isRequired,
  fontSize: stringOrNumber.isRequired,
  fontColor: PropTypes.string,
  modificator: PropTypes.string,
};
