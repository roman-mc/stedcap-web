import React from 'react';
import PropTypes from 'prop-types';
import { injectIntl, intlShape } from 'react-intl';


import messages from './messages';

import './style.css';

class SearchField extends React.PureComponent {
  onInputChange = (event) => {
    const newValue = event.target.value;
    this.props.onChange(newValue);
  }

  render() {
    const { intl: { formatMessage }, placeholder, value = '' } = this.props;
    return (
      <div styleName="search-field">
        <div styleName="loupe">
          <i className="icon-icon-search"></i>
        </div>
        <input
          type="text"
          value={value}
          onChange={this.onInputChange}
          styleName="input"
          placeholder={placeholder || formatMessage(messages.inputPlaceholder)}
        />
      </div>
    );
  }
}

export default injectIntl(SearchField);


SearchField.propTypes = {
  intl: intlShape,
  onChange: PropTypes.func.isRequired,
  placeholder: PropTypes.string,
  value: PropTypes.string,
};
