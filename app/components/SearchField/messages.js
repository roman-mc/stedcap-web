import { defineMessages } from 'react-intl';

export default defineMessages({
  inputPlaceholder: {
    id: 'app.components.SearchField.inputPlaceholder',
    defaultMessage: 'Search',
  },
});
