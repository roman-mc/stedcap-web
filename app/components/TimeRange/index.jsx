import React from 'react';
import PropTypes from 'prop-types';
import { FormattedMessage } from 'react-intl';


import messages from './messages';

import './style.css';

const periods = [
    { id: '1m', message: messages.oneMonth },
    { id: '3m', message: messages.threeMonth },
    { id: '6m', message: messages.sixMonth },
    { id: '1y', message: messages.oneYear },
    { id: '3y', message: messages.threeYear },
    { id: '5y', message: messages.fiveYear },
    { id: 'ytd', message: messages.YTD },
];

export default function TimeRange({ period, onClick }) {
    return (
        <div styleName="time-range">
            <div styleName="label">
                <FormattedMessage {...messages.label} />
            </div>
            <div styleName="ranges">
                {
                    periods.map(({ id, message }) =>
                        <button
                            key={id}
                            styleName={`range-elm ${period === id ? '_selected' : ''}`}
                            onClick={() => onClick(id)}
                        >
                            <FormattedMessage {...message} />
                        </button>
                    )
                }
            </div>
        </div>
    );
}

TimeRange.propTypes = {
    onClick: PropTypes.func.isRequired,
    period: PropTypes.string.isRequired,
};
