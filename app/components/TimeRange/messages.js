import { defineMessages } from 'react-intl';

export default defineMessages({
  oneDay: {
    id: 'app.components.TimeRange.oneDay',
    defaultMessage: '1D',
  },

  oneWeek: {
    id: 'app.components.TimeRange.oneWeek',
    defaultMessage: '1W',
  },

  oneMonth: {
    id: 'app.components.TimeRange.oneMonth',
    defaultMessage: '1M',
  },

  threeMonth: {
    id: 'app.components.TimeRange.threeMonth',
    defaultMessage: '3M',
  },

  sixMonth: {
    id: 'app.components.TimeRange.sixMonth',
    defaultMessage: '6M',
  },

  oneYear: {
    id: 'app.components.TimeRange.oneYear',
    defaultMessage: '1Y',
  },

  threeYear: {
    id: 'app.components.TimeRange.threeYear',
    defaultMessage: '3Y',
  },

  fiveYear: {
    id: 'app.components.TimeRange.fiveYear',
    defaultMessage: '5Y',
  },

  YTD: {
    id: 'app.components.TimeRange.YTD',
    defaultMessage: 'YTD',
  },

  label: {
    id: 'app.components.TimeRange.label',
    defaultMessage: 'Time range',
  },
});
