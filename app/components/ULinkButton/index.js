/**
 * This element looks like <a>
 */

import React from 'react';
import PropTypes from 'prop-types';

import styles from '../uiStyles.css';

const standardClassNames = [styles.text, styles.link].join(' ');

const ULinkButton = (props) => {
  const { onClick, className, children, type, disabled } = props;

  const fullClassName = className ? `${standardClassNames} ${className}` : standardClassNames;

  return (
    <button onClick={onClick} className={fullClassName} type={type} disabled={disabled}>
      {children}
    </button>
  );
};

ULinkButton.defaultProps = {
  type: 'button',
};

ULinkButton.propTypes = {
  onClick: PropTypes.func.isRequired,
  className: PropTypes.string,
  children: PropTypes.any,
  type: PropTypes.string,
  disabled: PropTypes.bool,
};

export default ULinkButton;
