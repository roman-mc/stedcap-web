import React from 'react';
import PropTypes from 'prop-types';
import { FormattedMessage } from 'react-intl';

import Button from './components/Button';
import FormGroup from './components/FormGroup';

import LabelWrapper from './components/LabelWrapper';
import GDPRNotification from './components/GDPRNotification';
import { Input, Password, PhoneInput } from './components/Inputs';
import FooterLink from './components/FooterLink';

import './style.css';

export function withStopPropagation(cb) {
  return (event) => {
    event.stopPropagation();
    cb();
  };
}


export function Text({ text }) {
  return (<div styleName="form-text">
    {text}
  </div>);
}

Text.propTypes = {
  text: PropTypes.string.isRequired,
};

export function AuthForm({ children, subtitle }) {
  return (
    <main styleName="auth-form">
      <GDPRNotification />
      <div styleName="container">
        <div styleName="logo">
          <div styleName="logo-img">
          </div>
          {subtitle && <div styleName="logo-subtitle">
            <FormattedMessage {...subtitle} />
          </div>}
        </div>
        {children}
      </div>
    </main>
  );
}

AuthForm.propTypes = {
  children: PropTypes.node.isRequired,
  subtitle: PropTypes.object,
};

export {
  Button,
  FormGroup,
  Input,
  LabelWrapper,
  Password,
  PhoneInput,
  FooterLink,
};
