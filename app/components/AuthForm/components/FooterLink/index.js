import React from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';

import style from './style.css';

export default function FooterLink({ children, to }) {
  return (
    <Link to={to} className={style.footerLink}>{children}</Link>
  );
}

FooterLink.propTypes = {
  children: PropTypes.node.isRequired,
  to: PropTypes.string.isRequired,
};
