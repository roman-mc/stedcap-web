import React from 'react';

import { Button } from '../../../GUI';

import './style.css';

export default class GDPRNotification extends React.PureComponent {
  state = {
    hidden: false,
  }

  render() {
    if (localStorage.getItem('gdpr_hidden') || this.state.hidden) {
      return null;
    }
    return (
      <div styleName="gdpr-container">
        <p>By using our site, you acknowledge that you have read and understand our <a target="_blank" href="https://stedcap.com/docs/Privacy_Policy.pdf">Privacy Policy</a>. </p>
        <Button type="primary" onClick={() => { localStorage.setItem('gdpr_hidden', 'true'); this.setState({ hidden: true }); }}><span styleName="ok-button">OK</span></Button>
      </div>
    );
  }
}
