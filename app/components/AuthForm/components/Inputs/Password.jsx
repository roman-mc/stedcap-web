import React from 'react';
import PropTypes from 'prop-types';

import { Field } from 'redux-form/immutable';

import InputRenderer, { inputProps } from './InputRenderer';

import './style.css';

export default class Password extends React.PureComponent {
    state = {
      eyeOpened: false,
    }

    eyeOnClick = () => {
      this.setState({ eyeOpened: !this.state.eyeOpened });
    }

    renderEye = () => {
      const { eyeOpened } = this.state;
      return (
        <i // eslint-disable-line jsx-a11y/no-static-element-interactions
          onClick={this.eyeOnClick}
          styleName="icon"
          className={`${eyeOpened ? 'icon-eye-blocked' : 'icon-eye'}`}
        />
      );
    }

    render() {
      const { eyeOpened } = this.state;
      const { id, name, placeholder, validate } = this.props;
      return (
        <Field
          id={id}
          name={name}
          type={eyeOpened ? 'text' : 'password'}
          className="password"
          component={InputRenderer}
          renderSuffix={this.renderEye}
          placeholder={placeholder}
          validate={validate}
          autoComplete="new-password"
        />
      );
    }
}

Password.propTypes = {
  id: PropTypes.string.isRequired,
  name: PropTypes.string.isRequired,
  validate: PropTypes.array,
  ...inputProps,
};
