import React from 'react';
import PropTypes from 'prop-types';
import { Field } from 'redux-form/immutable';

import InputRenderer, { inputProps } from './InputRenderer';

export default function Input(props) {
    return (
        <Field
            className="input"
            component={InputRenderer}
            {...props}
        />
    );
}

Input.propTypes = {
    id: PropTypes.string,
    name: PropTypes.string,
    validate: PropTypes.array,
    ...inputProps,
};
