import React from 'react';
import PropTypes from 'prop-types';

import { FormattedMessage } from 'react-intl';
import LabelWrapper from '../LabelWrapper';

import './style.css';

export default function InputRenderer({
  renderPrefix = () => { }, renderSuffix = () => { }, maxLength, placeholder, className = '', input, input: { value }, type, meta: { touched, error, warning },
  autoComplete = 'off',
}) {
  return (
    <div styleName={className}>
      <div styleName={`wrapper ${value ? 'with-text' : ''}`}>
        {renderPrefix(value)}
        <LabelWrapper labelText={placeholder} textDisplay={!!value} />
        <input autoComplete={autoComplete} {...input} maxLength={maxLength} styleName="field" placeholder={placeholder} type={type} />
        {renderSuffix(value)}
      </div>
      {touched
                && ((error && <span styleName="error-message" className="error-message" data-error-message="error"><FormattedMessage {...error} /></span>)
                    || (warning && <span styleName="error-message" className="error-message" data-error-message="warning">{warning}</span>))}
    </div>
  );
}


export const inputProps = {
  autoComplete: PropTypes.string,
  maxLength: PropTypes.string,
  placeholder: PropTypes.string,
  type: PropTypes.string,
};

InputRenderer.propTypes = {
  renderSuffix: PropTypes.func,
  renderPrefix: PropTypes.func,
  className: PropTypes.string,
  input: PropTypes.shape({
    value: PropTypes.any,
  }),
  meta: PropTypes.shape({
    touched: PropTypes.bool,
    error: PropTypes.any,
    warning: PropTypes.any,
  }),
  ...inputProps,
};
