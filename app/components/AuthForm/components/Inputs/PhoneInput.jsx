import React from 'react';
import PropTypes from 'prop-types';
import { Field } from 'redux-form/immutable';

import { getRegion } from 'Utils/phoneUtils';
import { normalizePhone } from 'Utils/form/normalization';
import InputRenderer from './InputRenderer';

import './flags.css';

function renderPrefix(value) {
    const countryCode = getRegion(value.replace(/[\s+-]/g, ''));
    if (countryCode === 'ZZ') {
        return null;
    }
    return (<span styleName={`flag flag-${countryCode.toLowerCase()}`}></span>);
}

export default function ReduxFormPhoneInput({ validate, id, name, placeholder }) {
    return (
        <Field
            id={id}
            name={name}
            type="text"
            className="input"
            component={InputRenderer}
            placeholder={placeholder}
            renderPrefix={renderPrefix}
            maxLength="16"
            normalize={normalizePhone}
            validate={validate}
        />
    );
}


ReduxFormPhoneInput.propTypes = {
    id: PropTypes.string,
    name: PropTypes.string,
    placeholder: PropTypes.string,
    validate: PropTypes.array,
};
