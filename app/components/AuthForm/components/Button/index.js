import React from 'react';
import PropTypes from 'prop-types';

import './style.css';

export default function Button({
  className, type, text, disabled = false, onClick = () => { },
}) {
  return (
    <button
      type={type}
      styleName={`button ${className}`}
      disabled={disabled}
      onClick={onClick}
    >
      {text}
    </button>
  );
}


Button.propTypes = {
  text: PropTypes.string.isRequired,
  disabled: PropTypes.bool,
  type: PropTypes.oneOf(['button', 'submit']).isRequired,
  onClick: PropTypes.func,
  className: PropTypes.string.isRequired,
};
