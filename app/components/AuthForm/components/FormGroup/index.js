import React from 'react';
import PropTypes from 'prop-types';

import './style.css';

export default function FormGroup({ children, column }) {
  const childrenArr = React.Children.toArray(children);

  return (
    <div styleName={`form-group ${column ? 'column' : ''}`}>
      {
        childrenArr.map((child, i) => {
          const subClass = childrenArr.length === 1 ? 'single' : `${i === 0 ? 'first' : ''} ${i === childrenArr.length - 1 ? 'last' : ''}`;

          return (
            <div
              key={`${(i + 1)}`}
              styleName={`cell ${subClass}`}
              style={{
                flexBasis: child.props.basis && `${child.props.basis}%`,
              }}
            >
              {child}
            </div>
          );
        })
      }
    </div>
  );
}


FormGroup.propTypes = {
  children: PropTypes.node.isRequired,
  column: PropTypes.bool,
};
