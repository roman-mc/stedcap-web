import React from 'react';
import PropTypes from 'prop-types';

import './style.css';

export default function LabelWrapper({ labelText, textDisplay }) {
    return (
        <div styleName="label">
            {textDisplay && <span styleName="text">{labelText}</span>}
        </div>
    );
}


LabelWrapper.propTypes = {
    labelText: PropTypes.oneOfType([PropTypes.object, PropTypes.string]).isRequired,
    textDisplay: PropTypes.bool.isRequired,
};
