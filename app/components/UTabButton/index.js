import React from 'react';
import PropTypes from 'prop-types';

import {} from './styles.css';

const UTabButton = ({ label, onClick, active, className }) => (
  <button
    type="button"
    styleName={`radio-button ${active ? 'active' : ''}`}
    onClick={onClick}
    className={className}
  >
    {label}
  </button>
);

UTabButton.propTypes = {
  label: PropTypes.string,
  onClick: PropTypes.func,
  className: PropTypes.string,

  active: PropTypes.bool,
};

export default UTabButton;
