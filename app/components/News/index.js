import React from 'react';
import PropTypes from 'prop-types';
import moment from 'moment/moment';

import { STANDARD_TIME_DATE, STANDARD_TIME } from 'unity-frontend-core/utils/dateUtils';

import ScrollBar from 'Components/Scrollbar';

import { } from './style.css';

export class NewsListItem extends React.PureComponent {
  onClickHandler = () => {
    const { item, onClick } = this.props;
    onClick(item);
  };

  render() {
    const { item, selected } = this.props;
    return (
      <div
        styleName={`news-list-item ${selected ? 'selected' : ''}`}
        onClick={this.onClickHandler}
        title={item.header}
      >
        <div styleName="news-list-item-header">
          <div styleName="news-list-item-header-time">
            {moment(item.time).format(STANDARD_TIME)}
          </div>
          <div styleName="news-list-item-header-text">
            {item.header}
          </div>
        </div>
      </div>
    );
  }
}


NewsListItem.propTypes = {
  item: PropTypes.object.isRequired,
  selected: PropTypes.bool,
  onClick: PropTypes.func.isRequired,
};

export class NewsListHeader extends React.PureComponent {
  render() {
    const { title } = this.props;
    return (
      <div styleName="news-list-group">
        <div styleName="news-list-group-header">
          {title}
        </div>
      </div>
    );
  }
}

NewsListHeader.propTypes = {
  title: PropTypes.string.isRequired,
};


export class NewsDetails extends React.PureComponent {
  render() {
    const { item } = this.props;
    return (
      <div styleName="news-full-item">
        <div styleName="news-full-date">
          {moment(item.time).format(STANDARD_TIME_DATE)}
        </div>
        <div styleName="news-full-header">
          {item.header}
        </div>
        <ScrollBar style={{ position: 'static' }}>
          <div
            styleName="news-full-content"
            dangerouslySetInnerHTML={{ __html: item.content }} // eslint-disable-line react/no-danger
          />
        </ScrollBar>
      </div>
    );
  }
}

NewsDetails.propTypes = {
  item: PropTypes.object.isRequired,
};
