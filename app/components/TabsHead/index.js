import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';

import Scrollbar from '../Scrollbar';

import { styleNames } from '../../utils/styles';
import styles from './styles.css';

class TabsHead extends PureComponent {
  constructor(props) {
    super(props);

    this.scrollableContainer = null;
    this.refsForTabs = new Map();

    this.state = {
      clientWidth: 0,
      scrollLeft: 0,
      scrollWidth: 0,
    };
  }

  componentDidMount() {
    this.onScrollX(this.scrollableContainer);
    this.scrollToActiveTab();
  }

  componentDidUpdate(prevProps) {
    if (this.props.tabs.length !== prevProps.tabs.length
        || this.props.selectedTab.value !== prevProps.selectedTab.value
        || this.props.size.width !== prevProps.size.width
    ) {
      this.onScrollX(this.scrollableContainer);
      this.scrollToActiveTab();
    }
  }


  // TODO: debounce/throttle this func
  onScrollX = (element) => {
    const { clientWidth, scrollLeft, scrollWidth } = element;

    this.setState({
      clientWidth,
      scrollLeft,
      scrollWidth,
    });
  }

  getShowingGradients = () => {
    const { scrollLeft, clientWidth, scrollWidth } = this.state;

    // 4 is randome value that I use as offset to showing shadow
    return {
      showLeftGradient: scrollLeft > 4,
      showRightGradient: scrollWidth - Math.abs(scrollLeft + clientWidth) > 4,
      leftGradientOffset: scrollLeft,
      rightGradientOffset: scrollLeft + clientWidth,
    };
  };

  /**
   * @param  {string|number} tabValue
   * @param  {HTMLElement} ref
   */
  saveTabRef = (tabValue, ref) => {
    this.refsForTabs.set(tabValue, ref);
  }

  scrollToActiveTab = () => {
    const active = this.props.tabs.find((t) => t.value === this.props.selectedTab.value);

    if (!active) {
      return;
    }

    const tabElement = this.refsForTabs.get(active.value);
    const tabsElement = this.scrollableContainer;

    const visibleZoneStart = tabsElement.scrollLeft;
    const visibleZoneEnd = tabsElement.clientWidth + tabsElement.scrollLeft;
    const tabVisibleStart = tabElement.offsetLeft;
    const tabVisibleEnd = tabElement.offsetLeft + tabElement.offsetWidth;

    if (visibleZoneStart > tabVisibleStart ||
        visibleZoneEnd < tabVisibleEnd
    ) {
      // 40 is shadow offset from the right
      const xToScrollMax = Math.max((tabVisibleEnd - tabsElement.clientWidth) + 40, 0);
      const xToScrollActual = Math.min(tabsElement.scrollWidth, xToScrollMax);

      tabsElement.scrollLeft = xToScrollActual;
    }
  }


  renderTab = ({ item, index, onClick }) => {
    const buttonClasses = styleNames(
      styles['tab-text'],
      item.value === this.props.selectedTab.value && styles['tab-text--active'],
    );

    // QUESTION: Tabs have to be upper cased ?
    // TODO: if so, remove upper case here, use toUppserCase outside this component
    return (
      <button
        ref={(ref) => this.saveTabRef(item.value, ref)}
        type="button"
        className={buttonClasses}
        onClick={() => onClick(item)}
        key={`${item.value} ${index}`}
      >
        {item.label.toUpperCase()}
      </button>
    );
  };

  // TODO: gradient customization
  render() {
    const { tabs, selectedTab, containerStyle, scrollbarStyle, onTabClick } = this.props;
    const {
      showLeftGradient, showRightGradient,
    } = this.getShowingGradients();

    const containerClasses = styleNames(
      styles['tabs-container'],
      containerStyle
    );

    const scrollbarClasses = styleNames(
      styles.scrollbar,
      scrollbarStyle
    );

    return (
      <div className={containerClasses}>
        <Scrollbar
          options={{
            suppressScrollY: true,
            useBothWheelAxes: true,
          }}
          onScrollX={this.onScrollX}
          className={scrollbarClasses}
          containerRef={(ref) => this.scrollableContainer = ref}
        >
          {tabs.map((item, i) =>
            this.renderTab({ item, selectedTab, index: i, onClick: onTabClick, key: i })
          )}

        </Scrollbar>
        {showLeftGradient
          && <div className={styles.shadow} />
        }
        {showRightGradient
          && <div className={`${styles.shadow} ${styles.right}`} />
        }
      </div>

    );
  }
}

TabsHead.defaultProps = {
  size: {},
};

TabsHead.propTypes = {
  selectedTab: PropTypes.shape({
    value: PropTypes.any.isRequired,
  }).isRequired, // object, with {value}
  tabs: PropTypes.arrayOf(
    PropTypes.shape({
      value: PropTypes.any.isRequired,
      label: PropTypes.string.isRequired,
    })
  ).isRequired, // array with {value, label}
  containerStyle: PropTypes.string,
  scrollbarStyle: PropTypes.string,
  onTabClick: PropTypes.func.isRequired,
  // If you want handle resizing
  size: PropTypes.shape({
    width: PropTypes.number,
    height: PropTypes.number,
  }),
};

export default TabsHead;
