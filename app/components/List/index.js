import React from 'react';
import PropTypes from 'prop-types';

import './style.css';

export function ListItem({ disabled, onClick, children, renderRight, nestingLevel }) {
  return (
    <button
      type="button"
      disabled={disabled}
      styleName="list-item"
      onClick={onClick}
      style={{ paddingLeft: nestingLevel * 10 }}
    >
      <div styleName="list-item-left">{children}</div>
      <div>{renderRight()}</div>
    </button>
  );
}

ListItem.defaultProps = {
  renderRight: () => {},
  nestingLevel: 0,
};

ListItem.propTypes = {
  children: PropTypes.node.isRequired,
  renderRight: PropTypes.func,
  onClick: PropTypes.func,
  disabled: PropTypes.bool,
  nestingLevel: PropTypes.number,
};

export function CheckMark() {
  return (<div className="icon-icon-ok" styleName="list-item-row-ok" />);
}
