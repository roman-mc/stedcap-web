import React from 'react';
import PropTypes from 'prop-types';

import { getRegion } from 'Utils/phoneUtils';

import './flags.css';

const Flag = ({ phone = '', className = '' }) => {
  const countryCode = getRegion(phone.replace(/[\s+-]/g, ''));
  if (countryCode === 'ZZ') {
    return null;
  }
  return (<span styleName={`flag flag-${countryCode.toLowerCase()}`} className={className} />);
};

Flag.propTypes = {
  phone: PropTypes.string.isRequired,
  className: PropTypes.string,
};

export default Flag;
