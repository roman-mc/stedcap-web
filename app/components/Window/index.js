import React from 'react';
import PropTypes from 'prop-types';

import { styleNames } from '../../utils/styles';
import './style.css';

function WindowHeaderTitle({ icon, children, active, onClick, dataWindowName }) {
  return (
    <button
      data-window-name={dataWindowName}
      onMouseDown={(e) => { if (onClick) { onClick(); e.stopPropagation(); } }} // prevent dragging if onClick defined
      styleName={`header-title ${onClick ? 'is-clickable' : ''} ${active ? 'active' : ''}`}
    >
      {icon && <i className={`header-icon ${icon}`} />}
      {children}
    </button>
  );
}

WindowHeaderTitle.defaultProps = {
  active: true,
};

WindowHeaderTitle.propTypes = {
  children: PropTypes.node.isRequired,
  active: PropTypes.bool,
  icon: PropTypes.string,
  dataWindowName: PropTypes.string,
  onClick: PropTypes.func,
};

function WindowHeaderButtons({ children, onWindowRemove }) {
  return (
    <div styleName="header-button-container">
      {children}
      {onWindowRemove
        && <button
          styleName="header-button"
          onClick={onWindowRemove}
        >
          <i className="icon-icon-cancel"></i>
        </button>
      }
    </div>
  );
}

WindowHeaderTitle.defaultProps = {
  active: true,
};

WindowHeaderButtons.propTypes = {
  children: PropTypes.node.isRequired,
  onWindowRemove: PropTypes.func,
};

class WindowHeader extends React.PureComponent { // eslint-disable-line react/prefer-stateless-function
  render() {
    const { renderTitle, renderButtons } = this.props;
    return (
      <div styleName="header">
        <div styleName="header-title-container">
          {renderTitle()}
        </div>
        <div styleName="header-button-container">
          {renderButtons(this.props)}
        </div>
      </div>
    );
  }
}

WindowHeader.defaultProps = {
  renderTitle: () => { },
  renderButtons: (props) => <WindowHeaderButtons {...props}>
    <span></span>
  </WindowHeaderButtons>,
};

WindowHeader.propTypes = {
  renderTitle: PropTypes.func,
  renderButtons: PropTypes.func,
};

function Window({ fullWidth, fullHeight, children, headerHidden, onClick }) {
  const containerStyles = styleNames(
    'window',
    fullHeight && 'full-height',
    fullWidth && 'full-width',
    headerHidden && 'header-hidden'
  );

  return (
    <div styleName={containerStyles} onClick={onClick}>
      {children}
    </div>
  );
}

Window.propTypes = {
  children: PropTypes.node,
  headerHidden: PropTypes.bool,
  fullWidth: PropTypes.bool,
  fullHeight: PropTypes.bool,
  onClick: PropTypes.func,
};

export {
  WindowHeader,
  WindowHeaderTitle,
  Window,
};
