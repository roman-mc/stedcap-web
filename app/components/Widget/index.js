import React from 'react';
import PropTypes from 'prop-types';

import './style.css';

export function NavigationButton({ onClick = () => { }, isActive, children }) {
  return (
    <button
      onClick={onClick}
      styleName={`navigation-button ${(isActive ? '_active' : '')}`}
    >
      {children}
    </button>
  );
}

NavigationButton.propTypes = {
  onClick: PropTypes.func,
  isActive: PropTypes.bool.isRequired,
  children: PropTypes.node.isRequired,
};

export default function Widget({
  blockId, navigation, children, showHandler, navigationNote,
}) {
  /* eslint-disable no-param-reassign */
  return (
    <div ref={(ref) => ref ? ref.blockId = blockId : null} className="widget" styleName="container">
      <div styleName="widget">
        <div></div>
        <div styleName="widget-navigation">
          {navigation}
          {showHandler && <div className="widget__navigation__handle" styleName="navigation-handle"></div>}
          {navigationNote && <div styleName="navigation-note">{navigationNote}</div>}
        </div>
        <div styleName="widget-component">
          {children}
        </div>
      </div>
    </div>
  );
  /* eslint-enable */
}

Widget.propTypes = {
  blockId: PropTypes.array.isRequired,
  navigation: PropTypes.node.isRequired,
  children: PropTypes.node.isRequired,
  showHandler: PropTypes.bool,
  navigationNote: PropTypes.node,
};
