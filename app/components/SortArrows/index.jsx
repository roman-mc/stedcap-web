import React from 'react';
import PropTypes from 'prop-types';

import styles from './style.css';

export const SORT_ORDER = {
  ASC: 'asc',
  DESC: 'desc',
};

export default class SortArrows extends React.PureComponent {
  onClick = () => {
    const { sortOrder, sortColumn, name, onChange = () => {} } = this.props;
    if (sortColumn !== name) {
      return onChange(name, SORT_ORDER.ASC);
    }
    const newOrder = sortOrder === SORT_ORDER.ASC ? SORT_ORDER.DESC : SORT_ORDER.ASC;

    onChange(name, newOrder);
    return false;
  }

  getVisibilityClass(orderType) {
    const { sortOrder, sortColumn, name } = this.props;
    if (sortColumn !== name) {
      return styles.hidden;
    }
    return sortOrder !== orderType ? styles.hidden : '';
  }

  renderArrows() {
    return (
      <div styleName="arrows">
        <button
          className={this.getVisibilityClass(SORT_ORDER.ASC)}
        >
                  &#x25b2;
        </button>
        <button
          className={this.getVisibilityClass(SORT_ORDER.DESC)}
        >
                  &#x25bC;
        </button>
      </div>
    );
  }

  render() {
    const { arrowPosition, label, sortable } = this.props;
    return (
    /* eslint-disable jsx-a11y/no-static-element-interactions */
      <div styleName="sort-arrows" onClick={this.onClick}>
        {sortable && arrowPosition === 'before' && this.renderArrows()}
        <div styleName="label">{label}</div>
        {sortable && arrowPosition === 'after' && this.renderArrows()}
      </div>
    /* eslint-enable */
    );
  }
}


SortArrows.propTypes = {
  sortColumn: PropTypes.string,
  sortOrder: PropTypes.oneOf(Object.values(SORT_ORDER)),
  arrowPosition: PropTypes.oneOf(['before', 'after']),
  name: PropTypes.string,
  label: PropTypes.oneOfType([PropTypes.string, PropTypes.node]),
  sortable: PropTypes.bool,
  onChange: PropTypes.func,
};
