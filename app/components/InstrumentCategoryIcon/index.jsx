import React from 'react';
import PropTypes from 'prop-types';


import { INSTRUMENT_COLORS } from 'Model/enums';
import './style.css';

export default function InstrumentCategoryIcon({ category, modificator, size = '28px', fontColor, fontSize = '11px' }) {
  return (
    <div styleName="label" className={modificator}>
      <div
        styleName="label-box"
        style={{
          backgroundColor: INSTRUMENT_COLORS[category.toUpperCase()],
          width: size,
          height: size,
          fontSize,
        }}
      >
        <div styleName="label-box-text" style={{ color: fontColor }}>
          {category}
        </div>
      </div>
    </div>
  );
}

const stringOrNumber = PropTypes.oneOfType([PropTypes.string, PropTypes.number]);


InstrumentCategoryIcon.propTypes = {
  category: PropTypes.string.isRequired,
  size: stringOrNumber,
  fontSize: stringOrNumber,
  fontColor: PropTypes.string,
  modificator: PropTypes.string,
};
