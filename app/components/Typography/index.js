import React from 'react';
import PropTypes from 'prop-types';

import { styleNames } from '../../utils/styles';
import style from './style.css';


const TypoProptypes = {
  children: PropTypes.node.isRequired,
  centered: PropTypes.bool,
  cleanColor: PropTypes.bool,
};


export function H2({ children, centered, cleanColor }) {
  const styles = styleNames(
    style.h2,
    centered && style.centered,
    cleanColor && style.clean,
  );
  return (<h2 className={styles}>{children}</h2>);
}

H2.propTypes = TypoProptypes;


export function Italic({ children, centered, cleanColor }) {
  const styles = styleNames(
    style.italic,
    centered && style.centered,
    cleanColor && style.clean,
  );
  return (<span className={styles}>{children}</span>);
}

Italic.propTypes = TypoProptypes;
