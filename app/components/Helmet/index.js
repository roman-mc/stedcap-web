import React from 'react';
import PropTypes from 'prop-types';
import ReactHelmet from 'react-helmet';
import { injectIntl, intlShape } from 'react-intl';

function Helmet({ intl: { formatMessage }, pageTitle, pageDescription }) {
  return (
    <ReactHelmet
      title={formatMessage(pageTitle)}
      meta={[
        { name: 'description', content: formatMessage(pageDescription) },
      ]}
    />
  );
}

Helmet.propTypes = {
  intl: intlShape,
  pageDescription: PropTypes.object.isRequired,
  pageTitle: PropTypes.object.isRequired,
};

export default injectIntl(Helmet);
