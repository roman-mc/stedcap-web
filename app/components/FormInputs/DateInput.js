import React from 'react';
import PropTypes from 'prop-types';
import { Field } from 'redux-form/immutable';
import DatePicker from 'react-datepicker';
import 'moment/locale/en-gb';

import { STANDARD_DATE } from 'unity-frontend-core/utils/dateUtils';

import { } from './style.css';


function ElementWrapper({ label, disabled, children, wrapperClassName = '', inputClassName = '', labelClassName = '', required, meta: { touched, error, warning, submitFailed } = {} }) {
  /* eslint-disable jsx-a11y/label-has-for */
  const isError = submitFailed && touched && (error || warning);

  return (
    <div className={`${isError ? 'has-error' : ''}`} styleName={`form-element-wrapper ${disabled ? 'disabled' : ''}  ${wrapperClassName}`}>
      <label styleName="label-wrapper" className={labelClassName}>
        {label}
        {required && <span styleName="required-mark">
          *
        </span>}
      </label>
      <div styleName={`input-wrapper ${wrapperClassName}`} className={inputClassName}>
        {children}
        {isError && <div styleName="input-error-text">
          {error}
        </div>}
      </div>
    </div>
  );
}

ElementWrapper.propTypes = {
  label: PropTypes.oneOfType([PropTypes.string, PropTypes.node]),
  children: PropTypes.node,
  wrapperClassName: PropTypes.string,
  inputClassName: PropTypes.string,
  labelClassName: PropTypes.string,
  required: PropTypes.bool,
  disabled: PropTypes.bool,
  meta: PropTypes.any,
};

export function DateInput({ value, onChange, input = {}, micro, minDate, maxDate, disabled, label, required, mini, meta, inputClassName = '', labelClassName = '' }) {
  const miniClass = mini ? 'mini' : '';
  const microClass = micro ? 'micro' : '';
  return (
    <ElementWrapper
      wrapperClassName={`${microClass} ${miniClass}`}
      inputClassName={inputClassName}
      labelClassName={labelClassName}
      disabled={disabled}
      label={label}
      required={required}
      meta={meta}
    >
      <DatePicker
        {...input}
        onChange={onChange}
        value={value}
        dateFormat={STANDARD_DATE}
        selected={value}
        peekNextMonth
        showMonthDropdown
        showYearDropdown
        dropdownMode="select"
        minDate={minDate}
        maxDate={maxDate}
        disabled={disabled}
        locale="en-gb"
      />
      <i styleName="input-icon" className="icon-icon-calendar" />
    </ElementWrapper>
  );
}

DateInput.propTypes = {
  input: PropTypes.object,
  label: PropTypes.oneOfType([PropTypes.string, PropTypes.node]),
  required: PropTypes.bool,
  micro: PropTypes.bool,
  disabled: PropTypes.bool,
  mini: PropTypes.bool,
  meta: PropTypes.any,
  minDate: PropTypes.object,
  maxDate: PropTypes.object,
  value: PropTypes.any,
  onChange: PropTypes.func,
  inputClassName: PropTypes.string,
  labelClassName: PropTypes.string,
};


function DateInputField({ input }) {
  return (
    <DateInput
      value={input.value}
      onChange={input.onChange}
    />
  );
}

DateInputField.propTypes = {
  input: PropTypes.any,
};

export function DateInputRedux(props) {
  return (
    <Field
      component={DateInputField}
      {...props}
    />
  );
}
