/**
 *
 */

import React from 'react';
import PropTypes from 'prop-types';

import styles from '../uiStyles.css';

const standardClassNames = styles.button;

const ULinkButton = (props) => {
  const { onClick, className, children, type, elementProps } = props;

  const fullClassName = className ? `${standardClassNames} ${className}` : standardClassNames;

  return (
    <button
      onClick={onClick}
      className={fullClassName}
      type={type}
      {...elementProps}
    >
      {children}
    </button>
  );
};

ULinkButton.defaultProps = {
  type: 'button',
  elementProps: {},
};

ULinkButton.propTypes = {
  onClick: PropTypes.func.isRequired,
  className: PropTypes.string,
  children: PropTypes.any,
  type: PropTypes.string,
  elementProps: PropTypes.object,
};

export default ULinkButton;
