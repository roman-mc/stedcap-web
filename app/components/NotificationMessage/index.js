import React from 'react';
import { FormattedHTMLMessage } from 'react-intl';
import ImmutableProptypes from 'react-immutable-proptypes';

import InstrumentResolver from 'unity-frontend-core/containers/InstrumentResolver';

import { NOTIFICATION_TYPE } from 'unity-frontend-core/Models/Enums';

import Notification from 'Model/Notification';

import messages from './messages';

import { } from './style.css';

export const messagesByType = {
  [NOTIFICATION_TYPE.dealExecuted]: {
    general: messages.trade,
    details: messages.tradeDetails,
  },
  [NOTIFICATION_TYPE.orderPlaced]: {
    general: messages.order,
  },
  [NOTIFICATION_TYPE.orderExecuted]: {
    general: messages.orderExecuted,
  },
  [NOTIFICATION_TYPE.orderRejected]: {
    general: messages.rejectDetails,
  },
  [NOTIFICATION_TYPE.orderIncorrect]: {
    general: messages.orderIncorrect,
  },
  [NOTIFICATION_TYPE.orderRequestCreated]: {
    general: messages.orderRequestPlaced,
  },
  [NOTIFICATION_TYPE.orderRequestCanceled]: {
    general: messages.orderRequestCanceled,
  },
  [NOTIFICATION_TYPE.orderCanceled]: {
    general: messages.orderCanceled,
  },
  [NOTIFICATION_TYPE.orderUpdated]: {
    general: messages.orderUpdated,
  },
  [NOTIFICATION_TYPE.withdrawalRequestDeclined]: {
    general: messages.withdrawalRequestDeclined,
  },
  [NOTIFICATION_TYPE.withdrawalRequestExecuted]: {
    general: messages.withdrawalRequestExecuted,
  },
  [NOTIFICATION_TYPE.marginWarning]: {
    general: messages.marginWarning,
  },
};

class NotificationMessage extends React.PureComponent {
  renderNotification = (instrument, id, { message, notification }) => {
    return (
      <FormattedHTMLMessage
        {...message.general}
        values={{
          direction: notification.data.direction,
          price: notification.data.price,
          amount: notification.data.amount,
          currency: notification.data.currency,
          symbol: instrument && instrument.displayName,
          valueDate: notification.data.valueDate,
          reason: notification.data.reason,
          text: notification.data.text,
          orderRequestAmount: notification.data.orderRequest ? notification.data.orderRequest.amount : undefined,
          symbolDescription: notification.data.orderRequest ? notification.data.orderRequest.symbolDescription : undefined,
          expectedPrice: notification.data.orderRequest ? notification.data.orderRequest.expectedPrice : undefined,
        }}
      />
    );
  }

  render() {
    const { notification } = this.props;
    const message = messagesByType[notification.notificationType] || {};

    const body = notification.data.instrumentId
      ? <InstrumentResolver id={notification.data.instrumentId} notification={notification} message={message} render={this.renderNotification} />
      : this.renderNotification(null, null, { notification, message });

    return (
      <div>
        <p styleName="paragraph">
          {message.general && body}
        </p>
        {
          message.details
          && <p styleName="paragraph">
            <FormattedHTMLMessage
              {...messagesByType[notification.notificationType].details}
              values={{
                valueDate: notification.data.valueDate,
                reason: notification.data.reason,
              }}
            />
          </p>
        }
      </div>
    );
  }
}

NotificationMessage.propTypes = {
  notification: ImmutableProptypes.recordOf(Notification),
};

export default NotificationMessage;
