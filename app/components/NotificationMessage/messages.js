/*
 * notificationMessage Messages
 *
 * This contains all the text for the notificationMessage component.
 */
import { defineMessages } from 'react-intl';

export default defineMessages({
  orderRejected: {
    id: 'app.notificationMessage.orderRejected',
    defaultMessage: 'Your order {direction, select, 1 {to sell} 0 { to buy } other { }} rejected',
  },
  trade: {
    id: 'app.notificationMessage.trade',
    defaultMessage: '{direction, select, 1 {-} other { }}{amount, number} {symbol}@{price}',
  },
  order: {
    id: 'app.notificationMessage.order',
    defaultMessage: '{direction, select, 1 {-} 0 { } other { }}{amount, number} {symbol}@{price}',
  },
  tradeDetails: {
    id: 'app.notificationMessage.tradeDetails',
    defaultMessage: 'Value date: {valueDate}',
  },
  orderIncorrect: {
    id: 'app.notificationMessage.orderIncorrect',
    defaultMessage: 'Order {direction, select, 1 {to sell} 0 { to buy } other { }} incorrect',
  },
  orderRequestPlaced: {
    id: 'app.notificationMessage.orderRequestPlaced',
    defaultMessage: '{orderRequestAmount, number} {symbolDescription}@{expectedPrice}',
  },
  orderCanceled: {
    id: 'app.notificationMessage.orderCanceled',
    defaultMessage: '{amount, number} {symbol}@{price}',
  },
  orderExecuted: {
    id: 'app.notificationMessage.orderExecuted',
    defaultMessage: '{direction, select, 1 {to sell} 0 { to buy } other { }}',
  },
  rejectDetails: {
    id: 'app.notificationMessage.rejectDetails',
    defaultMessage: 'Reason: {reason}',
  },
  orderUpdated: {
    id: 'app.notificationMessage.orderUpdated',
    defaultMessage: '{direction, select, 1 {-} 0 { } other { }} {amount, number} {symbol}@{price}',
  },
  withdrawalRequestDeclined: {
    id: 'app.notificationMessage.withdrawalRequestDeclined',
    defaultMessage: '{amount, number} {currency}',
  },
  withdrawalRequestExecuted: {
    id: 'app.notificationMessage.withdrawalRequestExecuted',
    defaultMessage: '{amount, number} {currency}',
  },
  marginWarning: {
    id: 'app.notificationMessage.marginWarning',
    defaultMessage: '{text}',
  },
});
