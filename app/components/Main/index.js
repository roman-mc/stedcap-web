import React from 'react';
import PropTypes from 'prop-types';

import './style.css';

export default function Main({ withoutFooter, children }) {
  return (
    <main styleName={`main ${withoutFooter ? '_without-footer' : ''}`}>
      {children}
    </main>
  );
}

Main.propTypes = {
  children: PropTypes.node,
  withoutFooter: PropTypes.bool,
};
