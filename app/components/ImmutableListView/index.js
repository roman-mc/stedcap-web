import React, { Fragment } from 'react';
import PropTypes from 'prop-types';

export default function ImmutableListView({ immutableData, renderRow, renderSection }) {
  return (
    <Fragment>
      {immutableData.entrySeq().map(([key, items], i) =>
        <Fragment
          key={`imd_${key}_${i}`} // eslint-disable-line react/no-array-index-key
        >
          {renderSection(key, items, i)}
          {renderRow && items.map((item, k) => renderRow(item, k, key))}
        </Fragment>)}
    </Fragment>
  );
}

ImmutableListView.propTypes = {
  immutableData: PropTypes.object.isRequired,
  renderRow: PropTypes.func,
  renderSection: PropTypes.func.isRequired,
};
