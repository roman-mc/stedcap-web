import React from 'react';
import PropTypes from 'prop-types';

import { timeFormat } from 'd3-time-format';
import { format } from 'd3-format';

import { ChartCanvas, Chart } from 'react-stockcharts';
import { CandlestickSeries } from 'react-stockcharts/lib/series';
import { XAxis, YAxis } from 'react-stockcharts/lib/axes';
import { fitWidth } from 'react-stockcharts/lib/helper';
import { last } from 'react-stockcharts/lib/utils';
import { PriceCoordinate } from 'react-stockcharts/lib/coordinates';
import { HoverTooltip } from 'react-stockcharts/lib/tooltip';
import { discontinuousTimeScaleProvider } from 'react-stockcharts/lib/scale';

import { colors as themeColors } from '../../assets/styles/variables';

const tooltipDateFormat = timeFormat('%d.%m.%Y %H:%M');
const numberFormat = format('.4f');

const locale = {
  dateTime: '%A, %e %B %Y г. %X',
  date: '%d.%m.%Y',
  time: '%H:%M:%S',
  periods: ['AM', 'PM'],
  days: ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'],
  shortDays: ['Sun', 'Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat'],
  months: ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'],
  shortMonths: ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'],
};

const defaultFormatters = {
  yearFormat: '%Y',
  quarterFormat: '%b %Y',
  monthFormat: '%b',
  weekFormat: '%d %b',
  dayFormat: '%a %d',
  hourFormat: '%H:%M',
  minuteFormat: '%H:%M',
  secondFormat: '%H:%M:%S',
  milliSecondFormat: '%L',
};

const COLORS = {
  positive: themeColors.$salem,
  negative: themeColors.$scarlet,
};

const resolveBarColor = (d) => d.close > d.open ? COLORS.positive : COLORS.negative;

const fontFamily = 'Lato, Helvetica Neue, Helvetica, Arial, sans-serif';

class CandleStickChart extends React.PureComponent {
  componentDidCatch(error, info) {
    console.warn('Error on chart build', error, info);
  }
  parseData = (data) => {
    const res = data.map((d, i) => {
      const n = {};
      n.index = i;
      n.date = new Date(d.ts);
      n.open = d.open;
      n.high = d.high;
      n.low = d.low;
      n.close = d.close;
      return n;
    });
    return res;
  };

  tooltipContent = () => (
    ({ currentItem, xAccessor }) => (
      {
        x: tooltipDateFormat(xAccessor(currentItem)),
        y: [
          { label: 'Open', value: currentItem.open && numberFormat(currentItem.open) },
          { label: 'High', value: currentItem.high && numberFormat(currentItem.high) },
          { label: 'Low', value: currentItem.low && numberFormat(currentItem.low) },
          { label: 'Close', value: currentItem.close && numberFormat(currentItem.close) },
        ],
      }
    )
  );

  render() {
    const { type, width, data: initialData, ratio, height, theme } = this.props;
    const formatedData = this.parseData(initialData);

    const xScaleProvider = discontinuousTimeScaleProvider.setLocale(locale, defaultFormatters).inputDateAccessor((d) => d.date);

    const {
      data,
      xScale,
      xAccessor,
      displayXAccessor,
    } = xScaleProvider(formatedData);

    const xExtents = [
      xAccessor(last(data)),
      xAccessor(data[Math.max(0, data.length - 40)]),
    ];

    const margin = { left: 10, right: 60, top: 10, bottom: 30 };
    const gridHeight = height - margin.top - margin.bottom;
    const gridWidth = width - margin.left - margin.right;

    const yGrid = {
      innerTickSize: -1 * gridWidth,
      tickStrokeDasharray: 'Solid',
      tickStrokeOpacity: 0.1,
      tickStrokeWidth: 1,
    };

    const xGrid = {
      innerTickSize: -1 * gridHeight,
      tickStrokeDasharray: 'Solid',
      tickStrokeOpacity: 0.1,
      tickStrokeWidth: 1,
    };

    const colors = {
      mainColor: theme === 'dark-theme' ? '#ffffff' : themeColors.$baseCodeGray,
      bgColor: theme === 'dark-theme' ? themeColors.$tundora : '#ffffff',
    };

    const lastPrice = formatedData[formatedData.length - 1].close;
    const lastOpenPrice = formatedData[formatedData.length - 1].open;

    return (
      <ChartCanvas
        clamp="both"
        zoomEvent
        ratio={ratio}
        width={width}
        height={height}
        margin={margin}
        type={type}
        seriesName="MSFT"
        data={data}
        xScale={xScale}
        xAccessor={xAccessor}
        displayXAccessor={displayXAccessor}
        xExtents={xExtents}
        padding={{ left: 20, right: 30 }}
      >
        <Chart id={1} yExtents={(d) => [d.high, d.low]} padding={{ top: 30, bottom: 30 }}>
          <CandlestickSeries
            stroke={resolveBarColor}
            wickStroke={resolveBarColor}
            fill={resolveBarColor}
            candleStrokeWidth={1}
            widthRatio={0.5}
            opacity={1}
            clip={false}
          />
          <HoverTooltip
            yAccessor={(d) => d.open}
            tooltipContent={this.tooltipContent()}
            fontSize={15}
            fill={colors.bgColor}
            bgFill={colors.bgColor}
            bgOpacity={0.5}
            stroke={colors.bgColor}
            fontFill={colors.mainColor}
            fontFamily="Lato, Helvetica Neue, Helvetica, Arial, sans-serif"
          />
          <XAxis
            axisAt="bottom"
            orient="bottom"
            ticks={6}
            {...xGrid}
            tickStroke={colors.mainColor}
            stroke={colors.mainColor}
            opacity={0.2}
            tickFormat={this.formatXAxisDate}
            fontFamily="Lato, Helvetica Neue, Helvetica, Arial, sans-serif"
          />
          <YAxis
            axisAt="right"
            orient="right"
            ticks={5}
            {...yGrid}
            tickFormat={numberFormat}
            tickStroke={colors.mainColor}
            stroke={colors.mainColor}
            opacity={0.2}
            fontFamily="Lato, Helvetica Neue, Helvetica, Arial, sans-serif"
          />
          <PriceCoordinate
            at="right"
            orient="right"
            price={lastPrice}
            strokeWidth={2}
            textFill="#FFFFFF"
            stroke={lastPrice > lastOpenPrice ? COLORS.positive : COLORS.negative}
            lineStroke={lastPrice > lastOpenPrice ? COLORS.positive : COLORS.negative}
            fill={lastPrice > lastOpenPrice ? COLORS.positive : COLORS.negative}
            arrowWidth={0}
            strokeDasharray="ShortDot"
            displayFormat={numberFormat}
            lineOpacity={1}
            rectHeight={15}
            fontSize={12}
            fontFamily={fontFamily}
          />

        </Chart>
      </ChartCanvas>
    );
  }
}

CandleStickChart.propTypes = {
  data: PropTypes.array.isRequired,
  width: PropTypes.number.isRequired,
  height: PropTypes.number.isRequired,
  ratio: PropTypes.number.isRequired,
  type: PropTypes.oneOf(['svg', 'hybrid']).isRequired,
  theme: PropTypes.string.isRequired,
};

CandleStickChart.defaultProps = {
  type: 'svg',
};

export default fitWidth(CandleStickChart);

