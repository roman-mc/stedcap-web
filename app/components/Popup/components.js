import React from 'react';
import PropTypes from 'prop-types';

import './style.css';

export function Column({ rightBackslash, leftBackslash, directionColumn, leftBorder, rightBorder, withPadding, children, subClass = '', half }) {
  const checkProps = { rightBackslash, leftBackslash, directionColumn, leftBorder, rightBorder, withPadding, half };

  const modificators = {
    leftBorder: 'left-border',
    rightBorder: 'right-border',
    withPadding: 'with-padding',
    leftBackslash: 'left-backslash',
    rightBackslash: 'right-backslash',
    directionColumn: 'direction-column',
    half: 'half',
  };

  const className = Object.keys(checkProps)
    .filter((k) => checkProps[k])
    .map((k) => modificators[k])
    .join(' ');

  return (<div styleName={`column ${className}`} className={`${subClass}`}>{children}</div>);
}

Column.propTypes = {
  subClass: PropTypes.string,
  children: PropTypes.node,
  leftBorder: PropTypes.bool,
  withPadding: PropTypes.bool,
  rightBorder: PropTypes.bool,
  directionColumn: PropTypes.bool,
  leftBackslash: PropTypes.bool,
  rightBackslash: PropTypes.bool,
  half: PropTypes.bool,
};


export function Row({ withoutPadding, withoutHeightPadding, withoutBorder, withDownBackslash, children, subClass = '' }) {
  const checkProps = { withoutPadding, withoutHeightPadding, withoutBorder, withDownBackslash };

  const modificators = {
    withoutBorder: 'without-border',
    withoutPadding: 'without-padding',
    withoutHeightPadding: 'without-height-padding',
    withDownBackslash: 'with-down-backslash',
  };

  const className = Object.keys(checkProps)
    .filter((k) => checkProps[k])
    .map((k) => modificators[k])
    .join(' ');

  return (<div styleName={`row ${className}`} className={`${subClass}`}>{children}</div>);
}

Row.propTypes = {
  subClass: PropTypes.string,
  children: PropTypes.node,
  withoutBorder: PropTypes.bool,
  withoutPadding: PropTypes.bool,
  withDownBackslash: PropTypes.bool,
  withoutHeightPadding: PropTypes.bool,
};
