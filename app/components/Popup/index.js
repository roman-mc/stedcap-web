
import React from 'react';
import PropTypes from 'prop-types';
import { Portal } from 'react-portal';

import {} from './style.css';

import { WindowHeader, Window } from '../Window';


export default class Popup extends React.PureComponent {
  static defaultProps = {
    handleBackdropClick: true,
  }

  componentWillMount() {
    window.addEventListener('keydown', this.keyListener);
  }

  componentWillUnmount() {
    window.removeEventListener('keydown', this.keyListener);
  }

  keyListener = (e) => {
    if (e.keyCode === 27 && this.props.removePopup) {
      this.props.removePopup(this.props.popupKey);
    }
  }

  handleBackdropClick = () => {
    if (this.props.handleBackdropClick && this.props.removePopup) {
      this.props.removePopup(this.props.popupKey);
    }
  }

  preventHiding = (e) => {
    e.stopPropagation();
  }

  renderPopup = () => {
    const { headerHidden, header, children, removePopup, popupKey, portal = true, offset = [0, 0], backgroundTransparent } = this.props;

    const styles = {
      top: `${offset[1]}px`,
      left: `${offset[0]}px`,
    };
    const childrenWithProps = React.Children
      .map(children,
        (child) => child && React.cloneElement(child, removePopup ? { removePopup: () => removePopup(popupKey) } : {})
      );
    return <div styleName={`popup ${portal ? '' : 'not-portal'} ${backgroundTransparent ? 'background-transparent' : ''}`} onMouseDown={this.preventHiding} onClick={this.handleBackdropClick} style={styles}>
      <Window headerHidden={headerHidden} onClick={this.preventHiding}>
        {!headerHidden && <WindowHeader
          renderTitle={() => <span styleName="header-text">{header}</span>}
          onWindowRemove={() => removePopup(popupKey)}
        />}
        <div styleName="content">{childrenWithProps}</div>
      </Window>
    </div>;
  }

  render() {
    const { portal = true } = this.props;
    return (
      portal ? <Portal node={document && document.getElementById('modal-windows-container')}>
        {this.renderPopup()}
      </Portal> : this.renderPopup()
    );
    /* eslint-enable */
  }
}

Popup.propTypes = {
  popupKey: PropTypes.string.isRequired,
  header: PropTypes.oneOfType([PropTypes.string, PropTypes.node]),
  children: PropTypes.oneOfType([PropTypes.object, PropTypes.array]).isRequired,
  removePopup: PropTypes.func,
  handleBackdropClick: PropTypes.bool,
  headerHidden: PropTypes.bool,
  portal: PropTypes.bool,
  offset: PropTypes.array,
  backgroundTransparent: PropTypes.bool,
};
