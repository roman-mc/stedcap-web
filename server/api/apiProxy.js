const proxy = require('http-proxy-middleware');

const serverEnv = process.env.SERVER_ENV || 'stage1';

const configs = {
  stage1: {
    apiHost: 'https://test.unity.finance',
    wsHost: 'http://test.unity.finance:8080',
  },
  stage2: {
    apiHost: 'https://stage2.unity.finance',
    wsHost: 'http://stage2.unity.finance:8080',
  },
  stage3: {
    apiHost: 'https://stage3.unity.finance',
    wsHost: 'http://stage3.unity.finance:8080',
  },
  stage4: {
    apiHost: 'https://stage4.unity.finance',
    wsHost: 'http://stage4.unity.finance:8080',
  },
  production: {
    apiHost: 'https://trade.stedcap.com/rest',
    wsHost: 'http://trade.stedcap.com:8080',
  },
};

const API_HOST = configs[serverEnv].apiHost;

const WS_HOST = configs[serverEnv].wsHost;
// const WS_HOST = 'http://localhost:8888';
const pathRewrite = (path) => path.replace('/rest', '');

const wsProxy = () => proxy({
  target: `${WS_HOST}`,
  changeOrigin: true,
  logLevel: 'debug',
});


module.exports = (app) => {
  app.use('/socket.io', wsProxy());
  app.use('/rest', proxy({
    target: API_HOST, changeOrigin: true, pathRewrite, secure: false,
  }));
  return wsProxy;
};
