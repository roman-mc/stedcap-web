/* eslint-disable */

const express = require('express');
const logger = require('./logger');

const argv = require('minimist')(process.argv.slice(2));
const setup = require('./middlewares/frontendMiddleware');
const isDev = process.env.NODE_ENV !== 'production';
const ngrok = (isDev && process.env.ENABLE_TUNNEL) || argv.tunnel ? require('ngrok') : false;
const resolve = require('path').resolve;
const app = express();
const setupProxy = require('./api/apiProxy');

const http = require('http').Server(app);
// If you need a backend, e.g. an API, add your custom backend-specific middleware here
// app.use('/api', myApi);
const proxy = setupProxy(app);

console.log('process.env.API_ONLY', process.env.API_ONLY);
// In production we need to pass these values in instead of relying on webpack
if (!process.env.API_ONLY) {
  setup(app, {
    outputPath: resolve(process.cwd(), 'build'),
    publicPath: '/',
  });
}

// // get the intended host and port number, use localhost and port 3000 if not provided
const customHost = argv.host || process.env.HOST;
const host = customHost || null; // Let http.Server use its default IPv6/4 host
const prettyHost = customHost || 'localhost';

const port = argv.port || process.env.PORT || 3000;

console.log('host', host);
// // // Start your app.
const server = http.listen(port, host, (err) => {
  if (err) {
    return logger.error(err.message);
  }

  // Connect to ngrok in dev mode
  if (ngrok) {
    return ngrok.connect(port, (innerErr, url) => {
      if (innerErr) {
        return logger.error(innerErr);
      }

      return logger.appStarted(port, prettyHost, url);
    });
  }
  return logger.appStarted(port, prettyHost);
});


// = app.listen(3000);
server.on('upgrade', (req, ...args) => {
  console.log('!!!!requrl', req.url);
  proxy(req.url.replace('/socket.io', '')).upgrade(req, ...args);
});
