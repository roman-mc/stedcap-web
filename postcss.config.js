const autoprefixer = require('autoprefixer');
const simpleVars = require('postcss-simple-vars');
const nested = require('postcss-nested');
const mixins = require('postcss-mixins');
const atImport = require('postcss-import');
const hexRgba = require('postcss-hexrgba');

const variables = require('./app/assets/styles/variables');

const plugins = [
  atImport(),
  mixins(),
  nested(),
  simpleVars({ variables }),
  autoprefixer({ browsers: ['> 3%'] }),
  hexRgba(),
];

module.exports = () => ({
  plugins,
});
