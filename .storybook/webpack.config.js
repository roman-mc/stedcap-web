
module.exports = (baseConfig, env, defaultConfig) => {
  defaultConfig.module.rules[2] = {
    test: /\.css$/,
    include: (p) => p.indexOf('.global.css') === -1 || p.indexOf('node_modules') === -1,
    use: [
      'style-loader',
      {
        loader: 'css-loader',
        options: {
          modules: true,
          localIdentName: '[path]___[name]__[local]___[hash:base64:5]',
        },
      },
      'postcss-loader',
    ],
  };
  defaultConfig.module.rules[0].use[0].options.plugins.push(require.resolve('babel-plugin-react-css-modules'));

  return defaultConfig;
};
