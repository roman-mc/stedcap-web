import React, { Fragment } from 'react';

import { storiesOf } from '@storybook/react';
// import { action } from '@storybook/addon-actions';
// import { linkTo } from '@storybook/addon-links';

// import { Welcome } from '@storybook/react/demo';

import Button from '../app/components/GUI/Button';

import './style.css';

const DarkWrapper = (storyFn) => (
  <div styleName="container" className="_dark-theme" style={{ background: '#000', padding: '10px' }}>
    {storyFn()}
  </div>
);
const LightWrapper = (storyFn) => (
  <div styleName="container" className="_light-theme" style={{ background: '#ffffff', padding: '10px' }}>
    {storyFn()}
  </div>
);

const ButtonSet = () => <Fragment>
  <h2>
Color variations
  </h2>
  <Button type="primary">
Primary button
  </Button>
  <Button type="secondary">
Secondary button
  </Button>
  <Button type="tertiary">
Tertiary button
  </Button>
  <h2>
Size variations
  </h2>
  <Button type="primary" smaller>
Small paddings
  </Button>
  <Button type="primary" largePaddings>
Large paddings
  </Button>
  <Button type="primary" big>
Large button
  </Button>
  <Button type="primary" fullWidth>
Full width
  </Button>
  <h2>
State variations
  </h2>
  <Button type="primary" disabled>
Disabled Button
  </Button>
</Fragment>;

storiesOf('Buttons', module)
  .addDecorator(DarkWrapper)
  .add('Dark theme', ButtonSet);

storiesOf('Buttons', module)
  .addDecorator(LightWrapper)
  .add('Light theme', ButtonSet);
