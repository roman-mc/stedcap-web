import React, { Fragment } from 'react';
import PropTypes from 'prop-types';
import { storiesOf } from '@storybook/react';
// import { action } from '@storybook/addon-actions';
// import { linkTo } from '@storybook/addon-links';

// import { Welcome } from '@storybook/react/demo';
import './style.css';

import colors from '../app/assets/styles/colors.json';


const Color = ({ color }) => (
  <div styleName="color">
    <div styleName="color-wrapper" style={{ background: colors[color] }}></div>
    {color}
    {' '}
- (
    {colors[color]}
)
  </div>
);

Color.propTypes = {
  color: PropTypes.string.isRequired,
};

const ColorSet = () => <Fragment>
  <h2>
Application colors
  </h2>
  <div styleName="colors-container">
    {Object.keys(colors).map((c) => <Color color={c} key={c} />)}
  </div>
</Fragment>;

storiesOf('Colors', module)
  .add('All application colors', ColorSet);
